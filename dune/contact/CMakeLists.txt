add_subdirectory(assemblers)
add_subdirectory(common)
add_subdirectory(estimators)
add_subdirectory(projections)
add_subdirectory(solvers)

install(FILES
    contact2b.hh
    postprocessresidual.hh
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/contact)
