#ifndef ANALYTIC_DIRECTIONS_CONTACT_PROJECTION_HH
#define ANALYTIC_DIRECTIONS_CONTACT_PROJECTION_HH

#include <dune/common/function.hh>

#include <dune/contact/projections/normalprojection.hh>

#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>

#include <dune/istl/bvector.hh>

namespace Dune {
namespace Contact {

/** \brief A contact projection where the search directions are given by an analytical function. */
template <class BoundaryPatchType>
class AnalyticDirectionsProjection : public  NormalProjection<BoundaryPatchType>
{
    public:
        typedef NormalProjection<BoundaryPatchType> Base;
        typedef typename BoundaryPatchType::iterator BoundaryIterator;
        typedef typename Base::BoundarySegment BoundarySegment;   
        typedef typename Base::CoordinateType CoordinateType;
        typedef typename Base::GridView GridView;
        typedef typename Base::ctype ctype;
        using Base::dim;
        typedef Dune::VirtualFunction<CoordinateType,CoordinateType> FunctionType;
        typedef Dune::BlockVector<CoordinateType> BlockVectorType;
        typedef BasisGridFunction<P1NodalBasis<GridView>,BlockVectorType> P1GridFunction; 

        AnalyticDirectionsProjection(const FunctionType* obsDir, ctype overlap = 1e-1) :
            NormalProjection<BoundaryPatchType>(overlap),
            analyticDirections_(obsDir)
        {}

        /** \brief Project the vertices of nonmortar boundary onto points of the mortar boundary
         *         and compute the distance and the direction of the projection. 
         */
        virtual void project(const BoundaryPatchType& nonmortar, const BoundaryPatchType& mortar,
                const ctype couplingDist = std::numeric_limits<ctype>::max());


    private:
        //! The analytic function that computes the search directions
        const FunctionType* analyticDirections_; 

};

template <class BoundaryPatchType>
void AnalyticDirectionsProjection<BoundaryPatchType>::project(const BoundaryPatchType& nonmortar,
                            const BoundaryPatchType& mortar, ctype couplingDist)
{
    ctype eps = 1e-5;    

    this->obstacles_.resize(nonmortar.numVertices());

    // local indices for the nonmortar vertices
    std::vector<int> globalToLocal;
    nonmortar.makeGlobalToLocal(globalToLocal); 

    const GridView& gridView = nonmortar.gridView();
    const typename GridView::IndexSet& indexSet = gridView.indexSet();

    // compute analytic search directions
    this->obsDirections_.resize(nonmortar.numVertices());

    // if the analytic function is a p1 basis grid function then the directions 
    // are just the coefficients
    const BlockVectorType* coeffs = NULL;
    if (dynamic_cast<const P1GridFunction* >(analyticDirections_))
        coeffs = &(dynamic_cast<const P1GridFunction* >(analyticDirections_)->coefficientVector());
        
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;
    VertexIterator vIt = gridView.template begin<dim>();
    VertexIterator vEndIt = gridView.template end<dim>();

    // compute boundary segments of the mortar side
    std::vector<BoundarySegment> mortarSegments;
    this->computeMortarSide(mortarSegments,mortar);

    // loop over all nonmortar vertices
    for (; vIt != vEndIt; ++vIt) {

        int localIdx = globalToLocal[indexSet.index(*vIt)];
        if (localIdx <0)
            continue; 

        CoordinateType target = vIt->geometry().corner(0);
        // if analytic function is a p1 basis function then get the coefficient
        if (coeffs)
            this->obsDirections_[localIdx] = (*coeffs)[indexSet.index(*vIt)];
        else // else evaluate the function
            analyticDirections_->evaluate(target,this->obsDirections_[localIdx]);

        // normalize
        this->obsDirections_[localIdx] /= this->obsDirections_[localIdx].two_norm();        

        // compute normal distance 
        this->obstacles_[localIdx] = this->computeNormalDistance(target,this->obsDirections_[localIdx],
                mortarSegments,couplingDist, this->overlap_);
    }
}

} /* namespace Contact */
} /* namespace Dune */

#endif
