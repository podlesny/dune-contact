// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_CONTACT_PROJECTIONS_NORMAL_PROJECTION_HH
#define DUNE_CONTACT_PROJECTIONS_NORMAL_PROJECTION_HH

#include <dune/common/fmatrix.hh>

#include "contactprojection.hh"

namespace Dune {
namespace Contact {

template <class BoundaryPatchType0, class BoundaryPatchType1=BoundaryPatchType0>
class NormalProjection : public ContactProjection<BoundaryPatchType0,BoundaryPatchType1>
{
    public:
        typedef ContactProjection<BoundaryPatchType0,BoundaryPatchType1> Base;
        typedef typename BoundaryPatchType0::iterator BoundaryIterator;
        typedef typename Base::BoundarySegment BoundarySegment;
        typedef typename Base::CoordinateType CoordinateType;
        typedef typename Base::GridView GridView;
        typedef typename Base::ctype ctype;
        using Base::dim;

        NormalProjection(ctype overlap = 1e-2) :
            Base(overlap)
        {}

        /** \brief Project the vertices of nonmortar boundary onto points of the mortar boundary
         *         and compute the distance and the direction of the projection.
         */
        virtual void project(const BoundaryPatchType0& nonmortar, const BoundaryPatchType1& mortar,
                const ctype couplingDist = std::numeric_limits<ctype>::max());


        static ctype computeNormalDistance(const CoordinateType& basePoint,
                const CoordinateType& normal,
                const std::vector<BoundarySegment>& mortarElements,
                const ctype couplingDist = std::numeric_limits<double>::max(),
                ctype overlap = 1e-2);

        static bool rayIntersectsLine(const CoordinateType& basePoint,
                const CoordinateType& direction,
                const CoordinateType& a,
                const CoordinateType& b,
                ctype& normalDist, ctype eps);

        static bool rayIntersectsTriangle(const CoordinateType& basePoint,
                const CoordinateType& direction,
                const CoordinateType& a,
                const CoordinateType& b,
                const CoordinateType& c,
                ctype& normalDist, ctype eps, ctype overlap=1e-2);
};

template <class BoundaryPatchType0, class BoundaryPatchType1>
void NormalProjection<BoundaryPatchType0,BoundaryPatchType1>::project(const BoundaryPatchType0& nonmortar,
        const BoundaryPatchType1& mortar,
        const ctype couplingDist)
{
    this->obstacles_.resize(nonmortar.numVertices());

    // local indices for the nonmortar vertices
    std::vector<int> globalToLocal;
    nonmortar.makeGlobalToLocal(globalToLocal);

    const GridView& gridView = nonmortar.gridView();
    const auto& indexSet = gridView.indexSet();

    // compute averaged vertex normals of the nonmortar vertices
    std::vector<CoordinateType> normals;

    nonmortar.getNormals(normals);

    this->obsDirections_.resize(nonmortar.numVertices());
    // copy the relevant normals into the obsDirections vector
    for (size_t i=0; i<normals.size(); i++)
        if (globalToLocal[i]>=0)
            this->obsDirections_[globalToLocal[i]] = normals[i];

    // compute boundary segments of the mortar side
    std::vector<BoundarySegment> mortarSegments;
    this->computeMortarSide(mortarSegments,mortar);

    // loop over all nonmortar vertices
    for (const auto& v : vertices(gridView)) {

        int localIdx = globalToLocal[indexSet.index(v)];
        if (localIdx <0)
            continue;

        CoordinateType target = v.geometry().corner(0);

        this->obstacles_[localIdx] = computeNormalDistance(target,this->obsDirections_[localIdx],
            mortarSegments,couplingDist,this->overlap_);

    }
}

template <class BoundaryPatchType0, class BoundaryPatchType1>
auto NormalProjection<BoundaryPatchType0,BoundaryPatchType1>::
computeNormalDistance(const CoordinateType& basePoint,
        const CoordinateType& normal,
        const std::vector<BoundarySegment>& mortarElements,
        const ctype couplingDist,
        ctype overlap) -> ctype
{
    const ctype eps = 1e-4;

    int numMortarTriangles = mortarElements.size();

    ctype bestDist = std::numeric_limits<ctype>::max();

    for (int i=0; i<numMortarTriangles; i++) {

        ctype dist;
        const BoundarySegment& theSegment = mortarElements[i];

        switch (theSegment.nVertices) {

            case 2:

                // Current side is an edge
                if (rayIntersectsLine(basePoint, normal, theSegment.vertices[0], theSegment.vertices[1], dist, eps)) {

                    if (fabs(dist) < fabs(bestDist) && (dist <eps || theSegment.unitOuterNormal*normal < eps)
                            && fabs(dist) < fabs(couplingDist) ) {
                        bestDist = dist;
                    }
                }


                break;

            case 3: {

                        // current side is a triangle
                        if (rayIntersectsTriangle(basePoint, normal,
                                                  theSegment.vertices[0],
                                                  theSegment.vertices[1],
                                                  theSegment.vertices[2],
                                                  dist, eps, overlap)) {

                            // ignore obstacle if normals are not converse
                            if (dist < bestDist && ( dist <eps || theSegment.unitOuterNormal*normal < eps)
                                    && fabs(dist) < fabs(couplingDist)) {
                                bestDist = dist;
                            }

                        }
                        break;
                    }
            case 4: {

                        // current side is a quadrilateral. Treat it as two triangles.
                        if (rayIntersectsTriangle(basePoint, normal,
                                    theSegment.vertices[0],
                                    theSegment.vertices[1],
                                    theSegment.vertices[2], dist, eps, overlap)) {

                            // ignore obstacle if normals are not converse
                            if (dist < bestDist && (dist < eps || theSegment.unitOuterNormal*normal < eps)
                                    && fabs(dist) < fabs(couplingDist)) {
                                bestDist = dist;
                            }

                        }

                        // get the positions of the three vertices for the second triangle
                        if (rayIntersectsTriangle(basePoint, normal,
                                    theSegment.vertices[2],
                                    theSegment.vertices[1],
                                    theSegment.vertices[3], dist, eps, overlap)) {

                            // ignore obstacle if normals are not converse
                            if (fabs(dist) < fabs(bestDist) && (dist <eps || theSegment.unitOuterNormal*normal < eps)
                                    && fabs(dist) < fabs(couplingDist)) {
                                bestDist = dist;
                            }

                        }
                        break;
                    }
            default:
                    DUNE_THROW(Dune::NotImplemented, "computeNormalDistance for boundary segments with "
                            << theSegment.nVertices << " vertices!");
        }

    }

    return bestDist;

}


template <class BoundaryPatchType0, class BoundaryPatchType1>
bool NormalProjection<BoundaryPatchType0,BoundaryPatchType1>::rayIntersectsLine(const CoordinateType& basePoint,
        const CoordinateType& direction,
        const CoordinateType& a,
        const CoordinateType& b,
        ctype& normalDist, ctype eps)
{
    if (dim!=2)
        DUNE_THROW(Dune::Exception, "Method rayIntersectsLine() should only be called for dim==2!");

    // we solve the equation basePoint + x_0 * normal = a + x_1 * (b-a)

    Dune::FieldMatrix<ctype,dim,dim> mat;
    for (int i=0; i<2; i++)
        mat[i] = {direction[i],a[i]-b[i]};

    CoordinateType rhs = a-basePoint;
    CoordinateType x;

    // Solve the system.  If it is singular the normal and the segment
    // are parallel and there is no intersection
    if (mat.determinant() == 0)
        return false;

    mat.solve(x,rhs);

    // x[0] is the distance, x[1] is the intersection point
    // in local coordinates on the segment
    if (x[1]<-eps || x[1] > 1+eps)
        return false;

    normalDist = x[0];
    return true;

}

template <class BoundaryPatchType0, class BoundaryPatchType1>
bool NormalProjection<BoundaryPatchType0,BoundaryPatchType1>::rayIntersectsTriangle(const CoordinateType& basePoint,
        const CoordinateType& direction,
        const CoordinateType& a,
        const CoordinateType& b,
        const CoordinateType& c,
        ctype& normalDist, ctype eps, ctype overlap)
{
    if (dim!=3)
        DUNE_THROW(Dune::Exception, "Method rayIntersectsTriangle() should only be called for dim==3!");

    CoordinateType ba = b - a;
    CoordinateType ca = c - a;

    CoordinateType e1 = ba;
    CoordinateType e2 = ca;

    // e1 = e1 / norm{e1};
    e1 /= e1.two_norm();

    // e2 = e2 / norm{e2}
    e2 /= e2.two_norm();

    // check if ray and triangle are parallel by checking if the triangles edges and the ray are linearly independent
    Dune::FieldMatrix<ctype, dim, dim> eeD;
    for (int i=0; i<dim; i++)
        eeD[i] = {e1[i],e2[i],direction[i]};
    bool parallel = fabs(eeD.determinant()) < eps;

    CoordinateType pa = basePoint - a;

    if (!parallel){
        // triangle and edge are not parallel

        // solve: (1-x[0]-x[1])*a + x[0]*b + x[1]*c = basePoint + x[2]*direction
        // and check if solution values are barycentric coordinates for the triangle

        Dune::FieldMatrix<ctype, dim, dim> eeD2;
        for (int i=0; i<dim; i++)
            eeD2[i] = {ba[i], ca[i], direction[i]};

        CoordinateType x(0);

        //solve
        eeD2.solve(x,pa);

        if ( (x[0] < -eps) || (x[1]<-eps) || (x[0] + x[1] > 1+eps))
            return false;

        // only look in the direction of the outward normal
        // but allow some overlap
        if (x[2]>overlap)
            return false;

        normalDist = -x[2];

        return true;

    } else {
        // TODO Check this: it always returns false
        // triangle and edge are parallel
        //float alpha = McMat3f(b-a, c-a, p-a).det();
        Dune::FieldMatrix<ctype, dim, dim> alphaMat;
        for (int i=0; i<dim; i++)
            alphaMat[i] = {ba[i], ca[i], pa[i]};
        ctype alpha = alphaMat.determinant();

        if (alpha<-eps || alpha>eps)
            return false;
        else {
            //printf("ray and triangle are parallel!\n");
            return false;

        }

    }

}

} /* namespace Contact */
} /* namespace Dune */

#endif
