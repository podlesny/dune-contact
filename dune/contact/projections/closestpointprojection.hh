// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_CONTACT_PROJECTIONS_CLOSEST_POINT_PROJECTION_HH
#define DUNE_CONTACT_PROJECTIONS_CLOSEST_POINT_PROJECTION_HH

#include <dune/common/dynmatrix.hh>
#include <dune/common/dynvector.hh>

#include <dune/contact/projections/contactprojection.hh>

namespace Dune {
namespace Contact {

/** \brief The Closes point projection.
 *
 *  Each vertex of the nonmortar side is mapped to the closest point at the mortar side
 *  w.r.t to the euclidean norm. Additionally the vector (v_nonmortar - v_projected) may not
 *  run through the mortar body.
 *
 *  \tparam BoundaryPatchType - The type of the boundary patches.
 */
template <class BoundaryPatchType>
class ClosestPointProjection : public ContactProjection<BoundaryPatchType>
{

public:
    typedef ContactProjection<BoundaryPatchType> Base;
    typedef typename BoundaryPatchType::iterator BoundaryIterator;
    typedef typename Base::BoundarySegment BoundarySegment;
    typedef typename Base::CoordinateType CoordinateType;
    typedef typename Base::GridView GridView;
    typedef typename Base::ctype ctype;
    using Base::dim;

    ClosestPointProjection(int numIt, ctype overlap = 1e-1) :
        Base(overlap),
        numIt_(numIt)
    {}

    /** \brief Project the vertices of nonmortar boundary onto points of the mortar boundary
     *         and compute the distance and the direction of the projection.
     */
    virtual void project(const BoundaryPatchType& nonmortar, const BoundaryPatchType& mortar,
                         const ctype couplingDist = std::numeric_limits<ctype>::max());


    /** \brief A method that computes the closest point projection of a given point to a boundary segment.
     *
     *  The projection is computed by solving simplex constrained quadratic problems for the barycentric
     *  coordinates. The constraint that sum_j \lambda_j == 1 is fullfilled by using the edges as search
     *  directions for the Gauß-Seidel method and positivity constraints for the dofs ensure "barycentricity".
     *
     *  I'm not yet sure that this works for types different than Simplex.
     */
    CoordinateType computeClosestPoint(const CoordinateType& target, const BoundarySegment& segment);

private:
    //! Number of iterations for the local closest point projection
    const int numIt_;

};

template <class BoundaryPatchType>
void ClosestPointProjection<BoundaryPatchType>::project(const BoundaryPatchType& nonmortar,
                                                        const BoundaryPatchType& mortar,
                                                        const ctype couplingDist)
{
    ctype eps = 1e-11;

    // initialize
    this->obsDirections_.resize(nonmortar.numVertices());
    this->obstacles_.resize(nonmortar.numVertices());

    // compute boundary segments of the mortar side
    std::vector<BoundarySegment> mortarSegments;
    this->computeMortarSide(mortarSegments,mortar);

    // local indices for the nonmortar vertices
    std::vector<int> globalToLocal;
    nonmortar.makeGlobalToLocal(globalToLocal);

    const GridView& gridView = nonmortar.gridView();
    const typename GridView::IndexSet& indexSet = gridView.indexSet();

    // compute averaged vertex normals of the nonmortar vertices
    // we need these to check if the obstacle direction runs through the nonmortar body
    auto&& averagedNonmortarNormals = nonmortar.getNormals();

    // loop over all nonmortar vertices
    for (const auto& v : vertices(gridView)) {

        int globalIdx = indexSet.index(v);
        int localIdx = globalToLocal[globalIdx];

        if (localIdx <0)
            continue;

        // nonmortar vertex coordinates
        CoordinateType target = v.geometry().corner(0);

        ctype dist = std::numeric_limits<ctype>::max();
        int bestTri = -1;
        // save the direction vector of the optimal feasible point, i.e. (v_proj - v_nonmortar)
        CoordinateType projDirection;

        // loop over the mortar faces
        for (size_t i=0;i<mortarSegments.size(); i++) {

            CoordinateType proj = computeClosestPoint(target,mortarSegments[i]);

            //compute the direction and distance
            proj -= target;
            ctype projDist = proj.two_norm();
            proj /= projDist;

            // if point is not closer then skip it
            if (projDist > dist || projDist > couplingDist)
                continue;

            // the point is feasible if the direction does not run through the mortar or nonmortar body
            // we allow this infeasibility if the distance is very small
            if ( ( (mortarSegments[i].unitOuterNormal*proj>eps) || (averagedNonmortarNormals[globalIdx]*proj<-eps) )
                        && projDist >this->overlap_)
                continue;

            projDirection = proj;
            bestTri = i;
            dist = projDist;
        }

        // if we found a feasible closest point save the distance and
        if (bestTri != -1) {
            this->obstacles_[localIdx] = dist;
            // if there is overlap, fix orientation of the direction
            if (dist<this->overlap_ and (projDirection*mortarSegments[bestTri].unitOuterNormal)>eps) {
                this->obsDirections_[localIdx] = projDirection;
                this->obsDirections_[localIdx] *= -1;
            // if obstacle distance is very small, use the inner mortar normal as direction
            } else if (dist<eps) {
                this->obsDirections_[localIdx] = mortarSegments[bestTri].unitOuterNormal;
                this->obsDirections_[localIdx] *= -1;
            } else
                this->obsDirections_[localIdx] = projDirection;

        } else {
            this->obstacles_[localIdx] = std::numeric_limits<ctype>::max();
            this->obsDirections_[localIdx] = averagedNonmortarNormals[globalIdx];
        }
    }
}

template <class BoundaryPatchType>
typename ClosestPointProjection<BoundaryPatchType>::CoordinateType ClosestPointProjection<BoundaryPatchType>::computeClosestPoint(const CoordinateType& target, const BoundarySegment& segment)
{
    /**
     *  The functional to be minimized is: 0.5*norm(\sum_i lambda_i corner_i  - target)^2
     *  where lambda_i are barycentric coordinates, i.e. 0\leq lambda_i \leq 1 and \sum_i lambda_i=1
     *
     *  The resulting quadratic minimization problem is given by 0.5 Lambda^T*A*Lambda - l^T*Lambda
     *  with
     *      A_ij = (corner_i,corner_j)  and l_i = (corner_i, target)
    */
    int nCorners = segment.nVertices;
    Dune::DynamicMatrix<ctype> A(nCorners,nCorners);

    for (int i=0;i<nCorners;i++)
        for (int j=0;j<=i; j++)
            A[i][j] = segment.vertices[i]*segment.vertices[j];

    //make symmetric
    for (int i=0;i<nCorners; i++)
        for (int j=i+1;j<nCorners;j++)
            A[i][j] = A[j][i];

    Dune::DynamicVector<ctype> l(nCorners);
    for (int i=0;i<nCorners;i++)
        l[i] = target*segment.vertices[i];

    //choose a feasible initial solution
    Dune::DynamicVector<ctype> sol(nCorners, 1.0/nCorners);

    // use a Gauß-Seidel like method for positive semi-definite quadratic problems with simplex constraints.
    for (int i=0;i<numIt_; i++) {

        // compute the residual
        Dune::DynamicVector<ctype> rhs = l;
        A.mmv(sol,rhs);

        // use the edge vectors as search directions
        ctype alpha = 0.0;
        for(int j1=0; j1<(nCorners-1); ++j1)
            for(int j2=j1+1; j2<nCorners; ++j2)
            {
                // compute matrix entry and rhs for edge direction
                ctype a  = A[j1][j1] - A[j1][j2] - A[j2][j1] + A[j2][j2];
                ctype b = rhs[j1] - rhs[j2];

                // compute minimizer for correction
                if (a>0) {
                    //if convex
                    alpha = b/a;
                    // project alpha such that we stay positiv
                    if (sol[j2]-alpha<-1e-14) {
                        alpha = sol[j2];
                    }  else if (sol[j1] + alpha<-1e-14) {
                        alpha = -sol[j1];
                    }
                }
                else {
                    // if linear the minimum is achieved at one of the boundaries,
                    // i.e. at sol_[j2] or -sol_[j1]

                    ctype sum = sol[j1] + sol[j2];

                    ctype lValue = 0.5*A[j1][j1]*sum*sum - rhs[j1]*sum;
                    ctype uValue = 0.5*A[j2][j2]*sum*sum - rhs[j2]*sum;

                    alpha = (lValue < uValue) ? sol[j2] : -sol[j1];

                }
                // apply correction
                sol[j1] += alpha;
                sol[j2] -= alpha;

                // update the local residual for corrections
                for (int p=0; p<nCorners; p++)
                    rhs[p] -= (A[p][j1] -A[p][j2])* alpha;
            }
    }

    ctype eps = 1e-5;
    ctype sum = 0;
    for (size_t i=0; i<sol.size(); i++) {
        if (sol[i]<-eps)
            DUNE_THROW(Dune::Exception, "No barycentric coords "<<sol[1]<<" nr. "<<i);
        sum += sol[i];
    }
    if (sum>1+eps)
        DUNE_THROW(Dune::Exception, "No barycentric coords, sum: "<<sum);

    CoordinateType projected(0);
    for (int i=0; i<nCorners; i++)
        projected.axpy(sol[i],segment.vertices[i]);

    return projected;
}

} /* namespace Contact */
} /* namespace Dune */

#endif
