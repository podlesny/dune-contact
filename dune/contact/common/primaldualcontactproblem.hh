// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:
#ifndef DUNE_CONTACT_COMMON_PRIMAL_DUAL_CONTACT_PROBLEM_HH
#define DUNE_CONTACT_COMMON_PRIMAL_DUAL_CONTACT_PROBLEM_HH

#include <dune/contact/common/staticmgcontactproblem.hh>

namespace Dune {
namespace Contact {

/** \brief Static contact problem class for the filter multigrid method.*/
template <class VectorType, class MaterialType>
class PrimalDualContactProblem : public StaticMgContactProblem<VectorType, MaterialType>
{
  using Base = StaticMgContactProblem<VectorType, MaterialType>;
  using DefGridComplex = typename Base::DefGridComplex;

  using DeformedGridType = typename Base::DeformedGridType;
  using ContactCoupling = typename Base::ContactCoupling;
  using Basis = typename MaterialType::GlobalBasis;
  using BasisGridFunction = typename Base::BasisGridFunction;

public:
  using ContactAssembler = typename Base::ContactAssembler;
  using BlockVector = VectorType;
  constexpr static int dim = BlockVector::block_type::dimension;
  using field_type = typename BlockVector::field_type;
  using ObstacleVector = typename Base::ObstacleVector;
  using ScalarVector = ObstacleVector;
  using ScalarMatrix = Dune::BCRSMatrix<Dune::FieldMatrix<field_type, 1, 1> >;
  using MatrixType = typename Base::MatrixType;
  using JacobianType = typename Base::JacobianType;


  PrimalDualContactProblem(ContactAssembler& contactAssembler,
                         std::vector<MaterialType>& materials,
                         DefGridComplex& complex,
                         const std::vector<BlockVector>& extForces,
                         field_type priDualConstant,
                         field_type fd_eps,
                         bool full_linearisation,
                         field_type initActiveSetBound) :
    Base(contactAssembler, materials, complex, extForces),
    priDualConstant_(priDualConstant),
    changedActiveNodes_(-1),
    fd_eps_(fd_eps),
    full_linearisation_(full_linearisation),
    initActiveSetBound_(initActiveSetBound)
  {
    activeSet_.resize(1, false);
  }

  //! Assemble the linearisations of energy and constraints
  void assembleLinearisations(const std::vector<BlockVector>& iterates);

  //! Setup the primal-dual active set problem
  void assemblePrimalDualProblem(const ScalarVector& lagrangeMultiplier);

  //! Return the quadratic term of the linearised energy
  const ScalarMatrix& priDualMatrix() const {return priDualMatrix_;}

  //! Return the linear term of linearised energy
  const ScalarVector& priDualRhs() const {return priDualRhs_;}

  //! Return the nonlinear constraints
  const ObstacleVector& getConstraints() const
  {
    return this->contactAssembler_->getContactCouplings()[0]->getWeakObstacle();
  }

  //! Update the active set and assemble new constraints
  void updateActiveSetAndLagrangeMultiplier(ScalarVector& lagrangeMultiplier);

  //! Get active set
  const BitSetVector<1>& getActiveSet() const
  {
    return activeSet_;
  }

  void addHardDirichlet(const ScalarVector& dirV, const BitSetVector<1>& dirN);

  //! Return how many nodes changed from active to inactive and vice versa
  int changedActiveNodes() const {return changedActiveNodes_;}

  void initialiseEmptyActiveSet() {
    const auto& constraints = getConstraints();
    activeSet_.resize(getConstraints().size(), false);
    for (size_t i=0; i<constraints.size(); i++)
      if (constraints[i][0]<initActiveSetBound_)
        activeSet_[i] = true;
  }

  ScalarVector leastSquaresLagrangeMultiplier();

  std::vector<BlockVector> residuals(const ScalarVector& lagrangeMultiplier) {
    std::vector<BlockVector> residuals = this->rhs_;

    auto coupling = std::dynamic_pointer_cast<ContactCoupling>(
          this->contactAssembler_->getContactCouplings()[0]);
    const auto& gridIdx = this->contactAssembler_->getCoupling(0).gridIdx_;

    coupling->exactNonmortarMatrix().umtv(lagrangeMultiplier, residuals[gridIdx[0]]);
    coupling->exactMortarMatrix().umtv(lagrangeMultiplier, residuals[gridIdx[1]]);

    return residuals;
  }

protected:

  //! Compute index pattern for the primal-dual system matrix
  void addPriDualMatrixIndices();
  //! Add the entries to the primal-dual system matrix
  void addPriDualMatrixEntries(const ScalarVector& lagrangeMultiplier);
  //! Assemble the right hand side for the primal-dual right hand side
  void assemblePriDualRhs(const ScalarVector& lagrangeMultiplier);

  //! Compute FD approximation of the constraint Hessian times Lagrange multiplier
  void computeFDConstraintHessian(const ScalarVector& lagrangeMultiplier);

  //! Compute j-th entry of k-th grid constraint Hessian entry
  void addFDEntry(MatrixType& denseContactHessian, const ScalarVector& lagrangeMultiplier, int gridIdx, int component);

  //! Condense the dense matrix into a scalar condensed one
  void condenseConstraintHessian(const MatrixType& denseConstraintHessian);

  std::vector<BlockVector> currentIterates_;

  //! The condensed Hessian of the constraints
  ScalarMatrix condensedConstraintHessian_;

  //! The matrix of the Newton problem in the pri-dual method
  ScalarMatrix priDualMatrix_;

  //! The rhs of the pri-dual problem
  ScalarVector priDualRhs_;
  //! The active set
  Dune::BitSetVector<1> activeSet_;

  //! This is an algorithmic parameter used to detect the active set
  field_type priDualConstant_;

  int changedActiveNodes_;

  field_type fd_eps_;

  bool full_linearisation_;

  field_type initActiveSetBound_;
};

} /* namespace Contact */
} /* namespace Dune */

#include "primaldualcontactproblem.cc"

#endif
