// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:
#ifndef DUNE_CONTACT_COMMON_CONTACT_ASSEMBLER_FACTORY_HH
#define DUNE_CONTACT_COMMON_CONTACT_ASSEMBLER_FACTORY_HH

#include <dune/common/parametertree.hh>

#include <dune/contact/assemblers/largedeformationcontactassembler.hh>

namespace  Dune {
namespace  Contact {

class ContactAssemblerFactory
{
public:
  template <class GridComplex>
  static LargeDeformationContactAssembler<typename GridComplex::DeformedGridType, typename GridComplex::Vector> createLargeDeformationAssembler(
                const GridComplex& gridComplex,
                const Dune::ParameterTree& config);
};

} // end namespace Contact
} // end namespace Dune

#include "contactassemblerfactory.cc"
#endif
