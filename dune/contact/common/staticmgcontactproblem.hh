// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:
#ifndef DUNE_CONTACT_COMMON_STATIC_MG_CONTACT_PROBLEM_HH
#define DUNE_CONTACT_COMMON_STATIC_MG_CONTACT_PROBLEM_HH

#include <dune/fufem/functions/basisgridfunction.hh>

#include <dune/contact/common/deformedcontinuacomplex.hh>
#include <dune/contact/assemblers/largedeformationcontactassembler.hh>

namespace Dune {
namespace Contact {

/** \brief Static contact problem class for the filter multigrid method.*/
template <class VectorTypeT, class MaterialType>
class StaticMgContactProblem
{
public:
  using GridType = typename MaterialType::GridType;
  using VectorType = VectorTypeT;

private:
  using DefGridComplex = DeformedContinuaComplex<GridType, VectorType>;

public:
  const static int dim = VectorType::block_type::dimension;
  using field_type = typename VectorType::field_type;
  using DeformedGridType = typename DefGridComplex::DeformedGridType;
  using ContactAssembler = LargeDeformationContactAssembler<DeformedGridType,VectorType>;
  using ContactCoupling = typename ContactAssembler::LargeDefCoupling;
  using ObstacleVector = typename ContactCoupling::ObstacleVector;
  using MatrixType = typename ContactAssembler::MatrixType;
  using JacobianType = typename ContactAssembler::JacobianType;

  using Basis = typename MaterialType::GlobalBasis;
  using BasisGridFunction = ::BasisGridFunction<Basis,VectorType>;

  StaticMgContactProblem(ContactAssembler& contactAssembler,
                         std::vector<MaterialType>& materials,
                         DefGridComplex& complex,
                         const std::vector<VectorType>& extForces,
                         bool exactHessian = false) :
    contactAssembler_(stackobject_to_shared_ptr<ContactAssembler>(contactAssembler)),
    materials_(stackobject_to_shared_ptr<std::vector<MaterialType> >(materials)),
    complex_(stackobject_to_shared_ptr<DefGridComplex>(complex)),
    extForces_(stackobject_to_shared_ptr<const std::vector<VectorType> >(extForces)),
    exactHessian_(exactHessian)
  {
    rhs_.resize(materials.size());
    stiffMat_.resize(materials.size());
  }

  //! Assemble the quadratic problem
  void assembleQP(const std::vector<VectorType>& iterates);

  //! Compute the energy of the quadratic minimization problem
  virtual field_type energy(const std::vector<VectorType>& iterate) const;

  //! Compute the infeasibility of the current iterate
  field_type infeasibility(const std::vector<VectorType>& iterates);

  //! Set external forces
  void setExternalForces(const std::vector<VectorType>& extForces)
  {
    extForces_ = Dune::stackobject_to_shared_ptr<const std::vector<VectorType> >(extForces);
  }

  //! Return the contact assembler
  std::shared_ptr<ContactAssembler> contactAssembler() {return contactAssembler_; }

  //! Return the quadratic term of the linearised energy
  const MatrixType& A() const {return A_;}

  //! Return the linear term of linearised energy
  const VectorType& f() const {return f_;}

  //! Return the linear term of linearised energy
  const std::vector<VectorType>& rhs() const {return rhs_;}

  //! Return the nonlinear constraints
  const auto& constraints() const
  {
    return contactAssembler_->totalObstacles_;
  }

  //! Return the i'th material
  MaterialType& getMaterial(size_t i) {return (*materials_)[i]; }

  //! Set surface mass matrices in the case of Robin conditions
  void setMassMatrices(const std::vector<MatrixType>& mass)
  {
    mass_ = stackobject_to_shared_ptr<const std::vector<MatrixType> >(mass);
  }

protected:

  //! Deform grids with iterates
  void deformGrids(const std::vector<VectorType>& iterates) {
    for (size_t i=0; i<complex_->size(); i++)
      complex_->setDeformation(iterates[i],i);
  }

  //! Mass matrices to regularisation pure Neumann problems
  std::shared_ptr<const std::vector<MatrixType> > mass_;

  //! The quadratic part of the linearised energy
  MatrixType A_;

  //! The linear part of the linearised energy
  VectorType f_;

  //! Iteration matrices, we need to store these for the error computation
  std::vector<const MatrixType*> stiffMat_;

  //! The  linear parts of the nonlinear energy for every involved grid
  std::vector<VectorType> rhs_;

  //! The contact assembler, it is only used to store the contact couplings
  std::shared_ptr<ContactAssembler> contactAssembler_;

  //! The involved materials
  std::shared_ptr<std::vector<MaterialType> > materials_;

  //! The deformed grids
  std::shared_ptr<DefGridComplex> complex_;

  //! The external forces
  std::shared_ptr<const std::vector<VectorType> > extForces_;

  //! Flag to determine if the exact transformation should be used or the lumpped one
  bool exactHessian_;
};

} /* namespace Contact */
} /* namespace Dune */

#include "staticmgcontactproblem.cc"

#endif
