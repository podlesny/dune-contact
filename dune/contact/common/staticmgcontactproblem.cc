// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:


#include <dune/fufem/assemblers/functionalassembler.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/solvers/computeenergy.hh>

namespace Dune {
namespace Contact {

template <class VectorType, class MaterialType>
typename VectorType::field_type StaticMgContactProblem<VectorType,MaterialType>::infeasibility(const std::vector<VectorType>& iterates)
{
  // deform the grids
  deformGrids(iterates);

  // The infeasibility is the maximal nonlinear constraint violation
  field_type infeasibility{0.0};

  for (int i=0; i<contactAssembler_->nCouplings(); i++) {
    auto coupling = std::dynamic_pointer_cast<ContactCoupling>(contactAssembler_->getContactCouplings()[i]);
    // rebuild coupling and evaluate obstacles
    auto obstacles = coupling->assembleObstacles(true);

    for (const auto& obstacle : obstacles)
      infeasibility = std::max(infeasibility, -obstacle);
  }

  return infeasibility;
}

template <class VectorType, class MaterialType>
typename VectorType::field_type StaticMgContactProblem<VectorType,MaterialType>::energy(const std::vector<VectorType>& iterates) const
{
  // The energy is given by the material energy and the external forces
  field_type energy(0);

  for (size_t i=0;i<iterates.size();i++) {

    const auto& material = (*materials_)[i];

    // potential energy
    auto it = std::make_shared<BasisGridFunction>(material.basis(), iterates[i]);
    energy += material.energy(it);
    if (mass_)
      energy += computeEnergy((*mass_)[i],iterates[i]);


    // external terms
    energy -= ((*extForces_)[i]*iterates[i]);
  }

  return energy;
}

template <class VectorType, class MaterialType>
void StaticMgContactProblem<VectorType,MaterialType>::assembleQP(const std::vector<VectorType>& iterates)
{
  // assemble linear and quadratic terms
  for (size_t i=0 ; i < iterates.size(); i++) {

    auto& material = (*this->materials_)[i];
    const auto& basis = material.basis();

    auto displace = std::make_shared<BasisGridFunction>(basis, iterates[i]);

    // assemble quadratic term
    OperatorAssembler<Basis,Basis> globalAssembler(basis, basis);
    if (stiffMat_[i])
      delete(stiffMat_[i]);

    MatrixType* mat = new MatrixType;
    globalAssembler.assemble(material.secondDerivative(displace), *mat);
    if (mass_)
      *mat += (*mass_)[i];
    stiffMat_[i] = mat;

    // assemble linear term
    rhs_[i].resize(iterates[i].size());

    FunctionalAssembler<Basis> funcAssembler(basis);
    funcAssembler.assemble(material.firstDerivative(displace), rhs_[i]);
    rhs_[i] -= (*extForces_)[i];
    if (mass_)
      (*mass_)[i].umv(iterates[i], rhs_[i]);
    rhs_[i] *= -1;
  }

  // assemble the linearisations
  contactAssembler_->assembleLinearisations();
  contactAssembler_->assembleClpReflections();
  contactAssembler_->assembleObstacle();

  // this is not needed any more I think
  contactAssembler_->computeInverseTransformationMatrix();

  if (exactHessian_) {
    contactAssembler_->computeExactTransformationMatrix();
    contactAssembler_->assembleRightHandSide(rhs_, f_);
    contactAssembler_->assembleExactJacobian(stiffMat_, A_);
  } else {
    contactAssembler_->computeLumppedTransformationMatrix();
    contactAssembler_->assembleExactRightHandSide(rhs_, f_);
    contactAssembler_->assembleJacobian(stiffMat_, A_);
  }

  // store old patches in case they are still needed
  contactAssembler_->storePatches();
}

} /* namespace Contact */
} /* namespace Dune */
