// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:

#include <string>
#include <sstream>

#include <dune/common/stringutility.hh>
#include <dune/common/parametertree.hh>
#include <dune/fufem/utilities/boundarypatchfactory.hh>

#include <dune/contact/assemblers/largedeformationcontactassembler.hh>
#include <dune/contact/projections/closestpointprojection.hh>
#include <dune/contact/projections/normalprojection.hh>
#include <dune/contact/common/couplingpair.hh>

#ifdef HAVE_DUNE_GRID_GLUE
#include <dune/grid-glue/merging/conformingmerge.hh>
#include <dune/grid-glue/merging/contactmerge.hh>
#include <dune/grid-glue/merging/standardmerge.hh>
#endif

namespace Dune {
namespace Contact {

template <class GridComplex>
LargeDeformationContactAssembler<typename GridComplex::DeformedGridType, typename GridComplex::Vector>
ContactAssemblerFactory::createLargeDeformationAssembler(
                const GridComplex& complex,
                const Dune::ParameterTree& config)
{
  using Grid = typename GridComplex::DeformedGridType;
  using field_type = typename GridComplex::Vector::field_type;
  constexpr int dim = Grid::dimension, dimworld = Grid::dimensionworld;

  using ContactMerger = Dune::GridGlue::ContactMerge<dimworld, field_type>;
  auto projectionType = (config.get<std::string>("projectionType", "closestPoint") == "outerNormal")
              ? ContactMerger::OUTER_NORMAL : ContactMerger::CLOSEST_POINT;

  //   Create contact assembler
  size_t nCouplings = config.get<size_t>("nCouplings");
  bool exactPostprocess = config.get<bool>("exactPostprocess");
  // by now all couplings must have the same allowed overlap and type
  auto overlap = config.get<field_type>("overlap");
  LargeDeformationContactAssembler<Grid, typename GridComplex::Vector> contactAssembler(complex.size(), nCouplings,
                                                                                        exactPostprocess,
                                                                                        config.get("coveredArea",0.99),
                                                                                        overlap, projectionType);
  contactAssembler.setGrids(complex.gridVector());
  auto path = config.get<std::string>("path");

  for (size_t i=0; i < nCouplings; i++) {

    const auto& coupleConfig = config.sub(Dune::formatString("coupling%u", i));

    // the involved grids
    auto nonmortarGrid = coupleConfig.get<std::string>("nonmortarGrid");
    auto mortarGrid = coupleConfig.get<std::string>("mortarGrid");

    // read field describing the nonmortar boundary, if nothing is found in the parameter file, take all
    BoundaryPatch<typename Grid::LevelGridView> patch0(complex.grid(nonmortarGrid).levelGridView(0));
    if (!BoundaryPatchFactory<typename Grid::LevelGridView>::setupBoundaryPatch(coupleConfig.sub(nonmortarGrid), patch0, path))
      patch0.setup(complex.grid(nonmortarGrid).levelGridView(0), true);

    BoundaryPatch<typename Grid::LevelGridView> patch1(complex.grid(mortarGrid).levelGridView(0));
    if (!BoundaryPatchFactory<typename Grid::LevelGridView>::setupBoundaryPatch(coupleConfig.sub(mortarGrid), patch1, path))
      patch1.setup(complex.grid(mortarGrid).levelGridView(0), true);

    // Set the coupling
    using BPatch = BoundaryPatch<typename Grid::LeafGridView>;
    std::shared_ptr<Dune::Contact::ContactProjection<BPatch> > contactProjection;
    if (projectionType == ContactMerger::OUTER_NORMAL)
      contactProjection = std::make_shared<NormalProjection<BPatch> >(overlap);
    else
      contactProjection = std::make_shared<ClosestPointProjection<BPatch> >(coupleConfig.get("projectionIterations", 50), overlap);

    // Choose which merger to use
    std::shared_ptr<Dune::GridGlue::StandardMerge<field_type, dim-1, dim-1, dimworld> > merger;

    auto backEnd = coupleConfig.get<std::string>("backEnd");
    std::transform(backEnd.begin(), backEnd.end(), backEnd.begin(), ::tolower);
    if (backEnd == "conforming") {
      merger = std::make_shared<Dune::GridGlue::ConformingMerge<dim-1, dimworld, field_type> >();
    } else if (backEnd == "contact") {
      auto contactMerger = std::make_shared<ContactMerger>(overlap, projectionType);
      contactMerger->minNormalAngle(coupleConfig.get<field_type>("minNormalAngle", std::acos(-0.1)));
      merger = std::move(contactMerger);
    } else
      DUNE_THROW(Dune::Exception, "Type " << backEnd << "unknown. Options are: 'conforming' or 'contact'");

    CouplingPair<Grid, Grid, field_type> coupling;
    coupling.set(complex.idx(nonmortarGrid), complex.idx(mortarGrid),
                 std::move(patch0), std::move(patch1),
                 coupleConfig.get<field_type>("couplingDistance"),
                 CouplingPairBase::type(coupleConfig.get<std::string>("couplingType")),
                 contactProjection, merger);

    contactAssembler.setCoupling(coupling, i);
  }

  // setup contact couplings
  contactAssembler.constructCouplings();

  return contactAssembler;
}

} // namespace Contact
} // namespace Dune
