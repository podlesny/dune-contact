// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_COMMON_COUPLING_PAIR_HH
#define DUNE_CONTACT_COMMON_COUPLING_PAIR_HH

#include <string>

#include <dune/common/deprecated.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/solvers/common/wrapownshare.hh>

#ifdef HAVE_DUNE_GRID_GLUE
#include <dune/grid-glue/merging/merger.hh>
#endif

#include <dune/contact/projections/contactprojection.hh>

namespace Dune {
namespace Contact {

class CouplingPairBase {

public:

    enum CouplingType {NONE, CONTACT, STICK_SLIP, GLUE, RIGID};

    /** \brief Get coupling type from string */
    static CouplingType type(const std::string& typeString) {
        if (typeString == "NONE" or typeString == "none")
            return NONE;
        if (typeString == "CONTACT" or typeString == "contact")
            return CONTACT;
        if (typeString == "STICK_SLIP" or typeString == "stick_slip")
            return STICK_SLIP;
        if (typeString == "GLUE" or typeString == "glue")
            return GLUE;
        if (typeString == "RIGID" or typeString == "rigid")
            return RIGID;

        DUNE_THROW(Dune::Exception, "Type '" << typeString << "' is unknown!");
    }

    CouplingType type_;

};

template <class GridType0, class GridType1, class field_type>
class CouplingPair : public CouplingPairBase {

public:
    typedef Dune::FieldVector<field_type,GridType0::dimensionworld> CoordinateType;
    typedef std::function<field_type (const CoordinateType&, const CoordinateType&)> Obstacle;

    typedef BoundaryPatch<typename GridType0::LevelGridView> LevelBoundaryPatch0;
    typedef BoundaryPatch<typename GridType1::LevelGridView> LevelBoundaryPatch1;

    typedef BoundaryPatch<typename GridType0::LeafGridView> LeafBoundaryPatch0;
    typedef BoundaryPatch<typename GridType1::LeafGridView> LeafBoundaryPatch1;

    typedef Dune::GridGlue::Merger<field_type,GridType0::dimension-1,GridType1::dimension-1,GridType0::dimensionworld> BackEndType;
    typedef ContactProjection<LeafBoundaryPatch0, LeafBoundaryPatch1> Projection;

    template <class Patch>
    void set(int gridIdx0, int gridIdx1,
             Patch&& patch,
             field_type dist,
             CouplingType type) {
        gridIdx_[0]  = gridIdx0;
        gridIdx_[1]  = gridIdx1;
        obsPatch_ = Dune::Solvers::wrap_own_share<const LevelBoundaryPatch0>(std::forward<Patch>(patch));
        obsDistance_ = dist;
        type_        = type;
    }

    //! Setter for 1-body contact problems
    template <class Patch>
    void set(Patch&& patch,
             field_type dist,
             CouplingType type,
             Obstacle obsFunction) {
        obsPatch_ = Dune::Solvers::wrap_own_share<const LevelBoundaryPatch0>(std::forward<Patch>(patch));
        obsDistance_ = dist;
        type_        = type;
        obsFunction_ = obsFunction;
    }

    template <class Patch0, class Patch1, class ProjectionT, class BackEnd>
    void set(int gridIdx0, int gridIdx1,
             Patch0&& patch0, Patch1&& patch1,
             field_type dist, CouplingType type,
             ProjectionT&& projection,
             BackEnd&& backend) {

        set(gridIdx0, gridIdx1, std::forward<Patch0>(patch0), dist, type);
        mortarPatch_ = Dune::Solvers::wrap_own_share<const LevelBoundaryPatch1>(std::forward<Patch1>(patch1));

        contactProjection_ = Dune::Solvers::wrap_own_share<Projection>(std::forward<ProjectionT>(projection));
        gridGlueBackend_ = Dune::Solvers::wrap_own_share<BackEndType>(std::forward<BackEnd>(backend));
    }

    std::shared_ptr<const LevelBoundaryPatch0> patch0() {return obsPatch_;}

    std::shared_ptr<const LevelBoundaryPatch1> patch1() {return mortarPatch_;}

    std::shared_ptr<Projection> projection()
        { return contactProjection_; }

    std::shared_ptr<const Projection> projection() const
        { return contactProjection_; }

    template <class ProjectionT>
    void projection(ProjectionT&& newProjection) {
      contactProjection_ = Dune::Solvers::wrap_own_share<Projection>(std::forward<ProjectionT>(newProjection));
    }

    std::shared_ptr<BackEndType> backend()
        { return gridGlueBackend_; }

    std::shared_ptr<const BackEndType> backend() const
        { return gridGlueBackend_; }

    template <class BackEnd>
    void backend(BackEnd&& newBackend) {
      gridGlueBackend_ = Dune::Solvers::wrap_own_share<BackEndType>(std::forward<BackEnd>(newBackend));
    }

    /** \brief The nonmortar boundary patch, or the contact boundary for 1-body problems.*/
    std::shared_ptr<const LevelBoundaryPatch0> obsPatch_;
    /** \brief The mortar boundary patch. */
    std::shared_ptr<const LevelBoundaryPatch1> mortarPatch_;

    /** \brief For n-body problems we need to know which grids are involved. */
    std::array<int, 2> gridIdx_;

    /** \brief the maximal search distance. */
    field_type obsDistance_;

    /** \brief The analytical or discrete obstacle for 1-body problems.
               This functions expects a position and a search direction and
               computes the distance to the obstacle. */
    Obstacle obsFunction_;

private:
    /** \brief A projection which determines the direction of the contact normals. */
    std::shared_ptr<Projection> contactProjection_;

    /** \brief dune-grid-glue Merger, to be set if they shall differ from ContactMerge.
     *         For conforming coupling surfaces, one may use ConformingMerge for some speedUp.
     */
    std::shared_ptr<BackEndType> gridGlueBackend_;
};

} /* namespace Contact */
} /* namespace Dune */

#endif
