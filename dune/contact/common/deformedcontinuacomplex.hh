// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_COMMON_DEFORMED_CONTINUA_COMPLEX_HH
#define DUNE_CONTACT_COMMON_DEFORMED_CONTINUA_COMPLEX_HH

#include <dune/common/shared_ptr.hh>

#include <dune/grid/geometrygrid/grid.hh>
#include <dune/fufem/functions/deformationfunction.hh>

#include <dune/solvers/common/wrapownshare.hh>

namespace Dune {
namespace Contact {

template <class GridType, class VectorType>
class DeformedContinuaComplex
{
public:
    using Vector = VectorType;
    using HostGrid = GridType;
    using DeformationFunction = ::DeformationFunction<typename GridType::LeafGridView, Vector>;
    using DeformedGridType = Dune::GeometryGrid<GridType, DeformationFunction>;

    DeformedContinuaComplex() = default;

    /** \brief Construct complex from a map of shared pointer */
    DeformedContinuaComplex(std::map<std::string, std::shared_ptr<HostGrid> >& grids) {
      for (auto&& it : grids)
        addGrid(*it.second, it.first);
    }

    /** \brief Construct complex from a map of objects which contain the grid
     *         This is needed when constructing the grids for a heterogeneous joint model.
     */
    template <class ContinuumData>
    DeformedContinuaComplex(std::vector<std::pair<std::string, ContinuumData> >& complex) {
      for (auto&& it : complex)
        addGrid(it.second.getGrid(), it.first);
    }

    //! Add non-deformed grid by handing over the host grid
    void addGrid(GridType& grid, const std::string& name) {
      deformations_.push_back(std::make_pair(name, std::make_shared<DeformationFunction>(grid.leafGridView())));
      deformedGrids_.push_back(std::make_pair(name, std::make_shared<DeformedGridType>(grid, *deformations_.back().second)));
    }

    //! Add deformed grid, taking over ownership for r-values and shared_pointer
    template <class DefGrid, class DefFunction>
    void addDeformedGrid(DefGrid&& defGrid,
                         const std::string& name,
                         DefFunction&& def) {
      deformations_.push_back(std::make_pair(name, Dune::Solvers::wrap_own_share<DeformationFunction>(std::forward<DefFunction>(def))));
      deformedGrids_.push_back(std::make_pair(name, Dune::Solvers::wrap_own_share<DeformedGridType>(std::forward<DefGrid>(defGrid))));
    }

    //! Get a deformed grid by name
    DeformedGridType& grid(const std::string& name) {
      return find(deformedGrids_, name);
    }

    //! Get a deformed grid by name
    const DeformedGridType& grid(const std::string& name) const {
      auto it = std::find_if(deformedGrids_.begin(), deformedGrids_.end(),
                    [&](const auto& val){return val.first == name;});
      return *(it->second);
    }

    //! Get the deformation by name
    DeformationFunction& deformation(const std::string& name) {
      return find(deformations_, name);
    }

    //! Get the deformation by index
    DeformationFunction& deformation(size_t i) {
      return *(deformations_[i].second);
    }

    //! Deform a grid by name
    void setDeformation(const VectorType& def, const std::string name) {
      find(deformations_, name).setDeformation(def);
    }

    //! Deform a grid by index
    void setDeformation(const VectorType& def, size_t i) {
      deformations_[i].second->setDeformation(def);
    }

    //! Return vector containing const grid pointers
    const std::vector<const DeformedGridType*> gridVector() const {
      std::vector<const DeformedGridType*> grids;
      for (const auto& it : deformedGrids_)
        grids.push_back(it.second.get());
      return grids;
    }

    //! Return number of grids that are hold in the complex
    size_t size() const {return deformations_.size();}

    //! Return the index of a grid in the complex
    size_t idx(const std::string& name) const {
      int index(-1);
      auto it = std::find_if(deformations_.begin(), deformations_.end(),
                    [&](const auto& val){index++; return val.first == name;});
      if (it != deformations_.end())
        return index;
      else
        DUNE_THROW(Dune::RangeError, "No deformed grid with name " + name);
    }

    std::string name(size_t index) const {
      if (index < deformations_.size())
        return deformations_[index].first;
      else
        DUNE_THROW(Dune::RangeError, "No deformed grid with index " + std::to_string(index));
    }

private:

    template <class DeformedEntity>
    DeformedEntity& find(std::vector<std::pair<std::string, std::shared_ptr<DeformedEntity> > >& entityVector,
                         std::string name) {
      auto it = std::find_if(entityVector.begin(), entityVector.end(),
                    [&](const auto& val){return val.first == name;});
      return *(it->second);
    }

    //! The deformed grids
    std::vector<std::pair<std::string, std::shared_ptr<DeformedGridType> > > deformedGrids_;

    //! The deformation
    std::vector<std::pair<std::string, std::shared_ptr<DeformationFunction> > > deformations_;
};

} /* namespace Contact */
} /* namespace Dune */

#endif
