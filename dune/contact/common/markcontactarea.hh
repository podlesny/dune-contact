// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_COMMON_MARKCONTACTAREA_HH
#define DUNE_CONTACT_COMMON_MARKCONTACTAREA_HH

#include <dune/contact/projections/normalprojection.hh>

#include <dune/fufem/boundarypatch.hh>

namespace Dune {
namespace Contact {

class MarkContactArea {

public:

	template <class GridView>
        static void markPatchArea(typename GridView::Grid &grid, const BoundaryPatch<GridView> &boundaryPatch)
	{
		// mark corresponding grid elements
		for (const auto& pIt : boundaryPatch)
			grid.mark(1, pIt.inside());
		return;
	}


	template <class GridType>
	static void markPossibleContactArea(GridType &grid0, GridType &grid1, double threshold,
            std::shared_ptr<BoundaryPatch<typename GridType::LeafGridView> > boundaryPatch0 = nullptr,
            std::shared_ptr<BoundaryPatch<typename GridType::LeafGridView> > boundaryPatch1 = nullptr)
	{
        typedef BoundaryPatch<typename GridType::LeafGridView> LeafBoundaryPatch;
		const int dim = GridType::dimension;

		// if no contact patches have been committed use the whole grid boundary
        if (!boundaryPatch0)
            boundaryPatch0 = std::make_shared<BoundaryPatch<typename GridType::LeafGridView> >(grid0.leafGridView(), true);

        if (!boundaryPatch1)
            boundaryPatch1 = std::make_shared<BoundaryPatch<typename GridType::LeafGridView> >(grid1.leafGridView(), true);


		// get boundary segments for the grids
		std::vector<typename NormalProjection<LeafBoundaryPatch>::BoundarySegment> boundarySegments0, boundarySegments1;
        createBoundarySegments(*boundaryPatch1, boundarySegments1);
		// get normal vectors
        auto normals0 = boundaryPatch0->getNormals();
        const auto& indexSet0 = grid0.leafGridView().indexSet();
        // loop over boundary elements of grid0
		for (const auto& p0It : *boundaryPatch0) {

            const auto& inside = p0It.inside();
            const auto& geometry = inside.geometry();
			int nCorners = geometry.corners();

			for (int i=0; i<nCorners; i++) {
				int vertexIndex = indexSet0.subIndex(inside, i, dim);

                auto dist = NormalProjection<LeafBoundaryPatch>::computeNormalDistance(geometry.corner(i),
						normals0[vertexIndex], boundarySegments1);
				if (dist <= threshold)
					grid0.mark(1, inside);
			}
		}

        // get normal vectors
        auto normals1 = boundaryPatch1->getNormals();

        createBoundarySegments(*boundaryPatch0, boundarySegments0);
        const auto& indexSet1 = grid1.leafGridView().indexSet();
        // loop over boundary elements of grid1
		for (const auto& p1It : *boundaryPatch1) {

            const auto& inside = p1It.inside();
            const auto& geometry = inside.geometry();
			int nCorners = geometry.corners();

			for (int i=0; i<nCorners; i++) {
				int vertexIndex = indexSet1.template subIndex(inside, i, dim);

                auto dist = NormalProjection<LeafBoundaryPatch>::computeNormalDistance(geometry.corner(i),
						normals1[vertexIndex], boundarySegments0);
				if (dist <= threshold)
					grid1.mark(1, inside);
			}
		}
	}


    template <class Patch>
    static void createBoundarySegments(Patch& boundaryPatch,
            std::vector<typename NormalProjection<Patch>::BoundarySegment> &boundarySegments)
	{
		boundarySegments.resize(boundaryPatch.numFaces());

        Dune::FieldVector<typename Patch::GridView::ctype, Patch::GridView::dimension-1> zero(0);

        int j=0;
		for (const auto& nIt : boundaryPatch) {

            typename NormalProjection<Patch>::BoundarySegment newSegment;
            const auto& geometry = nIt.geometry();

			newSegment.nVertices = geometry.corners();

            for (int i=0; i<newSegment.nVertices; i++)
                newSegment.vertices[i] = geometry.corner(i);

            // use outer normal at 0
            newSegment.unitOuterNormal = nIt.unitOuterNormal(zero);

            boundarySegments[j++] = newSegment;
        }
    }
};

} /* namespace Contact */
} /* namespace Dune */

#endif /*DUNE_CONTACT_COMMON_MARKCONTACTAREA_HH_*/
