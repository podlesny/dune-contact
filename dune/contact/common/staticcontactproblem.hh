// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_COMMON_STATIC_CONTACT_PROBLEM_HH
#define DUNE_CONTACT_COMMON_STATIC_CONTACT_PROBLEM_HH

#include<dune/istl/bdmatrix.hh>

#include <dune/fufem/functions/basisgridfunction.hh>

#include <dune/contact/common/gapfunction.hh>
#include <dune/contact/assemblers/nbodyassembler.hh>

namespace Dune {
namespace Contact {

template <class VectorTypeT, class MatrixTypeT, class MaterialType>
class StaticContactProblem
{

public:
    using GridType = typename MaterialType::GridType;
    using VectorType = VectorTypeT;
    using MatrixType = MatrixTypeT;
    using field_type = typename VectorType::field_type;
    using DiagonalMatrixType = Dune::BDMatrix<typename MatrixType::block_type>;
    using ContactConstraint = GapFunction<GridType, field_type>;
    using ConstraintType = typename ContactConstraint::RangeType;
    using JacobianType = typename ContactConstraint::JacobianType;
    using DeformedGridType = typename ContactConstraint::DeformedGridType;
    using ContactAssembler = NBodyAssembler<DeformedGridType,VectorType>;
    using Basis = typename MaterialType::GlobalBasis;
    using BasisGridFunction = ::BasisGridFunction<Basis,VectorType>;
    const static int dim = VectorType::block_type::dimension;

    StaticContactProblem(const ContactAssembler& contactAssembler,
                   std::vector<MaterialType>& materials,
                   const std::vector<VectorType>& extForces,
                   std::vector<ContactConstraint>& contactConstraints) :
        contactAssembler_(Dune::stackobject_to_shared_ptr<const ContactAssembler>(contactAssembler)),
        materials_(Dune::stackobject_to_shared_ptr<std::vector<MaterialType> >(materials)),
        extForces_(Dune::stackobject_to_shared_ptr<const std::vector<VectorType> >(extForces)),
        contactConstraints_(Dune::stackobject_to_shared_ptr<std::vector<ContactConstraint> >(contactConstraints))
    {
        stiffMat_.resize(extForces.size());
        rhs_.resize(extForces.size());
        constraints_.resize(contactConstraints.size());
        constraintJacobians_.resize(contactConstraints.size());
    }

    //! Assemble the quadratic problem
    virtual void assembleQP(const std::vector<VectorType>& iterates);

    //! Assemble the preconditioned defect problem
    virtual void assembleDefectQP(const std::vector<VectorType>& iterates,
                          std::vector<VectorType>& residual,
                          std::vector<DiagonalMatrixType>& hessians);

    //! Compute the energy of the quadratic minimization problem
    virtual field_type energy(const std::vector<VectorType>& iterate) const;

    /** \brief Compute the infeasibility of the current iterate
     *
     *  \param iterate The current iterates of the grids
     *  \param glue Flag which determines if the contact projection should be computed.
     */
    field_type infeasibility(const std::vector<VectorType>& iterates);

    /** \brief Setup the linearised mortar non-penetration constraints
     *
     *  \param iterates The configuration where the linearisation is evaluated
     *  \param glue Flag which determines if the contact projection should be recomputed.
     */
    void assembleConstraints(const std::vector<VectorType>& iterates, bool glue);

     //! Set external forces
    void setExternalForces(const std::vector<VectorType>& extForces)
    {
        extForces_ = Dune::stackobject_to_shared_ptr<const std::vector<VectorType> >(extForces);
    }

    //! Return the contact assembler
    std::shared_ptr<const ContactAssembler> contactAssembler() const {return contactAssembler_; }

    //! Return the quadratic term of the linearised energy
    const MatrixType& A() const {return A_;}

    //! Return the linear term of linearised energy
    const VectorType& f() const {return f_;}

    //! Return the constraint linearisations
    const JacobianType& constraintJacobian() const {return totalConstrJacobian_; }

    //! Return the nonlinear constraints
    const std::vector<BoxConstraint<field_type,1> >& constraints() const {return totalConstraints_;}

protected:
    //! The quadratic part of the linearised energy
    MatrixType A_;

    //! The linear part of the linearised energy
    VectorType f_;

    //! Matrix containing all constraint jacobians
    JacobianType totalConstrJacobian_;

    //! Vector containing all nonlinear constraints in a box-constraint-vector
    std::vector<BoxConstraint<field_type,1> > totalConstraints_;

    //! Iteration matrices, we need to store these for the error computation
    std::vector<const MatrixType*> stiffMat_;

    //! The  linear parts of the nonlinear energy for every involved grid
    std::vector<VectorType> rhs_;

    //! The contact assembler, it is only used to store the contact couplings
    std::shared_ptr<const ContactAssembler> contactAssembler_;

    //! The involved materials
    std::shared_ptr<std::vector<MaterialType> > materials_;

    //! The external forces
    std::shared_ptr<const std::vector<VectorType> > extForces_;

    //! The mortar contact constraint functions for each coupling
    std::shared_ptr<std::vector<ContactConstraint> > contactConstraints_;

    //! The mortar discretised nonlinear constraints
    std::vector<ConstraintType> constraints_;

    //! The exact constraint linearisations
    std::vector<JacobianType> constraintJacobians_;

};

} /* namespace Contact */
} /* namespace Dune */

#include "staticcontactproblem.cc"

#endif
