#ifndef POSTPROCESS_RESIDUAL_HH
#define POSTPROCESS_RESIDUAL_HH

#include <dune/istl/solvers.hh>

namespace Dune {
namespace Contact {

template <int dim>
void postprocessResidual(const Dune::BlockVector<Dune::FieldVector<double,dim> >& residual,
                         Dune::BlockVector<Dune::FieldVector<double,dim> >& residualStandard,
                         const Dune::BCRSMatrix<Dune::FieldMatrix<double,dim,dim> >& BT)
{
    using namespace Dune;

    typedef BlockVector<FieldVector<double,dim> > VectorType;
    typedef BCRSMatrix<FieldMatrix<double,dim,dim> > MatrixType;
    MatrixIndexSet BIdx(BT.N(), BT.M());

    for (int i=0; i<BT.N(); i++) {

        typename BCRSMatrix<FieldMatrix<double,dim,dim> >::row_type::ConstIterator cIt = BT[i].begin();
        typename BCRSMatrix<FieldMatrix<double,dim,dim> >::row_type::ConstIterator cEndIt = BT[i].end();

        for (; cIt!=cEndIt; ++cIt)
            BIdx.add(cIt.index(), i);

    }

    BCRSMatrix<FieldMatrix<double,dim,dim> > B;
    BIdx.exportIdx(B);

    for (int i=0; i<BT.N(); i++) {

        typename BCRSMatrix<FieldMatrix<double,dim,dim> >::row_type::ConstIterator cIt = BT[i].begin();
        typename BCRSMatrix<FieldMatrix<double,dim,dim> >::row_type::ConstIterator cEndIt = BT[i].end();

        for (; cIt!=cEndIt; ++cIt)
            B[cIt.index()][i] = *cIt;

    }


    // ////////////////////////////////////////////////////////////
    //   B * dsol = dsol_standard
    //   Solve this with a BiCGStab
    // ////////////////////////////////////////////////////////////
    // Make small cg solver
    MatrixAdapter<MatrixType,VectorType,VectorType> op(B);

    // A preconditioner
    SeqILU0<MatrixType,VectorType,VectorType> ilu0(B,1.0);

    // A cg solver for nonsymmetric matrices
    BiCGSTABSolver<VectorType> bicgstab(op,ilu0,1E-6,100,2);

    // Object storing some statistics about the solving process
    InverseOperatorResult statistics;

   // Solve!
    residualStandard.resize(residual.size());
    residualStandard = 0;

    // The solver leaves the residual in the right hand side, but the right
    // hand side is const, so we make a local copy
    VectorType residualCopy = residual;
    bicgstab.apply(residualStandard, residualCopy, statistics);

    //std::cout << "rhs standard\n" << rhs_standard << std::endl;
}

} /* namespace Contact */
} /* namespace Dune */

#endif
