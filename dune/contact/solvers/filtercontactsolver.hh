// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:
#ifndef DUNE_CONTACT_SOLVERS_FILTER_CONTACT_SOLVER_HH
#define DUNE_CONTACT_SOLVERS_FILTER_CONTACT_SOLVER_HH

#include <dune/common/parametertree.hh>
#include <dune/common/bitsetvector.hh>

#include <dune/contact/common/filter.hh>

#include <dune/solvers/solvers/iterativesolver.hh>
#include <dune/solvers/norms/norm.hh>

namespace Dune {
namespace Contact {

/** \brief Filter Trust-Region SQP method with the infinity-norm as trust region norm.
 *
 *  The filter method ensures global convergence, by using the trust--region mechanism
 *  to achieve a sufficient reduction and the 'acceptable to the filter' criterion
 *  to enforce asymptotic convergence towards the feasible set. See
 *
 *  Fletcher, Gould, Toint (2002)
 *  "Global convergence of a trust-region SQP-filter algorithm for general nonlinear programming"
 *
 *  for a detailed description.
*/
template<class ProblemType>
class FilterContactSolver : public Dune::Solvers::IterativeSolver<typename ProblemType::VectorType>
{

  using Vector = typename ProblemType::VectorType;
  using Matrix = typename ProblemType::MatrixType;
  using field_type = typename Dune::FieldTraits<Vector>::field_type;
  using JacobianType = typename ProblemType::JacobianType;

  static const int dim = Vector::block_type::dimension;
  using Base = Dune::Solvers::IterativeSolver<Vector>;

public:

  FilterContactSolver(const Dune::ParameterTree& config,
                      std::vector<Vector>& x,
                      ProblemType& problem,
                      const std::vector<std::shared_ptr<Norm<Vector> > >& errorNorms) :
    Base(config.get<field_type>("tolerance"),config.get<int>("maxIterations"),
         config.get("verbosity",NumProc::FULL))
  {
    setupParameter(config);
    setProblem(x, problem);
    setErrorNorm(errorNorms);

    size_t totalSize(0);
    for (size_t i=0; i<x.size(); i++)
      totalSize += x[i].size();

    dirichletNodes_.resize(totalSize,false);
  }

  //! Setup the trust region parameter from a config file
  void setupParameter(const Dune::ParameterTree& config) {
    filter_ = Filter<field_type>(config.get("filterTolerance",1e-4));
    initTR_ = config.get<field_type>("trustRegionInitRadius");
    maxTR_ = config.get("trustRegionMaxRadius",10*initTR_);
    localTol_ = config.get<field_type>("localTolerance");
    localMaxIterations_ = config.get<int>("localMaxIterations");
    linearSolverType_ = config.get("linearSolverType","");
  }

  /** \brief Set the Dirichlet values.
     *
     * The Dirichlet conditions are enforced during the solution
     * of the first linearised problem rather than modifying the
     * initial iterate of the filter scheme. This avoids possible problems
     *in the energy evaluation due to inverted elements.
     * Hence Dirichlet values for the correction must be handed over!
     */
  void setDirichletValues(const std::vector<Vector>& dirichletValues,
                          const std::vector<Dune::BitSetVector<dim> >& dirichletNodes)
  {
    // only adjust correction Dirichlet conditions are prescribed
    correction_.resize(dirichletNodes_.size());
    int counter(0);
    for (size_t i=0; i<dirichletNodes.size(); i++)
      for (size_t j=0; j<dirichletNodes[i].size(); counter++,j++)
        if (dirichletNodes[i][j].any()) {
          dirichletNodes_[counter] = dirichletNodes[i][j];
          correction_[counter] = dirichletValues[i][j];
        }
  }

  //! Set initial iterate and the problem class
  void setProblem(std::vector<Vector>& x, ProblemType& problem)
  {
    iterates_ = Dune::stackobject_to_shared_ptr<std::vector<Vector> >(x);
    problem_ = Dune::stackobject_to_shared_ptr<ProblemType>(problem);
  }

  //! Set the error norms to be used within the filter method
  void setErrorNorm(const std::vector<std::shared_ptr<Norm<Vector> > >& errorNorms) {
    errorNorms_ = errorNorms;
  }

  //! Solve the problem
  void solve();

  //! Return solution object
  std::vector<Vector> getSol() {return *iterates_;}

private:
  //! The iterates of the global scheme
  std::shared_ptr<std::vector<Vector> > iterates_;

  //! Dirichlet degrees of freedom
  Dune::BitSetVector<dim> dirichletNodes_;

  //! The iterate for the local problems, containing possible Dirichlet values
  Vector correction_;

  //! The problem type, providing methods to assemble the nonlinear and linearised
  //! energy and constraints
  std::shared_ptr<ProblemType> problem_;

  //! The norms to estimate the errors
  std::vector<std::shared_ptr<Norm<Vector> > > errorNorms_;

  //! The filter set
  Filter<field_type> filter_;

  //! Initial trust region radius
  field_type initTR_;
  //! Maximal trust region radius
  field_type maxTR_;
  //! Tolerance for the solution of the local problem
  field_type localTol_;
  //! Max iterations for the solution of the local problem
  int localMaxIterations_;

  //! Store the old energy
  field_type energy_;
  //! Store the infeasibility of the old iterate
  field_type infeasibility_;

  //! The linear solver to be used within IpOpt
  std::string linearSolverType_;
};

} /* namespace Contact */
} /* namespace Dune */

#include "filtercontactsolver.cc"

#endif
