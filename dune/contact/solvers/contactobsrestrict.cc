// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

namespace Dune {
namespace Contact {

/**
 * \param f Constraints of the fine grid level
 * \param t Vector to write the restricted coarse obstacles to
 * \param fHasObstacle Bitfield declaring which fine grid nodes have an obstacles
 * \param tHasObstacle Bitfield declaring which coarse grid nodes have an obstacles
 * \param transfer Standard multigrid transfer operator
 * \param critical Bitfield determining which fine grid constraints are critical
 */
template <class VectorType>
void ContactObsRestriction<VectorType>::
restrict(const std::vector<BoxConstraint<field_type,dim> >& f,
         std::vector<BoxConstraint<field_type,dim> >& t,
         const BitSetVector<dim>& fHasObstacle,
         const BitSetVector<dim>& tHasObstacle,
         const MultigridTransfer<VectorType>& transfer,
         const BitSetVector<dim>& critical)
{
    if (critical.size() != fHasObstacle.size())
        DUNE_THROW(Exception, "The critical bitfield doesn't have the correct length!");

    // Get matrix of the transfer operator
    const auto& transferMatrix = dynamic_cast<const DenseMultigridTransfer<VectorType>*>(&transfer)->getMatrix();

    if (t.size()!= transferMatrix.M())
        t.resize(transferMatrix.M());

    // ///////////////////////////////////////////////
    //    Clear coarse obstacles before restriction
    // ///////////////////////////////////////////////
    for (size_t i=0; i<t.size(); i++)
        t[i].clear();

    //std::cout << "coarse obstacles before\n" << t << std::endl;

    //std::cout << "Critical nodes before\n" << critical << std::endl;

    for (size_t fVec=0; fVec<transferMatrix.N(); fVec++) {

        if (fHasObstacle[fVec].none())
            continue;

        const auto& row = transferMatrix[fVec];

        // Make a list of all coarse grid vectors vec couples with
        int noOfCvec = 0;
        auto imatEnd = row.end();
        for (auto imat = row.begin(); imat != imatEnd; ++imat)
            if (tHasObstacle[imat.index()][0] || imat->infinity_norm() < 1e-8)
                noOfCvec++;

        // /////////////////////////////////////////////////////////////////
        //   If vec couples with coarse grid vectors merge its obstacle
        // /////////////////////////////////////////////////////////////////

        const BoxConstraint<field_type, dim>& fObsVal = f[fVec];

        // Loop over all couplings
        for (auto imat = row.begin(); imat != imatEnd; ++imat) {

            if (tHasObstacle[imat.index()].none() || imat->infinity_norm() < 1e-8)
                continue;

            BoxConstraint<field_type, dim>& cObsVal = t[imat.index()];

            for(int fDir=0; fDir < dim; fDir++) {

                // If the fine grid vector is critical and we're doing
                // truncated multigrid the obstacle doesn't get restricted
                if (critical[fVec][fDir])
                    continue;

                /* Count number of nonzero entries in imat ! */
                int sum_nzero = 0;
                for(int cDir=0; cDir<dim; cDir++)
                    if((*imat)[fDir][cDir] != 0)
                        sum_nzero++;

                for(int cDir=0; cDir<dim; cDir++) {

                    // compute e^i(p) * e^j(q)
                    field_type ep_eq = (*imat)[fDir][cDir] * noOfCvec;

                    /** \todo Define this constant properly */
                    const double my_DBL_EPSILON = 1e-14;
                    if(fabs(ep_eq) <= my_DBL_EPSILON)
                        continue;

                    field_type weight = ep_eq * sum_nzero;

                    if(weight > 0.0) {

                        cObsVal.upper(cDir) = std::min( fObsVal.upper(fDir) / weight, cObsVal.upper(cDir));
                        cObsVal.lower(cDir) = std::max( fObsVal.lower(fDir) / weight, cObsVal.lower(cDir));

                    } else if (weight < 0.0) {

                        cObsVal.upper(cDir) = std::min( fObsVal.lower(fDir) / weight, cObsVal.upper(cDir));
                        cObsVal.lower(cDir) = std::max( fObsVal.upper(fDir) / weight, cObsVal.lower(cDir));

                    }

                }

            }

        }

    }


}

} /* namespace Contact */
} /* namespace Dune */
