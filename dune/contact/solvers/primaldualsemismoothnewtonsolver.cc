// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:

#include <limits>
#include <dune/common/timer.hh>
#include <dune/istl/io.hh>

#include <dune/solvers/solvers/quadraticipopt.hh>
#include <dune/solvers/common/boxconstraint.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/umfpack.hh>
#include <dune/istl/solvers.hh>

namespace Dune {
namespace Contact {

template<class ProblemType>
void PrimalDualSemiSmoothNewtonSolver<ProblemType>::solve()
{
  auto& iterates = *iterates_;

  size_t totalSize(0);
  for (size_t i=0; i<iterates.size(); i++)
    totalSize += iterates[i].size();

  // If no Dirichlet conditions are prescribed we have to setup the initial correction
  if (correction_.size() != dim*totalSize) {
    correction_.resize(dim*totalSize);
    correction_ = 0;
  }

  problem_->contactAssembler()->getContactCouplings()[0]->path_ = debugPath_;
  problem_->contactAssembler()->getContactCouplings()[0]->it_ = -1;

  // compute energy infeasibility of initial iterate and build the contact mapping
  energy_ = problem_->energy(iterates);
  infeasibility_ = problem_->infeasibility(iterates);

  // if there are non-homogeneous Dirichlet conditions,
  // then we have to adjust the initial infeasibility
  infeasibility_ = std::max(infeasibility_, correction_.infinity_norm());

  if (this->verbosity_ == Solver::REDUCED or this->verbosity_== Solver::FULL)
    std::cout<< " Initial energy: " << energy_
             << " and infeasibility: " << infeasibility_ << std::endl;

  // initialise the iteration counter and error
  int it(0);
  field_type error = std::numeric_limits<field_type>::max();

  // assemble linearisations of initial deformation
  problem_->assembleLinearisations(iterates);
  // initialise Lagrange multiplier with zero
  lagrangeMultiplier_.resize(problem_->getConstraints().size());
 // lagrangeMultiplier_ = -1000;
  lagrangeMultiplier_ = problem_->leastSquaresLagrangeMultiplier();

  problem_->initialiseEmptyActiveSet();

  // create IpOpt solver
  QuadraticIPOptSolver<ScalarMatrix, ScalarVector> ipoptSolver(localTol_, localMaxIterations_,
                                                            Solver::QUIET, linearSolverType_);

  bool activeSetChanged(true);
  // Loop until desired tolerance or maximum number of iterations is reached
  for (; activeSetChanged or (it<this->maxIterations_ and (error>this->tolerance_ or std::isnan(error))); it++)
  {

    if (this->verbosity_ == Solver::REDUCED or this->verbosity_== Solver::FULL)
      std::cout << "************************\n"
                << "*  PriDual iteration " << it << "\n"
                << "************************\n";

    problem_->contactAssembler()->getContactCouplings()[0]->path_ = debugPath_;
    problem_->contactAssembler()->getContactCouplings()[0]->it_ = it;

    // Measure norm of the old iterate
    field_type oldNorm(0);
    for (size_t i=0; i<iterates_->size(); i++)
      oldNorm += (*errorNorms_[i])(iterates[i]);

    // assemble the local quadratic problem
    problem_->assemblePrimalDualProblem(lagrangeMultiplier_);

    ScalarVector priDualIterate = combineVector(correction_, ScalarVector(lagrangeMultiplier_.size(),0));

    field_type newEnergy;
    field_type newInfeasibility;
    std::vector<BlockVector> newIterates;

    // setup the linearised problem
    problem_->addHardDirichlet(correction_, dirichletNodes_);
    ipoptSolver.setProblem(problem_->priDualMatrix(), priDualIterate, problem_->priDualRhs());

    Timer timer;

#if HAVE_SUITESPARSE_UMFPACK
    UMFPack<ScalarMatrix> umfPack(problem_->priDualMatrix(), 0);
    std::cout<<"took "<<timer.elapsed()<<" to decompose matrix\n";

    // solve
    InverseOperatorResult res;
    auto copyRhs = problem_->priDualRhs();
    umfPack.apply(priDualIterate, copyRhs, res);
    if (!res.converged)
      std::cout<<"Warning: Not converged! Reduction "<<res.reduction<<" iterations "<<res.iterations<<std::endl;
#endif

    auto residual = problem_->priDualRhs();
    problem_->priDualMatrix().mmv(priDualIterate, residual);
    std::cout<<"Residual " << residual.infinity_norm()<<std::endl;



    if (this->verbosity_ == Solver::FULL)
      std::cout << "Solved local problem in " << timer.stop() << " secs\n";

    std::tie(correction_, lagrangeMultiplier_) = decomposeVector(priDualIterate);

    // new iterate
    int counter(0);
    newIterates = iterates;
    for (size_t i=0; i<newIterates.size(); i++)
      for (size_t j=0; j<newIterates[i].size(); j++)
        for (int k = 0; k < dim; k++)
          newIterates[i][j][k] += correction_[counter++][0];

    std::cout<<"iterates inf "<<newIterates[0].infinity_norm() << " and " << newIterates[1].infinity_norm()<<std::endl;
    // compute new energy and infeasibility
    newEnergy = problem_->energy(newIterates);
    newInfeasibility = problem_->infeasibility(newIterates);

    // update the active set, needs to be called after the deformation is updated
    // which is done in "infeasibility"
    problem_->updateActiveSetAndLagrangeMultiplier(lagrangeMultiplier_);
    activeSetChanged = (problem_->changedActiveNodes() > 0);

    if (this->verbosity_ == Solver::FULL) {
      std::cout << "Active nodes changed " << problem_->changedActiveNodes()
                << " active now " << problem_->getActiveSet().count() << " \n";
      std::cout << "New energy of potential iterate " << newEnergy
                << " and infeasibility " << newInfeasibility << std::endl;
    }

    // update the Newton problem
    problem_->assembleLinearisations(newIterates);

    // if the correction is very small stop the loop
    field_type infNorm = correction_.infinity_norm();
    if(this->verbosity_==Solver::FULL)
      std::cout << "Correction infinite norm: " << infNorm << std::endl;

    // compute error estimate
    std::vector<BlockVector> corrections(iterates.size());
    counter = 0;
    for (size_t i=0; i<iterates.size();i++) {
      corrections[i].resize(iterates[i].size());
      for (size_t j=0; j<corrections[i].size(); j++)
        for (int k = 0; k < dim; k++)
          corrections[i][j][k] = correction_[counter++][0];
    }

    auto residuals = problem_->residuals(lagrangeMultiplier_);
    for (int k=0; k<2; k++)
      for (size_t j=0; j<residuals[k].size();j++)
        if (dN_[k][j].any())
          residuals[k][j] = 0;

    field_type absError = 0;
    for (size_t i=0; i < corrections.size(); ++i)
      absError += (*errorNorms_[i])(residuals[i]);
    absError += newInfeasibility;

    error = absError;
    if (this->verbosity_ == Solver::REDUCED or this->verbosity_== Solver::FULL)
      std::cout << "In filter iteration "<< it
                << ", absolut norm of correction : " << absError
                << " relative error " << error
                << "\nEnergy " << newEnergy
                << " and infeasibility " << newInfeasibility << std::endl;

    // update iterates, energy and infeasibility
    iterates = newIterates;
    energy_ = newEnergy;
    infeasibility_ = newInfeasibility;

    // set correction to zero to remove any Dirichlet values
    correction_  = 0;

  } // end of filter loop

  if (this->verbosity_ == Solver::REDUCED or this->verbosity_== Solver::FULL)
    std::cout << "Solved problem in " << it << " iterations with final energy " << energy_
              << " and infeasibility " << infeasibility_ << std::endl;
}

} /* namespace Contact */
} /* namespace Dune */
