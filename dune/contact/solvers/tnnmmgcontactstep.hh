// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_CONTACT_SOLVERS_TNNMMG_CONTACT_STEP_HH
#define DUNE_CONTACT_SOLVERS_TNNMMG_CONTACT_STEP_HH

#include <dune/solvers/common/wrapownshare.hh>
#include <dune/solvers/transferoperators/truncatedmgtransfer.hh>
#include <dune/solvers/iterationsteps/mmgstep.hh>
#include <dune/solvers/iterationsteps/trustregiongsstep.hh>
#include <dune/solvers/transferoperators/mandelobsrestrictor.hh>

namespace Dune {
namespace Contact {

/** \brief Truncated Non-smooth Newton Monotone Multigrid Step
 *
 *  This method is an extension of TNNMG to large deformation contact problems
 *  that are solved using a trust-region method.
 *
 *  The idea is to perform a coarse correction by dropping the contact constraints
 *  and only keep the trust-region constraints that help to handle positive indefiniteness
 *  This way the defect problem can be solved using a classical monotone multigrid method
 *  and no further modification on the coarse levels, like the monotone multigid method of
 *  Krause and Wohlmuth requires, is needed.
 *
*/
template<class MatrixType, class VectorType>
class TnnmmgContactStep : public MonotoneMGStep<MatrixType, VectorType>
{
  static const int blockSize = VectorType::block_type::dimension;

  using Base = MonotoneMGStep<MatrixType, VectorType>;
  using field_type = typename VectorType::field_type;
  using Transfer = TruncatedMGTransfer<VectorType>;

public:

  using Base::Base;
  using Base::setProblem;

  TnnmmgContactStep() :
    trScaling_(nullptr)
  {
    this->setSmoother(std::make_shared<TrustRegionGSStep<MatrixType, VectorType> >());
    this->setObstacleRestrictor(MandelObstacleRestrictor<VectorType>());
    this->setHasObstacles(BitSetVector<blockSize>());
  }

  //! Perform one iteration the method
  virtual void iterate();

  //! Set the current trust-region radius
  void setTrustRegion(field_type trustRegion) { trustRegion_ = trustRegion;}

  //! Preprocess
  void preprocess() {

    // create bitfield that is true for all trust-region obstacles, i.e. all except Dirichlet dofs
    this->hasObstacle_->resize(this->x_->size());
    this->hasObstacle_->setAll();
    for (size_t j=0; j < this->hasObstacle_->size(); j++)
      for(int k=0; k < blockSize; k++)
        if (this->ignore()[j][k])
          (*this->hasObstacle_)[j][k] = false;
    // Unset the recompute bitfields, so we compute the full stiffness matrix hierarchy at the beginning

    Base::preprocess();
  }

  //! Set the scaling for iteration dependent trust-region inf-norm
  template <class Vector>
  void setTrScaling(Vector&& scaling) {
    trScaling_ = Dune::Solvers::wrap_own_share<const VectorType>(std::forward<Vector>(scaling));
  }

  //! Set the trust-region obstacles to a specific value
  void setupTrustRegionObstacles(field_type trustRegion) {
    trustRegion_ = trustRegion;

    auto& obs = *this->obstacles_;
    obs.resize(this->ignore().size());
    for (size_t i=0; i < obs.size(); i++)
      for (int j=0; j < blockSize; j++) {
        field_type val = (trScaling_) ? trustRegion*trScaling_[i][j] : trustRegion;
        obs[i][j] = {-val, val};
      }
  }

private:
  //! The current trust-region radius
  field_type trustRegion_;

  //! Scaling for the trust-region norm
  std::shared_ptr<const VectorType> trScaling_;
};

} /* namespace Contact */
} /* namespace Dune */

#include "tnnmmgcontactstep.cc"

#endif
