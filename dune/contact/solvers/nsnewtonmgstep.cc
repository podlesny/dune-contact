#include <memory>
#include <typeinfo>

#include <dune/solvers/transferoperators/truncateddensemgtransfer.hh>
#include <dune/solvers/iterationsteps/projectedblockgsstep.hh>
#include <dune/solvers/computeenergy.hh>

#ifdef HAVE_IPOPT
#include <dune/solvers/solvers/quadraticipopt.hh>
#endif

namespace Dune {
namespace Contact {

template <class MatrixType, class VectorType>
void NonSmoothNewtonMGStep<MatrixType, VectorType>::preprocess()
{
    // Remove any recompute filed so that initially the full transferoperator is assembled
    for (size_t i=0; i<this->mgTransfer_.size(); i++)
        std::dynamic_pointer_cast<TruncatedMGTransfer<VectorType> >(this->mgTransfer_[i])->setRecomputeBitField(nullptr);

    // Call preprocess of the base class
    MultigridStep<MatrixType,VectorType>::preprocess();

    // Then specify the subset of entries to be reassembled at each iteration
    recompute_.resize(this->numLevels());
    recompute_[recompute_.size()-1] = *hasObstacle_;
    for (size_t i=this->mgTransfer_.size(); i>=1; i--)
        this->mgTransfer_[i-1]->restrict(recompute_[i], recompute_[i-1]);

    for (size_t i=0; i<this->mgTransfer_.size(); i++)
        std::dynamic_pointer_cast<TruncatedMGTransfer<VectorType> >(this->mgTransfer_[i])->setRecomputeBitField(&recompute_[i]);

    // /////////////////////////////////////////////
    //   Set up base solver
    // /////////////////////////////////////////////

    typedef ::LoopSolver<VectorType> MyLoopSolver;

    if (typeid(*this->basesolver_) == typeid(MyLoopSolver)) {

        ::LoopSolver<VectorType>* loopBaseSolver

            = dynamic_cast<MyLoopSolver*> (this->basesolver_.get());

        typedef ProjectedBlockGSStep<MatrixType, VectorType> SmootherType;

        if (this->numLevels() == 1) {
            // only a single level: base grid solver is fine grid solver
            dynamic_cast<SmootherType*>(&loopBaseSolver->getIterationStep())->hasObstacle_ = hasObstacle_.get();
            dynamic_cast<SmootherType*>(&loopBaseSolver->getIterationStep())->obstacles_   = obstacles_.get();
        }

#ifdef HAVE_IPOPT
    } else if (typeid(*this->basesolver_) == typeid(QuadraticIPOptSolver<MatrixType,VectorType>)) {

        auto* ipoptBaseSolver = dynamic_cast<QuadraticIPOptSolver<MatrixType,VectorType>*> (this->basesolver_.get());

        if (this->numLevels() == 1)
            ipoptBaseSolver->setObstacles(*obstacles_);
#endif
    } else {
        DUNE_THROW(SolverError, "You can't use " << typeid(*this->basesolver_).name()
                   << " as a base solver for contact problems!");
    }

}


template <class MatrixType, class VectorType>
void NonSmoothNewtonMGStep<MatrixType, VectorType>::iterate()
{

    int& level = this->level_;

    // Define references just for ease of notation
    std::vector<std::shared_ptr<const MatrixType> >& mat = this->matrixHierarchy_;
    std::vector<std::shared_ptr<VectorType> >& x   = this->xHierarchy_;
    std::vector<VectorType>& rhs = this->rhsHierarchy_;

    BitSetVector<dim> critical(x[level]->size(), false);

    // Solve directly if we're looking at the coarse problem
    if (level == 0) {

        this->basesolver_->solve();

    } else {

        // Presmoothing

        ProjectedBlockGSStep<MatrixType,VectorType>* presmoother = dynamic_cast<ProjectedBlockGSStep<MatrixType, VectorType>*>(this->presmoother_[level].get());
        assert(presmoother);

        presmoother->setProblem(*(mat[level]), *x[level], rhs[level]);

        /** \todo Hack: we should have ProjectedBlockGSStep only on the finest level! */
        BitSetVector<dim> dummyHasObstacle;
        std::vector<BoxConstraint<field_type,dim> > dummyObstacles;

        if ((size_t) level == this->numLevels()-1) {
            presmoother->obstacles_ = obstacles_.get();
            presmoother->hasObstacle_ = hasObstacle_.get();
        } else {
            dummyHasObstacle.resize(x[level]->size());
            dummyHasObstacle.unsetAll();
            dummyObstacles.resize(x[level]->size());
            for (size_t i=0; i<dummyObstacles.size(); i++)
                dummyObstacles[i].clear();

            presmoother->obstacles_ = &dummyObstacles;
            presmoother->hasObstacle_ = &dummyHasObstacle;
        }

        presmoother->ignoreNodes_ = this->ignoreNodesHierarchy_[level];

        for (int i=0; i<this->nu1_; i++)
            presmoother->iterate();

        // ///////////////////////
        //    Truncation
        // ///////////////////////

        // Determine critical nodes
        if ((size_t) level == this->numLevels()-1) {
            const double eps = 1e-12;
            for (size_t i=0; i<obstacles_->size(); i++) {

                for (int j=0; j<dim; j++) {

                    if ((*obstacles_)[i].lower(j) >= -eps + (*x[level])[i][j]
                        || (*obstacles_)[i].upper(j) <= eps + (*x[level])[i][j]) {
                        critical[i][j] = true;
                    }

                }

            }

        }

        auto mgTransfer = std::dynamic_pointer_cast<TruncatedMGTransfer<VectorType> >(this->mgTransfer_[level-1]);
        mgTransfer->setCriticalBitField(&critical);

        // Restrict stiffness matrix
        mgTransfer->galerkinRestrict(*(mat[level]), *(const_cast<MatrixType*>(mat[level-1].get())));

        // //////////////////////////////////////////////////////////////////////
        // Restriction:  fineResidual = rhs[level] - mat[level] * x[level];
        // //////////////////////////////////////////////////////////////////////
        VectorType fineResidual = rhs[level];
        mat[level]->mmv(*x[level], fineResidual);

        // restrict residual
        mgTransfer->restrict(fineResidual, rhs[level-1]);

        // Choose all zeros as the initial correction
        *x[level-1] = 0;

        // ///////////////////////////////////////
        // Recursively solve the coarser system
        // ///////////////////////////////////////
        level--;
        for (int i=0; i<this->mu_; i++)
            iterate();
        level++;

        // ////////////////////////////////////////
        // Prolong

        // add correction to the presmoothed solution
        VectorType v;
        mgTransfer->prolong(*x[level-1], v);

        if ((size_t) level != this->numLevels()-1) {

            *x[level] += v;

        } else {

            // //////////////////////////////////////////////////////////
            //   Line search in the direction of the projected coarse
            //   grid correction to ensure monotonicity.
            // //////////////////////////////////////////////////////////

            // L2-projection of the correction onto the defect obstacle
            for (size_t i=0; i<v.size(); i++) {

                if (!(*hasObstacle_)[i][0])
                    continue;

                for (int j=0; j<dim; j++) {

                    v[i][j] = std::max(v[i][j], (*obstacles_)[i].lower(j) - (*x[level])[i][j]);
                    v[i][j] = std::min(v[i][j], (*obstacles_)[i].upper(j) - (*x[level])[i][j]);

                }

            }

            using std::isnan;
            // Construct obstacles in the direction of the projected correction
            BoxConstraint<field_type,1> lineSearchObs;
            for (size_t i=0; i<v.size(); i++) {

                if (!(*hasObstacle_)[i][0])
                    continue;

                for (int j=0; j<dim; j++) {

                    if (v[i][j] > 0) {

                        // This division can cause nan on some platforms...
                        if (!isnan( ((*obstacles_)[i].lower(j)-(*x[level])[i][j]) / v[i][j]) )
                            lineSearchObs.lower(0) = std::max(lineSearchObs.lower(0),
                                                              ((*obstacles_)[i].lower(j)-(*x[level])[i][j]) / v[i][j]);

                        if (!isnan( ((*obstacles_)[i].upper(j)-(*x[level])[i][j]) / v[i][j]) )
                            lineSearchObs.upper(0) = std::min(lineSearchObs.upper(0),
                                                              ((*obstacles_)[i].upper(j)-(*x[level])[i][j]) / v[i][j]);
                    }

                    if (v[i][j] < 0) {
                        if (!isnan( ((*obstacles_)[i].upper(j)-(*x[level])[i][j]) / v[i][j]) )
                            lineSearchObs.lower(0) = std::max(lineSearchObs.lower(0),
                                                              ((*obstacles_)[i].upper(j)-(*x[level])[i][j]) / v[i][j]);
                        if (!isnan( ((*obstacles_)[i].lower(j)-(*x[level])[i][j]) / v[i][j]) )
                            lineSearchObs.upper(0) = std::min(lineSearchObs.upper(0),
                                                              ((*obstacles_)[i].lower(j)-(*x[level])[i][j]) / v[i][j]);
                    }

                }

            }

            // Line search - only if correction is non-zero
                VectorType tmp(x[level]->size());
                tmp = 0;

                mat[level]->umv(v,tmp);

                field_type alpha(0);
                if (v.two_norm() > 1e-10)
                    alpha = (fineResidual*v) / (tmp*v);

                alpha = std::max(std::min(alpha, lineSearchObs.upper(0)), lineSearchObs.lower(0));

//              if (this->verbosity_ == NumProc::FULL)
//                  std::cout << "Linesearch obstacle: " << lineSearchObs << "   parameter: " << alpha << std::endl;

                // add scaled correction
                x[level]->axpy(alpha,v);
        }

        //std::cout << x << std::endl;

        // //////////////////////////////////////
        //   Postsmoothing
        // //////////////////////////////////////
        // Remove pointer to the temporary critical bitfield
        // this avoids a memory problem when the same mmg step is reused
        mgTransfer->setCriticalBitField(nullptr);


        ProjectedBlockGSStep<MatrixType,VectorType>* postsmoother = dynamic_cast<ProjectedBlockGSStep<MatrixType, VectorType>*>(this->postsmoother_[level].get());
        assert(postsmoother);

        postsmoother->setProblem(*(mat[level]), *x[level], rhs[level]);

        /** \todo Hack: we should have ProjectedBlockGSStep only on the finest level! */

        if ((size_t) level == this->numLevels()-1) {
            postsmoother->obstacles_   = obstacles_.get();
            postsmoother->hasObstacle_ = hasObstacle_.get();
        } else {
            postsmoother->obstacles_ = &dummyObstacles;
            postsmoother->hasObstacle_ = &dummyHasObstacle;
        }

        postsmoother->ignoreNodes_ = this->ignoreNodesHierarchy_[level];

        for (int i=0; i<this->nu2_; i++)
            postsmoother->iterate();

    }

    // ////////////////////////////////////////////////////////////////////
    //   Track the number of critical nodes found during this iteration
    // ////////////////////////////////////////////////////////////////////

    // if we only have one level the critical nodes haven't been computed yet
    if (this->numLevels() == 1) {

       const double eps = 1e-12;
        for (size_t i=0; i<obstacles_->size(); i++) {

                for (int j=0; j<dim; j++) {

                    if ((*obstacles_)[i].lower(j) >= -eps + (*x[0])[i][j]
                        || (*obstacles_)[i].upper(j) <= eps + (*x[0])[i][j]) {
                        critical[i][j] = true;
                    }

                }
        }
    }

    if ((size_t) level==this->numLevels()-1 && this->verbosity_ == NumProc::FULL) {

        std::cout << critical.count() << " critical nodes found on level " << level;

        int changes = 0;
        for (unsigned int i=0; i<oldCritical.size(); i++)
            for (int j=0; j<dim; j++)
                if (oldCritical[i][j]!=critical[i][j])
                    changes++;

        std::cout << ", and " << changes << " changes." << std::endl;
        oldCritical = critical;
    }

    // Debug: output energy
    if ((size_t) level==this->numLevels()-1 && this->verbosity_==NumProc::FULL)
        std::cout << "Total energy: "
                  << std::setprecision(10) << computeEnergy(*mat[level], *x[level], rhs[level]) << std::endl;

}

} /* namespace Contact */
} /* namespace Dune */
