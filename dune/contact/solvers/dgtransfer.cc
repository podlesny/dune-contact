// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:

#include <dune/common/fvector.hh>
#include <dune/solvers/transferoperators/densemultigridtransfer.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/localfunctions/lagrange/pqkfactory.hh>
#include <dune/fufem/facehierarchy.hh>

namespace Dune {
namespace Contact {

template<class GridType>
const Dune::BCRSMatrix<Dune::FieldMatrix<typename GridType::ctype, 1, 1> >* DGMultigridTransfer<GridType>::
getDGProlongation(const DGIndexSet<typename GridType::LevelGridView>& coarseDGIndex,
                  const DGIndexSet<typename GridType::LevelGridView>& fineDGIndex,
                  const GridType& grid, int cL, int fL)
{
    if (fL != cL+1)
        DUNE_THROW(Dune::Exception, "The two function spaces don't belong to consecutive levels!");

    const int dim = GridType::dimension;
    typedef typename GridType::ctype ctype;

    int rows = fineDGIndex.size();
    int cols = coarseDGIndex.size();

    //
    MatrixType* mat = new MatrixType(rows, cols, MatrixType::random);

    typedef HierarchicFaceIterator<GridType> HierarchicFaceIterator;

    typename GridType::LevelGridView levelView = grid.levelGridView(cL);


    // ///////////////////////////////////////////
    // Determine the indices present in the matrix
    // /////////////////////////////////////////////////
    Dune::MatrixIndexSet indices(rows, cols);

    typedef Dune::PQkLocalFiniteElementCache<ctype, ctype, dim, elementOrder> FiniteElementCache;
    FiniteElementCache cache;

    for (const auto& cIt : elements(levelView)) {

        const auto& coarseBaseSet = cache.get(cIt.type());

        const auto& cRefElement = Dune::ReferenceElements<ctype, dim>::general(cIt.type());

        // -----------------------------------------
        // Loop over all dofs on the coarse element
        for (const auto& cNIt : intersections(levelView, cIt)) {

            //  get all sub-faces
            Face<GridType> coarseFace(grid,cIt,cNIt.indexInInside());
            HierarchicFaceIterator hEnd = coarseFace.hend(fL);

            // The number of vertices of this coarse grid face
            int cN = cRefElement.size(cNIt.indexInInside(),1,dim);

            for (int i=0; i<cN; i++) {

                int globalCoarse = coarseDGIndex(cIt, cNIt.indexInInside()) + i;

                // -----------------------------------------
                // Loop over all dofs on the fine element
                HierarchicFaceIterator hIt = coarseFace.hbegin(fL);

                for (;hIt!=hEnd;hIt++){

                    const auto& hElement = hIt.element();
                    const auto& fRefElement = Dune::ReferenceElements<ctype, dim>::general(hElement.type());
                    // The number of vertices of this fine grid face
                    int fN = fRefElement.size(hIt.index(),1,dim);

                    // store a copy of the geometry mappings
                    const auto& geomInFather = hElement.geometryInFather();

                    for (int j=0; j<fN; j++) {

                        int globalFine = fineDGIndex(hElement, hIt.index()) + j;

                        // Evaluate coarse grid base function at the location of the fine grid dof

                        // first determine local fine grid dof position
                        int fVerticesj = fRefElement.subEntity(hIt.index(), 1, j, dim);
                        const auto& fineLocal = fRefElement.position(fVerticesj,dim);
                        const auto& local = geomInFather.global(fineLocal);

                        // Evaluate coarse grid base function
                        std::vector<Dune::FieldVector<ctype,1> > values;
                        coarseBaseSet.localBasis().evaluateFunction(local,values);

                        ctype value = values[cRefElement.subEntity(cNIt.indexInInside(), 1, i, dim)];

                        // The following conditional is a hack:  evaluation the coarse
                        // grid base function will often return 0.  However, we don't
                        // want explicit zero entries in our prolongation matrix.  Since
                        // the whole code works for P1-elements only anyways, we know
                        // that value can only be 0, 0.5, or 1.  Thus testing for nonzero
                        // by testing > 0.0001 is safe.
                        if (fabs(value) > 0.0001)
                            indices.add(globalFine, globalCoarse);

                    }

                }

            }

        }

    }

    indices.exportIdx(*mat);

    // Clear to define the values created through padding
    *mat = 0;

    // /////////////////////////////////////////////
    // Compute the matrix
    // /////////////////////////////////////////////

    for (const auto& cIt : elements(levelView)) {

        const auto& coarseBaseSet = cache.get(cIt.type());

        const auto& cRefElement = Dune::ReferenceElements<ctype, dim>::general(cIt.type());

        // -----------------------------------------
        // Loop over all dofs on the coarse element
        for (const auto& cNIt : intersections(levelView, cIt)) {

            //  get all sub-faces
            Face<GridType> coarseFace(grid,cIt,cNIt.indexInInside());
            HierarchicFaceIterator hEnd = coarseFace.hend(fL);

            // Number of vertices of the current coarse grid face
            int cN = cRefElement.size(cNIt.indexInInside(),1,dim);

            for (int i=0; i<cN; i++) {

                int globalCoarse = coarseDGIndex(cIt, cNIt.indexInInside()) + i;

                // -----------------------------------------
                // Loop over all dofs on the fine element
                HierarchicFaceIterator hIt = coarseFace.hbegin(fL);

                for (;hIt!=hEnd;hIt++){

                    const auto& hElement = hIt.element();
                    const auto& fRefElement = Dune::ReferenceElements<ctype, dim>::general(hElement.type());
                    int fN = fRefElement.size(hIt.index(),1,dim);

                    // store a copy of the geometry mappings
                    const auto& geomInFather = hElement.geometryInFather();

                    for (int j=0; j<fN; j++) {

                        int globalFine = fineDGIndex(hElement, hIt.index()) + j;

                        // Evaluate coarse grid base function at the location of the fine grid dof

                        // first determine local fine grid dof position
                        int fVerticesj = fRefElement.subEntity(hIt.index(), 1, j, dim);
                        const auto& fineLocal = fRefElement.position(fVerticesj,dim);
                        const auto& local = geomInFather.global(fineLocal);


                        // Evaluate coarse grid base function
                        int cVerticesi = cRefElement.subEntity(cNIt.indexInInside(), 1, i, dim);
                        std::vector<Dune::FieldVector<ctype,1> > values;
                        coarseBaseSet.localBasis().evaluateFunction(local,values);
                        ctype value = values[cVerticesi];

                        // The following conditional is a hack:  evaluation the coarse
                        // grid base function will often return 0.  However, we don't
                        // want explicit zero entries in our prolongation matrix.  Since
                        // the whole code works for P1-elements only anyways, we know
                        // that value can only be 0, 0.5, or 1.  Thus testing for nonzero
                        // by testing > 0.0001 is safe.
                        if (fabs(value) > 0.0001)
                            (*mat)[globalFine][globalCoarse] = value;

                    }

                }

            }

        }

    }

    return mat;
}

template<class GridType>
template<class GridType1>
void DGMultigridTransfer<GridType>::
restrictCouplingOperator(const GridType& grid0, const GridType1& grid1,
                         int cL, int fL,
                         const MatrixType& fineDGMatrix,
                         MatrixType& coarseDGMatrix)
{
    assert(fL==cL+1);

    // If the grid max levels don't equal we virtually copy the grid levels
    int cL0(cL),fL0(fL);
    if (fL > grid0.maxLevel()) {
        fL0 = grid0.maxLevel();
        cL0 = grid0.maxLevel();
    }

    int cL1(cL),fL1(fL);
    if (fL > grid1.maxLevel()) {
        fL1 = grid1.maxLevel();
        cL1 = grid1.maxLevel();
    }

    auto cLView = grid0.levelGridView(cL0);
    auto fLView = grid0.levelGridView(fL0);

    DGIndexSet<typename GridType::LevelGridView> coarseDGIndex(cLView);
    DGIndexSet<typename GridType::LevelGridView> fineDGIndex(fLView);

    // Create DG prolongation matrix for the first grid
    const MatrixType* transfer0;

    if (cL0==fL0) {
        // make identity matrix

         MatrixType* mat = new MatrixType;

        Dune::MatrixIndexSet indices(fineDGIndex.size(),fineDGIndex.size());
        for (int i=0; i<fineDGIndex.size(); i++)
            indices.add(i,i);
        indices.exportIdx(*mat);
        *mat = 0;

        for (int i=0; i<fineDGIndex.size();i++)
            (*mat)[i][i] = 1;

        transfer0 = mat;

    } else
        transfer0 = getDGProlongation(coarseDGIndex, fineDGIndex, grid0, cL0, fL0);

    const int dim1 = GridType1::dimension;

    // Create standard prolongation matrix for the second grid
    DenseMultigridTransfer<Dune::BlockVector<Dune::FieldVector<ctype, 1> > > scalarLagrangeTransfer;
    const MatrixType* transfer1;
    if (cL1==fL1) {
       // make identity matrix
        MatrixType* mat = new MatrixType;

        Dune::MatrixIndexSet indices(grid1.size(fL1,dim1),grid1.size(fL1,dim1));
        for (int i=0; i<grid1.size(fL1,dim1); i++)
            indices.add(i,i);
        indices.exportIdx(*mat);
        *mat = 0;

        for (int i=0; i<grid1.size(fL1,dim1);i++)
            (*mat)[i][i] = 1;

        transfer1 = mat;

    } else {
        scalarLagrangeTransfer.setup(grid1, cL1, fL1);
        transfer1 = &scalarLagrangeTransfer.getMatrix();
    }

    // //////////////////////////////////////////////////////////////////////////
    //   Restrict operator by computing transfer0^T \times OP \times transfer1
    // //////////////////////////////////////////////////////////////////////////

    Dune::MatrixIndexSet indexSet(coarseDGIndex.size(), grid1.size(cL1,dim1));

    // Determine the matrix entries
    for (size_t v=0; v<fineDGMatrix.N(); v++) {

        const auto& row = fineDGMatrix[v];

        // Loop over all columns of the fine grid matrix
        auto m    = row.begin();
        auto mEnd = row.end();

        for (; m!=mEnd; ++m) {

            int w = m.index();

            // Loop over all coarse grid vectors iv that have v in their support
            auto im    = (*transfer0)[v].begin();
            auto imEnd = (*transfer0)[v].end();
            for (; im!=imEnd; ++im) {

                int iv = im.index();

                // Loop over all coarse grid vectors jv that have w in their support
                auto jm    = (*transfer1)[w].begin();
                auto jmEnd = (*transfer1)[w].end();

                for (; jm!=jmEnd; ++jm)
                    indexSet.add(iv, jm.index());

            }

        }

    }

    // Clear coarse matrix
    indexSet.exportIdx(coarseDGMatrix);
    coarseDGMatrix = 0;

    // Actually compute the matrix values
    for (size_t v=0; v<fineDGMatrix.N(); v++) {

        const auto& row = fineDGMatrix[v];

        // Loop over all columns of the stiffness matrix
        auto m    = row.begin();
        auto mEnd = row.end();

        for (; m!=mEnd; ++m) {

            int w = m.index();

            // Loop over all coarse grid vectors iv that have v in their support
            auto im    = (*transfer0)[v].begin();
            auto imEnd = (*transfer0)[v].end();
            for (; im!=imEnd; ++im) {

                int iv = im.index();

                // Loop over all coarse grid vectors jv that have w in their support
                auto jm    = (*transfer1)[w].begin();
                auto jmEnd = (*transfer1)[w].end();

                for (; jm!=jmEnd; ++jm) {

                    int jv = jm.index();

                    coarseDGMatrix[iv][jv] += (*im)[0][0] * (*m)[0][0] * (*jm)[0][0];

                }

            }

        }

    }

    // Delete the two transfer matrices
    delete(transfer0);
    if (cL1==fL1)
        delete(transfer1);

}

} /* namespace Contact */
} /* namespace Dune */
