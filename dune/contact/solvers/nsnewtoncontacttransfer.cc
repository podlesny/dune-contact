#include <dune/common/fvector.hh>
#include <dune/common/exceptions.hh>
#include <dune/istl/bdmatrix.hh>

#include <dune/fufem/assemblers/transferoperatorassembler.hh>
#include <dune/matrix-vector/blockmatrixview.hh>

#include <dune/matrix-vector/addtodiagonal.hh>
#include <dune/matrix-vector/axpy.hh>
#include <dune/solvers/common/numproc.hh>
#include <dune/solvers/transferoperators/densemultigridtransfer.hh>

namespace Dune {
namespace Contact {

template<class VectorType>
template<class GridType>
void NonSmoothNewtonContactTransfer<VectorType>::setup(const GridType& grid, int colevel,
                                                         const CoordSystemVector& fineLocalCoordSystems)
{
    int cL = grid.maxLevel() - colevel - 1;
    int fL = grid.maxLevel() - colevel;

    // Create standard prolongation matrix
    TruncatedDenseMGTransfer<VectorType>::setup(grid, cL, fL);

    if (colevel != 0)
        return;

    // ///////////////////////////////////////////////////////////////
    // Transform matrix to account for the local coordinate systems
    // ///////////////////////////////////////////////////////////////

    for(size_t rowIdx=0; rowIdx<this->matrix_.N(); rowIdx++) {

        auto& row = this->matrix_[rowIdx];

        for(auto& col : row) {

            // The multiplication should really be transposed, but we know that
            // the matrix is symmetric anyways
            col.leftmultiply(fineLocalCoordSystems[rowIdx]);

        }

    }

}

/** \todo This method is not very memory efficient! */
template<class VectorType>
template<class GridType0, class GridType1>
void NonSmoothNewtonContactTransfer<VectorType>::setup(const GridType0& grid0, const GridType1& grid1,
                                                         int colevel,
                                                         const MatrixType& mortarTransferOperator,
                                                         const CoordSystemVector& fineLocalCoordSystems,
                                                         const BitSetVector<1>& fineHasObstacle)
{
    const int nGrids     = 2;

    std::vector<int> cL(nGrids), fL(nGrids);

    cL[0] = std::max(0, grid0.maxLevel() - colevel - 1);
    fL[0] = std::max(0, grid0.maxLevel() - colevel);

    cL[1] = std::max(0, grid1.maxLevel() - colevel - 1);
    fL[1] = std::max(0, grid1.maxLevel() - colevel);

    assert(cL[0] >= 0);
    assert(cL[1] >= 0);

    // ////////////////////////////////////////////////////////////
    //   Create the standard prolongation matrices for each grid
    // ////////////////////////////////////////////////////////////

    std::vector<TruncatedDenseMGTransfer<VectorType>* > gridTransfer(nGrids);
    std::vector<const MatrixType*> submat(nGrids);

    if (fL[0] > 0) {
        gridTransfer[0] = new TruncatedDenseMGTransfer<VectorType>;
        gridTransfer[0]->setup(grid0, cL[0], fL[0]);
        submat[0] = &gridTransfer[0]->getMatrix();
    } else {
        // when the maxLevels of the grids differ then we add copys of the coarsest level to the "smaller" grid
        BDMatrix<MatrixBlock>* newMatrix = new BDMatrix<MatrixBlock>(grid0.size(0,GridType0::dimension));
        for (size_t i=0; i<newMatrix->N(); i++)
            Dune::MatrixVector::addToDiagonal((*newMatrix)[i][i], 1.0);

        submat[0] = newMatrix;
    }

    if (fL[1] > 0) {
        gridTransfer[1] = new TruncatedDenseMGTransfer<VectorType>;
        gridTransfer[1]->setup(grid1, cL[1], fL[1]);
        submat[1] = &gridTransfer[1]->getMatrix();
    } else {
        // when the maxLevels of the grids differ then we add copys of the coarsest level to the "smaller" grid
        BDMatrix<MatrixBlock>* newMatrix = new BDMatrix<MatrixBlock>(grid1.size(0,GridType1::dimension));
        for (size_t i=0; i<newMatrix->N(); i++)
            Dune::MatrixVector::addToDiagonal((*newMatrix)[i][i], 1.0);

        submat[1] = newMatrix;
    }

    // ///////////////////////////////////
    //   Combine the two submatrices
    // ///////////////////////////////////

    if (colevel==0) {
      std::vector<const MatrixType*> transferOperatorVec(1);
      transferOperatorVec[0] = &mortarTransferOperator;

      std::vector<const BitSetVector<1>*> fineHasObstacleVec(1);

      fineHasObstacleVec[0] = &fineHasObstacle;

      std::vector<std::array<int,2> > gridIdx(1);
      gridIdx[0][0] = 0;
      gridIdx[0][1] = 1;

      combineSubMatrices(submat, transferOperatorVec, fineLocalCoordSystems, fineHasObstacleVec, gridIdx);
    } else {
        Dune::MatrixVector::BlockMatrixView<MatrixType>::setupBlockMatrix(submat, this->matrix_);
    }

    for (size_t i=0; i<nGrids; i++)
        if (fL[i]==0)
            delete(submat[i]);

    // Delete separate transfer objects
    for (int i=0; i<nGrids; i++)
        delete(gridTransfer[i]);
}


template<class VectorType>
template<class GridType>
void NonSmoothNewtonContactTransfer<VectorType>::setup(const std::vector<const GridType*>& grids, int colevel,
                                                         const std::vector<const MatrixType*>& mortarTransferOperator,
                                                         const CoordSystemVector& fineLocalCoordSystems,
                                                         const std::vector<const BitSetVector<1>*>& fineHasObstacle,
                                                         const std::vector<std::array<int,2> >& gridIdx)
{
    const size_t nGrids     = grids.size();
    const size_t nCouplings = mortarTransferOperator.size();

    if (gridIdx.size() != nCouplings)
        DUNE_THROW(SolverError, "Number of couplings doesn't match number of coupling matrices!");

    std::vector<int> cL(nGrids), fL(nGrids);

    for (size_t i=0; i<nGrids; i++) {

        cL[i] = std::max(0, grids[i]->maxLevel() - colevel - 1);
        fL[i] = std::max(0, grids[i]->maxLevel() - colevel);
        assert(cL[i] >= 0);

    }

    // ////////////////////////////////////////////////////////////
    //   Create the standard prolongation matrices for each grid
    // ////////////////////////////////////////////////////////////

    std::vector<TruncatedDenseMGTransfer<VectorType>* > gridTransfer(nGrids);
    std::vector<const MatrixType*> submat(nGrids);

    for (size_t i=0; i<nGrids; i++) {

        if (fL[i] > 0) {

            gridTransfer[i] = new TruncatedDenseMGTransfer<VectorType>;
            gridTransfer[i]->setup(*grids[i], cL[i], fL[i]);
            submat[i] =&gridTransfer[i]->getMatrix();

        } else {

            // when the maxLevels of the grids differ then we add copys of the coarsest level to the "smaller" grid
            BDMatrix<MatrixBlock>* newMatrix = new BDMatrix<MatrixBlock>(grids[i]->size(0,GridType::dimension));

            for (size_t j=0; j<newMatrix->N(); j++)
                for (int k=0; k<blocksize; k++)
                    for (int l=0; l<blocksize; l++)
                        (*newMatrix)[j][j][k][l] = (k==l);

            submat[i] = newMatrix;

        }

    }

    // ///////////////////////////////////
    //   Combine the submatrices
    // ///////////////////////////////////

    if (colevel == 0)
      combineSubMatrices(submat, mortarTransferOperator, fineLocalCoordSystems, fineHasObstacle, gridIdx);
    else
      Dune::MatrixVector::BlockMatrixView<MatrixType>::setupBlockMatrix(submat, this->matrix_);

    for (size_t i=0; i<nGrids; i++)
        if (fL[i]==0)
            delete(submat[i]);

    // Delete separate transfer objects
    for (size_t i=0; i<nGrids; i++)
        delete(gridTransfer[i]);
}

template<class VectorType>
template<class GridType>
void NonSmoothNewtonContactTransfer<VectorType>::setupHierarchy(std::vector<std::shared_ptr<NonSmoothNewtonContactTransfer<VectorType> > >& mgTransfers,
                                    const std::vector<const GridType*> grids,
                                    const std::vector<const MatrixType*> mortarTransferOperator,
                                    const CoordSystemVector& fineLocalCoordSystems,
                                    const std::vector<const BitSetVector<1>*>& fineHasObstacle,
                                    const std::vector<std::array<int,2> >& gridIdx)
{
    const size_t nGrids     = grids.size();

    std::vector<std::vector<TruncatedDenseMGTransfer<VectorType>* > > gridTransfer(nGrids);
    std::vector<const MatrixType*> submat(nGrids);

    // ////////////////////////////////////////////////////////////
    //   Create the standard prolongation matrices for each grid
    // ////////////////////////////////////////////////////////////

    for (size_t i=0; i<nGrids; i++) {

        gridTransfer[i].resize(grids[i]->maxLevel());
        for (size_t j=0; j<gridTransfer[i].size(); j++)
            gridTransfer[i][j] = new TruncatedDenseMGTransfer<VectorType>;

        // Assemble standard transfer operator
        TransferOperatorAssembler<GridType> transferOperatorAssembler(*grids[i]);
        transferOperatorAssembler.assembleOperatorPointerHierarchy(gridTransfer[i]);
    }

    // ////////////////////////////////////////////////////////////////////////////
    //      Combine matrices in one matrix and add mortar entries on the fine level
    // ///////////////////////////////////////////////////////////////////////////
    int toplevel = mgTransfers.size();

    for (size_t colevel=0; colevel<mgTransfers.size(); colevel++) {

        std::vector<int> fL(nGrids);

        for (size_t i=0; i<nGrids; i++) {
            fL[i] = std::max(size_t(0), grids[i]->maxLevel() - colevel);

            // If the prolongation matrix exists, take it
            if (fL[i] > 0) {

                submat[i] =&gridTransfer[i][fL[i]-1]->getMatrix();
            } else {
                // when the maxLevels of the grids differ then we add copys of the coarsest level to the "smaller" grid
                BDMatrix<MatrixBlock>* newMatrix = new BDMatrix<MatrixBlock>(grids[i]->size(0,GridType::dimension));
                *newMatrix = 0;

                for (size_t j=0; j<newMatrix->N(); j++)
                    for (int k=0; k<blocksize; k++)
                            (*newMatrix)[j][j][k][k] = 1.0;

                submat[i] = newMatrix;
            }
        }

        if (colevel == 0)
            mgTransfers[toplevel-colevel-1]->combineSubMatrices(submat, mortarTransferOperator, fineLocalCoordSystems, fineHasObstacle, gridIdx);
        else
          Dune::MatrixVector::BlockMatrixView<MatrixType>::setupBlockMatrix(submat, mgTransfers[toplevel-colevel-1]->matrix_);

        for (size_t i=0; i<nGrids; i++)
            if (fL[i]==0)
                delete(submat[i]);
    }

    // Delete separate transfer objects
    for (size_t i=0; i<nGrids; i++)
        for (size_t j=0; j<gridTransfer[i].size(); j++)
            delete(gridTransfer[i][j]);
}

template <class VectorType>
template <class GridType, class TransferPtr>
void NonSmoothNewtonContactTransfer<VectorType>::setupHierarchy(std::vector<TransferPtr>& mgTransfers,
                           std::shared_ptr<LargeDeformationContactAssembler<GridType,VectorType> > contactAssembler)
{
    const auto& grids = contactAssembler->getGrids();
    const size_t nGrids     = grids.size();

    std::vector<std::vector<DenseMultigridTransfer<VectorType>* > > gridTransfer(nGrids);
    std::vector<const MatrixType*> submat(nGrids);

    // ////////////////////////////////////////////////////////////
    //   Create the standard prolongation matrices for each grid
    // ////////////////////////////////////////////////////////////

    for (size_t i=0; i<nGrids; i++) {

        gridTransfer[i].resize(grids[i]->maxLevel());
        for (size_t j=0; j<gridTransfer[i].size(); j++)
            gridTransfer[i][j] = new DenseMultigridTransfer<VectorType>;

        // Assemble standard transfer operator
        TransferOperatorAssembler<GridType> transferOperatorAssembler(*grids[i]);
        transferOperatorAssembler.assembleOperatorPointerHierarchy(gridTransfer[i]);
    }

    // ////////////////////////////////////////////////////////////////////////////
    //      Combine matrices in one matrix and add mortar entries on the fine level
    // ///////////////////////////////////////////////////////////////////////////

    int toplevel = mgTransfers.size();

    for (size_t colevel=0; colevel<mgTransfers.size(); colevel++) {

        std::vector<int> fL(nGrids);

        for (size_t i=0; i<nGrids; i++) {
            fL[i] = std::max(size_t(0), grids[i]->maxLevel() - colevel);

            // If the prolongation matrix exists, take it
            if (fL[i] > 0) {

                submat[i] =&gridTransfer[i][fL[i]-1]->getMatrix();
            } else {
                // when the maxLevels of the grids differ then we add copys of the coarsest level to the "smaller" grid
                BDMatrix<MatrixBlock>* newMatrix = new BDMatrix<MatrixBlock>(grids[i]->size(0,GridType::dimension));
                *newMatrix = 0;

                for (size_t j=0; j<newMatrix->N(); j++)
                    for (int k=0; k<blocksize; k++)
                            (*newMatrix)[j][j][k][k] = 1.0;

                submat[i] = newMatrix;
            }
        }

        // ///////////////////////////////////
        //   Combine the submatrices
        // ///////////////////////////////////

        namespace MV = Dune::MatrixVector;
        // only on the leaf view the transferoperator is different from the standard one
        if (colevel != 0) {
            MV::BlockMatrixView<MatrixType>::setupBlockMatrix(submat, mgTransfers[toplevel-colevel-1]->matrix_);

            for (size_t i=0; i<nGrids; i++)
              if (fL[i]==0)
                delete(submat[i]);
            continue;
        }

        const size_t nCouplings = contactAssembler->nCouplings();
        MV::BlockMatrixView<MatrixType> bV(submat);

        MatrixIndexSet totalIndexSet(bV.nRows(), bV.nCols());
        for (size_t i=0; i<nGrids; i++)
            totalIndexSet.import(*submat[i], bV.row(i,0), bV.col(i,0));

        // ///////////////////////////////////////////////////////////////
        //   Add additional entries from the inverse mortar transformation
        // ///////////////////////////////////////////////////////////////

        std::vector<std::vector<int> > localToGlobal(nCouplings);
        using ContactAssembler = LargeDeformationContactAssembler<GridType, VectorType>;
        using ContactCoupling = typename ContactAssembler::LargeDefCoupling;

        for (size_t i=0; i<nCouplings; i++) {

          auto coupling = std::dynamic_pointer_cast<ContactCoupling>(contactAssembler->getContactCouplings()[i]);

          std::vector<std::reference_wrapper<const typename ContactCoupling::JacobianType> > mortarMatrices;
          mortarMatrices = {{std::cref(coupling->exactNonmortarMatrix()), std::cref(coupling->exactMortarMatrix())}};

          localToGlobal[i].resize(mortarMatrices[0].get().N());
          const auto& nmPatch = coupling->nonmortarBoundary();
          const auto& gridIdx = contactAssembler->getCoupling(i).gridIdx_;

          size_t idx = 0;
          for (size_t j=0; j<submat[gridIdx[0]]->N(); j++)
            if (nmPatch.containsVertex(j))
              localToGlobal[i][idx++] = j;

          assert(idx==localToGlobal[i].size());

          for (size_t p=0; p<2; p++)
            for (size_t j=0; j<mortarMatrices[p].get().N(); j++) {

              auto&& cTIt = mortarMatrices[p].get()[j].begin();
              auto&& cEndIt = mortarMatrices[p].get()[j].end();

              for (; cTIt != cEndIt; ++cTIt) {

                size_t k = cTIt.index();

                auto&& cMIt = (*submat[gridIdx[p]])[k].begin();
                auto&& cMEndIt = (*submat[gridIdx[p]])[k].end();
                for (; cMIt != cMEndIt; ++cMIt)
                  totalIndexSet.add(bV.row(gridIdx[0], localToGlobal[i][j]),
                      bV.col(gridIdx[p], cMIt.index()));
              }
            }
        }

        auto& matrix = mgTransfers[toplevel-colevel-1]->matrix_;
        totalIndexSet.exportIdx(matrix);
        matrix = 0;

        // add all remaining entries of the standard transfer operators
        for (size_t i=0; i<nGrids; i++) {

          std::vector<std::reference_wrapper<const typename ContactAssembler::LeafBoundaryPatch> > nmPatches;
          for (size_t j=0; j<nCouplings; j++)
            if (contactAssembler->getCoupling(j).gridIdx_[0]==(int) i)
              nmPatches.emplace_back(std::cref(contactAssembler->getContactCouplings()[j]->nonmortarBoundary()));

          // lambda that checks if vertex is contained in any non-mortar boundary
          auto contains = [&] (size_t idx) {

            for (size_t k=0; k<nmPatches.size(); k++)
              if (nmPatches[k].get().containsVertex(idx))
                return true;
            return false;
          };

          for(size_t j=0; j<submat[i]->N(); j++) {

            // skip nonmortar rows
            if (contains(j))
              continue;

            auto&& end = (*submat[i])[j].end();
            for (auto&& it = (*submat[i])[j].begin(); it != end; it++)
                matrix[bV.row(i,j)][bV.col(i,it.index())] = *it;
          }
        }


        // ///////////////////////////////////////////////////////////////
        //   Add additional mortar entries $
        //
        //  The inverse transformation is simply consisting of the negative
        //  non-mortar matrix and the negative mortar matrix
        //
        // ///////////////////////////////////////////////////////////////

        for (size_t i=0; i<nCouplings; i++) {

          // The grids involved in this coupling
          const auto& gridIdx = contactAssembler->getCoupling(i).gridIdx_;

          auto coupling = std::dynamic_pointer_cast<ContactCoupling>(contactAssembler->getContactCouplings()[i]);

          const auto& nmPatch = coupling->nonmortarBoundary();
          const auto& localCoordSystem = contactAssembler->getLocalCoordSystems();

          std::vector<std::reference_wrapper<const typename ContactCoupling::JacobianType> > mortarMatrices;
          mortarMatrices = {{std::cref(coupling->exactNonmortarMatrix()), std::cref(coupling->exactMortarMatrix())}};

          for (size_t p=0; p<2; p++)
            for (size_t j=0; j<mortarMatrices[p].get().N(); j++) {

              auto&& cIt    = mortarMatrices[p].get()[j].begin();
              auto&& cEndIt = mortarMatrices[p].get()[j].end();

              for (; cIt!=cEndIt; ++cIt) {

                // column
                int k = cIt.index();

                // diagonal entries of nonmortar degrees of freedom have to be modified
                MatrixBlock invTrans(0);

                if (p==0 and nmPatch.containsVertex(k) and (localToGlobal[i][j]==k))
                  invTrans = localCoordSystem[bV.row(gridIdx[p],k)];

                invTrans[0] = (*cIt)[0];
                invTrans[0] *= -1;

                auto cMIt = (*submat[gridIdx[p]])[k].begin();
                auto cMEndIt = (*submat[gridIdx[p]])[k].end();

                for (; cMIt != cMEndIt; ++cMIt) {

                  auto& currentMatrixBlock = matrix[bV.row(gridIdx[0], localToGlobal[i][j])]
                      [bV.col(gridIdx[p], cMIt.index())];

                  const auto& subMatBlock = *cMIt;
                  MV::addProduct(currentMatrixBlock, invTrans, subMatBlock);
                }
              }
            }
        }

        for (size_t i=0; i<nGrids; i++)
          if (fL[i]==0)
            delete(submat[i]);
    }

    // Delete separate transfer objects
    for (size_t i=0; i<nGrids; i++)
      for (size_t j=0; j<gridTransfer[i].size(); j++)
        delete(gridTransfer[i][j]);
}


template<class VectorType>
void NonSmoothNewtonContactTransfer<VectorType>::combineSubMatrices(const std::vector<const MatrixType*>& submat,
                                                                      const std::vector<const MatrixType*>& mortarTransferOperator,
                                                                      const CoordSystemVector& fineLocalCoordSystems,
                                                                      const std::vector<const BitSetVector<1>*>& fineHasObstacle,
                                                                      const std::vector<std::array<int,2> >& gridIdx)
{
    // ///////////////////////////////////
    //   Combine the submatrices
    // ///////////////////////////////////

    const size_t nGrids     = submat.size();
    const size_t nCouplings = mortarTransferOperator.size();

    Dune::MatrixVector::BlockMatrixView<MatrixType> view(submat);

    MatrixIndexSet totalIndexSet(view.nRows(), view.nCols());

    // import indices of canonical transfer operator
    for (size_t i=0; i<nGrids; i++)
        totalIndexSet.import(*submat[i], view.row(i, 0), view.col(i, 0));

    // ///////////////////////////////////////////////////////////////
    //   Add additional matrix entries  $ -D^{-1}M I_{mm} $
    // ///////////////////////////////////////////////////////////////

    typedef typename OperatorType::row_type RowType;
    typedef typename RowType::ConstIterator ConstIterator;

    std::vector<std::vector<int> > localToGlobal(nCouplings);

    for (size_t i=0; i<nCouplings; i++) {

      if (fineHasObstacle[i]->size() != submat[gridIdx[i][0]]->N())
        DUNE_THROW(Dune::Exception,
                   "fineHasObstacle[" << i << "] doesn't have the proper length!");

      localToGlobal[i].resize(mortarTransferOperator[i]->N());
      size_t idx = 0;
      for (size_t j=0; j<fineHasObstacle[i]->size(); j++)
        if ((*fineHasObstacle[i])[j][0])
          localToGlobal[i][idx++] = j;

      assert(idx==localToGlobal[i].size());

      for (size_t j=0; j<mortarTransferOperator[i]->N(); j++) {

        ConstIterator cTIt = (*mortarTransferOperator[i])[j].begin();
        ConstIterator cEndIt = (*mortarTransferOperator[i])[j].end();
        for (; cTIt != cEndIt; ++cTIt) {

          int k = cTIt.index();

          ConstIterator cMIt = (*submat[gridIdx[i][1]])[k].begin();
          ConstIterator cMEndIt = (*submat[gridIdx[i][1]])[k].end();
          for (; cMIt != cMEndIt; ++cMIt)
            totalIndexSet.add(view.row(gridIdx[i][0], localToGlobal[i][j]),
                view.col(gridIdx[i][1], cMIt.index()));

        }

      }

    }

    totalIndexSet.exportIdx(this->matrix_);
    this->matrix_ = 0;

    // Copy matrices
    for (size_t i=0; i<nGrids; i++) {

        for(size_t rowIdx=0; rowIdx<submat[i]->N(); rowIdx++) {

            const RowType& row = (*submat[i])[rowIdx];

            ConstIterator cIt    = row.begin();
            ConstIterator cEndIt = row.end();

            for(; cIt!=cEndIt; ++cIt)
                this->matrix_[view.row(i, rowIdx)][view.col(i, cIt.index())] = *cIt;

        }

    }

    // ///////////////////////////////////////////////////////////////
    //   Add additional matrix entries  $ -D^{-1}M I_{mm} $
    // ///////////////////////////////////////////////////////////////

    for (size_t i=0; i<nCouplings; i++) {

        for (size_t j=0; j<mortarTransferOperator[i]->N(); j++) {

            ConstIterator cTIt = (*mortarTransferOperator[i])[j].begin();
            ConstIterator cTEndIt = (*mortarTransferOperator[i])[j].end();

            for (; cTIt != cTEndIt; ++cTIt) {

                int k = cTIt.index();

                ConstIterator cMIt = (*submat[gridIdx[i][1]])[k].begin();
                ConstIterator cMEndIt = (*submat[gridIdx[i][1]])[k].end();

                for (; cMIt != cMEndIt; ++cMIt) {

                    auto& currentMatrixBlock = this->matrix_[view.row(gridIdx[i][0], localToGlobal[i][j])][view.col(gridIdx[i][1], cMIt.index())];

                    // the entry in the prolongation matrix of the mortar grid
                    const auto& subMatBlock = this->matrix_[view.row(gridIdx[i][1], k)][view.col(gridIdx[i][1], cMIt.index())];

                    // the - is due to the negation formula for BT
                    Dune::MatrixVector::subtractProduct(currentMatrixBlock,(*cTIt), subMatBlock);
                }
            }
        }
    }

    // ///////////////////////////////////////////////////////////////
    // Transform matrix to account for the local coordinate systems
    // ///////////////////////////////////////////////////////////////

    /** \todo Hack.  Those should really be equal */
    assert(fineLocalCoordSystems.size() <= this->matrix_.N());

    for(size_t rowIdx=0; rowIdx<fineLocalCoordSystems.size(); rowIdx++) {

        auto& row = this->matrix_[rowIdx];

        for(auto& col : row)
            col.leftmultiply(fineLocalCoordSystems[rowIdx]);

    }

}

} /* namespace Contact */
} /* namespace Dune */
