#ifndef NONSMOOTH_NEWTON_MULTIGRID_STEP_HH
#define NONSMOOTH_NEWTON_MULTIGRID_STEP_HH

#include <dune/solvers/iterationsteps/multigridstep.hh>
#include <dune/solvers/iterationsteps/projectedblockgsstep.hh>
#include <dune/solvers/transferoperators/multigridtransfer.hh>
#include <dune/solvers/common/boxconstraint.hh>
#include "contactobsrestrict.hh"

namespace Dune {
namespace Contact {

/** \brief The nonsmooth Newton multigrid solver for one-body and two-body
    contact problems without friction
*/
template<class MatrixType, class VectorType>
class NonSmoothNewtonMGStep : public MultigridStep<MatrixType, VectorType>
{

    static const int dim = VectorType::block_type::dimension;

    using Base = MultigridStep<MatrixType, VectorType>;
    using field_type = typename VectorType::field_type;
    using ObstacleVectorType = std::vector<BoxConstraint<field_type,dim> >;

public:

    NonSmoothNewtonMGStep() {}

    NonSmoothNewtonMGStep(const MatrixType& mat,
                  VectorType& x,
                  const VectorType& rhs)
        : Base(mat, x, rhs)
    {
        oldCritical.resize(x.size(), false);
    }

    virtual ~NonSmoothNewtonMGStep() {}

    virtual void setProblem(const MatrixType& mat,
                            VectorType& x,
                            const VectorType& rhs)
    {
        Base::setProblem(mat,x,rhs);
        oldCritical.resize(x.size(), false);
    }

    virtual void iterate();

    virtual void preprocess();

    //! Set the hasObstacle bitfield
    template <class BitVector>
    void setHasObstacle(BitVector&& hasObstacle) {
      hasObstacle_ = Dune::Solvers::wrap_own_share<Dune::BitSetVector<dim> >(std::forward<BitVector>(hasObstacle));
    }

    //! Set the obstacle field
    template <class ObstacleVector>
    void setObstacles(ObstacleVector&& obstacles) {
      obstacles_ = Dune::Solvers::wrap_own_share<ObstacleVectorType>(std::forward<ObstacleVector>(obstacles));
    }

protected:

    std::shared_ptr<BitSetVector<dim> > hasObstacle_;

    std::vector<BitSetVector<dim> > recompute_;

    std::shared_ptr<std::vector<BoxConstraint<field_type,dim> > > obstacles_;

    // Needed to track changes in the set of critical bits, and allows
    // to check which dofs where critical after the last iteration
    BitSetVector<dim> oldCritical;
};

} /* namespace Contact */
} /* namespace Dune */

#include "nsnewtonmgstep.cc"

#endif
