// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_SOLVERS_CONTACTTRANSFEROPERATORASSEMBLER_HH
#define DUNE_CONTACT_SOLVERS_CONTACTTRANSFEROPERATORASSEMBLER_HH

#include <dune/common/fmatrix.hh>
#include <dune/fufem/boundarypatchprolongator.hh>

#include <dune/contact/solvers/contacttransfer.hh>

#include <dune/contact/assemblers/onebodyassembler.hh>
#include <dune/contact/assemblers/twobodyassembler.hh>
#include <dune/contact/assemblers/nbodyassembler.hh>
#include <dune/contact/assemblers/dualmortarcouplinghierarchy.hh>

namespace Dune {
namespace Contact {

template <class VectorType, class GridType0, class GridType1=GridType0>
class ContactTransferOperatorAssembler
{
  protected:
    typedef typename VectorType::field_type field_type;
    enum {blocksize = VectorType::block_type::dimension};

    typedef Dune::FieldMatrix<field_type, blocksize, blocksize> MatrixBlock;
    typedef std::vector<MatrixBlock> CoordSystemVector;
    typedef BCRSMatrix<MatrixBlock> MatrixType;
    using ContactAssembler = Dune::Contact::ContactAssembler<GridType0::dimension, field_type>;
    using HierarchyCouplingType = DualMortarCouplingHierarchy<field_type, GridType0>;


  public:

    /** \brief Assemble monotone multigrid transfer operators for the one-body problem. */
    static void assembleOperatorHierarchy(const OneBodyAssembler<GridType0,field_type>& contactAssembler,
                                          std::vector<ContactMGTransfer<VectorType> >& mgTransfers) {
        std::vector<ContactMGTransfer<VectorType>* > mgTransferPtrs(mgTransfers.size());
        for (size_t i = 0; i < mgTransfers.size(); ++i)
            mgTransferPtrs[i] = &mgTransfers[i];

        assembleOperatorHierarchy(contactAssembler, mgTransferPtrs);
    }

    /** \brief Assemble monotone multigrid transfer operators for the one-body problem. */
    static void assembleOperatorHierarchy(const OneBodyAssembler<GridType0,field_type>& contactAssembler,
                                          std::vector<ContactMGTransfer<VectorType>* >& mgTransfers)
    {
        const GridType0& grid = contactAssembler.getGrid();

        // setup the local coordinate systems for each level
        typedef BoundaryPatch<typename GridType0::LevelGridView> LevelBoundaryPatch;
        std::vector<LevelBoundaryPatch> obsPatches(grid.maxLevel()+1);
        obsPatches[0] = contactAssembler.getCoarseObsPatch();
        BoundaryPatchProlongator<GridType0>::prolong(obsPatches);

        std::vector<CoordSystemVector> localCoordSystems(grid.maxLevel()+1);

        for (size_t i=0; i<obsPatches.size();i++)
            ContactAssembler::computeLocalCoordinateSystems(obsPatches[i], localCoordSystems[i], obsPatches[i].getNormals());

        for (size_t i=0; i<mgTransfers.size(); i++)
            mgTransfers[i]->setup(grid, i, i+1, localCoordSystems[i], localCoordSystems[i+1]);
    }
    /** \brief Assemble monotone multigrid transfer operators for the two-body problem. */


    static void assembleOperatorHierarchy(const TwoBodyAssembler<GridType0,VectorType>& contactAssembler,
                                          std::vector<ContactMGTransfer<VectorType>* >& mgTransfers)
    {
         const GridType0& grid0 = *contactAssembler.grids_[0];
         const GridType0& grid1 = *contactAssembler.grids_[1];

        // setup the local coordinate systems for each level
        std::vector<CoordSystemVector> localCoordSystems(grid0.maxLevel()+1);

        // the contact coupling
        const auto& coupling = *dynamic_cast<HierarchyCouplingType*>(contactAssembler.contactCoupling_[0].get());
        //TODO By now only works with NormalProjection :-(
        NormalProjection<BoundaryPatch<typename GridType0::LevelGridView> > projection;

        for (int i=0; i<grid0.maxLevel()+1;i++) {

            // max levels might not be equal
            int maxLevel0 = std::min(coupling.grid0_->maxLevel(),i);
            int maxLevel1 = std::min(coupling.grid1_->maxLevel(),i);

            // use the contact projection to compute the obstacle directions
            projection.project(coupling.nonmortarBoundary(maxLevel0),
                               coupling.mortarBoundary(maxLevel1),
                               contactAssembler.coupling_[0].obsDistance_);

            std::vector<typename VectorType::block_type> directions;
            projection.getObstacleDirections(directions);

            ContactAssembler::computeLocalCoordinateSystems(coupling.nonmortarBoundary(maxLevel0),
                                                       localCoordSystems[i], directions);
        }

        for (size_t i=0; i<mgTransfers.size(); i++)
            mgTransfers[i]->setup(grid0, grid1, i, i+1,
                    coupling.mortarLagrangeMatrix(i),
                    localCoordSystems[i], localCoordSystems[i+1],
                    *coupling.nonmortarBoundary(std::min(int(i),grid0.maxLevel())).getVertices(),
                    *coupling.nonmortarBoundary(std::min(int(i+1),grid0.maxLevel())).getVertices());

    }

    /** \brief Assemble monotone multigrid transfer operators for the n-body problem. */
    static void assembleOperatorHierarchy(const NBodyAssembler<GridType0,VectorType>& contactAssembler,
                                          std::vector<ContactMGTransfer<VectorType>* >& mgTransfers)
    {
         const std::vector<const GridType0* >& grids = contactAssembler.grids_;
         int nCouplings = contactAssembler.nCouplings();

         // Get overall top level
         int toplevel = 0;
         for (size_t i=0; i<grids.size(); ++i)
             toplevel = std::max(toplevel, grids[i]->maxLevel());

         std::vector<std::array<int,2> > gridIdx(nCouplings);

        // setup the local coordinate systems for each level and coupling
        std::vector<std::vector<CoordSystemVector> > localCoordSystems(nCouplings);

        for (int j=0; j<nCouplings; j++) {

            gridIdx[j] = contactAssembler.coupling_[j].gridIdx_;

            const auto& coupling = *dynamic_cast<HierarchyCouplingType*>(contactAssembler.contactCoupling_[j].get());
            //TODO By now only works with NormalProjection :-(
            NormalProjection<BoundaryPatch<typename GridType0::LevelGridView> > projection;

            localCoordSystems[j].resize(toplevel+1);

            for (int i=0; i<localCoordSystems[j].size();i++) {

                // max levels might not be equal
                int maxLevel0 = std::min(coupling.grid0_->maxLevel(),i);
                int maxLevel1 = std::min(coupling.grid1_->maxLevel(),i);

                // use the contact projection to compute the obstacle directions
                projection.project(coupling.nonmortarBoundary(maxLevel0),
                                   coupling.mortarBoundary(maxLevel1),
                                   contactAssembler.coupling_[j].obsDistance_);

                std::vector<typename VectorType::block_type> directions;
                projection.getObstacleDirections(directions);

                ContactAssembler::computeLocalCoordinateSystems(coupling.nonmortarBoundary(maxLevel0),
                       localCoordSystems[j][i], directions);
            }
        }

        std::vector<const Dune::BitSetVector<1>*> coarseHasObstacle(nCouplings);
        std::vector<const Dune::BitSetVector<1>*> fineHasObstacle(nCouplings);
        std::vector<const CoordSystemVector*> coarseCoordSystem(nCouplings);
        std::vector<const CoordSystemVector*> fineCoordSystem(nCouplings);
        std::vector<const MatrixType*> mortarTransfer(nCouplings);

        // setup the transfer operators
        for (size_t i=0; i<mgTransfers.size(); i++) {

            for (int j=0; j<nCouplings; j++) {
                coarseCoordSystem[j] = &localCoordSystems[j][i];
                fineCoordSystem[j] = &localCoordSystems[j][i+1];

                const auto& coupling = *dynamic_cast<HierarchyCouplingType*>(contactAssembler.contactCoupling_[j].get());

                // if the maxlevels of all grids are not the same, we use virtual copies
                int fineLevel = std::min(coupling.grid0_->maxLevel(),(int) i+1);
                int coarseLevel = std::min(coupling.grid0_->maxLevel(),(int) i);

                coarseHasObstacle[j]  = coupling.nonmortarBoundary(coarseLevel).getVertices();
                fineHasObstacle[j]  = coupling.nonmortarBoundary(fineLevel).getVertices();

                mortarTransfer[j] = &coupling.mortarLagrangeMatrix(i);
            }

            mgTransfers[i]->setup(grids, i, i+1,
                                 coarseCoordSystem, fineCoordSystem,
                                 coarseHasObstacle, fineHasObstacle,
                                 mortarTransfer,
                                 gridIdx);
        }
    }
};

} /* namespace Contact */
} /* namespace Dune */

#endif
