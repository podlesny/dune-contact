// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef CONTACT_OBSTACLE_RESTRICTION_HH
#define CONTACT_OBSTACLE_RESTRICTION_HH

#include <vector>

#include <dune/solvers/common/boxconstraint.hh>
#include <dune/solvers/transferoperators/densemultigridtransfer.hh>
#include <dune/solvers/transferoperators/obstaclerestrictor.hh>

namespace Dune {
namespace Contact {

/**
 * \todo Do we need the template parameter?
 */
template <class VectorType>
class ContactObsRestriction : public ObstacleRestrictor<VectorType>
{

    enum {dim = VectorType::block_type::dimension};

    using field_type = typename VectorType::field_type;

    using MatrixBlock = FieldMatrix<field_type, dim, dim>;

public:

    virtual void restrict(const std::vector<BoxConstraint<field_type,dim> >& f,
                          std::vector<BoxConstraint<field_type,dim> >& t,
                          const BitSetVector<dim>& fHasObstacle,
                          const BitSetVector<dim>& tHasObstacle,
                          const MultigridTransfer<VectorType>& transfer,
                          const BitSetVector<dim>& critical);


};

} /* namespace Contact */
} /* namespace Dune */

// include implementation
#include "contactobsrestrict.cc"

#endif
