// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_ASSEMBLERS_TWO_BODY_ASSEMBLER_HH
#define DUNE_CONTACT_ASSEMBLERS_TWO_BODY_ASSEMBLER_HH

#include <vector>

#include <dune/solvers/common/boxconstraint.hh>

#include <dune/contact/assemblers/nbodyassembler.hh>
#include <dune/contact/assemblers/dualmortarcouplinghierarchy.hh>
#include <dune/contact/assemblers/dualmortarcoupling.hh>
#include <dune/contact/common/couplingpair.hh>
#include <dune/contact/projections/contactprojection.hh>

namespace Dune {
namespace Contact {

template <class GridType, class VectorType>
class TwoBodyAssembler
    : public NBodyAssembler<GridType,VectorType>
{
    enum {dim = GridType::dimension};

    using Base = NBodyAssembler<GridType, VectorType>;
    using field_type = typename Base::field_type;
    using MatrixBlock = typename Base::MatrixBlock;
    using MatrixType = typename Base::MatrixType;

    using LeafBoundaryPatch = typename Base::LeafBoundaryPatch;
    using LevelBoundaryPatch = BoundaryPatch<typename GridType::LevelGridView>;

    using MyCouplingPair = CouplingPair<GridType, GridType, field_type>;
    using Projection = typename MyCouplingPair::Projection;
    using GridGlueBackend = typename MyCouplingPair::BackEndType;
public:

    /** \brief Default constructor

    Sets the coupling type to NONE and the obstacle distance to 0.
    */
    TwoBodyAssembler(bool hierarchyCoupling=false, field_type coveredArea = 0.99)
        : NBodyAssembler<GridType,VectorType>(2,1,hierarchyCoupling,coveredArea)
    {}

    /** \brief Construct the assembler
     *
     *   \param grid0 The nonmortar grid involved in the contact problem
     *   \param grid1 The mortar grid involved in the contact problem
     *   \param nonmortarPatch The nonmortar boundary on level 0
     *   \param mortarPatch The mortar boundary on level 0
     *   \param obsDistance Maximal distance to look for the second body
     *   \param couplingType The type of the coupling
     *   \param projection A function identifying the nonmortar boundary with parts of the mortar boundary
     *   \param backend A gridglue backend, if not set then ContactMerge is used
     *   \param hierarchyCoupling This boolean determines if the whole hierarchy of mortar matrices should be setup
     *
    */
    TwoBodyAssembler(const GridType& grid0, const GridType& grid1,
                     std::shared_ptr<const LevelBoundaryPatch> nonmortarPatch,
                     std::shared_ptr<const LevelBoundaryPatch> mortarPatch,
                     field_type obsDistance, typename CouplingPairBase::CouplingType couplingType,
                     std::shared_ptr<Projection> projection,
                     bool hierarchyCoupling=false,
                     std::shared_ptr<GridGlueBackend> backend = nullptr,
                     field_type coveredArea = 0.99)
        : NBodyAssembler<GridType,VectorType>(2,1,hierarchyCoupling,coveredArea)
    {
        this->grids_[0] = &grid0;
        this->grids_[1] = &grid1;
        this->coupling_[0].set(0,1, nonmortarPatch, mortarPatch, obsDistance, couplingType, projection, backend);
    }

    /** \brief Set up the assembler object
     *   \param grid0 The nonmortar grid involved in the contact problem
     *   \param grid1 The mortar grid involved in the contact problem
     *   \param nonmortarPatch The nonmortar boundary on level 0
     *   \param mortarPatch The mortar boundary on level 0
     *   \param obsDistance Maximal distance to look for the second body
     *   \param couplingType The type of the coupling
     *   \param projection A function identifying the nonmortar boundary with parts of the mortar boundary
     *   \param backend A gridglue backend, if not set then ContactMerge is used
     *
    */
    void setup(const GridType& grid0, const GridType& grid1,
               std::shared_ptr<const LevelBoundaryPatch> nonmortarPatch,
               std::shared_ptr<const LevelBoundaryPatch> mortarPatch,
               field_type obsDistance,
               typename CouplingPairBase::CouplingType couplingType,
               std::shared_ptr<Projection> projection,
               std::shared_ptr<GridGlueBackend> backend = nullptr);

    void setup() {
        assembleTransferOperator();
        assembleObstacle();
    }
    /** \brief Set up the assembler object using previous settings*/
    void assembleTransferOperator();

    /** \brief Couple the two FE stiffness matrices and transform them to the mortar coordinates */
    void assembleJacobian(const std::array<const MatrixType*, 2>& submat,
                  MatrixType& totalMatrix);

    /** \brief Compute the obstacles and set the hasObstacle bitfield. */
    virtual void assembleObstacle();

    /** \brief Set the contact coupling. */
    void setContactCoupling(std::shared_ptr<typename Base::CouplingType> contactCoupling) {
        this->contactCoupling_[0] = contactCoupling;
    }

protected:
    /** \brief Assemble the mortar transformation matrix.*/
    void computeTransformationMatrix();

};

} /* namespace Contact */
} /* namespace Dune */

#include "twobodyassembler.cc"

#endif
