// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:

#include <dune/istl/matrixindexset.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/io.hh>

#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/type.hh>
#include <dune/geometry/referenceelements.hh>

#include <dune/localfunctions/lagrange/p1.hh>
#include <dune/localfunctions/dualmortarbasis/dualpq1factory.hh>
#include <dune/localfunctions/lagrange/pqkfactory.hh>

#include <dune/grid-glue/merging/contactmerge.hh>

#include <dune/matrix-vector/addtodiagonal.hh>
#include <dune/matrix-vector/algorithm.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/functions/basisgridfunction.hh>

#include <dune/contact/assemblers/linearisedaveragednormalsassembler.hh>
#include <dune/contact/assemblers/linearisedintersectionassembler.hh>

namespace Dune {
namespace Contact {

template <class field_type, class GridType0, class GridType1>
void LargeDeformationCoupling<field_type, GridType0,GridType1>::buildProjection()
{

  // if no merger is set, then create a ContactMerger
    using ContactMerger = Dune::GridGlue::ContactMerge<dimworld, ctype>;
  if (!this->gridGlueBackend_)
    this->gridGlueBackend_ = std::make_shared<ContactMerger>(ctype(this->overlap_), projectionType_);

  // setup gridglue if necessary
  const auto& gridView0 = unrednonmortarBoundary_.gridView();
  const auto& gridView1 = unredmortarBoundary_.gridView();

  using Element0 = typename GridType0::LeafGridView::template Codim<0>::Entity;
  using Element1 = typename GridType1::LeafGridView::template Codim<0>::Entity;

  auto desc0 = [&] (const Element0& e, unsigned int face) {
    return unrednonmortarBoundary_.contains(e,face);
  };

  auto desc1 = [&] (const Element1& e, unsigned int face) {
    return unredmortarBoundary_.contains(e,face);
  };

  this->glue_ = std::make_shared<Glue>(std::make_shared<typename Base::Extractor0>(gridView0, desc0),
                                       std::make_shared<typename Base::Extractor1>(gridView1,desc1),
                                       this->gridGlueBackend_);
  this->glue_->build();

  nonmortarBoundary_.setup(gridView0);
  mortarBoundary_.setup(gridView1);

  // restrict the contact boundary to where the projection could be built
  const auto& indexSet0 = gridView0.indexSet();

  // get all fine grid boundary segments that are totally covered by the grid-glue segments
  using Pair = std::pair<int,int>;
  std::map<Pair, ctype> coveredArea, fullArea;

  // initialize with area of boundary faces
  for (const auto& bIt : unrednonmortarBoundary_) {
    const Pair p(indexSet0.index(bIt.inside()), bIt.indexInInside());
    fullArea[p] = bIt.geometry().volume();
    coveredArea[p] = 0;
  }

  // sum up the remote intersection areas to find out which are totally covered
  for (const auto& rIs : intersections(*this->glue_))
    coveredArea[Pair(indexSet0.index(rIs.inside()), rIs.indexInInside())] += rIs.geometry().volume();

  // add all fine grid faces that are totally covered by the contact mapping
  for (const auto& bIt : unrednonmortarBoundary_) {
    const auto& inside = bIt.inside();
    if(coveredArea[Pair(indexSet0.index(inside), bIt.indexInInside())]/
        fullArea[Pair(indexSet0.index(inside), bIt.indexInInside())] >= this->coveredArea_)
      nonmortarBoundary_.addFace(inside, bIt.indexInInside());
  }

  for (const auto& rIs : intersections(*this->glue_))
    if (nonmortarBoundary_.contains(rIs.inside(), rIs.indexInInside()))
      mortarBoundary_.addFace(rIs.outside(), rIs.indexInOutside());

  std::cout << "Reduced Nonmortar contains " << nonmortarBoundary_.numFaces() << " of "
            << unrednonmortarBoundary_.numFaces() << " original nonmortar faces " << std::endl;
}

template <class field_type, class GridType0, class GridType1>
typename LargeDeformationCoupling<field_type, GridType0, GridType1>::ObstacleVector LargeDeformationCoupling<field_type, GridType0,GridType1>::assembleObstacles(bool build)
{
  if (build)
    buildProjection();

  bool isNormalProjection = (projectionType_ == ProjectionType::OUTER_NORMAL);

  // get index sets
  const auto& gridView0 = grid0_->leafGridView();
  const auto& indexSet0 = gridView0.indexSet();

  // cache for the dual functions on the boundary
  using DualFECache =DualPQ1LocalFiniteElementCache<ctype, field_type, dim, true>;
  DualFECache dualCache;

  auto globalToLocal = nonmortarBoundary_.makeGlobalToLocal();

  // assemble weak obstacles
  ObstacleVector weakObstacle(nonmortarBoundary_.numVertices());
  weakObstacle = 0;

  GeometryType remoteGt = Dune::GeometryTypes::simplex(dim-1);

  std::vector<Coordinate> contactNormals;
  if (isNormalProjection)
    contactNormals = unrednonmortarBoundary_.getNormals();
  else
    contactNormals = unredmortarBoundary_.getNormals();

  using Basis = P1NodalBasis<typename GridType0::LeafGridView, field_type>;
  Basis basis((isNormalProjection) ? grid0_->leafGridView() : grid1_->leafGridView());
  BasisGridFunction<Basis, std::vector<Coordinate> > normalField(basis, contactNormals);

  for (const auto& rIs : intersections(*this->glue_)) {

    // types of the elements supporting the boundary segments in question
    const auto& inside = rIs.inside();

    if (!nonmortarBoundary_.contains(inside,rIs.indexInInside()))
      continue;

    const auto& outside = rIs.outside();
    GeometryType nmElemType = inside.type();

    const auto& nmRef = ReferenceElements<ctype, dim>::general(nmElemType);
    const auto& mRef = ReferenceElements<ctype, dim>::general(outside.type());

    auto nmFaceType = nmRef.type(rIs.indexInInside(), 1);
    auto mFaceType  = mRef.type(rIs.indexInOutside(), 1);

    // Select a quadrature rule
    // 2 in 2d and for integration over triangles in 3d.  If one (or both) of the two faces involved
    // are quadrilaterals, then the quad order has to be risen to 3 (4).
    int quadOrder = 4 + (!nmFaceType.isSimplex()) + (!mFaceType.isSimplex());
    const auto& quad = QuadratureRuleCache<ctype, dim-1>::rule(remoteGt, quadOrder, false);

    const auto& dualFiniteElement = dualCache.get(nmElemType);
    using LocalBasisType = typename DualFECache::FiniteElementType::Traits::LocalBasisType;

    std::vector<typename LocalBasisType::Traits::RangeType> dualQuadValues;

    const auto& rGeom = rIs.geometry();
    const auto& rGeomOutside = rIs.geometryOutside();
    const auto& rGeomInInside = rIs.geometryInInside();
    const auto& rGeomInOutside = rIs.geometryInOutside();

    std::vector<int> nmFaceIdx(nmRef.size(rIs.indexInInside(),1,dim));
    for (size_t i=0; i<nmFaceIdx.size(); i++)
      nmFaceIdx[i] = nmRef.subEntity(rIs.indexInInside(), 1, i, dim);

    for (const auto& pt : quad) {

      auto intElement = rGeom.integrationElement(pt.position());
      auto nmQP = rGeomInInside.global(pt.position());

      Coordinate normalQp;
      if (isNormalProjection)
        normalField.evaluateLocal(inside, nmQP, normalQp);
      else
        normalField.evaluateLocal(outside, rGeomInOutside.global(pt.position()), normalQp);
      normalQp /= normalQp.two_norm();

      // the gap
      auto gap = (rGeom.global(pt.position()) - rGeomOutside.global(pt.position()))*normalQp;
      gap *= intElement*pt.weight();

      dualFiniteElement.localBasis().evaluateFunction(nmQP, dualQuadValues);

      // loop over all dual shape functions
      for (const auto& idx : nmFaceIdx) {

        auto globalIdx = indexSet0.subIndex(inside, idx, dim);
        int rowIdx = globalToLocal[globalIdx];

        weakObstacle[rowIdx][0] += dualQuadValues[idx] * gap;
      }
    }
  }

  if (isNormalProjection)
      weakObstacle *= -1;

  return weakObstacle;
}

template <class field_type, class GridType0, class GridType1>
void LargeDeformationCoupling<field_type, GridType0,GridType1>::setup()
{
  auto dyadic = [](const Coordinate& a, const Coordinate& b)
  {
    MatrixBlock c(0);
    for (int i=0; i < dim; i++)
      c[i].axpy(a[i],b);

    return c;
  };
  namespace MV = Dune::MatrixVector;

  const auto& indexSet0 = grid0_->leafIndexSet();
  const auto& indexSet1 = grid1_->leafIndexSet();
  size_t size0 = indexSet0.size(dim);
  size_t size1 = indexSet1.size(dim);

  bool isNormalProjection = (projectionType_ == ProjectionType::OUTER_NORMAL);

  // cache for the dual functions on the element
  using DualFECache = DualPQ1LocalFiniteElementCache<ctype, field_type, dim,true>;
  DualFECache dualCache;

  using DualFaceFECache = DualPQ1LocalFiniteElementCache<ctype, field_type, dim-1>;
  DualFaceFECache dualFaceCache;

  // cache for the Lagrange shapefunctions element
  using LagrangeFECache = PQkLocalFiniteElementCache<ctype, field_type, dim, 1>;
  LagrangeFECache femCache;

  // cache for Lagrange shape functions on the faces we need them to compute
  // the linearisation of the local geometries of the remote intersecitons more stable
  using LagrangeFaceFECache = PQkLocalFiniteElementCache<ctype, field_type, dim-1, 1>;
  LagrangeFaceFECache femFaceCache;

  // Create mapping from the global idx to local ones on the contact boundary
  auto nmGlobalToLocal = nonmortarBoundary_.makeGlobalToLocal();
  auto mGlobalToLocal = mortarBoundary_.makeGlobalToLocal();
  const auto& normalGlobalToLocal = (isNormalProjection) ? nmGlobalToLocal : mGlobalToLocal;

  // get the nodal averaged unit normals
  auto avNormals = (isNormalProjection) ? unrednonmortarBoundary_.getNormals()
                                        : unredmortarBoundary_.getNormals();

  int sign = (isNormalProjection) ? 1 : -1;

  size_t offsetDom = (isNormalProjection) ? 0 : size0;
  size_t offsetTar = (isNormalProjection) ? size0 : 0;

  using P1Basis = P1NodalBasis<typename GridType0::LeafGridView,field_type>;
  auto basis = (isNormalProjection) ? P1Basis(grid0_->leafGridView())
                                    : P1Basis(grid1_->leafGridView());
  BasisGridFunction<P1Basis, std::vector<Coordinate> > normalField(basis, avNormals);

  // compute derivative of the averaged unit normals w.r.t the nonmortar coefficients
  MatrixType linAvNormals;
  // and of the mortar field, this is needed when the projection is linearised
  if (isNormalProjection)
    LinearisedAveragedNormalsAssembler::assemble(nonmortarBoundary_, unrednonmortarBoundary_, linAvNormals);
  else
    LinearisedAveragedNormalsAssembler::assemble(mortarBoundary_, unredmortarBoundary_, linAvNormals);

  // compute derivative of the remote vertices w.r.t the nonmortar coefficients
  using ContactMerger = Dune::GridGlue::ContactMerge<dimworld, ctype>;
  MatrixType linRemoteVertices;

  /////////////////////////////////7
  /// Wants to have the lin outer normals of the domain grid in the projection

  LinearisedIntersectionAssembler<Glue> remoteAssembler(this->glue_, isNormalProjection);
  remoteAssembler.assemble(nonmortarBoundary_, unrednonmortarBoundary_,
                           mortarBoundary_, unredmortarBoundary_,
                           std::dynamic_pointer_cast<ContactMerger>(this->gridGlueBackend_)->getOverlap(),
                           std::cos(std::dynamic_pointer_cast<ContactMerger>(this->gridGlueBackend_)->minNormalAngle()),
                           linRemoteVertices, linAvNormals);

  //   Get the occupation structure for the linearization matrix
  setupIndexPattern(linRemoteVertices, linAvNormals, nmGlobalToLocal, mGlobalToLocal);

  this->weakObstacle_.resize(nonmortarBoundary_.numVertices());
  this->weakObstacle_ = 0;

  // the remote geometry type is always a simplex
  auto remoteGt = GeometryTypes::simplex(dim-1);

  // get local basis for the remote intersection type, this is always a simplex
  // cache for the remote shapefunctions on the boundary
  P1LocalBasis<ctype, field_type, dim-1> remoteLfe;
  std::vector<FieldVector<field_type, 1> > remoteQuadValues;

  const auto& rIsIndexSet = this->glue_->indexSet();

  // loop over remote intersections
  for (const auto& rIs : intersections(*this->glue_)) {

    const auto& inside = rIs.inside();

    if (!nonmortarBoundary_.contains(inside, rIs.indexInInside()))
      continue;

    const auto& outside = rIs.outside();

    // TODO Adjust quadrature
    // 4 seems to be required
    int quadOrder = 4;
    const auto& quad = QuadratureRuleCache<ctype, dim-1>::rule(remoteGt, quadOrder, false);

    // get local basis functions for the dual and mortar elements
    const auto& nmLfe = femCache.get(inside.type()).localBasis();
    const auto& mLfe = femCache.get(outside.type()).localBasis();
    const auto& dualLfe = dualCache.get(inside.type()).localBasis();

    std::vector<FieldVector<field_type, 1> > mValues, nmValues, dualValues;

    // create a mapping to check which element basis functions correspond to the face
    const auto& nmRef = ReferenceElements<ctype, dim>::general(inside.type());
    std::vector<int> nmIdx(nmRef.size(rIs.indexInInside(), 1, dim));
    std::vector<int> nmglobalIdx(nmIdx.size());
    for (size_t i=0; i<nmIdx.size(); i++) {
      nmIdx[i] = nmRef.subEntity(rIs.indexInInside(), 1, i, dim);
      nmglobalIdx[i] = indexSet0.subIndex(inside, nmIdx[i], dim);
    }

    const auto& mRef = ReferenceElements<ctype,dim>::general(outside.type());
    std::vector<int> mIdx(mRef.size(rIs.indexInOutside(), 1, dim));
    std::vector<int> mglobalIdx(mIdx.size());
    for (size_t i=0; i< mIdx.size(); i++) {
      mIdx[i] = mRef.subEntity(rIs.indexInOutside(), 1, i, dim);
      mglobalIdx[i] = indexSet1.subIndex(outside, mIdx[i], dim);
    }

    // get finite element of the faces
    const auto& nmFaceLfe = femFaceCache.get(nmRef.type(rIs.indexInInside(),1)).localBasis();
    const auto& dualFaceLfe = dualFaceCache.get(nmRef.type(rIs.indexInInside(),1)).localBasis();
    const auto& mFaceLfe = femFaceCache.get(mRef.type(rIs.indexInOutside(),1)).localBasis();

    // compute local geometries for the faces
    typename GridType0::LeafGridView::Intersection nmIntersection;
    for (auto&& is0 : intersections(grid0_->leafGridView(), inside))
      if (is0.indexInInside()==rIs.indexInInside()) {
        nmIntersection = std::move(is0);
        break;
      }

    typename GridType1::LeafGridView::Intersection mIntersection;
    for (auto&& is1 : intersections(grid1_->leafGridView(), outside))
      if (is1.indexInInside()==rIs.indexInOutside()) {
        mIntersection = std::move(is1);
        break;
      }

    const auto& nmLocalFaceGeometry = nmIntersection.geometryInInside();
    const auto& mLocalFaceGeometry = mIntersection.geometryInInside();

    // global geometry of the inside and outside element
    const auto& nmElemGeom = inside.geometry();
    const auto& mElemGeom = outside.geometry();

    // geometries of the remote intersection
    const auto& rGlobGeom = rIs.geometry();
    const auto& rGeomOut = rIs.geometryInOutside();
    const auto& rGeomIn = rIs.geometryInInside();

    CoordinateVector nmCorners(nmIdx.size()), mCorners(mIdx.size());
    for (size_t i=0; i<nmIdx.size(); i++)
      nmCorners[i] = nmElemGeom.corner(nmIdx[i]);

    for (size_t i=0; i<mIdx.size(); i++)
      mCorners[i] = mElemGeom.corner(mIdx[i]);

    // By now simplices are hardwired,
    size_t nmSize = nmCorners.size();
    size_t mSize = mCorners.size();

    const auto& normalIdx = (isNormalProjection) ? nmIdx : mIdx;
    const auto& normalGlobalIdx = (isNormalProjection) ? nmglobalIdx : mglobalIdx;
    const auto& oppositeGlobalIdx = (isNormalProjection) ? mglobalIdx : nmglobalIdx;
    const auto& normalCorners = (isNormalProjection) ? nmCorners : mCorners;
    const auto& oppositeCorners = (isNormalProjection) ? mCorners : nmCorners;

    // compute non-mortar edges
    size_t opSize = (isNormalProjection) ? mSize : nmSize;
    CoordinateVector edges(opSize);
    for (size_t i=0; i < opSize; i++)
      edges[i] = oppositeCorners[(i+2)%opSize] - oppositeCorners[(i+1)%opSize];

    Coordinate oppositeNormal;
    if (dim==3) {
      // we want 1-0 cross 2-0
      oppositeNormal = MV::crossProduct(edges[1], edges[2]);
    } else if (dim==2)
      oppositeNormal = {{edges[1][1], edges[0][0]}};

    // compute linearisation of the projected mortar vertices
    std::vector<MatrixType> linVertex(normalIdx.size());
    CoordinateVector projVertex(normalIdx.size());

    // the linearisation geometry requires linearisation of forward projected vertices
    // for outer normal, we project the nonmortar corners, else the mortar corners
    for (size_t i=0; i < normalIdx.size(); ++i)
      AnalyticLocalIntersectionAssembler<Glue, MatrixType>::linearisedForwardProjection(
          linVertex[i], projVertex[i], offsetDom, offsetTar, size0+size1, normalGlobalIdx[i], normalCorners[i],
          avNormals[normalGlobalIdx[i]], linAvNormals[normalGlobalToLocal[normalGlobalIdx[i]]],
          oppositeGlobalIdx, oppositeCorners[0], edges, oppositeNormal);

    ////////////////////////////////////////////////////////////////////////
    //  Derivation of the integration element w.r.t nonmortar coefficients
    ///////////////////////////////////////////////////////////////////////

    int realRemoteIdx = rIsIndexSet.index(rIs);
    int rIdx = realRemoteIdx*dim;

    // the integration element is assumed to be 2*remoteVolume
    // it is given by the norm of the cross-product of the edges
    // thus it involves the linearisation of the remote corners
    MatrixIndexSet areaIndexSet(1,size0+size1);
    for (int i=0; i<dim; i++) {
      const auto& row = linRemoteVertices[rIdx+i];
      const auto& cEndIt = row.end();
      for (auto cIt=row.begin(); cIt != cEndIt; cIt++)
        areaIndexSet.add(0, cIt.index());
    }

    JacobianType linIntElem;
    areaIndexSet.exportIdx(linIntElem);
    linIntElem=0;

    // make vector containing the edge vectors of the remote intersection
    CoordinateVector edge(dim-1);
    for (size_t i=0; i<edge.size(); i++)
      edge[i] = rGlobGeom.corner(i+1) - rGlobGeom.corner(0);

    // a normal of the remote intersection
    Coordinate n;
    if (dim ==3)
      n = MV::crossProduct(edge[0], edge[1]);
    else if (dim==2)
      n = edge[0]; // this is not the normal but we don't need the normal
    n /= n.two_norm();

    // terms that appear in the linearisation
    CoordinateVector nTimesEdge(dim);
    if (dim==3) {
      nTimesEdge[2] = MV::crossProduct(n, edge[0]);
      nTimesEdge[1] = MV::crossProduct(edge[1], n);
      nTimesEdge[0] = 0; nTimesEdge[0] -=nTimesEdge[2]+nTimesEdge[1];
    } else if (dim==2) {
      nTimesEdge[0] = 0; nTimesEdge[0].axpy(-1, n);
      nTimesEdge[1] = 0; nTimesEdge[1].axpy(1, n);
    }

    for (int i=0; i<dim; i++) {
      const auto& cEndIt = linRemoteVertices[rIdx+i].end();
      for (auto cIt=linRemoteVertices[rIdx+i].begin(); cIt != cEndIt; cIt++)
        cIt->umtv(nTimesEdge[i], linIntElem[0][cIt.index()][0]);
    }
    auto intElem = (dim-1)*rGlobGeom.volume();

    // store integral of the products between mortar and dual functions
    std::vector<VectorType> sfIntM(nmSize),sfIntNM(nmSize);
    std::vector<VectorType> gapVectors(nmSize);
    ObstacleVector gapValues(nmSize);
    gapValues = 0;

    for (size_t k=0; k<nmSize; k++) {
      sfIntM[k].resize(mSize);
      sfIntNM[k].resize(nmSize);
      sfIntM[k] = 0;
      sfIntNM[k] = 0;
      gapVectors[k].resize((isNormalProjection) ? nmSize : mSize);
      gapVectors[k] = 0;
    }

    // loop over quadrature points
    for (const auto& pt : quad) {

      // (dim-1)-quadrature point in the dim-reference element coordinates
      auto nmQP = rGeomIn.global(pt.position());
      auto mQP = rGeomOut.global(pt.position());

      // QP on the nm face
      auto nmFaceQP = nmLocalFaceGeometry.local(nmQP);
      auto mFaceQP = mLocalFaceGeometry.local(mQP);

      //evaluate shape functions at the quadrature point
      std::vector<FieldMatrix<field_type, 1, dim-1> > nmRefFaceJacobians, mRefFaceJacobians, dualRefFaceJacobians;

      remoteLfe.evaluateFunction(pt.position(), remoteQuadValues);
      mLfe.evaluateFunction(mQP, mValues);
      nmLfe.evaluateFunction(nmQP, nmValues);
      dualLfe.evaluateFunction(nmQP, dualValues);
      mFaceLfe.evaluateJacobian(mFaceQP, mRefFaceJacobians);
      nmFaceLfe.evaluateJacobian(nmFaceQP, nmRefFaceJacobians);
      dualFaceLfe.evaluateJacobian(nmFaceQP, dualRefFaceJacobians);

      const auto& normalFEVal = (isNormalProjection) ? nmValues : mValues;

      // resort local jacobians to match the orientation within the undelying element
      size_t count(0);
      std::vector<FieldVector<field_type, dim-1> > nmFaceJacobians(nmRefFaceJacobians.size()), mFaceJacobians(mRefFaceJacobians.size());
      std::vector<FieldVector<field_type, dim-1> > dualFaceJacobians(dualRefFaceJacobians.size());
      for (size_t i=0; i<nmSize; i++) {
        for (size_t j=0; j<nmSize; j++)
          if ((nmCorners[i]-nmIntersection.geometry().corner(j)).two_norm()<1e-10) {
            // transform gradients?
              //nmIntersection.geometry().jacobianInverseTransposed(nmQP).mtv(nmRefFaceJacobians[j][0], nmFaceJacobians[i]);
              //nmIntersection.geometry().jacobianInverseTransposed(nmQP).mtv(dualRefFaceJacobians[j][0], dualFaceJacobians[i]);
            nmFaceJacobians[i] = nmRefFaceJacobians[j][0];
            dualFaceJacobians[i] = dualRefFaceJacobians[j][0];
            count++;
            break;
          }
      }

      assert(count == nmFaceJacobians.size());

      count= 0;
      for (size_t i=0; i<mSize; i++) {
        for (size_t j=0; j<mSize; j++)
          if ((mCorners[i]-mIntersection.geometry().corner(j)).two_norm()<1e-10) {
            //  mIntersection.geometry().jacobianInverseTransposed(mQP).mtv(mRefFaceJacobians[j][0], mFaceJacobians[i]);
            mFaceJacobians[i] = mRefFaceJacobians[j][0];
            count++;
            break;
          }
      }

      assert(count == mFaceJacobians.size());

      Coordinate normalQp;
      if (isNormalProjection)
        normalField.evaluateLocal(inside, nmQP, normalQp);
      else
        normalField.evaluateLocal(outside, mQP, normalQp);

      auto normNormalQp = normalQp.two_norm();
      auto unitNormalQp = normalQp;
      unitNormalQp /= normNormalQp;

      for (size_t j=0; j < nmSize; ++j) {

        // the integrals of the products of dual and nonmortar functions
        for (size_t k=0; k < nmSize; ++k)
          sfIntNM[j][k].axpy(intElem*dualValues[nmIdx[j]]*nmValues[nmIdx[k]]*pt.weight(), unitNormalQp);

        // the integrals of the products of dual and mortar functions
        for (size_t k=0; k<mSize; k++)
          sfIntM[j][k].axpy(intElem*dualValues[nmIdx[j]]*mValues[mIdx[k]]*pt.weight(), unitNormalQp);
      }

      // The gap function at quadrature point
      auto gapVector = mElemGeom.global(mQP) - nmElemGeom.global(nmQP);
      // the sign switches for the two types of projections
      gapVector *= sign;

      for (size_t j = 0; j < nmSize; ++j) {
        for (size_t k = 0; k < gapVectors[j].size(); ++k)
          gapVectors[j][k].axpy(pt.weight()*intElem*dualValues[nmIdx[j]]*normalFEVal[normalIdx[k]]/normNormalQp, gapVector);

        gapValues[j]+= pt.weight()*dualValues[nmIdx[j]]*(gapVector*unitNormalQp);
      }

      ////////////////////////////////////////////////////////////
      ///          Derivation of local geometries Part A
      ///////////////////////////////////////////////////////////////

      /////////////////////////////////////////////////////////
      ///     To compute the linearisation of the geometryInInside method
      ///     we derive the following equation:
      ///     nonmortarGlobalQP = rGlobQP
      /// //////////////////////////////////////////////////////////

      // the index pattern of the non-mortar linearised geometry
      // and the integration element are the same
      MatrixType nmLinGeometry;
      MatrixIndexSet nmGeomIndexSet(1, size0+size1);
      for (int i=0; i < dim; ++i)
          MV::sparseRangeFor(linRemoteVertices[rIdx+i],
              [&](auto&&, auto&& idx){nmGeomIndexSet.add(0, idx);});

      for (const auto& nmIdx : nmglobalIdx)
        nmGeomIndexSet.add(0, nmIdx);

      nmGeomIndexSet.exportIdx(nmLinGeometry);
      nmLinGeometry = 0;

      // from the product rule of the left side
      for (size_t i=0; i < nmSize; ++i)
        MV::addToDiagonal(nmLinGeometry[0][nmglobalIdx[i]], -nmValues[nmIdx[i]]);

      for (int i=0; i < dim; ++i)
          MV::sparseRangeFor(linRemoteVertices[rIdx+i], [&](auto&& col, auto&& idx){
              nmLinGeometry[0][idx].axpy(remoteQuadValues[i], col);});

      auto nmDualCovBasis = dualCovBasis(nmFaceJacobians, nmCorners);

      //////////////////////////////////////////////////////
      /// Local Mortar: derive MortarQP = NonmortarQP at proj. corners  (for Outer Normal)
      ///               or Remote QP = Mortar QP at proj. corners (for closest point)
      ////////////////////////////////////////////////////

      MatrixIndexSet mGeomIndexSet(1, size0+size1);
      mGeomIndexSet.import(nmLinGeometry);
      for (const auto& linVert : linVertex)
          MV::sparseRangeFor(linVert[0],
              [&](auto&&, auto&& idx){mGeomIndexSet.add(0, idx);});

      MatrixType mLinGeometry;
      mGeomIndexSet.exportIdx(mLinGeometry);
      mLinGeometry = 0;

      // from the product rule of the left side
      for (size_t i=0; i < linVertex.size(); ++i)
        MV::sparseRangeFor(linVertex[i][0], [&](auto&& col, auto&& idx){
          mLinGeometry[0][idx].axpy(sign*normalFEVal[normalIdx[i]], col);});

      if (isNormalProjection) {
        for (size_t i=0; i < mSize; ++i)
          MV::addToDiagonal(mLinGeometry[0][size0 + mglobalIdx[i]], -mValues[mIdx[i]]);
      } else {
        for (int i=0; i < dim; i++)
          MV::sparseRangeFor(linRemoteVertices[rIdx+i], [&](auto&& col, auto&& idx){
            mLinGeometry[0][idx].axpy(remoteQuadValues[i], col);});
      }

      auto mDualCovBasis = (isNormalProjection) ? dualCovBasis(mFaceJacobians, mCorners)
                                                : dualCovBasis(mFaceJacobians, projVertex);

      // at this point for the outer normal projection the part in mortar geometry is missing
      // that depends on linearised nonmortar geometry

      /////////////////////////////////////////////////////////////////
      // Derivation of local geometries Part B
      /////////////////////////////////////////////////////////////////////////

      // loop over the dual functions
      for (size_t j=0; j<nmSize; j++) {

        int rowIdx = nmGlobalToLocal[nmglobalIdx[j]];

        /////////////////////////////////////////////////////////////////////////
        // part from the local nonmortar geometry
        ///////////////////////////////////////////////////////////////////////

        auto cNmEndIt = nmLinGeometry[0].end();
        for (auto cIt = nmLinGeometry[0].begin();cIt != cNmEndIt; ++cIt) {

          Coordinate vecNm(0),vecDual(0);
          for (size_t p=0; p<nmDualCovBasis.size(); p++) {
            Coordinate dummy(0);
            cIt->mtv(nmDualCovBasis[p], dummy);
            vecDual.axpy(dualFaceJacobians[j][p], dummy);
            vecNm.axpy(nmFaceJacobians[j][p], dummy);
          }

          // non-mortar point
          for (size_t k=0; k<nmSize; k++) {

            int rowIdxk = nmGlobalToLocal[nmglobalIdx[k]];
            auto scal = -sign*(unitNormalQp*nmCorners[j])*pt.weight()*intElem;

            if (cIt.index() < size0)
              exactNonmortarMatrix_[rowIdxk][cIt.index()][0].axpy(scal*dualValues[nmIdx[k]], vecNm);
            else
              exactMortarMatrix_[rowIdxk][cIt.index()-size0][0].axpy(scal*dualValues[nmIdx[k]],vecNm);
          }

          // linearisation of dual function
          auto scal = (gapVector*unitNormalQp)*pt.weight()*intElem;
          if (cIt.index() < size0)
            exactNonmortarMatrix_[rowIdx][cIt.index()][0].axpy(scal, vecDual);
          else
            exactMortarMatrix_[rowIdx][cIt.index() - size0][0].axpy(scal, vecDual);

          // for outer normal projection, add the last part to the lin mortar geometry,
          // which involves the linearised non-mortar geometry
          if (isNormalProjection)
            mLinGeometry[0][cIt.index()] += dyadic(projVertex[j], vecNm);
        }
      }

      // loop over the dual functions
      for (size_t j=0; j<nmSize; j++) {

        int rowIdx = nmGlobalToLocal[nmglobalIdx[j]];

        ////////////////////////////////////////////
        //  Derivation of the local mortar geometry
        ////////////////////////////////////////////

        const auto& cEndMGeom = mLinGeometry[0].end();
        for (size_t k=0; k<mSize; k++) {

          auto scal = sign*(unitNormalQp*mCorners[k])*pt.weight()*intElem*dualValues[nmIdx[j]];
          for (auto cIt = mLinGeometry[0].begin(); cIt != cEndMGeom; cIt++) {

            // derivation of the un-normed av. normal FE function and the mortar point
            Coordinate vec(0);
            for (size_t p=0; p<mDualCovBasis.size(); p++) {
              Coordinate dummy(0);
              cIt->mtv(mDualCovBasis[p], dummy);
              vec.axpy(mFaceJacobians[k][p], dummy);
            }
            if (cIt.index() < size0)
              exactNonmortarMatrix_[rowIdx][cIt.index()][0].axpy(scal, vec);
            else
              exactMortarMatrix_[rowIdx][cIt.index() - size0][0].axpy(scal, vec);
          }
        }

        ////////////////////////////////////////////
        //  Derivation of the normal field and its norm
        ////////////////////////////////////////////

        auto& normalLin = (isNormalProjection) ? exactNonmortarMatrix_ : exactMortarMatrix_;
        const auto& normalGeometry = (isNormalProjection) ? nmLinGeometry : mLinGeometry;
        const auto& normalDualCovBasis = (isNormalProjection) ? nmDualCovBasis : mDualCovBasis;
        const auto& normalFaceJacobians = (isNormalProjection) ? nmFaceJacobians : mFaceJacobians;

        const auto& cEndNGeom = normalGeometry[0].end();
        for (size_t k=0; k < normalGlobalIdx.size(); k++) {

          // derivation of normal fe function
          auto scal = (gapVector*avNormals[normalGlobalIdx[k]])*pt.weight()*intElem*dualValues[nmIdx[j]]/normNormalQp;

          // derivation of the norm in the denominator
          auto scalNor = -(normalQp*avNormals[normalGlobalIdx[k]])*pt.weight()*intElem*dualValues[nmIdx[j]]/std::pow(normNormalQp,3);
          for (auto cIt = normalGeometry[0].begin(); cIt != cEndNGeom; cIt++) {

            // derivation of the un-normed av. normal FE function and the mortar point
            Coordinate vec(0);
            for (size_t p=0; p<normalDualCovBasis.size(); p++) {
              Coordinate dummy(0);
              cIt->mtv(normalDualCovBasis[p], dummy);
              vec.axpy(normalFaceJacobians[k][p], dummy);
            }
            if (cIt.index() < size0)
              exactNonmortarMatrix_[rowIdx][cIt.index()][0].axpy(scal, vec);
            else
              exactMortarMatrix_[rowIdx][cIt.index() - size0][0].axpy(scal, vec);

            // first part of the linearisation of the averaged normal field
            // at the quadrature point the normal has to be norm again and
            // this division has to be linearised to
            vec *= scalNor;
            auto linNorm = dyadic(normalQp,vec);

            if (cIt.index() < size0)
              linNorm.umtv(gapVector,exactNonmortarMatrix_[rowIdx][cIt.index()][0]);
            else
              linNorm.umtv(gapVector,exactMortarMatrix_[rowIdx][cIt.index()-size0][0]);
          }

          // second part of linearisation of the average normal norm
          // derivation of norm includes the average normal
          auto idx = normalGlobalToLocal[normalGlobalIdx[k]];
          const auto& cEnd = linAvNormals[idx].end();
          // The linearised normals are computed w.r.t local indices
          for (auto cI = linAvNormals[idx].begin(); cI != cEnd; cI++) {
            Coordinate avNorLin;
            cI->mtv(normalQp, avNorLin);
            avNorLin *= -normalFEVal[normalIdx[k]]*dualValues[nmIdx[j]]*pt.weight()*intElem/std::pow(normNormalQp,3);

            auto linNorm = dyadic(normalQp, avNorLin);
            linNorm.umtv(gapVector, normalLin[rowIdx][cI.index()][0]);
          }
        }
      }
    }

    /////////////////////////////////////////////
    //  Derivation of the av. normals in the normal field (indep. of QP)
    //////////////////////////////////////////////

    // linearisation of the average nodal normals
    auto& normalLin = (isNormalProjection) ? exactNonmortarMatrix_ : exactMortarMatrix_;
    for (size_t j=0; j<nmSize; j++)
      for (size_t k=0; k < normalGlobalIdx.size(); k++) {

        int rowIdx = nmGlobalToLocal[nmglobalIdx[j]];
        int normalIdx = normalGlobalToLocal[normalGlobalIdx[k]];

        MV::sparseRangeFor(linAvNormals[normalIdx], [&](auto&& col, auto&& idx){
          col.umtv(gapVectors[j][k], normalLin[rowIdx][idx][0]);});
      }

    ///////////////////////////////////////////////////////////////////////////////
    //      Derivation of the integration element and the deformation coefficients
    /////////////////////////////////////////////////////////////////////////////////

    const auto& cEnd = linIntElem[0].end();

    for (size_t j=0; j<nmSize; j++) {
      int rowIdx = nmGlobalToLocal[nmglobalIdx[j]];

      for (auto cIt = linIntElem[0].begin(); cIt!= cEnd; cIt++)
        if (cIt.index() <size0)
          exactNonmortarMatrix_[rowIdx][cIt.index()][0].axpy(gapValues[j],(*cIt)[0]);
        else
          exactMortarMatrix_[rowIdx][cIt.index() - size0][0].axpy(gapValues[j],(*cIt)[0]);
    }

    // loop over all nonmortar shape functions
    for (size_t j=0; j<nmSize; j++) {

      int rowIdx = nmGlobalToLocal[nmglobalIdx[j]];

      this->weakObstacle_[rowIdx][0] += gapValues[j]*intElem;

      // derivation w.r.t. the deformation  coefficient,
      for (size_t k=0; k<nmSize; k++)
        exactNonmortarMatrix_[rowIdx][nmglobalIdx[k]][0].axpy(-sign, sfIntNM[j][k]);

      // mortar part
      for (size_t k=0; k<mSize; k++)
        exactMortarMatrix_[rowIdx][mglobalIdx[k]][0].axpy(sign, sfIntM[j][k]);
    }
  }
}

template <class field_type, class GridType0, class GridType1>
void LargeDeformationCoupling<field_type, GridType0,GridType1>::setupIndexPattern(
                                     const MatrixType& linRemoteVertices,
                                     const MatrixType& linAvNormals,
                                     const std::vector<int>& nmGlobalToLocal,
                                     const std::vector<int>& mGlobalToLocal)
{

  namespace MV = Dune::MatrixVector;
  bool isNormalProjection = (projectionType_ == ProjectionType::OUTER_NORMAL);

  const auto& rIsIndexSet = this->glue_->indexSet();

  int nObstacles = nonmortarBoundary_.numVertices();

  const auto& indexSet0 = grid0_->leafIndexSet();
  const auto& indexSet1 = grid1_->leafIndexSet();
  size_t size0(indexSet0.size(dim)),size1(indexSet1.size(dim));

  MatrixIndexSet nmIndices(nObstacles, size0);
  MatrixIndexSet mIndices(nObstacles, size1);

  // loop over all intersections
  for (const auto& rIs : intersections(*this->glue_)) {

    const auto& inside = rIs.inside();
    const auto& outside = rIs.outside();

    // if intersection is not contained then skip it
    if (!nonmortarBoundary_.contains(inside, rIs.indexInInside()))
      continue;

    const auto& nmRef = ReferenceElements<ctype, dim>::general(inside.type());
    const auto& mRef = ReferenceElements<ctype, dim>::general(outside.type());

    int nmVertices = nmRef.size(dim);
    int mVertices = mRef.size(dim);

    for (int j=0; j<nmVertices; j++) {

      int nmGlobalIdx = indexSet0.subIndex(inside, j, dim);
      int rowIdx = nmGlobalToLocal[nmGlobalIdx];

      // if the vertex is not contained in the restricted contact boundary then dismiss it
      if (rowIdx == -1)
        continue;

      nmIndices.add(rowIdx, nmGlobalIdx);

      // coupling with nonmortar dofs
      for (int k=j+1; k<nmVertices; k++) {

        int colIdxk = indexSet0.subIndex(inside, k, dim);
        int rowIdxk = nmGlobalToLocal[colIdxk];

        // if the vertex is not contained in the restricted contact boundary then dismiss it
        if (rowIdxk != -1) {

          // these should already be entered
          nmIndices.add(rowIdx, colIdxk);
          nmIndices.add(rowIdxk, nmGlobalIdx);
        }
      }

      // coupling with mortar dofs
      for (int k=0; k<mVertices; k++) {

        int colIdx = indexSet1.subIndex(outside, k, dim);
        // if the vertex is not contained in the restricted contact boundary then dismiss it
        if (!mortarBoundary_.containsVertex(colIdx))
          continue;

        mIndices.add(rowIdx, colIdx);
      }

      size_t size = (isNormalProjection) ? nmVertices : mVertices;
      auto& normalIndices = (isNormalProjection) ? nmIndices : mIndices;
      for (size_t k = 0; k < size; ++k) {

        int localIdx = (isNormalProjection) ? nmGlobalToLocal[indexSet0.subIndex(inside, k, dim)]
                                             : mGlobalToLocal[indexSet1.subIndex(outside, k, dim)];
        if (localIdx < 0)
          continue;
        MV::sparseRangeFor(linAvNormals[localIdx], [&](auto&&, auto&& idx){
          normalIndices.add(rowIdx, idx);});
      }

      // add all indices of the linearised remote vertices, these are combined in one matrix
      int rIdx = rIsIndexSet.index(rIs)*dim;
      for (int k=0; k<dim; k++)
        MV::sparseRangeFor(linRemoteVertices[rIdx+k], [&](auto&&, auto&& idx) {
          if (idx < size0)
            nmIndices.add(rowIdx, idx);
          else
            mIndices.add(rowIdx, idx-size0);});
    }
  }

  nmIndices.exportIdx(exactNonmortarMatrix_);
  mIndices.exportIdx(exactMortarMatrix_);
  exactNonmortarMatrix_ = 0;
  exactMortarMatrix_ = 0;
}


} /* namespace Contact */
} /* namespace Dune */
