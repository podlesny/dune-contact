// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:
#include <memory>

#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/umfpack.hh>
#include <dune/matrix-vector/transpose.hh>
#include <dune/matrix-vector/addtodiagonal.hh>
#include <dune/matrix-vector/blockmatrixview.hh>
#include <dune/matrix-vector/algorithm.hh>

namespace Dune {
namespace Contact {

template <class GridType, class VectorType>
void LargeDeformationContactAssembler<GridType, VectorType>::assembleProblem()
{

  // assemble the rotations and the obstacles
  constructCouplings();
  for (int i=0; i<nCouplings(); i++)
    dynamic_cast<LargeDefCoupling*>(this->contactCoupling_[i].get())->buildProjection();

  assembleLinearisations();
  assembleClpReflections(); // TODO this has to be adjusted according to which projection is used?
  this->assembleObstacle();

  // compute the exact mortar transformation matrix
  computeExactTransformationMatrix();
}

template <class GridType, class VectorType>
void LargeDeformationContactAssembler<GridType, VectorType>::assembleClpReflections()
{
    //   Compute local coordinate systems for the dofs with obstacles
    std::vector<std::vector<MatrixBlock> > coordinateSystems(nCouplings());

    for (int i=0; i < nCouplings(); i++) {

        auto projection = this->coupling_[i].projection();

        const auto& nonmortarBoundary = this->contactCoupling_[i]->nonmortarBoundary();
        const auto& mortarBoundary    = this->contactCoupling_[i]->mortarBoundary();

        projection->project(nonmortarBoundary, mortarBoundary, this->coupling_[i].obsDistance_);

        std::vector<Dune::FieldVector<field_type,dim> > directions;
        projection->getObstacleDirections(directions);

        for (size_t j=0; j<directions.size(); j++) {
            directions[j] /= directions[j].two_norm();
            directions[j] *= -1; // TODO Is it +1 in the case of normal projection?
        }

        this->computeLocalCoordinateSystems(nonmortarBoundary, coordinateSystems[i], directions);
    }

    this->localCoordSystems_.resize(this->numDofs());

    Dune::ScaledIdentityMatrix<field_type,dim> id(1);
    for (auto& lcs : this->localCoordSystems_)
        lcs = id;

    for (int j=0; j < nCouplings(); ++j) {

        auto grid0Idx = this->coupling_[j].gridIdx_[0];

        int idx = 0;
        for (int k=0; k < grid0Idx; ++k)
            idx += grids_[k]->size(dim);

        const auto& nonmortarBoundary = this->contactCoupling_[j]->nonmortarBoundary();

        // if a body is non-mortar more then once, one has to be careful with not overwriting entries
        for (std::size_t k=0; k < coordinateSystems[j].size(); ++k)
          if(nonmortarBoundary.containsVertex(k))
            this->localCoordSystems_[idx + k] = coordinateSystems[j][k];
    }
}

template <class GridType, class VectorType>
void LargeDeformationContactAssembler<GridType, VectorType>::assembleExactJacobian(const std::vector<const MatrixType*>& submat,
                                                                                   MatrixType& totalMatrix) const
{
  namespace MV = Dune::MatrixVector;

  // TODO Clean this up
  // Create a block view of the grand matrix
  Dune::MatrixVector::BlockMatrixView<MatrixType> bV(submat);

  int nRows = bV.nRows();
  int nCols = bV.nCols();

  // Create the index set of B \hat{A} B^T
  MatrixIndexSet indicesBABT(nRows, nCols);

  // first import all indices
  for (int i=0; i<nGrids(); i++)
    indicesBABT.import(*submat[i],bV.row(i,0), bV.col(i,0));

  Timer timer;
  std::vector<std::array<std::set<int>, 2> > contactIdx(nCouplings());
  std::vector<std::set<int> > interiorIdx(nCouplings());
  for (int i=0; i<nCouplings(); i++) {

    auto idx0 = coupling_[i].gridIdx_[0];

    const auto& nmPatch = contactCoupling_[i]->nonmortarBoundary();

    // find any nonmortar node
    int first(-1);
    for (int i=0; i<grids_[idx0]->size(dim); i++)
      if (nmPatch.containsVertex(i)) {
        first=i;
        break;
      }

    // use fact that nonmortar and mortar matrix are dense
    auto offset0 = bV.col(idx0,0);
    MV::sparseRangeFor(BT_[first], [&](auto&&, auto&& idx) {
      // collected all mortar indices and nonmortar indices that are not
      // part of the reduced boundary
      if ((idx >= offset0 + this->grids_[idx0]->size(dim)) or
          (idx < offset0) or (!nmPatch.containsVertex(idx-offset0)))
        contactIdx[i][1].insert(idx);
      else // and all nonmortar indices
        contactIdx[i][0].insert(idx);
    });

    // now find all interior nonmortar nodes
    for (const auto& k : contactIdx[i][0])
      MV::sparseRangeFor((*submat[idx0])[k], [&](auto&&, auto&& idx) {
        if (!nmPatch.containsVertex(idx))
          interiorIdx[i].insert(bV.row(idx0,idx));
    });

    for (size_t j=0; j<2; j++) {

      for (const auto& k : contactIdx[i][j]) {

        // dense coupling with itself
        for (const auto& l : contactIdx[i][j])
          indicesBABT.add(k, l);

        // symmetic dense coupling with nonmortar interior
        for (const auto& l : interiorIdx[i]) {
          indicesBABT.add(k, l);
          indicesBABT.add(l, k);
        }
      }
    }

    // symmetric dense coupling nonmortar-mortar
    for (const auto& k : contactIdx[i][0])
      for (const auto& l : contactIdx[i][1]) {
        indicesBABT.add(k, l);
        indicesBABT.add(l, k);
      }
  }

  // ////////////////////////////////////////////////////////////////////
  //   Multiply transformation matrix to the global stiffness matrix
  // ////////////////////////////////////////////////////////////////////

  indicesBABT.exportIdx(totalMatrix);
  totalMatrix = 0;

  for (int i=0; i<nCouplings(); i++) {

    int idx0 = coupling_[i].gridIdx_[0];

    const auto& nmPatch = contactCoupling_[i]->nonmortarBoundary();
    int offset0 = bV.col(idx0, 0);

    std::vector<MatrixBlock> colAD(contactIdx[i][0].size());
    for (const auto& nmCol : contactIdx[i][0]) {

      // compute columns of A_{NN} D
      size_t count(0);
      for (const auto& cIdx : contactIdx[i][0]) {

        colAD[count] = 0;

        MV::sparseRangeFor((*submat[idx0])[cIdx-offset0], [&](auto&& col, auto&& idx) {
          if (nmPatch.containsVertex(idx))
            MV::addProduct(colAD[count], col, this->BT_[bV.row(idx0, idx)][nmCol]);
        });
        count++;
      }

      // D^T A_{NN} D
      for (const auto& row : contactIdx[i][0]) {
        if (row>nmCol)
          break;
        count =0;
        for (const auto& nmSum : contactIdx[i][0]) {
          MatrixBlock dummy;
          MV::transpose(BT_[nmSum][row],dummy);
          MV::addProduct(totalMatrix[row][nmCol], dummy, colAD[count++]);
        }
      }

      // M^T A_{NN} D
      for (const auto& row : contactIdx[i][1]) {
        count =0;
        for (const auto& nmSum : contactIdx[i][0]) {
          MatrixBlock dummy;
          MV::transpose(BT_[nmSum][row],dummy);
          MV::addProduct(totalMatrix[row][nmCol], dummy, colAD[count++]);
        }
      }
    }

    // symmetrize D^T A_NN D
    for (const auto& nmCol : contactIdx[i][0])
      for (const auto& nmRow : contactIdx[i][0]) {
        if (nmRow>=nmCol)
          break;
        MV::transpose(totalMatrix[nmRow][nmCol], totalMatrix[nmCol][nmRow]);
      }

    for (const auto& mCol : contactIdx[i][1]) {

      // compute columns of A_{NN} M
      size_t count(0);
      for (const auto& cIdx : contactIdx[i][0]) {

        colAD[count] = 0;

        MV::sparseRangeFor((*submat[idx0])[cIdx-offset0], [&](auto&& col, auto&& idx) {
          if (nmPatch.containsVertex(idx))
            MV::addProduct(colAD[count], col, this->BT_[bV.row(idx0, idx)][mCol]);});
        count++;
      }

      // M^T A_{NN} M
      for (const auto& mRow : contactIdx[i][1]) {
        if (mRow>mCol)
          break;
        count =0;
        for (const auto& nmSum : contactIdx[i][0]) {
          MatrixBlock dummy;
          MV::transpose(BT_[nmSum][mRow],dummy);
          MV::addProduct(totalMatrix[mRow][mCol], dummy, colAD[count++]);
        }
      }
    }

    // symmetrize M^T A_NN M
    for (const auto& mCol : contactIdx[i][1])
      for (const auto& mRow : contactIdx[i][1]) {
        if (mRow>=mCol)
          break;
        MatrixBlock dummy;
        MV::transpose(totalMatrix[mRow][mCol],dummy);
        totalMatrix[mCol][mRow] = dummy;
      }

    // symmetrize M^T A_NN D
    for (const auto&  nmRow : contactIdx[i][0])
      for (const auto&  mCol : contactIdx[i][1])
        MV::transpose(totalMatrix[mCol][nmRow], totalMatrix[nmRow][mCol]);

    // add couplings with interior nonmortar nodes
    for (const auto& intIdx : interiorIdx[i]) {

      const auto& rowIN = (*submat[idx0])[intIdx-offset0];

      for (size_t j=0; j<2; j++)
        for (const auto& col : contactIdx[i][j]) {

          MatrixBlock sum(0);
          MV::sparseRangeFor(rowIN, [&](auto&& entry, auto&& idx) {
            if (nmPatch.containsVertex(idx))
              MV::addProduct(sum, entry, this->BT_[bV.col(idx0, idx)][col]);
          });

          totalMatrix[intIdx][col] += sum;
          MatrixBlock sumT;
          MV::transpose(sum,sumT);
          totalMatrix[col][intIdx] += sumT;
        }
    }
  }

  // add all remaining entries of the submats
  for (int i=0; i<nGrids(); i++) {

    std::vector<std::reference_wrapper<const LeafBoundaryPatch> > nmPatches;
    for (int j=0; j<nCouplings(); j++)
      if (coupling_[j].gridIdx_[0]==(int) i)
        nmPatches.emplace_back(std::cref(contactCoupling_[j]->nonmortarBoundary()));

    auto contains = [&] (size_t idx) {

      for (size_t k=0; k<nmPatches.size(); k++)
        if (nmPatches[k].get().containsVertex(idx))
          return true;
      return false;
    };

    for(size_t j=0; j<submat[i]->N(); j++) {

      // skip nonmortar rows
      if (not contains(j))
        MV::sparseRangeFor((*submat[i])[j], [&](auto&& col, auto&& idx) {
          if (not contains(idx))
            totalMatrix[bV.row(i,j)][bV.col(i,idx)] += col;
        });
    }
  }
  std::cout<<"Transformed matrix in "<<timer.stop()<<std::endl;
}

template <class GridType, class VectorType>
void LargeDeformationContactAssembler<GridType, VectorType>::
computeExactTransformationMatrix()
{
  /**
   *  Compute the mortar transformation that decouples the non-penetration constraints
   *  in the case of large deformations.
   *  The transformation is constructed by first transforming the non-mortar matrix $D$ into
   *  normal and tangential coordinates $DO = (D_N, D_T)$, where $O$ is the matrix consisting of
   *  Householder reflections. Then the normal part is inverted $D_N^{-1}$ using the LU
   *  decomposition of umfpack. Let the linearisation w.r.t non-mortar coordinates
   *  that are not part of the reduced non-mortar boundary and w.r.t  mortar coordinates be denoted
   *  by M. Then, assuming that the coefficient vector is given by
   *  $u = (U_N|U_T,U_M,U_I)$ where $U_I$ are all remaining dofs, the transformation is given by
   *
   *
   * T = (-OD_N^{-1}|-OD_N^{-1}D_T) -OD_N^{-1}M       0 )
   *     (     0                         Id           0 )
   *     (     0                          0           Id)
   *
   * Note that the non-mortar and mortar matrices D,M are 1xdim block-matrices and the dimxdim
   * transformation above has blocks where only the first row contains the corresponding (non-)
   * mortar entries.
   */

  // compute offsets for the involved grids
  std::vector<int> offsets(nGrids());
  offsets[0] = 0;

  for (int k=0; k< nGrids()-1; k++)
    offsets[k+1] = offsets[k] + grids_[k]->size(dim);

  int nRows = offsets.back() + grids_.back()->size(dim);
  int nCols = nRows;

  // /////////////////////////////////////////////////////////////////
  //   First create the index structure
  // /////////////////////////////////////////////////////////////////

  MatrixIndexSet indicesBT(nRows, nCols);

  // BT_ is the identity plus some off-diagonal elements
  for (size_t i=0; i<indicesBT.rows(); i++)
    indicesBT.add(i,i);

  // mapping back from local boundary indices to global ones
  std::vector<std::vector<int> > nonmortarToGlobal(nCouplings());

  // store information about which dofs are involved in the linearisation
  // mortar and the non-mortar ones that don't belong to the reduced boundary
  std::vector<std::array<std::set<int>, 2> > contactDofs(nCouplings());

  // Enter all the off-diagonal entries
  for (int i=0; i<nCouplings(); i++) {

    // The grids involved in this coupling
    int grid0Idx = coupling_[i].gridIdx_[0];
    int grid1Idx = coupling_[i].gridIdx_[1];

    auto coupling = std::dynamic_pointer_cast<LargeDefCoupling>(contactCoupling_[i]);
    const auto& nonmortarBoundary = coupling->nonmortarBoundary();

    // The mapping from boundary indices to grid indices
    nonmortarToGlobal[i].resize(nonmortarBoundary.numVertices());
    int idx = 0;
    for (int j=0; j<grids_[grid0Idx]->size(dim); j++)
      if (nonmortarBoundary.containsVertex(j))
        nonmortarToGlobal[i][idx++] = j;

    const auto& nmMatrix = coupling->exactNonmortarMatrix();
    const auto& mMatrix = coupling->exactMortarMatrix();

    for (size_t j=0; j<nmMatrix.N(); j++) {

      {
        const auto& row = nmMatrix[j];
        auto&& end = row.end();

        // TODO: only add entries that are non-zero in normal coords
        for (auto&& it = row.begin(); it != end; ++it)
          if (!nonmortarBoundary.containsVertex(it.index()))
            contactDofs[i][0].insert(offsets[grid0Idx] + it.index());
      }

      {
        const auto& row = mMatrix[j];
        auto&& end = row.end();
        for (auto&& it = row.begin(); it != end; ++it)
          contactDofs[i][1].insert(offsets[grid1Idx] + it.index());
      }
    }

    // the off-diagonal part, these are dense matrices
    for (size_t j=0; j<2; j++)
      for (const auto& dof : contactDofs[i][j])
        for (size_t j=0; j<nonmortarToGlobal[i].size(); j++)
          indicesBT.add(offsets[grid0Idx] + nonmortarToGlobal[i][j], dof);

    for (const auto& row : nonmortarToGlobal[i])
      for (const auto& col : nonmortarToGlobal[i])
        indicesBT.add(offsets[grid0Idx] + row, offsets[grid0Idx] + col);
  }

  // ////////////////////////////////////////////////////////////
  //   Enter the values of the different couplings
  // ////////////////////////////////////////////////////////////

  indicesBT.exportIdx(BT_);
  BT_ = 0;

  // Enter identity part
  for (size_t i=0; i< BT_.N(); i++)
    Dune::MatrixVector::addToDiagonal(BT_[i][i], 1.0);

  // Enter all the off-diagonal entries
  for (int i=0; i<nCouplings(); i++) {

    // The grids involved in this coupling
    int grid0Idx = coupling_[i].gridIdx_[0];

    auto* coupling = dynamic_cast<LargeDefCoupling*>(contactCoupling_[i].get());
    const auto& nonmortarBoundary = coupling->nonmortarBoundary();

    // transform matrix to normal coordinates
    // TODO: avoid copy
    auto nmNormalMatrix = coupling->exactNonmortarMatrix();

    auto globalToLocal = nonmortarBoundary.makeGlobalToLocal();
    // extract normal components
    MatrixIndexSet nmSet(nmNormalMatrix.N(), nmNormalMatrix.N());

    for (size_t j=0; j<nmNormalMatrix.N(); j++) {

      auto& row = nmNormalMatrix[j];
      for(auto&& col = row.begin(); col != row.end(); col++) {
        if (!nonmortarBoundary.containsVertex(col.index()))
          continue;
        const auto& householder = this->localCoordSystems_[offsets[grid0Idx] + col.index()];

        auto entry = (*col)[0];
        householder.mtv(entry, (*col)[0]);

        if (std::fabs((*col)[0][0])>1e-15)
          nmSet.add(j, globalToLocal[col.index()]);
      }
    }

    // the scalar nonmortar matrix containing only the normal components
    using ScalarMatrix = BCRSMatrix<FieldMatrix<field_type,1 , 1> >;
    ScalarMatrix nmInverse;
    nmSet.exportIdx(nmInverse);
    nmInverse = 0;

    // extract normal components
    for (size_t j=0; j< nmNormalMatrix.N(); j++) {

      const auto& row = nmNormalMatrix[j];
      for(auto&& col = row.begin(); col != row.end(); col++) {
        // copy first components of all nonmortar blocks
        if (nonmortarBoundary.containsVertex(col.index()) and std::fabs((*col)[0][0])>1e-15)
          nmInverse[j][globalToLocal[col.index()]] = (*col)[0][0];
      }
    }

    // compute inverse of the normal non-mortar matrix using a LU decomposition
#if HAVE_SUITESPARSE_UMFPACK
    Timer timer;
    UMFPack<ScalarMatrix> umfPack(nmInverse, 0);
    std::cout<<"took "<<timer.elapsed()<<" to decompose matrix\n";

    // backward substitute to transform the constraints
    // loop over involved nodes
    // by looping over all reduced non-mortar only, one skips the involved nodes that are in the unred patch
    using ScalarVector = BlockVector<FieldVector<double,1> >;
    for (size_t j=0; j<nonmortarToGlobal[i].size(); j++) {
      for (int k=0; k<dim; k++) {

        int col = nonmortarToGlobal[i][j];
        ScalarVector rhs(nmInverse.N());
        rhs = 0;

        // the non-mortar columns become the inverse normal non-mortar matrix
        if (k==0)
          rhs[j] = 1.0;
        // the other columns are the tangential non-mortar components
        else {
          for (size_t p=0; p<nmNormalMatrix.N(); p++)
            if(nmNormalMatrix.exists(p, col))
              rhs[p] = nmNormalMatrix[p][col][0][k];
        }

        // solve
        InverseOperatorResult res;
        ScalarVector x = rhs;

        umfPack.apply(x, rhs, res);
        if (!res.converged)
          std::cout<<"Warning: Not converged! Reduction "<<res.reduction<<" iterations "<<res.iterations<<std::endl;

        // move entries to the transformation matrix
        for (size_t p=0; p<nmNormalMatrix.N(); p++)
          BT_[offsets[grid0Idx] + nonmortarToGlobal[i][p]][offsets[grid0Idx] + col][0][k] = -x[p][0];
      }
    }

    // now add the remaining parts
    std::vector<std::reference_wrapper<const JacobianType> > linMatrices;
    linMatrices = {{std::cref(coupling->exactNonmortarMatrix()),
                    std::cref(coupling->exactMortarMatrix())}};

    for (size_t j =0; j<2; j++) {
      for (const auto& globalCol : contactDofs[i][j]) {
        for (int k=0; k<dim; k++) {

          int col = globalCol - offsets[coupling_[i].gridIdx_[j]];
          ScalarVector rhs(nmInverse.N());
          rhs = 0;

          // the other columns are the tangential non-mortar components
          for (size_t p=0; p<linMatrices[j].get().N(); p++)
            if(linMatrices[j].get().exists(p, col))
              rhs[p] = linMatrices[j].get()[p][col][0][k];

          // solve
          InverseOperatorResult res;
          ScalarVector x = rhs;

          umfPack.apply(x, rhs, res);
          if (!res.converged)
            std::cout<<"Warning: Not converged! Reduction "<<res.reduction<<" iterations "<<res.iterations<<std::endl;

          // move entries to the transformation matrix
          for (size_t p=0; p<linMatrices[j].get().N(); p++)
            BT_[offsets[grid0Idx] + nonmortarToGlobal[i][p]][globalCol][0][k] = -x[p][0];
        }
      }
    }
#else
    DUNE_THROW(Dune::Exception, "You need UMFPack if you want to use exact Hessians!");
#endif

    // finally rotate the nonmortar blocks
    //TODO might not work for more than one coupling
    for(size_t rowIdx=0; rowIdx<nmNormalMatrix.N(); ++rowIdx) {

      int globalRowIdx = offsets[grid0Idx] + nonmortarToGlobal[i][rowIdx];
      for (auto& cIt : BT_[globalRowIdx])
        cIt.leftmultiply(this->localCoordSystems_[globalRowIdx]);
    }
  }
}

template <class GridType, class VectorType>
void LargeDeformationContactAssembler<GridType, VectorType>::
computeLumppedTransformationMatrix()
{
  namespace MV = Dune::MatrixVector;
  /**
   *  Compute the mortar transformation that decouples the non-penetration constraints
   *  in the case of large deformations.
   *  The transformation is constructed by first transforming the non-mortar matrix $D$ into
   *  normal and tangential coordinates $DO = (D_N, D_T)$, where $O$ is the matrix consisting of
   *  Householder reflections. Then the normal part is inverted $D_N^{-1}$ using the LU
   *  decomposition of umfpack. Let the linearisation w.r.t non-mortar coordinates
   *  that are not part of the reduced non-mortar boundary and w.r.t  mortar coordinates be denoted
   *  by M. Then, assuming that the coefficient vector is given by
   *  $u = (U_N|U_T,U_M,U_I)$ where $U_I$ are all remaining dofs, the transformation is given by
   *
   *
   * T = (-OD_N^{-1}|-OD_N^{-1}D_T) -OD_N^{-1}M       0 )
   *     (     0                         Id           0 )
   *     (     0                          0           Id)
   *
   * Note that the non-mortar and mortar matrices D,M are 1xdim block-matrices and the dimxdim
   * transformation above has blocks where only the first row contains the corresponding (non-)
   * mortar entries.
   */
  // compute offsets for the involved grids
  std::vector<int> offsets(nGrids());
  offsets[0] = 0;

  for (int k=0; k< nGrids()-1; k++)
    offsets[k+1] = offsets[k] + grids_[k]->size(dim);

  int nRows = offsets.back() + grids_.back()->size(dim);
  int nCols = nRows;

  // /////////////////////////////////////////////////////////////////
  //   First create the index structure
  // /////////////////////////////////////////////////////////////////

  MatrixIndexSet indicesBT(nRows, nCols);

  // BT_ is the identity plus some off-diagonal elements
  for (size_t i=0; i<indicesBT.rows(); i++)
    indicesBT.add(i,i);

  // mapping back from local boundary indices to global ones
  std::vector<std::vector<size_t> > nonmortarToGlobal(nCouplings());

  // Enter all the off-diagonal entries
  for (int i=0; i < nCouplings(); ++i) {

    // The grids involved in this coupling
    int grid0Idx = coupling_[i].gridIdx_[0];
    int grid1Idx = coupling_[i].gridIdx_[1];

    auto coupling = std::dynamic_pointer_cast<LargeDefCoupling>(contactCoupling_[i]);
    const auto& nonmortarBoundary = coupling->nonmortarBoundary();

    // The mapping from boundary indices to grid indices
    nonmortarToGlobal[i].resize(nonmortarBoundary.numVertices());
    int idx = 0;
    for (int j=0; j < grids_[grid0Idx]->size(dim); ++j)
      if (nonmortarBoundary.containsVertex(j))
        nonmortarToGlobal[i][idx++] = j;

    const auto& nmMatrix = coupling->exactNonmortarMatrix();
    const auto& mMatrix = coupling->exactMortarMatrix();

    for (size_t j=0; j < nmMatrix.N(); ++j) {

      auto globalRowIdx = offsets[grid0Idx] + nonmortarToGlobal[i][j];
      indicesBT.add(globalRowIdx,globalRowIdx);

      MV::sparseRangeFor(nmMatrix[j],[&](auto&&, auto&& idx) {
        indicesBT.add(globalRowIdx, offsets[grid0Idx] + idx);});

      MV::sparseRangeFor(mMatrix[j],[&](auto&&, auto&& idx) {
        indicesBT.add(globalRowIdx, offsets[grid1Idx] + idx);});
    }
  }

  // ////////////////////////////////////////////////////////////
  //   Enter the values of the different couplings
  // ////////////////////////////////////////////////////////////

  indicesBT.exportIdx(BT_);
  BT_ = 0;

  // Enter identity part
  for (size_t i=0; i < BT_.N(); ++i)
    Dune::MatrixVector::addToDiagonal(BT_[i][i], 1.0);

  // Enter all the off-diagonal entries
  for (int i=0; i < nCouplings(); ++i) {

    // The grids involved in this coupling
    int grid0Idx = coupling_[i].gridIdx_[0];
    int grid1Idx = coupling_[i].gridIdx_[1];

    auto coupling = std::dynamic_pointer_cast<LargeDefCoupling>(contactCoupling_[i]);
    const auto& nmMat = coupling->exactNonmortarMatrix();
    const auto& mMat = coupling->exactMortarMatrix();
    const auto& nonmortarBoundary = coupling->nonmortarBoundary();

    // setup non-mortar in normal coordinates
    auto normalMat = nmMat;

    for (size_t j=0; j<normalMat.N(); j++)
      MV::sparseRangeFor(normalMat[j], [&](auto&& col, auto&& idx) {
        if (nonmortarBoundary.containsVertex(idx)) {

          const auto& householder = this->localCoordSystems_[offsets[grid0Idx] + idx];
          auto entry = col[0];
          householder.mtv(entry, col[0]);
        }
      });

    for (size_t j=0; j<normalMat.N(); j++) {

      // invert normal component
      field_type lumpedEntry(0);

      MV::sparseRangeFor(normalMat[j], [&](auto&& col, auto&& idx){
        if (nonmortarBoundary.containsVertex(idx))
          lumpedEntry += col[0][0];});

      if (std::fabs(lumpedEntry)<1e-15)
        DUNE_THROW(Dune::Exception,"Warning: Normal entry of the diagonal is zero!");
      else if (lumpedEntry <0)
        std::cout<<"Negative diagonal entry :-(\n";

      lumpedEntry = field_type(-1.0/lumpedEntry);

      // non-mortar entries
      auto globalRowIdx = offsets[grid0Idx] + nonmortarToGlobal[i][j];

      MV::sparseRangeFor(normalMat[j], [&](auto&& col, auto&& idx) {
        this->BT_[globalRowIdx][offsets[grid0Idx] + idx][0].axpy(lumpedEntry,col[0]);
        if (nonmortarBoundary.containsVertex(idx))
          this->BT_[globalRowIdx][offsets[grid0Idx] + idx][0][0] = 0;
      });

      BT_[globalRowIdx][globalRowIdx][0][0] = lumpedEntry;

      MV::sparseRangeFor(mMat[j], [&](auto&& col, auto&& idx) {
        this->BT_[globalRowIdx][offsets[grid1Idx] + idx][0].axpy(lumpedEntry, col[0]);
      });
    }

    // finally rotate the nonmortar blocks
    for(size_t rowIdx=0; rowIdx<nmMat.N(); rowIdx++) {

      int globalRowIdx = offsets[grid0Idx] + nonmortarToGlobal[i][rowIdx];
      for (auto& cIt : BT_[globalRowIdx])
        cIt.leftmultiply(this->localCoordSystems_[globalRowIdx]);
    }
  }
}

template <class GridType, class VectorType>
void LargeDeformationContactAssembler<GridType, VectorType>::
computeInverseTransformationMatrix()
{
  namespace MV = Dune::MatrixVector;
  /**
   *  Compute the inverse of the mortar transformation that decouples the non-penetration constraints
   *  in the case of large deformations.
   *  The transformation is constructed by first transforming the non-mortar matrix $D$ into
   *  normal and tangential coordinates $DO = (D_N, D_T)$, where $O$ is the matrix consisting of
   *  Householder reflections.
   *  Let the linearisation w.r.t non-mortar coordinates
   *  that are not part of the reduced non-mortar boundary and w.r.t  mortar coordinates be denoted
   *  by M. Then, assuming that the coefficient vector is given by
   *  $u = (U_N|U_T,U_M,U_I)$ where $U_I$ are all remaining dofs, the transformation is given by
   *
   *
   * T^{-1} = (-D_N^{-1}|-D_T)O               -M           0 )
   *          (     0                         Id           0 )
   *          (     0                          0           Id)
   *
   * Note that the non-mortar and mortar matrices D,M are 1xdim block-matrices and the dimxdim
   * transformation above has blocks where only the first row contains the corresponding (non-)
   * mortar entries.
   */
  // compute offsets for the involved grids
  std::vector<int> offsets(nGrids());
  offsets[0] = 0;

  for (int k=0; k< nGrids()-1; k++)
    offsets[k+1] = offsets[k] + grids_[k]->size(dim);

  int nRows = offsets.back() + grids_.back()->size(dim);
  int nCols = nRows;

  // /////////////////////////////////////////////////////////////////
  //   First create the index structure
  // /////////////////////////////////////////////////////////////////

  MatrixIndexSet inverseBT(nRows, nCols);

  // BT_ is the identity plus some off-diagonal elements
  for (size_t i=0; i<inverseBT.rows(); i++)
    inverseBT.add(i,i);

  // mapping back from local boundary indices to global ones
  std::vector<std::vector<size_t> > nonmortarToGlobal(nCouplings());

  // Enter all the off-diagonal entries
  for (int i=0; i<nCouplings(); i++) {

    // The grids involved in this coupling
    int grid0Idx = coupling_[i].gridIdx_[0];
    int grid1Idx = coupling_[i].gridIdx_[1];

    auto coupling = std::dynamic_pointer_cast<LargeDefCoupling>(contactCoupling_[i]);
    const auto& nonmortarBoundary = coupling->nonmortarBoundary();

    // The mapping from boundary indices to grid indices
    nonmortarToGlobal[i].resize(nonmortarBoundary.numVertices());
    int idx = 0;
    for (int j=0; j<grids_[grid0Idx]->size(dim); j++)
      if (nonmortarBoundary.containsVertex(j))
        nonmortarToGlobal[i][idx++] = j;

    const auto& nmMatrix = coupling->exactNonmortarMatrix();
    const auto& mMatrix = coupling->exactMortarMatrix();

    for (size_t j=0; j<nmMatrix.N(); j++) {

      auto globalRowIdx = offsets[grid0Idx] + nonmortarToGlobal[i][j];
      {
        const auto& row = nmMatrix[j];
        auto&& end = row.end();
        for (auto&& it = row.begin(); it != end; ++it)
          inverseBT.add(globalRowIdx, offsets[grid0Idx] + it.index());
      }

      {
        const auto& row = mMatrix[j];
        auto&& end = row.end();
        for (auto&& it = row.begin(); it != end; ++it)
          inverseBT.add(globalRowIdx, offsets[grid1Idx] + it.index());
      }
    }
  }

  // ////////////////////////////////////////////////////////////
  //   Enter the values of the different couplings
  // ////////////////////////////////////////////////////////////

  inverseBT.exportIdx(inverse_);
  inverse_ = 0;

  // Enter identity part
  for (size_t i=0; i< inverse_.N(); i++)
    Dune::MatrixVector::addToDiagonal(inverse_[i][i], 1.0);

  // Enter all the off-diagonal entries
  for (int i=0; i<nCouplings(); i++) {

    // The grids involved in this coupling
    int grid0Idx = coupling_[i].gridIdx_[0];
    int grid1Idx = coupling_[i].gridIdx_[1];

    auto coupling = std::dynamic_pointer_cast<LargeDefCoupling>(contactCoupling_[i]);
    const auto& nmMat = coupling->exactNonmortarMatrix();
    const auto& mMat = coupling->exactMortarMatrix();
    const auto& nonmortarBoundary = coupling->nonmortarBoundary();

    for (size_t j=0; j<nmMat.N(); j++) {

      // non-mortar entries
      auto globalRowIdx = offsets[grid0Idx] + nonmortarToGlobal[i][j];

      // TODO: only add entries that are non-zero in normal coords
      MV::sparseRangeFor(nmMat[j], [&](auto&& col, auto&& idx) {
        MatrixBlock invTrans(0);

        // for non-mortar components, the entrie is twice multiplied by the Householder
        // reflection, hence we don't have to compute normal coordinates
        if (nonmortarBoundary.containsVertex(idx) and
            nonmortarToGlobal[i][j]== idx)
          invTrans = this->localCoordSystems_[offsets[grid0Idx] + idx];

        invTrans[0] = col[0];
        invTrans[0] *= -1;

        inverse_[globalRowIdx][offsets[grid0Idx] + idx] = invTrans;
      });

      MV::sparseRangeFor(mMat[j], [&](auto&& col, auto&& idx) {
        MatrixBlock invTrans(0);
        invTrans[0] -= col[0];
        inverse_[globalRowIdx][offsets[grid1Idx] + idx] = invTrans;
      });
    }
  }

  // compute the norms of the columns, these determine the length of the
  // transformed basis vectors and are taken into account by the trust-region

  colNorms_.resize(inverse_.N());
  colNorms_ = 0;
  MV::sparseMatrixFor(inverse_, [&](auto&& entry, auto&&, auto&& colIdx) {
    for (int j=0; j < dim; ++j)
      for (int k=0; k < dim; ++k)
          colNorms_[colIdx][k] += std::pow(entry[j][k], 2);
  });

  field_type min(std::numeric_limits<field_type>::max()), max(-1);
  for (auto& colNorm : colNorms_)
    for (int j=0; j < dim; ++j) {
      field_type root = std::sqrt(colNorm[j]);
      colNorm[j] = 1.0/root;

      min = std::fmin(min, root);
      max = std::fmax(max, root);
    }
  std::cout<<" Transformed basis vector norms range from "<<min<<" to "<<max<<std::endl;
}

template <class GridType, class VectorType>
template <class VectorTypeContainer>
void LargeDeformationContactAssembler<GridType, VectorType>::
assembleExactRightHandSide(const VectorTypeContainer& rhs,
        VectorType& totalRhs) const
{
  namespace MV = Dune::MatrixVector;

  // first combine all regular parts
  this->concatenateVectors(rhs,totalRhs);

  // compute offsets for the involved grids
  std::vector<int> offsets(nGrids());
  offsets[0] = 0;

  for (int k=0; k< nGrids()-1; k++)
    offsets[k+1] = offsets[k] + grids_[k]->size(dim);

  // Enter all the off-diagonal entries
  for (int i=0; i<nCouplings(); i++) {

    // The grids involved in this coupling
    const auto gridIdx = coupling_[i].gridIdx_;

    auto coupling = std::dynamic_pointer_cast<LargeDefCoupling>(contactCoupling_[i]);
    const auto& nonmortarBoundary = coupling->nonmortarBoundary();

    auto&& globalToLocal = nonmortarBoundary.makeGlobalToLocal();

    // The mapping from boundary indices to grid indices
    std::vector<size_t> nonmortarToGlobal(nonmortarBoundary.numVertices());
    int idx = 0;
    for (int j=0; j<grids_[gridIdx[0]]->size(dim); j++)
      if (nonmortarBoundary.containsVertex(j))
        nonmortarToGlobal[idx++] = j;

    const auto& nmMatrix = coupling->exactNonmortarMatrix();

    // indices of normal nm matrix
    Dune::MatrixIndexSet nmIsN(nmMatrix.N(),nmMatrix.N());
    // for the normal projection the tangential matrix is not square
    MV::sparseMatrixFor(nmMatrix, [&](auto&&, auto&& rowIdx, auto&& colIdx) {
      if (nonmortarBoundary.containsVertex(colIdx))
        nmIsN.add(globalToLocal[colIdx], rowIdx);
    });

    using ScalarMatrix = BCRSMatrix<FieldMatrix<field_type,1 , 1> >;
    ScalarMatrix nmMatN;
    nmIsN.exportIdx(nmMatN);
    nmMatN = 0;

    // setup non-mortar matrix in normal and tangential coordinates
    // the first component in the tangential matrix is set to 1, so
    // multiplication with the inverse normal matrix yields the correct
    // transformed vector

    using ScalarVector = BlockVector<FieldVector<double,1> >;
    ScalarVector rhsNm(nmMatrix.N());
    rhsNm = 0;
    auto nmMatT = nmMatrix;

    // finally rotate the nonmortar blocks
    for(size_t rowIdx=0; rowIdx<nmMatrix.N(); rowIdx++) {

      int globalRowIdx = offsets[gridIdx[0]] + nonmortarToGlobal[rowIdx];

      this->localCoordSystems_[globalRowIdx].mtv(rhs[gridIdx[0]][nonmortarToGlobal[rowIdx]],totalRhs[globalRowIdx]);
      rhsNm[rowIdx] = -totalRhs[globalRowIdx][0];

      // set first component to zero for non-mortar diagonal dofs
      // there is not Identity  in the transformation matrix
      totalRhs[globalRowIdx][0] = 0;
    }

    MV::sparseMatrixFor(nmMatT, [&](auto&& entry, auto&& rowIdx, auto&& colIdx){
      if (nonmortarBoundary.containsVertex(colIdx)) {
        const auto& householder = this->localCoordSystems_[offsets[gridIdx[0]] + colIdx];

        auto val = entry[0];
        householder.mtv(val, entry[0]);

        nmMatN[globalToLocal[colIdx]][rowIdx][0][0] = entry[0][0];
        entry[0][0] = 0;
      }
    });

    // solve the small system nmMatN*transRhsNm = rhsNm;
    auto transRhsNm = rhsNm;

    // Make small non-symmetric cg solver
    MatrixAdapter<ScalarMatrix,ScalarVector,ScalarVector> op(nmMatN);
    SeqILU<ScalarMatrix,ScalarVector,ScalarVector> ilu0(nmMatN,1.0);
    BiCGSTABSolver<ScalarVector> bicgstab(op,ilu0,1E-12,100,1);
    InverseOperatorResult statistics;
    bicgstab.apply(transRhsNm, rhsNm, statistics);

    // the transformed rhs is given by linear combination of the (non-)mortar matrices and
    // the inverse of the normal nm matrix
    std::vector<std::reference_wrapper<const JacobianType> > linMatrices;
    linMatrices = {{std::cref(nmMatT), std::cref(coupling->exactMortarMatrix())}};

    for (size_t j =0; j < 2; ++j)
      MV::sparseMatrixFor(linMatrices[j].get(), [&](auto&& entry, auto&& rowIdx, auto&& colIdx) {
        totalRhs[offsets[gridIdx[j]] + colIdx].axpy(transRhsNm[rowIdx][0], entry[0]);});

    for (size_t p=0; p < transRhsNm.size(); ++p)
      totalRhs[offsets[gridIdx[0]] + nonmortarToGlobal[p]][0] += transRhsNm[p][0];
  }
}

template <class GridType, class VectorType>
template <class VectorTypeContainer>
void LargeDeformationContactAssembler<GridType, VectorType>::
postprocess(const VectorType& totalX, VectorTypeContainer& x) const
{
  namespace MV = Dune::MatrixVector;

  auto untransformedX = totalX;

  // compute offsets for the involved grids
  std::vector<int> offsets(nGrids());
  offsets[0] = 0;

  for (int k=0; k < nGrids()-1; k++)
    offsets[k+1] = offsets[k] + grids_[k]->size(dim);

  // Enter all the off-diagonal entries
  for (int i=0; i<nCouplings(); i++) {

    // The grids involved in this coupling
    const auto& gridIdx = coupling_[i].gridIdx_;

    auto coupling = std::dynamic_pointer_cast<LargeDefCoupling>(contactCoupling_[i]);
    const auto& nonmortarBoundary = storedNmPatches_[i];

    auto&& globalToLocal = nonmortarBoundary.makeGlobalToLocal();

    // The mapping from boundary indices to grid indices
    std::vector<size_t> nonmortarToGlobal(nonmortarBoundary.numVertices());
    int idx = 0;
    for (int j=0; j<grids_[gridIdx[0]]->size(dim); j++)
      if (nonmortarBoundary.containsVertex(j))
        nonmortarToGlobal[idx++] = j;

    const auto& nmMatrix = coupling->exactNonmortarMatrix();

    // indices of normal nm matrix
    Dune::MatrixIndexSet nmIsN(nmMatrix.N(),nmMatrix.N());
    // for the normal projection the tangential matrix is not square
    MV::sparseMatrixFor(nmMatrix, [&](auto&&, auto&& rowIdx, auto&& colIdx) {
      if (nonmortarBoundary.containsVertex(colIdx))
        nmIsN.add(rowIdx, globalToLocal[colIdx]);
    });

    using ScalarMatrix = BCRSMatrix<FieldMatrix<field_type,1 , 1> >;
    ScalarMatrix nmMatN;
    nmIsN.exportIdx(nmMatN);
    nmMatN = 0;

    // setup non-mortar matrix in normal and tangential coordinates
    using ScalarVector = BlockVector<FieldVector<double,1> >;
    ScalarVector rhsNm(nmMatrix.N());
    rhsNm = 0;

    for (size_t j=0; j<nmMatrix.N(); j++) {

      auto globalRowIdx = offsets[gridIdx[0]] + nonmortarToGlobal[j];
      rhsNm[j] =totalX[globalRowIdx][0];

      const auto& row = nmMatrix[j];

      for(auto col = row.begin(); col != nmMatrix[j].end(); col++) {

        auto entry = (*col)[0];
        if (nonmortarBoundary.containsVertex(col.index())) {
          const auto& householder = this->localCoordSystems_[offsets[gridIdx[0]] + col.index()];
          auto dummy = entry;
          householder.mtv(dummy, entry);
          nmMatN[j][globalToLocal[col.index()]][0][0] = entry[0];
          entry[0] = 0;
        }

        // now add the values
        auto globalColIdx = offsets[gridIdx[0]] + col.index();
        rhsNm[j] += (entry*totalX[globalColIdx]);
      }
    }

    // add mortar terms
    const auto& mMatrix = coupling->exactMortarMatrix();
    MV::sparseMatrixFor(mMatrix, [&](auto&& entry, auto&& rowIdx, auto&& colIdx) {
      rhsNm[rowIdx] += entry[0]*totalX[offsets[gridIdx[1]] + colIdx];});

    // we actually need the negative term
    rhsNm *= -1;

    // solve the small system nmMatN*transRhsNm = rhsNm;
    auto transRhsNm = rhsNm;

    // Make small non-symmetric cg solver
    MatrixAdapter<ScalarMatrix,ScalarVector,ScalarVector> op(nmMatN);
    SeqILU<ScalarMatrix,ScalarVector,ScalarVector> ilu0(nmMatN,1.0);
    BiCGSTABSolver<ScalarVector> bicgstab(op,ilu0,1E-12,300,1);
    InverseOperatorResult statistics;
    bicgstab.apply(transRhsNm, rhsNm, statistics);

    for (size_t j=0; j<nmMatrix.N();j++) {
      auto globalRowIdx = offsets[gridIdx[0]]+nonmortarToGlobal[j];
      untransformedX[globalRowIdx][0] = transRhsNm[j];

      // finally all non-mortar entries have to be rotated
      auto dummy = untransformedX[globalRowIdx];
      this->localCoordSystems_[globalRowIdx].mv(dummy,untransformedX[globalRowIdx]);
    }
  }


  // ** * OLD Version
  if (not exactPostprocess_) {
    std::cout << "Inexact postprocess!\n";
    BT_.mv(totalX, untransformedX);
  }
  //*/

  //   Split the total solution vector into the parts
  //   corresponding to the grids.
  int idx = 0;
  for (int i=0; i<nGrids(); i++) {

    x[i].resize(grids_[i]->size(dim));
    for (size_t j=0; j < x[i].size(); j++, idx++)
      x[i][j] = untransformedX[idx];
  }
}

} /* namespace Contact */
} /* namespace Dune */
