// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_ASSEMBLERS_ONEBODY_ASSEMBLER_HH
#define DUNE_CONTACT_ASSEMBLERS_ONEBODY_ASSEMBLER_HH

#include <memory>

#include <dune/contact/assemblers/contactassembler.hh>

#include <dune/solvers/common/boxconstraint.hh>
#include <dune/fufem/boundarypatch.hh>

namespace Dune {
namespace Contact {

template <class GridType, class field_type=double>
class OneBodyAssembler : public ContactAssembler<GridType::dimension, field_type>
{
protected:
    enum {dim = GridType::dimension};

    using Vertex = typename GridType::template Codim<dim>::Entity;
    using CoordinateType = Dune::FieldVector<field_type,dim>;

    using Obstacle = std::function<field_type (const CoordinateType& pos, const CoordinateType& direction)>;
    using LevelBoundaryPatch = BoundaryPatch<typename GridType::LevelGridView>;
    using LeafBoundaryPatch = BoundaryPatch<typename GridType::LeafGridView>;

public:

    OneBodyAssembler() = default;

    /** \brief Set up a new assembler
     *
     *  \param grid The grid.
     *  \param obsFunction Function that computes for a vertex and a search direction the obstacle.
     *  \param obsDistance The maximal distance to look for an obstacle.
     *  \param obsPatch The boundary that might come into contact with the obstacle.
     *  \param overlap Threshold for the initial overlap of the continua and the obstacle. configuration
     *  \param searchDirection A function that computes the search direction corr. to a vertex.
     *                         If not set the nodally averaged normals are used.
    */
    OneBodyAssembler(const GridType& grid, const Obstacle& obsFunction,
                     const LevelBoundaryPatch& obsPatch,
                     field_type overlap = 1e-8)
    {
        setup(grid, obsFunction, obsPatch, overlap);
    }

    void setup(const GridType& grid, const Obstacle& obsFunction,
               const LevelBoundaryPatch& obsPatch,
               field_type overlap = 1e-8) {
        grid_ = Dune::stackobject_to_shared_ptr<const GridType>(grid);
        obstacleFunction_ = obsFunction;
        coarseObsPatch_ = Dune::stackobject_to_shared_ptr<const LevelBoundaryPatch>(obsPatch);
        overlap_ = overlap;
    }

    /** \brief Computes the obstacles values and setup the householder reflections. */
    void assembleObstacles();

    //! Getter for the grid
    const GridType& getGrid() const {return *grid_;}

    //! Getter for the grid
    const LevelBoundaryPatch& getCoarseObsPatch() const {return *coarseObsPatch_;}

    //! Getter for the obstacle field
    std::vector<BoxConstraint<field_type, dim> >& getObstacles() {
      return obstacles_;
    }

    //! Const getter for the obstacle field
    const std::vector<BoxConstraint<field_type, dim> >& getObstacles() const {
      return obstacles_;
    }

    // //////////////////////////////////////////
    //    Data Members
    // //////////////////////////////////////////

protected:
    /** \brief The grid whose obstacle we represent */
    std::shared_ptr<const GridType> grid_;

    /** \brief Function which implements the geometry of the rigid obstacle
        This functions expects a position and a search direction and computes the distance to the obstacle
    */
    Obstacle obstacleFunction_;

    //! The coarse contact boundary.
    std::shared_ptr<const LevelBoundaryPatch> coarseObsPatch_;

    //! Threshold for initial overlap of the contiua and the obstacle
    field_type overlap_;

public:
    //! The contact boundary of the leaf grid.
    LeafBoundaryPatch obsPatch_;

    //! The obstacles for each node on the contact boundary.
    std::vector<BoxConstraint<field_type,dim> > obstacles_;
};

} /* namespace Contact */
} /* namespace Dune */

#include "onebodyassembler.cc"

#endif
