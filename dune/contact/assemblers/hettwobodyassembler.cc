#include <dune/istl/matrixindexset.hh>
#include <dune/istl/preconditioners.hh>

#include <dune/contact/projections/normalprojection.hh>

namespace Dune {
namespace Contact {

// Sets obstacles on the leaf level
template <class GridType0, class GridType1, class VectorType>
void HeterogeneousTwoBodyAssembler<GridType0, GridType1, VectorType>::assembleObstacle()
{
    obstacles_.resize(grid0_->size(dim) + grid1_->size(dim1));

    // ///////////////////////////////////////////
    //    Precompute the mortar side
    // ///////////////////////////////////////////

    // get an approximation of the weak obstacle
    //std::vector<field_type> obs;
    //contactProjection_->getObstacles(obs);

    // get the weak obstacle
    auto& obs = contactCoupling_->weakObstacle_;

    std::vector<int> globalToLocal;
    contactCoupling_->nonmortarBoundary().makeGlobalToLocal(globalToLocal);

    const auto& indexSet = grid0_->leafIndexSet();
    const auto leafView = grid0_->leafGridView();

    for (const auto& v : vertices(leafView)) {

        int vIndex = indexSet.index(v);

        // Set obstacles to a very large value
        obstacles_[vIndex].clear();

        if (globalToLocal[vIndex]<0)
            continue;

        // Set the obstacle
        switch (coupling_.type_) {

            case CouplingPairBase::CONTACT:
                obstacles_[vIndex].upper(0) = obs[globalToLocal[vIndex]];
                break;

            case CouplingPairBase::STICK_SLIP:
                obstacles_[vIndex].lower(0) = 0;
                obstacles_[vIndex].upper(0) = 0;
                break;

            case CouplingPairBase::GLUE:
                for (int j=0; j<dim; j++) {
                    obstacles_[vIndex].lower(j) = 0;
                    obstacles_[vIndex].upper(j) = 0;
                }
                break;

            case CouplingPairBase::NONE:
                break;
            default:
                DUNE_THROW(Dune::NotImplemented, "Coupling type not handled yet!");
        }

    }
}


template <class GridType0, class GridType1, class VectorType>
void HeterogeneousTwoBodyAssembler<GridType0, GridType1, VectorType>::postprocess(const VectorType& totalX,
                                                                std::array<VectorType, 2>& x) const
{
    int size0 = grid0_->size(dim);
    int size1 = grid1_->size(GridType1::dimension);

    // ///////////////////////////////////////////////////////
    //   Transform the vector to canonical coordinates
    // ///////////////////////////////////////////////////////

    VectorType totalX2(totalX.size());

    const std::vector<MatrixBlock>& lCS = this->localCoordSystems_;
    for (size_t i=0; i<totalX.size(); i++) {
        if (i<lCS.size())
            lCS[i].mv(totalX[i], totalX2[i]);
        else
            totalX2[i] = totalX[i];
    }

    // ///////////////////////////////////////////////////////
    //   Transform the solution vector to the nodal basis
    // ///////////////////////////////////////////////////////

    VectorType nodalBasisTotalX(totalX2.size());
    BT_.mv(totalX2, nodalBasisTotalX);


    // ///////////////////////////////////////////////////////
    //   Split the total solution vector into the two parts
    //   corresponding to the two grids.
    // ///////////////////////////////////////////////////////


    x[0].resize(size0);
    x[1].resize(size1);

    int idx = 0;
    for (size_t i=0; i<x[0].size(); i++, idx++)
        x[0][i] = nodalBasisTotalX[idx];

    for (size_t i=0; i<x[1].size(); i++, idx++)
        x[1][i] = nodalBasisTotalX[idx];

}

template <class GridType0, class GridType1, class VectorType>
void HeterogeneousTwoBodyAssembler<GridType0, GridType1, VectorType>::
setup(const GridType0& grid0, const GridType1& grid1,
               std::shared_ptr<const LevelBoundaryPatch0> nonmortarPatch,
               std::shared_ptr<const LevelBoundaryPatch1> mortarPatch,
               field_type obsDistance,
               typename CouplingPairBase::CouplingType couplingType,
               std::shared_ptr<Projection> projection,
               std::shared_ptr<GridGlueBackend> backend)
{
    grid0_ = &grid0;
    grid1_ = &grid1;

    coupling_.set(0, 1, nonmortarPatch, mortarPatch, obsDistance, couplingType, projection, backend);

    setup();
}

template <class GridType0, class GridType1, class VectorType>
void HeterogeneousTwoBodyAssembler<GridType0, GridType1, VectorType>::
setup()
{
    assembleTransferOperator();
    assembleObstacle();

}

template <class GridType0, class GridType1, class VectorType>
void HeterogeneousTwoBodyAssembler<GridType0, GridType1, VectorType>::
assembleTransferOperator()
{
    if (grid0_==NULL || grid1_==NULL)
        DUNE_THROW(Dune::Exception, "You have not provided two grids!");

    if (!coupling_.patch0())
        DUNE_THROW(Dune::Exception, "You have to supply a nonmortar with the obstacles!");

    if (!coupling_.patch1())
        DUNE_THROW(Dune::Exception, "You have to supply a mortar with the obstacles!");

    std::cout << "--- " << coupling_.patch0()->numVertices() << " obstacles marked on level 0! ---" << std::endl;

    // ////////////////////////////////////////////////////
    //   Set up Mortar element transfer operators
    // ///////////////////////////////////////////////////

    // Directly assemble the level coupling operator
    contactCoupling_->grid0_ = grid0_;
    contactCoupling_->grid1_ = grid1_;
    // Setup the contact boundaries on the fine levels
    contactCoupling_->setupContactPatch(*coupling_.patch0(),*coupling_.patch1());
    // Assemble the mortar and nonmortar matrix
    contactCoupling_->setup();

    // Compute local coordinate systems for the dofs with obstacles
    std::vector<Dune::FieldVector<field_type, dim> > directions;

    coupling_.projection()->project(contactCoupling_->nonmortarBoundary(),
            contactCoupling_->mortarBoundary(), coupling_.obsDistance_);
    coupling_.projection()->getObstacleDirections(directions);

    this->computeLocalCoordinateSystems(contactCoupling_->nonmortarBoundary(), this->localCoordSystems_, directions);

    // Assemble the basis transformation matrix
    setupTransformationMatrix();
}

template <class GridType0, class GridType1, class VectorType>
void HeterogeneousTwoBodyAssembler<GridType0, GridType1, VectorType>::
setupTransformationMatrix()
{

    const auto& nonmortarBoundary = contactCoupling_->nonmortarBoundary();

    std::vector<int> nonmortarToGlobal(nonmortarBoundary.numVertices());

    int idx = 0;
    for (int i=0; i<grid0_->size(dim); i++)
        if (nonmortarBoundary.containsVertex(i))
            nonmortarToGlobal[idx++] = i;

    // first create the index structure of the transformation matrix

    // P1 Basis is hardwired by now
    int nRows = grid0_->size(dim) + grid1_->size(dim1);
    Dune::MatrixIndexSet indicesBT(nRows, nRows);

    // BT_ is the identity plus some off-diagonal elements
    for (int i=0; i<nRows; i++)
        indicesBT.add(i,i);

    // the off-diagonal part
    const MatrixType& M = contactCoupling_->mortarLagrangeMatrix();

    for (size_t i=0; i<M.N(); i++) {

        const RowType& row = M[i];

        ConstColumnIterator cIt    = row.begin();
        ConstColumnIterator cEndIt = row.end();

        for (; cIt!=cEndIt; ++cIt) {
            indicesBT.add(nonmortarToGlobal[i], cIt.index() + grid0_->size(dim));
        }

    }

    indicesBT.exportIdx(BT_);
    BT_ = 0;

    // Enter identity part
    for (int i=0; i<nRows; i++)
        for (int j=0; j<dim; j++)
            BT_[i][i][j][j] = 1;

    for (size_t i=0; i<M.N(); i++) {

        const RowType& row = M[i];

        ConstColumnIterator cIt    = row.begin();
        ConstColumnIterator cEndIt = row.end();

        for (; cIt!=cEndIt; ++cIt) {
            BT_[nonmortarToGlobal[i]][cIt.index() + grid0_->size(dim)] = *cIt;
        }

    }
}

template <class GridType0, class GridType1, class VectorType>
void HeterogeneousTwoBodyAssembler<GridType0, GridType1, VectorType>::
assembleJacobian(const std::array<const MatrixType*, 2>& submat, MatrixType& totalMatrix)
{
    coupleSubMatrices(submat, totalMatrix);
    this->transformMatrix(totalMatrix);
}

template <class GridType0, class GridType1, class VectorType>
void HeterogeneousTwoBodyAssembler<GridType0, GridType1, VectorType>::
coupleSubMatrices(const std::array<const MatrixType*, 2>& submat,
                  MatrixType& totalMatrix)
{
    // ////////////////////////////////////////////////////
    //   Combine local stiffness matrices into one
    // ////////////////////////////////////////////////////

    int nRows = submat[0]->N()+submat[1]->N();
    int nCols = submat[0]->M()+submat[1]->M();

    Dune::MatrixIndexSet indices(nRows, nCols);

    // collect all indices of submat[0]
    for (size_t rowIdx=0; rowIdx<submat[0]->N(); rowIdx++) {

        const RowType& row = (*submat[0])[rowIdx];

        ConstColumnIterator cIt    = row.begin();
        ConstColumnIterator cEndIt = row.end();

        for(; cIt!=cEndIt; ++cIt)
            indices.add(rowIdx, cIt.index());

    }

    // collect all indices of submat[1]
    for (size_t rowIdx=0; rowIdx<submat[1]->N(); rowIdx++) {

        const RowType& row = (*submat[1])[rowIdx];

        ConstColumnIterator cIt    = row.begin();
        ConstColumnIterator cEndIt = row.end();

        for(; cIt!=cEndIt; ++cIt)
            indices.add(rowIdx+submat[0]->N(), cIt.index()+submat[0]->M());

    }

    // Enter indices into matrix
    Dune::BCRSMatrix<MatrixBlock> matrix;
    indices.exportIdx(matrix);

    // copy all entries of submat[0]
    for (size_t rowIdx=0; rowIdx<submat[0]->N(); rowIdx++) {

        const RowType& row = (*submat[0])[rowIdx];

        ConstColumnIterator cIt    = row.begin();
        ConstColumnIterator cEndIt = row.end();

        for(; cIt!=cEndIt; ++cIt)
            matrix[rowIdx][cIt.index()] = *cIt;

    }

    // copys all entries of submat[1]
    for (size_t rowIdx=0; rowIdx<submat[1]->N(); rowIdx++) {

        const RowType& row = (*submat[1])[rowIdx];

        ConstColumnIterator cIt    = row.begin();
        ConstColumnIterator cEndIt = row.end();

        for(; cIt!=cEndIt; ++cIt)
            matrix[rowIdx+submat[0]->N()][cIt.index()+submat[0]->M()] = *cIt;

    }

    // ////////////////////////////////////////////////////
    //   Modify the global stiffness matrix using the transfer operator
    // ////////////////////////////////////////////////////

    // Create the index set of B \hat{A} B^T
    Dune::MatrixIndexSet indicesBABT(nRows, nCols);

    for (int i=0; i<nRows; i++) {

        const RowType& row = matrix[i];

        // Loop over all columns of the stiffness matrix
        ConstColumnIterator j    = row.begin();
        ConstColumnIterator jEnd = row.end();

        for (; j!=jEnd; ++j) {

            ConstColumnIterator k    = BT_[i].begin();
            ConstColumnIterator kEnd = BT_[i].end();
            for (; k!=kEnd; ++k) {

                ConstColumnIterator l    = BT_[j.index()].begin();
                ConstColumnIterator lEnd = BT_[j.index()].end();

                for (; l!=lEnd; ++l)
                    indicesBABT.add(k.index(), l.index());

            }

        }

    }

    // Multiply B \hat{A} B^T
    indicesBABT.exportIdx(totalMatrix);
    totalMatrix = 0;

    for (int i=0; i<nRows; i++) {

        const RowType& row = matrix[i];

        // Loop over all columns of the stiffness matrix
        ConstColumnIterator j    = row.begin();
        ConstColumnIterator jEnd = row.end();

        for (; j!=jEnd; ++j) {

            ConstColumnIterator k    = BT_[i].begin();
            ConstColumnIterator kEnd = BT_[i].end();
            for (; k!=kEnd; ++k) {

                ConstColumnIterator l    = BT_[j.index()].begin();
                ConstColumnIterator lEnd = BT_[j.index()].end();
                for (; l!=lEnd; ++l) {

                    //BABT[k][l] += BT[i][k] * mat_[i][j] * BT[j][l];
                    MatrixBlock m_ij = *j;

                    MatrixBlock BT_ik_trans;
                    for (int m=0; m<dim; m++)
                        for (int n=0; n<dim; n++)
                            BT_ik_trans[m][n] = (*k)[n][m];

                    m_ij.leftmultiply(BT_ik_trans);
                    m_ij.rightmultiply(*l);

                    totalMatrix[k.index()][l.index()] += m_ij;

                }

            }

        }

    }

}

template <class GridType0, class GridType1, class VectorType>
void HeterogeneousTwoBodyAssembler<GridType0, GridType1, VectorType>::
concatenateVectors(const std::array<VectorType, 2>& parts, VectorType& whole)
{
    whole.resize(parts[0].size()+parts[1].size());
    size_t i=0;
    for (; i<parts[0].size(); i++)
        whole[i] = parts[0][i];

    int idx=i;
    for (i=0; i<parts[1].size(); i++, idx++)
        whole[idx] = parts[1][i];
}

template <class GridType0, class GridType1, class VectorType>
void HeterogeneousTwoBodyAssembler<GridType0, GridType1, VectorType>::
nodalToTransformed(const std::array<VectorType,2>& x, VectorType& transformedX) const
{
    // ////////////////////////////////////////////////////////////////////////////
    //   Turn the initial solutions from the nodal basis to the transformed basis
    // ////////////////////////////////////////////////////////////////////////////

    VectorType canonicalTotalX;

    concatenateVectors(x, canonicalTotalX);

    // Make small cg solver
    Dune::MatrixAdapter<MatrixType,VectorType,VectorType> op(getTransformationMatrix());

    // A preconditioner
    Dune::SeqILU<MatrixType,VectorType,VectorType> ilu0(getTransformationMatrix(),1.0);

    // A cg solver for nonsymmetric matrices
    Dune::BiCGSTABSolver<VectorType> bicgstab(op,ilu0,1E-4,100,0);

    // Object storing some statistics about the solving process
    Dune::InverseOperatorResult statistics;

    // Solve!
    transformedX = canonicalTotalX;  // seems to be a good initial value
    bicgstab.apply(transformedX, canonicalTotalX, statistics);

    this->transformVector(transformedX);
}

} /* namespace Contact */
} /* namespace Dune */
