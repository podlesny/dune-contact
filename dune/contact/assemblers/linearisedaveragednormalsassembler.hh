// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=2 sw=2 et sts=2:
#ifndef DUNE_CONTACT_ASSEMBLERS_LINEARISED_AVERAGED_NORMALS_ASSEMBLER_HH
#define DUNE_CONTACT_ASSEMBLERS_LINEARISED_AVERAGED_NORMALS_ASSEMBLER_HH

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/promotiontraits.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/istl/matrixindexset.hh>

#include <dune/fufem/boundarypatch.hh>
#include <dune/matrix-vector/crossproduct.hh>

namespace Dune {
namespace Contact {

/** \brief Assembler for the linearisation of a nodally averaged normal field w.r.t the deformation.*/
class LinearisedAveragedNormalsAssembler
{

public:
  /** \brief Assemble the linearisation.
   *
   * This is neccessary when considering large deformation contact problems.
   * The normal field is there defined on the nonmortar contact boundary
   * which is reduced corresponding to where dune-grid-glue could project
   * the surfaces.
   * To compute the correct linearisations at the boundary of the reduced
   * patch, one also has to linearise w.r.t to corners of the unreduced
   * patch, which have a common edge with a reduced patch vertex.
   */
  template <class GridView, class Matrix>
  static void assemble(const BoundaryPatch<GridView>& reducedPatch,
                const BoundaryPatch<GridView>& unreducedPatch,
                Matrix& avNormDeriv);
};

template <class GridView, class Matrix>
void LinearisedAveragedNormalsAssembler::assemble(const BoundaryPatch<GridView>& reducedPatch,
                                          const BoundaryPatch<GridView>& unreducedPatch,
                                          Matrix& avNormDeriv)
{
  const static int dim = GridView::dimension;

  const GridView& gridView = reducedPatch.gridView();
  const auto& indexSet = gridView.indexSet();

  // the rows are given in local coordinates of the boundary patch
  Dune::MatrixIndexSet matIndexSet(reducedPatch.numVertices(),gridView.size(dim));

  std::vector<int> globalToLocal = reducedPatch.makeGlobalToLocal();

  using field_type = typename PromotionTraits<typename GridView::ctype,
                     typename Matrix::field_type>::PromotedType;
  using Coordinate = FieldVector<field_type, dim>;

  std::vector<Coordinate> avNormals(reducedPatch.numVertices(), Coordinate(0));

  // compute index pattern and averaged normals
  for (const auto bIt : unreducedPatch) {

    const auto& inside = bIt.inside();

    const auto& ref = Dune::ReferenceElements<field_type, dim>::general(inside.type());

    size_t nCorners = ref.size(bIt.indexInInside(), 1 , dim);
    std::vector<int> globalIdx(nCorners), localIdx;

    const auto& geomInInside = bIt.geometryInInside();

    for (size_t i=0; i < nCorners; i++) {

      globalIdx[i] = indexSet.subIndex(inside, ref.subEntity(bIt.indexInInside(), 1, i, dim), dim);

      if (!reducedPatch.containsVertex(globalIdx[i]))
        continue;

      localIdx.push_back(globalToLocal[globalIdx[i]]);

      const auto& posInElement = ref.position(i, dim);

      auto posInBoundary = geomInInside.local(posInElement);
      avNormals[localIdx.back()] += bIt.unitOuterNormal(posInBoundary);
    }

    // each vertex couples with all neighbouring vertices in the unreduced patch
    for (const auto& rowIdx  : localIdx)
      for (const auto& globalColIdx : globalIdx)
        matIndexSet.add(rowIdx, globalColIdx);
  }

  matIndexSet.exportIdx(avNormDeriv);
  avNormDeriv = 0;

  using MatrixBlock = typename Matrix::block_type;
  auto dyadicProduct = [] (const Coordinate& a, const Coordinate& b) {

    MatrixBlock c(0);

    for (int i=0; i<dim; i++)
      c[i].axpy(a[i],b);
    return c;
  };

  // now add the entries
  for (const auto& bIt : unreducedPatch) {

    const auto& inside = bIt.inside();
    const auto& insideGeometry = inside.geometry();

    const auto& ref = Dune::ReferenceElements<field_type, dim>::general(inside.type());

    size_t nCorners = ref.size(bIt.indexInInside(),1,dim);
    std::vector<int> globalIdx(nCorners), localIdx;
    std::vector<Coordinate> corners(nCorners);

    bool noVertexInReducedPatch = true;
    for (size_t k=0; k < nCorners; k++) {

      // global index
      int idx = ref.subEntity(bIt.indexInInside(), 1, k, dim);
      globalIdx[k] = indexSet.subIndex(inside, idx, dim);
      corners[k] = insideGeometry.corner(idx);

      if (reducedPatch.containsVertex(globalIdx[k])) {
        localIdx.push_back(globalToLocal[globalIdx[k]]);
        noVertexInReducedPatch = false;
      }
    }

    if (noVertexInReducedPatch)
      continue;

    Coordinate triangleNormal(0);
    if (dim==3)
      triangleNormal = Dune::MatrixVector::crossProduct(corners[1] - corners[0], corners[2] - corners[0]);
    else if (dim==2) {
      triangleNormal[0] = corners[1][1] - corners[0][1];
      triangleNormal[1] = corners[0][0] - corners[1][0];
    }

    // if the normal is not pointing outwards we have to switch the edges
    if ((triangleNormal*bIt.unitOuterNormal(FieldVector<field_type,dim-1>(0.0))) < 0 ) {

      std::swap(corners[dim-2], corners[dim-1]);
      std::swap(globalIdx[dim-2], globalIdx[dim-1]);

      triangleNormal *= -1;
    }

    field_type normNor = triangleNormal.two_norm();

    std::vector<MatrixBlock> linTriNormal(nCorners, MatrixBlock(0));

    for (size_t j=0; j < nCorners; j++) {

      // compute opposite edge and linearised normal
      Coordinate linNorm, edge;
      if (dim==3) {
        edge = corners[(j+2)%nCorners] - corners[(j+1)%nCorners];
        linNorm = Dune::MatrixVector::crossProduct(triangleNormal, edge);
      } else if (dim==2) {
        edge = {{0, std::pow(-1,j)}};
        linNorm = corners[j] - corners[(j+1)%nCorners];
      }

      // skew symmetric term
      for (int k=0; k < dim; k++) {
        for (int l=k+1;l<dim; l++) {
          linTriNormal[j][k][l] = edge[dim-(k+l)]*std::pow(-1,k+l)/normNor;
          linTriNormal[j][l][k] = -linTriNormal[j][k][l];
        }
      }

      // derivative of the norm
      linTriNormal[j].axpy(-1.0/std::pow(normNor,3), dyadicProduct(triangleNormal, linNorm));
    }

    for (const auto& rowIdx : localIdx) {

      field_type sumNorm = avNormals[rowIdx].two_norm();

      for (size_t j=0; j< globalIdx.size(); j++) {

        // derivation of the summed normals
        avNormDeriv[rowIdx][globalIdx[j]].axpy(1/sumNorm, linTriNormal[j]);

        // derivation of the norm of the summed normals
        Coordinate avNorLinTri;
        linTriNormal[j].mtv(avNormals[rowIdx], avNorLinTri);

        avNormDeriv[rowIdx][globalIdx[j]].axpy(-1.0/(std::pow(sumNorm, 3)),
                                                    dyadicProduct(avNormals[rowIdx], avNorLinTri));
      }
    }
  }
}

} /* namespace Contact */
} /* namespace Dune */

#endif
