// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=2 sw=2 sts=2:
#ifndef DUNE_CONTACT_ASSEMBLERS_LOCAL_INTERSECTION_ASSEMBLER_HH
#define DUNE_CONTACT_ASSEMBLERS_LOCAL_INTERSECTION_ASSEMBLER_HH

#include <dune/common/promotiontraits.hh>

#include <dune/fufem/boundarypatch.hh>

namespace Dune {
namespace Contact {


/** \brief Base class to assemble the linearisation of the remote intersection corners.*/
template <class GlueType>
class LocalIntersectionAssembler
{
protected:
  using GridView0 = typename GlueType::Grid0View;
  using GridView1 = typename GlueType::Grid1View;
  using ctype = typename PromotionTraits<typename GridView0::ctype,
                                         typename GridView1::ctype>::PromotedType;

  const static int dim = GridView0::dimension;

public:

  LocalIntersectionAssembler(const BoundaryPatch<GridView0>& reducedPatch0,
                                  const BoundaryPatch<GridView0>& unreducedPatch0,
                                  const BoundaryPatch<GridView1>& reducedPatch1,
                                  const BoundaryPatch<GridView1>& unreducedPatch1,
                                  ctype overlap, ctype maxNormalProduct) :
      patches0_({{reducedPatch0, unreducedPatch0}}),
      patches1_({{reducedPatch1, unreducedPatch1}}),
      overlap_(overlap), maxNormalProduct_(maxNormalProduct)
  {
    redGlobalToLocal0_ = reducedPatch0.makeGlobalToLocal();
    redGlobalToLocal1_ = reducedPatch1.makeGlobalToLocal();
  }

  //! Get reduced grid0 patch
  const BoundaryPatch<GridView0>& reducedPatch0() const {return patches0_[0].get();}
  //! Get reduced grid1 patch
  const BoundaryPatch<GridView1>& reducedPatch1() const {return patches1_[0].get();}
  //! Get unreduced grid0 patch
  const BoundaryPatch<GridView0>& unreducedPatch0() const {return patches0_[1].get();}
  //! Get unreduced grid1 patch
  const BoundaryPatch<GridView1>& unreducedPatch1() const {return patches1_[1].get();}

protected:
  //! the reduced and unreduced grid0 patches
  const std::array<std::reference_wrapper<const BoundaryPatch<GridView0> >,2> patches0_;

  //! the reduced and unreduced grid1 patches
  const std::array<std::reference_wrapper<const BoundaryPatch<GridView1> >,2> patches1_;

  //! global to local reduced grid0 indices
  std::vector<int> redGlobalToLocal0_;

  //! global to local reduced grid0 indices
  std::vector<int> redGlobalToLocal1_;

  //! the allowed overlap of the surfaces
  ctype overlap_;

  //! minimum angle in radians between normals at the point and its projection
  ctype maxNormalProduct_;
};

} /* namespace Contact */
} /* namespace Dune */

#endif
