// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#include <dune/common/exceptions.hh>

#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/type.hh>

#include <dune/istl/matrixindexset.hh>
#include <dune/istl/io.hh>

#include <dune/localfunctions/lagrange/pqkfactory.hh>

#include <dune/fufem/boundarypatchprolongator.hh>
#include <dune/fufem/boundarywriter.hh>
#include <dune/fufem/dgindexset.hh>

#include <dune/matrix-vector/addtodiagonal.hh>

#include <dune/contact/solvers/dgtransfer.hh>

#include <dune/grid-glue/merging/contactmerge.hh>
#include <dune/grid-glue/gridglue.hh>
#include <dune/grid-glue/extractors/codim1extractor.hh>
//#include <dune/grid-glue/adapter/gridgluevtkwriter.hh>
//#include <dune/grid-glue/adapter/gridglueamirawriter.hh>

#include <dune/contact/common/dualbasisadapter.hh>

namespace Dune {
namespace Contact {

/** \todo Properly assemble the nonmortar matrix by integrating over the possibly deformed elements.*/
template <class field_type, class GridType0, class GridType1>
void DualMortarCouplingHierarchy<field_type, GridType0, GridType1>::assembleNonmortarLagrangeMatrices()
{
    int topLevel = std::max(grid0_->maxLevel(),grid1_->maxLevel());
    nonmortarLagrangeMat_.resize(topLevel+1);

    for (int level=0; level<=topLevel; level++) {

        int level0 = std::min(level,grid0_->maxLevel());

        // Create mapping from the global set of block dofs to the ones on the contact boundary
        std::vector<int> globalToLocal;
        nonmortarBoundaryHierarchy_[level0].makeGlobalToLocal(globalToLocal);

        // clear matrix
        nonmortarLagrangeMat_[level] = Dune::BDMatrix<MatrixBlock>(nonmortarBoundaryHierarchy_[level0].numVertices());
        nonmortarLagrangeMat_[level] = 0;

        const auto& indexSet = grid0_->levelIndexSet(level0);
        // loop over all faces of the boundary patch
        for (const auto& nIt : nonmortarBoundaryHierarchy_[level0]) {

            const auto& geometry =  nIt.geometry();

            const int numCorners = geometry.corners();

            Dune::FieldVector<ctype, dim-1> dummy(0);
            ctype intElem = geometry.integrationElement(dummy);

            field_type sfI = (numCorners==3) ? intElem/6.0 : intElem/numCorners;

            // turn scalar element mass matrix into vector-valued one
            // and add element mass matrix to the global matrix

            // Get global node ids
            const auto& inside = nIt.inside();
            const auto& refElement = Dune::ReferenceElements<ctype, dim>::general(inside.type());

            // Add localMatrix to nonmortarLagrangeMat
            for (int i=0; i<numCorners; i++) {
                int v = globalToLocal[indexSet.subIndex(inside,refElement.subEntity(nIt.indexInInside(), 1, i, dim),dim)];
                Dune::MatrixVector::addToDiagonal(nonmortarLagrangeMat_[level][v][v],sfI);
            }

        }

    }

}


template <class field_type, class GridType0, class GridType1>
void DualMortarCouplingHierarchy<field_type, GridType0, GridType1>::
condenseDGMatrix(const Dune::BCRSMatrix<Dune::FieldMatrix<field_type,1,1> >& dgMatrix,
                 int level)
{
    // If the max levels don't equal we make a virtual copy of the grid level
    int level0 = std::min(level,grid0_->maxLevel());
    int level1 = std::min(level,grid1_->maxLevel());

    // create dg index set
    typename GridType0::LevelGridView levelView = grid0_->levelGridView(level0);
    DGIndexSet<typename GridType0::LevelGridView> dgIndex(levelView);

    // Create mapping from the global set of block dofs to the ones on the contact boundary
    std::vector<int> globalToLocal;
    nonmortarBoundaryHierarchy_[level0].makeGlobalToLocal(globalToLocal);

    const auto& indexSet = grid0_->levelIndexSet(level0);

    // ///////////////////////////////////////////////////////////
    //   Get the occupation structure for the condensed matrix
    // ///////////////////////////////////////////////////////////
    Dune::MatrixIndexSet condensedIndices(nonmortarBoundaryHierarchy_[level0].numVertices(),
                                          grid1_->size(level1, dim));

    typedef typename Dune::BCRSMatrix<Dune::FieldMatrix<field_type,1,1> >::row_type DGRowType;
    typedef typename DGRowType::ConstIterator DGColumnIterator;

    // loop over all faces of the boundary patch
    for (const auto& nIt : nonmortarBoundaryHierarchy_[level0]) {

        const auto&  eIt = nIt.inside();

        const auto& refElement = Dune::ReferenceElements<ctype, dim>::general(eIt.type());

        int n = refElement.size(nIt.indexInInside(),1,dim);

        for (int i=0; i<n; i++) {

            int dgIdx      = dgIndex(eIt, nIt.indexInInside())+i;
            int faceIdxi   = refElement.subEntity(nIt.indexInInside(),1,i,dim);
            int lagrangeIdx = indexSet.subIndex(eIt,faceIdxi,dim);

            const DGRowType& dgRow = dgMatrix[dgIdx];

            DGColumnIterator dgCIt    = dgRow.begin();
            DGColumnIterator dgEndCIt = dgRow.end();

            for (; dgCIt!=dgEndCIt; ++dgCIt)
                condensedIndices.add(globalToLocal[lagrangeIdx], dgCIt.index());

        }

    }

    // ///////////////////////////////////////////////////////////
    //   Condense the matrix values
    // ///////////////////////////////////////////////////////////
    condensedIndices.exportIdx(mortarLagrangeMat_[level]);
    mortarLagrangeMat_[level] = 0;

    // Loop over all neighbors
    for (const auto& nIt : nonmortarBoundaryHierarchy_[level0]) {

        const auto& eIt = nIt.inside();

        const auto& refElement = Dune::ReferenceElements<ctype, dim>::general(eIt.type());

        int n = refElement.size(nIt.indexInInside(),1,dim);

        for (int i=0; i<n; i++) {

            int dgIdx = dgIndex(eIt, nIt.indexInInside())+i;

            const DGRowType& dgRow = dgMatrix[dgIdx];

            DGColumnIterator dgCIt    = dgRow.begin();
            DGColumnIterator dgEndCIt = dgRow.end();

            for (; dgCIt!=dgEndCIt; ++dgCIt) {

                switch (n) {

                    case 2:
                    case 3:

                        for (int j=0; j<n; j++) {
                            int faceIdxj = refElement.subEntity(nIt.indexInInside(),1,j,dim);
                            int globalIdx = globalToLocal[indexSet.subIndex(eIt,faceIdxj,dim)];
                            Dune::MatrixVector::addToDiagonal(mortarLagrangeMat_[level][globalIdx][dgCIt.index()]
                               ,(i==j) ? (n*(*dgCIt)[0][0]) : (-1*(*dgCIt)[0][0]));

                        }
                        break;

                    case 4: {

                                /* Trick: For each vertex i of the current boundary face, I want the other ones in a
                                   circular order.  I CANNOT just write i, (i+1)%4, (i+2)%4, (i+3)%4, because
                                   quadrilateral vertices are not numbered cyclically.  Therefore, I switch to
                                   cyclic ordering by using i to index the renumber array, then add 1,2,3 and
                                   switch back.  Apparently this works. */
                                const int renumber[4] = {0,1,3,2};

                                int globalIdx[4];
                                for (int j=0; j<4; j++) {
                                    auto& circular = renumber[(renumber[i]+j)%4];
                                    globalIdx[j] = indexSet.subIndex(eIt,
                                                  refElement.subEntity(nIt.indexInInside(),1,circular,dim), dim);
                                }

                                Dune::MatrixVector::addToDiagonal(mortarLagrangeMat_[level][globalToLocal[globalIdx[0]]][dgCIt.index()], 4*(*dgCIt)[0][0]);
                                Dune::MatrixVector::addToDiagonal(mortarLagrangeMat_[level][globalToLocal[globalIdx[1]]][dgCIt.index()],-2*(*dgCIt)[0][0]);
                                Dune::MatrixVector::addToDiagonal(mortarLagrangeMat_[level][globalToLocal[globalIdx[2]]][dgCIt.index()],(*dgCIt)[0][0]);
                                Dune::MatrixVector::addToDiagonal(mortarLagrangeMat_[level][globalToLocal[globalIdx[3]]][dgCIt.index()],-2 *(*dgCIt)[0][0]);

                                break;
                            }
                    default:
                            DUNE_THROW(Dune::NotImplemented, "Does not work for this type of boundary segments");
                }

            }

        }

    }
}

template <class field_type, class GridType0, class GridType1>
void DualMortarCouplingHierarchy<field_type, GridType0, GridType1>::
markRelevantOverlaps(const GlueType& gridGlue, Dune::BitSetVector<1>& relevantOverlaps)
{
    int maxlevel = grid0_->maxLevel();

    // ///////////////////////////////////////////////////////////////////
    //   Get all fine grid boundary segments which carry a valid mapping
    //   The external contact library ensures if the contact mapping is defined on
    //   parts of a BS it is defined on the entire BS.
    // ///////////////////////////////////////////////////////////////////

    LevelBoundaryPatch0 validFineBS(grid0_->levelGridView(maxlevel));

    // Only use fine grid boundary segments that are totally covered by the grid-glue segments
    typedef std::pair<int,int> Pair;
    std::map<Pair,ctype> coveredArea, fullArea;

    const auto& indexSet0 = grid0_->levelIndexSet(maxlevel);

    // initialize with area of boundary faces
    for (const auto& bIt : nonmortarBoundaryHierarchy_.back()) {
        const Pair p(indexSet0.index(bIt.inside()),bIt.indexInInside());
        fullArea[p] = bIt.geometry().volume();
        coveredArea[p] = 0;
    }

    // sum up the remote intersection areas to find out which are totally covered
    for (const auto& rIs : intersections(gridGlue))
        coveredArea[Pair(indexSet0.index(rIs.inside()),rIs.indexInInside())] += rIs.geometry().volume();

    // add all fine grid faces that are totally covered by the contact mapping
    for (const auto& bIt : nonmortarBoundaryHierarchy_.back()) {

        const auto& inside = bIt.inside();
        if(coveredArea[Pair(indexSet0.index(inside),bIt.indexInInside())]/
            fullArea[Pair(indexSet0.index(inside),bIt.indexInInside())] >= this->coveredArea_)
            validFineBS.addFace(inside, bIt.indexInInside());
    }

    // Use all faces, even the ones that are not totally covered, this might lead to negative obstacles
    //for (const auto& rIs : intersections(gridGlue))
    //    validFineBS.addFace(rIs.inside(), rIs.indexInInside());

    // //////////////////////////////////////////////////////////////////
    //   Special handling for the case maxlevel==0
    // //////////////////////////////////////////////////////////////////

    if (maxlevel==0) {

        // Restrict the hasObstacle fields to the relevant obstacles
        nonmortarBoundaryHierarchy_[0] = validFineBS;

        std::cout << nonmortarBoundaryHierarchy_[0].numFaces() << " relevant boundary segments on level 0!" << std::endl;
        relevantOverlaps.resize(gridGlue.size());

        int i(0);
        for (const auto& rIs : intersections(gridGlue))
            relevantOverlaps[i++] = nonmortarBoundaryHierarchy_[0].contains(rIs.inside(), rIs.indexInInside());

        // recompute the fine leaf nonmortar boundary patch
        BoundaryPatchProlongator<GridType0>::prolong(nonmortarBoundaryHierarchy_[0],this->nonmortarBoundary_);


        return;

    }

    // 1. Possibility: Only use the coarse faces that are totally covered by the contact mapping

    // //////////////////////////////////////////////////////////////////
    //   Count for each coarse grid boundary segment how many of its fine
    //   grid bs carry the valid mapping.
    // //////////////////////////////////////////////////////////////////

    //these are gonna be the faces that are not totally covered by validFineBS
    std::vector<LevelBoundaryPatch0> dismissedFaces(maxlevel+1);
    for (int j=0;j<=maxlevel;j++)
        dismissedFaces[j].setup(grid0_->levelGridView(j));

    // loop over nonmortar faces
    for (const auto& bIt : nonmortarBoundaryHierarchy_.back()) {

        //if a face belongs to the nonmortar boundary and if it does not carry a mapping we add it here
        //	//and dismiss its coarse father from the relevantNonmortar patch
        const auto& inside = bIt.inside();
        if (validFineBS.contains(inside,bIt.indexInInside()))
            continue;

        auto fatherIt = FaceHierarchy<GridType0>::findFatherFaceIntersection(*grid0_, inside,bIt.indexInInside());
        dismissedFaces[maxlevel-1].addFace(fatherIt->inside(),fatherIt->indexInInside());
    }

    //get all father faces of the dismissed faces
    for (int j=maxlevel-1;j>0;j--){

        for (const auto& it : dismissedFaces[j]) {
            auto fatherIt = FaceHierarchy<GridType0>::findFatherFaceIntersection(*grid0_, it.inside(),it.indexInInside());

            dismissedFaces[j-1].addFace(fatherIt->inside(),fatherIt->indexInInside());
        }
    }

    // boundary hierarchy to save the reduced nonmortar boundary
    std::vector<LevelBoundaryPatch0> relevantNonmortar(maxlevel+1);
	relevantNonmortar[0].setup(grid0_->levelGridView(0));

	//only add the coarse grid nonmortar faces that are not marked as dismissed
        for (const auto& it : nonmortarBoundaryHierarchy_[0]) {
        const auto& inside = it.inside();
		if(!dismissedFaces[0].contains(inside,it.indexInInside()))
			relevantNonmortar[0].addFace(inside,it.indexInInside());
    }

    // Propagate from coarse fine
    BoundaryPatchProlongator<GridType0>::prolong(relevantNonmortar);

    // 2. Possibility: Take all coarse faces
    /*
    // boundary hierarchy to save the reduced nonmortar boundary
    std::vector<LevelBoundaryPatch0> relevantNonmortar(maxlevel+1);
	relevantNonmortar.back() = validFineBS;

    //add all father faces
    for (int j=maxlevel;j>0;j--) {
        relevantNonmortar[j-1].setup(grid0_->levelGridView(j-1));

         for (const auto& it : relevantNonmortar[j]) {
             auto fatherIt = FaceHierarchy<GridType0>::findFatherFaceIntersection(*grid0_, it.inside(),it.indexInInside());
             relevantNonmortar[j-1].addFace(fatherIt->inside(),fatherIt->indexInInside());
         }
     }
    */

    for (size_t i=0; i<relevantNonmortar.size(); i++)
        std::cout << relevantNonmortar[i].numFaces() << " relevant boundary segments on level " << i << std::endl;

    // ///////////////////////////////////////
    //   Mark the relevant overlaps
    // ///////////////////////////////////////

    relevantOverlaps.resize(gridGlue.size());

    const auto& remoteIndexSet = gridGlue.indexSet();
    for (const auto& rIs : intersections(gridGlue))
        relevantOverlaps[remoteIndexSet.index(rIs)] = relevantNonmortar[maxlevel].contains(rIs.inside(), rIs.indexInInside());

    // Restrict the nonmortar boundary to the relevant faces
    nonmortarBoundaryHierarchy_ = relevantNonmortar;

    // recompute the fine leaf nonmortar boundary patch
    BoundaryPatchProlongator<GridType0>::prolong(nonmortarBoundaryHierarchy_[0],this->nonmortarBoundary_);
}


template <class field_type, class GridType0, class GridType1>
void DualMortarCouplingHierarchy<field_type,GridType0,GridType1>::setup()
{

    int topLevel = std::max(grid0_->maxLevel(),grid1_->maxLevel());

    //////////////////////////////////
    // Determine the contact boundarys
    // //////////////////////////////

    typedef typename GridType0::LevelGridView GridView0;
    typedef typename GridType1::LevelGridView GridView1;

    GridView0 gridView0 = grid0_->levelGridView(grid0_->maxLevel());
    GridView0 gridView1 = grid1_->levelGridView(grid1_->maxLevel());

    typedef Dune::GridGlue::Codim1Extractor<GridView0> Extractor0;
    typedef Dune::GridGlue::Codim1Extractor<GridView1> Extractor1;

    typedef Dune::PQkLocalFiniteElementCache<ctype, field_type, dim, 1> FiniteElementCache0;
    typedef Dune::PQkLocalFiniteElementCache<typename GridType1::ctype, field_type, GridType1::dimension, 1> FiniteElementCache1;

    // cache for the dual functions on the boundary
    using DualCache = Dune::Contact::DualBasisAdapter<GridView0, field_type>;

    using Element0 = typename GridView0::template Codim<0>::Entity;
    using Element1 = typename GridView1::template Codim<0>::Entity;

    auto desc0 = [&] (const Element0& e, unsigned int face) {
        return nonmortarBoundaryHierarchy_.back().contains(e,face);
    };

    auto desc1 = [&] (const Element1& e, unsigned int face) {
        return mortarBoundaryHierarchy_.back().contains(e,face);
    };

    auto extract0 = std::make_shared<Extractor0>(gridView0,desc0);
    auto extract1 = std::make_shared<Extractor1>(gridView1,desc1);

    // psurface is the default backend
    if (!this->gridGlueBackend_)
        this->gridGlueBackend_ = std::make_shared< Dune::GridGlue::ContactMerge<dim, ctype> >(this->overlap_);
    GlueType glue(extract0, extract1, this->gridGlueBackend_);

    glue.build();

    std::cout << "Gluing successful!" << std::endl;
    //GridGlueVtkWriter::write<GlueType>(glue,debugPath_);
    //GridGlueAmiraWriter::write<GlueType>(glue,debugPath_);

    // Mark the overlaps that are relevant.  Overlaps are called relevant if they belong
    // to a base grid boundary segment which is completely covered by overlaps.
    Dune::BitSetVector<1> relevantOverlaps;
    markRelevantOverlaps(glue, relevantOverlaps);

    std::cout << glue.size() << " remote intersections found, "
              << relevantOverlaps.count() << " of which are relevant." << std::endl;


    // Assemble the diagonal matrix coupling the nonmortar side with the lagrange multiplyers there
    assembleNonmortarLagrangeMatrices();

    // Precompute the occupation pattern for the mortarLagrangeMatrix on the finest level
    DGIndexSet<GridView0> dgIndex(gridView0);

    mortarLagrangeMat_.resize(topLevel+1);

    // The matrix coupling the mortar side and the Lagrange multipliers
    std::vector<Dune::BCRSMatrix<Dune::FieldMatrix<field_type,1,1> > > dgMatrix(topLevel+1);

    //
    Dune::MatrixIndexSet dgMatIndices(dgIndex.size(), grid1_->size(dim));

    const auto& indexSet1 = gridView1.indexSet();
    const auto& indexSet0 = gridView0.indexSet();

    // loop over all intersections and compute the matrix entries
    const auto& remoteIndexSet = glue.indexSet();
    for (const auto& rIs : intersections(glue)) {

        if (!relevantOverlaps[remoteIndexSet.index(rIs)][0])
            continue;

        const auto& inside = rIs.inside();
        const auto& outside = rIs.outside();

        const auto& domainRefElement = Dune::ReferenceElements<ctype, dim>::general(inside.type());
        const auto& targetRefElement = Dune::ReferenceElements<ctype, dim>::general(outside.type());

        int nDomainVertices = domainRefElement.size(rIs.indexInInside(),1,dim);
        int nTargetVertices = targetRefElement.size(rIs.indexInOutside(),1,dim);

        for (int j=0; j<nDomainVertices; j++)
            for (int k=0; k<nTargetVertices; k++){

                int localIdx = targetRefElement.subEntity(rIs.indexInOutside(),1,k,dim);
                int globalTargetIdx = indexSet1.subIndex(outside, localIdx, dim);
                dgMatIndices.add(dgIndex(inside, rIs.indexInInside()) + j,
                        globalTargetIdx);
            }
    }

    // /////////////////////////////////////////////
    //   Enter all indices into the empty matrix
    // /////////////////////////////////////////////

    dgMatIndices.exportIdx(dgMatrix.back());

    // Clear it
    mortarLagrangeMat_.back() = 0;
    dgMatrix.back()           = 0;

    weakObstacle_.resize(nonmortarBoundaryHierarchy_.back().numVertices());
    weakObstacle_ = 0;


    //cache of local bases
    FiniteElementCache0 cache0;
    FiniteElementCache1 cache1;

    std::unique_ptr<DualCache> dualCache;
    dualCache = std::make_unique< Dune::Contact::DualBasisAdapterGlobal<GridView0, field_type> >();

    std::vector<int> globalToLocal;
    nonmortarBoundaryHierarchy_.back().makeGlobalToLocal(globalToLocal);
    auto avNormals = nonmortarBoundaryHierarchy_.back().getNormals();

    // loop over all intersections and compute the matrix entries
    for (const auto& rIs : intersections(glue)) {

        if (!relevantOverlaps[remoteIndexSet.index(rIs)][0])
            continue;

        const auto& inside = rIs.inside();
        const auto& outside = rIs.outside();

        const auto& domainRefElement = Dune::ReferenceElements<ctype, dim>::general(inside.type());

        const auto& targetRefElement = Dune::ReferenceElements<ctype, dim>::general(outside.type());

        int noOfMortarVec = targetRefElement.size(rIs.indexInOutside(),1,dim);

        // types of the elements supporting the boundary segments in question
        Dune::GeometryType nonmortarEType = inside.type();
        Dune::GeometryType mortarEType    = outside.type();

        // types of the corresponding faces in the supporting elements
        Dune::GeometryType nmFaceType = domainRefElement.type(rIs.indexInInside(),1);
        Dune::GeometryType mFaceType  = targetRefElement.type(rIs.indexInOutside(),1);

        // Select a quadrature rule
        // 2 in 2d and for integration over triangles in 3d.  If one (or both) of the two faces involved
        // are quadrilaterals, then the quad order has to be risen to 3 (4).
        int quadOrder = 2 + (!nmFaceType.isSimplex()) + (!mFaceType.isSimplex());
        const auto& quadRule = Dune::QuadratureRules<ctype, dim-1>::rule(rIs.type(), quadOrder);

        const auto& nonmortarFiniteElement = cache0.get(nonmortarEType);
        const auto& mortarFiniteElement = cache1.get(mortarEType);
        dualCache->bind(inside, rIs.indexInInside());

        std::vector<Dune::FieldVector<field_type,1> > nonmortarQuadValues,mortarQuadValues, dualQuadValues;

        const auto& rGeom = rIs.geometry();
        const auto& rGeomOutside = rIs.geometryOutside();
        const auto& rGeomInInside = rIs.geometryInInside();
        const auto& rGeomInOutside = rIs.geometryInOutside();

        // store which dofs belong to the face
        int nNonmortarFaceNodes = domainRefElement.size(rIs.indexInInside(),1,dim);
        std::vector<int> nonmortarFaceNodes;
        for (int i=0; i<nNonmortarFaceNodes; i++) {
            int faceIdxi = domainRefElement.subEntity(rIs.indexInInside(), 1, i, dim);
            nonmortarFaceNodes.push_back(faceIdxi);
        }

        for (const auto& quadPt : quadRule) {

            // compute integration element of overlap
            ctype integrationElement = rGeom.integrationElement(quadPt.position());

            // quadrature points in local element coordinates
            const auto nonmortarQuadPos = rGeomInInside.global(quadPt.position());
            const auto mortarQuadPos    = rGeomInOutside.global(quadPt.position());

            // The current quadrature point in world coordinates
            auto nonmortarQpWorld = rGeom.global(quadPt.position());
            auto mortarQpWorld    = rGeomOutside.global(quadPt.position());;

            // the gap direction (normal * gapValue)
            auto gapVector = mortarQpWorld  - nonmortarQpWorld;

            //evaluate all shapefunctions at the quadrature point
            nonmortarFiniteElement.localBasis().evaluateFunction(nonmortarQuadPos,nonmortarQuadValues);
            mortarFiniteElement.localBasis().evaluateFunction(mortarQuadPos,mortarQuadValues);
            dualCache->evaluateFunction(nonmortarQuadPos,dualQuadValues);

            // compute integrals on the slave (nonmortar) side
            // loop over all shape functions on the nonmortar side

            // loop over all Lagrange multiplier shape functions
            for (int j=0; j<nNonmortarFaceNodes; j++) {

                auto localIdx = nonmortarFaceNodes[j];
                int globalDomainIdx = indexSet0.subIndex(inside, localIdx, dim);

                // the weak obstacle
                weakObstacle_[globalToLocal[globalDomainIdx]][0] += integrationElement * quadPt.weight()
                    * dualQuadValues[localIdx] * (gapVector*avNormals[globalDomainIdx]);

                // Assemble extended operator on the DG space
                int rowIdx = dgIndex(inside, rIs.indexInInside()) + j;

                // loop over all shape functions on the mortar side
                for (int k=0; k<noOfMortarVec; k++) {

                    int colIdx = indexSet1.subIndex(outside,k,dim);
                    if (!mortarBoundaryHierarchy_.back().containsVertex(colIdx))
                        continue;

                    // Integrate over the product of two shape functions
                    field_type mortarMatValue = integrationElement * quadPt.weight()
                        * nonmortarQuadValues[localIdx]
                        * mortarQuadValues[k];

                    dgMatrix.back()[rowIdx][colIdx] += mortarMatValue;
                }
            }
        }
    }

    // ///////////////////////////////////////////////////////////////////////
    //   Restrict the dgMatrices to obtain the operator on the lower levels
    // ///////////////////////////////////////////////////////////////////////

    // ////////////////////////////////////////////////////////
    //      We assume that the maxLevel and leaf grid are equal!
    // ///////////////////////////////////////////////////
    for (int i=topLevel-1; i>=0; i--) {

        DGMultigridTransfer<GridType0> transfer;

        transfer.restrictCouplingOperator(*grid0_, *grid1_, i, i+1,
                                          dgMatrix[i+1],
                                          dgMatrix[i]);
    }


    // /////////////////////////////////////////////////////////////////////
    //   Condense dgMatrices and dgObstacles to obtain an operator on the dual mortar space
    // /////////////////////////////////////////////////////////////////////
    for (int i=0; i<=topLevel; i++)
        condenseDGMatrix(dgMatrix[i], i);

    // ///////////////////////////////////////
    //    Compute M = D^{-1} \hat{M}
    // ///////////////////////////////////////

    for (int level=0; level<=topLevel; level++) {

        MatrixType& M  = mortarLagrangeMat_[level];
        Dune::BDMatrix<MatrixBlock>& D = nonmortarLagrangeMat_[level];

        // First compute D^{-1}
        D.invert();

        // Then the matrix product D^{-1} \hat{M}
        for (auto rowIt = M.begin(); rowIt != M.end(); ++rowIt) {
            const auto rowIndex = rowIt.index();
            for (auto& entry : *rowIt)
                entry.leftmultiply(D[rowIndex][rowIndex]);
        }
    }

    // weakObstacles in transformed basis = D^{-1}*weakObstacle_
    for(size_t rowIdx=0; rowIdx<weakObstacle_.size(); rowIdx++)
        weakObstacle_[rowIdx] *= nonmortarLagrangeMat_.back()[rowIdx][rowIdx][0][0];

    this->gridGlueBackend_->clear();
}

} /* namespace Contact */
} /* namespace Dune */
