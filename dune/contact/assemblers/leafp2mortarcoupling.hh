// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_ASSEMBLERS_LEAF_P2_MORTAR_COUPLING_HH
#define DUNE_CONTACT_ASSEMBLERS_LEAF_P2_MORTAR_COUPLING_HH

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/functions/basisgridfunction.hh>

#include "dualmortarcoupling.hh"

namespace Dune {
namespace Contact {

/** \brief Assembles the transfer operator for two-body contact on the leaf level
    for second(!) order Lagrangian elements.
 */
template<class field_type, class GridType0, class GridType1=GridType0>
class LeafP2MortarCoupling : public DualMortarCoupling<field_type, GridType0, GridType1>
{
    using Base = DualMortarCoupling<field_type, GridType0, GridType1>;
    using typename Base::MatrixType;
    using typename Base::ctype;

    using typename Base::LeafBoundaryPatch0;
    using typename Base::LeafBoundaryPatch1;

    using VectorType0 = Dune::BlockVector<Dune::FieldVector<field_type,GridType0::dimension> >;
    using VectorType1 = Dune::BlockVector<Dune::FieldVector<field_type,GridType1::dimension> >;


    enum {dim = GridType0::dimension};

    enum {dimworld = GridType0::dimensionworld};


    static bool isOnFace(const Dune::GeometryType& type,
                         int entity, int codim,
                         int faceNumber) {

        const auto& refElement = Dune::ReferenceElements<ctype, dim>::general(type);

        for (int i = 0; i<refElement.size(faceNumber, 1, codim); i++)
            if (refElement.subEntity(faceNumber,1, i, codim) == entity)
                return true;

        return false;
    }

public:
    using Base::nonmortarBoundary_;
    using Base::mortarBoundary_;
    using Base::mortarLagrangeMatrix_;
    // this silences a -woverloaded-virtual warning
    using Base::setup;

    LeafP2MortarCoupling() = default;

    LeafP2MortarCoupling(const LeafBoundaryPatch0& nonmortarBoundary,
                         const LeafBoundaryPatch1& mortarBoundary)
        : Base(nonmortarBoundary.gridView().grid(),mortarBoundary.gridView().grid())
    {
        nonmortarBoundary_ = nonmortarBoundary;
        mortarBoundary_ = mortarBoundary;
    }

    /** \brief Sets up the mortar coupling operator on the leaf level
     * for given nonmortar and mortar basis */
    template <class Basis0, class Basis1>
    void setup(const BasisGridFunction<Basis0, VectorType0>& sol0,
               const BasisGridFunction<Basis1, VectorType1>& sol1);
    ~LeafP2MortarCoupling() {}

    //! Return the nonmortar Lagrange matrix
    const MatrixType& nonmortarLagrangeMatrix() const {
        return nonmortarLagrangeMatrix_;
    }

    //! Return the nonmortar Lagrange matrix
    MatrixType& nonmortarLagrangeMatrix() {
        return nonmortarLagrangeMatrix_;
    }

    //! Return the weak normals
    const VectorType0& weakNormals() const {
        return weakNormal_;
    }

    // /////////////////////////////////////////////
    //   Data members
    // /////////////////////////////////////////////

private:

    /** \brief The matrix coupling nonmortar side and Lagrange multiplyers */
    MatrixType nonmortarLagrangeMatrix_;

    VectorType0 weakNormal_;
};

} /* namespace Contact */
} /* namespace Dune */

#include "leafp2mortarcoupling.cc"

#endif
