// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_CONTACT_ASSEMBLERS_HETEROGENEOUS_TWO_BODY_ASSEMBLER_HH
#define DUNE_CONTACT_ASSEMBLERS_HETEROGENEOUS_TWO_BODY_ASSEMBLER_HH

#include <dune/contact/assemblers/contactassembler.hh>
#include <dune/contact/assemblers/dualmortarcouplinghierarchy.hh>
#include <dune/contact/assemblers/dualmortarcoupling.hh>
#include <dune/contact/common/couplingpair.hh>

#include <dune/fufem/boundarypatch.hh>

#include <dune/solvers/common/boxconstraint.hh>

namespace Dune {
namespace Contact {

template <class GridType0, class GridType1, class VectorType>
class HeterogeneousTwoBodyAssembler : public ContactAssembler<GridType0::dimension,typename VectorType::field_type>
{
    enum {dim = GridType0::dimension};
    enum {dim1 = GridType1::dimension};

    using field_type = typename VectorType::field_type;

    using MatrixBlock = Dune::FieldMatrix<field_type, dim, dim>;
    using MatrixType = Dune::BCRSMatrix<MatrixBlock>;

    using RowType = typename MatrixType::row_type;
    using ConstColumnIterator =  typename RowType::ConstIterator;

    using LevelBoundaryPatch0 = BoundaryPatch<typename GridType0::LevelGridView>;
    using LevelBoundaryPatch1 = BoundaryPatch<typename GridType1::LevelGridView>;

    using CouplingType = DualMortarCoupling<field_type, GridType0, GridType1>;
    using HierarchyCouplingType = DualMortarCouplingHierarchy<field_type, GridType0, GridType1>;

    using CouplingPair = Dune::Contact::CouplingPair<GridType0,GridType1,field_type>;
    using Projection = typename CouplingPair::Projection;
    using GridGlueBackend = typename CouplingPair::BackEndType;

public:

    /** \brief Default constructor

    Sets the coupling type to NONE and the obstacle distance to 0.
    */
    HeterogeneousTwoBodyAssembler() : grid0_(NULL), grid1_(NULL) {}

    HeterogeneousTwoBodyAssembler(const GridType0& grid0, const GridType0& grid1,
                                  std::shared_ptr<const LevelBoundaryPatch0> nonmortarPatch,
                                  std::shared_ptr<const LevelBoundaryPatch1> mortarPatch,
                                  field_type obsDistance, typename CouplingPairBase::CouplingType couplingType,
                                  std::shared_ptr<Projection> projection,
                                  std::shared_ptr<GridGlueBackend> backend = nullptr,
                                  bool hierarchyCoupling=false)
    {
        grid0_ = &grid0;
        grid1_ = &grid1;

        coupling_.set(0, 1, nonmortarPatch, mortarPatch, obsDistance, couplingType, projection, backend);

        if (hierarchyCoupling)
            contactCoupling_ = std::make_shared<HierarchyCouplingType>();
        else
            contactCoupling_ = std::make_shared<CouplingType>();

    }

    /** \brief Set up the assembler object
        \param grid0 The first grid involved in the contact problem
        \param grid1 The second grid involved in the contact problem
        \param nonmortarPatch The nonmortar boundary on level 0 of grid 0
        \param mortarPatch The mortar boundary on level 0 of grid 1
    */
    void setup(const GridType0& grid0, const GridType1& grid1,
               std::shared_ptr<const LevelBoundaryPatch0> nonmortarPatch,
               std::shared_ptr<const LevelBoundaryPatch1> mortarPatch,
               field_type obsDistance,
               typename CouplingPairBase::CouplingType couplingType,
               std::shared_ptr<Projection> projection,
               std::shared_ptr<GridGlueBackend> backend=nullptr);

    /** \brief Set up the assembler object using previous settings*/
    void setup();

    /** \brief Couple the two submatrices and transform then to normal/tangential coordinates */
    void assembleJacobian(const std::array<const MatrixType*, 2>& submat,
                  MatrixType& totalMatrix);

    /** \brief Combine two FE stiffness matrices and transform them to the special two-body-contact basis */
    void coupleSubMatrices(const std::array<const MatrixType*, 2>& submat,
                           MatrixType& totalMatrix);

    /** \brief Makes sure the solution vector is in canonical coordinates */
    void postprocess(const VectorType& totalX,
                     std::array<VectorType, 2>& x) const;

    /** \brief Concatenate two vectors */
    static void concatenateVectors(const std::array<VectorType, 2>& parts, VectorType& whole);

    void nodalToTransformed(const std::array<VectorType,2>& x,
                            VectorType& transformedX) const;

    const MatrixType& getTransformationMatrix() const {return BT_;}


    /** \brief Set the obstacles on the leaf level. */
    void assembleObstacle();

    /** \brief Set up the assembler object using previous settings*/
    void assembleTransferOperator();

  protected:

    /** \brief Setup the basis transformation matrix. */
    void setupTransformationMatrix();

    //! The basis transformation matrix
    MatrixType BT_;

  public:
    /** \brief The coupling pair. */
    CouplingPair coupling_;

    /** \brief The obstacles on the leafview. */
    std::vector<BoxConstraint<field_type,dim> > obstacles_;

    /** \brief The assembler for the mortar/nonmortar matrix. */
    std::shared_ptr<CouplingType> contactCoupling_;

    const GridType0* grid0_;
    const GridType1* grid1_;
};

} /* namespace Contact */
} /* namespace Dune */

#include "hettwobodyassembler.cc"

#endif
