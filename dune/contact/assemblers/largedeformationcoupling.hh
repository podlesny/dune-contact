// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:
#ifndef DUNE_CONTACT_ASSEMBLERS_LARGE_DEFORMATION_MORTAR_COUPLING_HH
#define DUNE_CONTACT_ASSEMBLERS_LARGE_DEFORMATION_MORTAR_COUPLING_HH

#include <dune/contact/assemblers/dualmortarcoupling.hh>
#include <dune/matrix-vector/transpose.hh>

namespace Dune {
namespace Contact {

/** \brief Assembles the transfer operator for two-body contact
 */
template<class field_type, class GridType0, class GridType1=GridType0>
class LargeDeformationCoupling : public DualMortarCoupling<field_type, GridType0, GridType1>
{

protected:

  static constexpr int dim = GridType0::dimension;
  static constexpr int dimworld = GridType0::dimensionworld;

  using Base = DualMortarCoupling<field_type, GridType0, GridType1>;
  using MatrixBlock = typename Base::MatrixBlock;
  using MatrixType = typename Base::MatrixType;
  using ctype = typename Base::ctype;
  using Coordinate = FieldVector<ctype, dim>;
  using CoordinateVector = std::vector<Coordinate>;
  using VectorType = Dune::BlockVector<Coordinate>;

  using LeafBoundaryPatch0 = typename Base::LeafBoundaryPatch0;
  using LeafBoundaryPatch1 = typename Base::LeafBoundaryPatch1;

  using LevelBoundaryPatch0 = typename Base::LevelBoundaryPatch0;
  using LevelBoundaryPatch1 = typename Base::LevelBoundaryPatch1;

public:

  using ProjectionType = typename GridGlue::ContactMerge<dimworld, ctype>::ProjectionType;
  using JacobianType = BCRSMatrix<FieldMatrix<field_type, 1, dim> >;
  using ObstacleVector = typename Base::ObstacleVector;
  using Glue = typename Base::Glue;

  using Base::Base;
  using Base::nonmortarBoundary_;
  using Base::mortarBoundary_;
  using Base::grid0_;
  using Base::grid1_;

  LargeDeformationCoupling(field_type overlap = 1e-2, field_type coveredArea = 1.0 - 1e-2, ProjectionType type = ProjectionType::CLOSEST_POINT)
    : Base(overlap, coveredArea),
      projectionType_(type)
  {}

  LargeDeformationCoupling(const GridType0& grid0, const GridType1& grid1,
                     field_type overlap = 1e-2, field_type coveredArea = 1.0 - 1e-2,
                     ProjectionType type = ProjectionType::CLOSEST_POINT)
    : Base(grid0, grid1, overlap, coveredArea),
      projectionType_(type)
    {}


  /** \brief Sets up the contact coupling operator on the grid leaf level */
  void setup();

  /** \brief Sets up the contact coupling operator and build the grid-glue projection. */
  void setup(bool build) {
    if (build)
      buildProjection();
    setup();
  }

  /** \brief Setup the grid-glue and build the boundary projection. */
  void buildProjection();

  /** \brief Assemble the obstacles, i.e., evaluate the nonlinear constraints.*/
  ObstacleVector assembleObstacles(bool build);

  /** \brief Get the mortar part of the linearisation. */
  const JacobianType& exactMortarMatrix() const {
    return exactMortarMatrix_;
  }

  /** \brief Get the nonmortar part of the linearisation  */
  const JacobianType& exactNonmortarMatrix() const {
    return exactNonmortarMatrix_;
  }

  /** \brief Setup leaf nonmortar and mortar patches. */
  void setupContactPatch(const LevelBoundaryPatch0& coarseNonmortar,
                         const LevelBoundaryPatch1& coarseMortar) {
    BoundaryPatchProlongator<GridType0>::prolong(coarseNonmortar, unrednonmortarBoundary_);
    BoundaryPatchProlongator<GridType1>::prolong(coarseMortar, unredmortarBoundary_);
  }

  /** \brief Return the leafnonmortar boundary. */
  const LeafBoundaryPatch0& unrednonmortarBoundary() const {
    return unrednonmortarBoundary_;
  }

  /** \brief Return the leafmortar boundary. */
  const LeafBoundaryPatch1& unredmortarBoundary() const {
    return unredmortarBoundary_;
  }

private:

  /** \brief Setup up the index pattern of the matrices */
  void setupIndexPattern(const MatrixType& linRemoteVertices,
                         const MatrixType& linAvNormals,
                         const std::vector<int>& nmGlobalToLocal,
                         const std::vector<int>& mGlobalToLocal);

  /** \brief Compute the dual covariant basis corresponding to the element geometry. */
  template <class Gradient>
  CoordinateVector dualCovBasis(const std::vector<Gradient>& jacobians,
      const CoordinateVector& corners) const
  {

    constexpr int jDim = Gradient::dimension;
    FieldMatrix<field_type,dim,jDim> mass(0);
    for (size_t j=0; j < corners.size(); ++j)
      for (size_t i=0; i < corners.size(); ++i)
        mass[j].axpy(corners[i][j], jacobians[i]);

    FieldMatrix<field_type, jDim, dim> trans;
    Dune::MatrixVector::transpose(mass, trans);
    auto inverse = mass.leftmultiplyany(trans);

    try {
      inverse.invert();
    } catch(Dune::FMatrixError& e) {
        DUNE_THROW(Dune::FMatrixError, "Computing dual covariant basis failed, metric not invertible: " << inverse.determinant() << " \n");
    }

    auto dual = inverse.rightmultiplyany(trans);

    CoordinateVector dualCovBasis(dual.N());
    for (size_t i=0; i<dual.N(); i++)
        dualCovBasis[i] = dual[i];

    return dualCovBasis;
  }

  /** \brief For each dof a bit specifying whether the dof carries an obstacle or not. */
  LeafBoundaryPatch0 unrednonmortarBoundary_;

  /** \brief The mortar boundary. */
  LeafBoundaryPatch1 unredmortarBoundary_;

  /** \brief The linearisation of the contact constraint w.r.t mortar dofs. */
  JacobianType exactMortarMatrix_;

  /** \brief The linearisation of the contact constraint w.r.t nonmortar dofs. */
  JacobianType exactNonmortarMatrix_;

  //! The type of the projection, i.e. normal or closest point
  ProjectionType projectionType_;

};

} /* namespace Contact */
} /* namespace Dune */

#include "largedeformationcoupling.cc"

#endif
