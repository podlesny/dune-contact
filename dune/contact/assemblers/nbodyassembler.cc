// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#include <memory>

#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>
#include <dune/matrix-vector/transpose.hh>
#include <dune/matrix-vector/blockmatrixview.hh>

#include <dune/fufem/boundarypatchprolongator.hh>
#include <dune/contact/projections/normalprojection.hh>

namespace Dune {
namespace Contact {

template <class GridType, class VectorType>
void NBodyAssembler<GridType, VectorType>::assembleTransferOperator()
{

    // /////////////////////////////////////////////////////////////////
    //   Check if contact data is present
    // /////////////////////////////////////////////////////////////////

    for (int i=0; i<nCouplings(); i++) {


         if (!coupling_[i].obsPatch_)
             DUNE_THROW(Dune::Exception, "You have to supply a nonmortar patch for the " << i << "-th coupling!");

         if (!coupling_[i].mortarPatch_)
             DUNE_THROW(Dune::Exception, "You have to supply a mortar patch for the " << i << "-th coupling!");
    }

    // ////////////////////////////////////////////////////
    //   Set up Mortar element transfer operators
    // ///////////////////////////////////////////////////

    std::cout<<"Setup mortar transfer operators\n";

    for (size_t i=0; i<contactCoupling_.size(); i++) {
        contactCoupling_[i]->setGrids(*grids_[coupling_[i].gridIdx_[0]],*grids_[coupling_[i].gridIdx_[1]]);
        contactCoupling_[i]->setupContactPatch(*coupling_[i].patch0(),*coupling_[i].patch1());
        contactCoupling_[i]->gridGlueBackend_ = coupling_[i].backend();
        contactCoupling_[i]->setCoveredArea(coveredArea_);
        contactCoupling_[i]->setup();
    }

    // setup Householder reflections
    assembleHouseholderReflections();

    // compute the mortar transformation matrix
    computeTransformationMatrix();
}

template <class GridType, class VectorType>
void NBodyAssembler<GridType, VectorType>::assembleHouseholderReflections()
{
    // //////////////////////////////////////////////////////////////////
    //   Compute local coordinate systems for the dofs with obstacles
    // //////////////////////////////////////////////////////////////////

    std::vector<std::vector<MatrixBlock> > coordinateSystems(nCouplings());

    for (int i=0; i<nCouplings(); i++) {

        double dist = coupling_[i].obsDistance_;
        auto projection = coupling_[i].projection();

        if (!projection)
            DUNE_THROW(Dune::Exception, "You have to supply a contact projection for the " << i << "-th coupling!");

        std::vector<Dune::FieldVector<field_type,dim> > directions;

        const auto& nonmortarBoundary = contactCoupling_[i]->nonmortarBoundary();
        const auto& mortarBoundary    = contactCoupling_[i]->mortarBoundary();

        projection->project(nonmortarBoundary, mortarBoundary,dist);
        projection->getObstacleDirections(directions);

        this->computeLocalCoordinateSystems(nonmortarBoundary,coordinateSystems[i], directions);
    }

    // ///////////////////////////////////////////////////////////////
    // Combine the coordinate systems for each grid to one long array
    // ///////////////////////////////////////////////////////////////
    this->localCoordSystems_.resize(numDofs());

    // initialise with identity
    Dune::ScaledIdentityMatrix<field_type,dim> id(1);
    for (size_t i=0; i<this->localCoordSystems_.size(); i++)
        this->localCoordSystems_[i] = id;

    for (int j=0; j<nCouplings(); j++) {

        int grid0Idx = coupling_[j].gridIdx_[0];

        // compute offset
        int idx = 0;
        for (int k=0;k<grid0Idx;k++)
            idx += grids_[k]->size(dim);

        const auto& nonmortarBoundary = contactCoupling_[j]->nonmortarBoundary();

        // if a body is non-mortar more then once, one has to be careful with not overwriting entries
        for (std::size_t k=0; k<coordinateSystems[j].size(); k++)
          if(nonmortarBoundary.containsVertex(k))
            this->localCoordSystems_[idx+k] = coordinateSystems[j][k];
    }
}

template <class GridType, class VectorType>
void NBodyAssembler<GridType, VectorType>::assembleObstacle()
{

    std::vector<std::vector<int> > globalToLocals(nCouplings());
    for (std::size_t i = 0; i < globalToLocals.size(); ++i)
        contactCoupling_[i]->nonmortarBoundary().makeGlobalToLocal(globalToLocals[i]);

    ///////////////////////////////////////
    //  Set the obstacle values
    ///////////////////////////////////////

    totalHasObstacle_.resize(numDofs(),false);

    for (int j=0; j<nCouplings(); j++) {

        int grid0Idx = coupling_[j].gridIdx_[0];
        const auto& nonmortarBoundary = contactCoupling_[j]->nonmortarBoundary();

        // compute offset
        int idx = 0;
        for (int k=0;k<grid0Idx;k++)
            idx += grids_[k]->size(dim);

        for (int k=0; k<grids_[grid0Idx]->size(dim); k++)
            if (nonmortarBoundary.containsVertex(k))
                totalHasObstacle_[idx+k] = true;
    }

    // //////////////////////////////////////////////////////////////////
    //   Set the obstacle distances
    // //////////////////////////////////////////////////////////////////

    // Combine the obstacle values for each grid to one long array
    totalObstacles_.resize(numDofs());

    for (size_t j=0; j<totalObstacles_.size(); j++)
        totalObstacles_[j].clear();

    // Init the obstacle values
    for (int i=0; i<nCouplings(); i++) {

        int grid0Idx = coupling_[i].gridIdx_[0];

        // compute offset
        int idx = 0;
        for (int k=0;k<grid0Idx;k++)
            idx += grids_[k]->size(dim);

        // The grids involved in this coupling
        const auto& indexSet = grids_[grid0Idx]->leafIndexSet();

        // the obstacles are stored using local indices
        const std::vector<int>& globalToLocal = globalToLocals[i];

        // the weak obstacles
        const auto& obs = contactCoupling_[i]->weakObstacle_;

        // get strong obstacles, the projection method of the correct level has already been called
        //std::vector<field_type> obs;
        //coupling_[i].projection()->getObstacles(obs);

        const auto& leafView = grids_[grid0Idx]->leafGridView();

        for (const auto& v : vertices(leafView)) {

            int vIdx = indexSet.index(v);
            if (globalToLocal[vIdx]<0)
                continue;

            // Set the obstacle
            switch (coupling_[i].type_) {

            case CouplingPairBase::CONTACT:
                totalObstacles_[idx+vIdx].upper(0) = obs[globalToLocal[vIdx]];
                break;

            case CouplingPairBase::STICK_SLIP:
                totalObstacles_[idx+vIdx].lower(0) = 0;
                totalObstacles_[idx+vIdx].upper(0) = 0;
                break;

            case CouplingPairBase::GLUE:
                for (int j=0; j<dim; j++) {
                    totalObstacles_[idx+vIdx].lower(j) = 0;
                    totalObstacles_[idx+vIdx].upper(j) = 0;
                }
                break;

            case CouplingPairBase::NONE:
                break;
            default:
                DUNE_THROW(Dune::NotImplemented, "Coupling type not handled yet!");
            }
        }
    }

}

template <class GridType, class VectorType>
void NBodyAssembler<GridType, VectorType>::
assembleJacobian(const std::vector<const MatrixType*>& submat,
                 MatrixType& totalMatrix) const
{
    // Create a block view of the grand matrix
    Dune::MatrixVector::BlockMatrixView<MatrixType> blockView(submat);

    int nRows = blockView.nRows();
    int nCols = blockView.nCols();

    // Create the index set of B \hat{A} B^T
    Dune::MatrixIndexSet indicesBABT(nRows, nCols);

    for (int i=0; i<nGrids(); i++) {

        for (size_t iRow = 0; iRow<submat[i]->N(); iRow++) {

            const RowType& row = (*submat[i])[iRow];

            // Loop over all columns of the stiffness matrix
            ConstColumnIterator j    = row.begin();
            ConstColumnIterator jEnd = row.end();

            for (; j!=jEnd; ++j) {

                ConstColumnIterator k    = BT_[blockView.row(i,iRow)].begin();
                ConstColumnIterator kEnd = BT_[blockView.row(i,iRow)].end();
                for (; k!=kEnd; ++k) {

                    ConstColumnIterator l    = BT_[blockView.col(i,j.index())].begin();
                    ConstColumnIterator lEnd = BT_[blockView.col(i,j.index())].end();

                    for (; l!=lEnd; ++l)
                        indicesBABT.add(k.index(), l.index());

                }

            }

        }

    }

    // ////////////////////////////////////////////////////////////////////
    //   Multiply transformation matrix to the global stiffness matrix
    // ////////////////////////////////////////////////////////////////////

    indicesBABT.exportIdx(totalMatrix);
    totalMatrix = 0;

    for (int i=0; i<nGrids(); i++) {

        for (size_t iRow = 0; iRow<submat[i]->N(); iRow++) {

            const RowType& row = (*submat[i])[iRow];

            // Loop over all columns of the stiffness matrix
            ConstColumnIterator j    = row.begin();
            ConstColumnIterator jEnd = row.end();

            for (; j!=jEnd; ++j) {

                ConstColumnIterator k    = BT_[blockView.row(i,iRow)].begin();
                ConstColumnIterator kEnd = BT_[blockView.row(i,iRow)].end();

                for (; k!=kEnd; ++k) {

                    ConstColumnIterator l    = BT_[blockView.col(i,j.index())].begin();
                    ConstColumnIterator lEnd = BT_[blockView.col(i,j.index())].end();

                    for (; l!=lEnd; ++l) {

                        //BABT[k][l] += BT[i][k] * mat_[i][j] * BT[j][l];
                        MatrixBlock m_ij = *j;

                        MatrixBlock BT_ik_trans;
                        Dune::MatrixVector::transpose(*k, BT_ik_trans);

                        m_ij.leftmultiply(BT_ik_trans);
                        m_ij.rightmultiply(*l);

                        totalMatrix[k.index()][l.index()] += m_ij;

                    }

                }

            }

        }

    }
}

template <class GridType, class VectorType>
template <class VectorTypeContainer>
void NBodyAssembler<GridType, VectorType>::
assembleRightHandSide(const VectorTypeContainer& rhs,
               VectorType& totalRhs) const
{
    // Concatenate the two rhs vectors to a large one
    VectorType untransformedTotalRhs(BT_.M());

    int idx = 0;

    for (size_t i=0; i<rhs.size(); i++) {

        for (size_t j=0; j<rhs[i].size(); j++)
            untransformedTotalRhs[idx++] = rhs[i][j];

    }

    if ((int) BT_.M() != idx)
        DUNE_THROW(Dune::Exception, "assembleRightHandSide: vector size and matrix size don't match!");

    // Transform the basis of the ansatz space
    totalRhs.resize(untransformedTotalRhs.size());
    totalRhs = 0;
    BT_.umtv(untransformedTotalRhs, totalRhs);
}


template <class GridType, class VectorType>
template <class VectorTypeContainer>
void NBodyAssembler<GridType, VectorType>::
postprocess(const VectorType& totalX, VectorTypeContainer& x) const
{
    // ///////////////////////////////////////////////////////
    //   Transform the solution vector to the nodal basis
    // ///////////////////////////////////////////////////////

    VectorType nodalBasisTotalX(totalX.size());
    BT_.mv(totalX, nodalBasisTotalX);


    // ///////////////////////////////////////////////////////
    //   Split the total solution vector into the parts
    //   corresponding to the grids.
    // ///////////////////////////////////////////////////////

    int idx = 0;
    for (int i=0; i<nGrids(); i++) {

        x[i].resize(grids_[i]->size(dim));

        for (size_t j=0; j<x[i].size(); j++, idx++)
            x[i][j] = nodalBasisTotalX[idx];
    }
}

template <class GridType, class VectorType>
template <class VectorTypeContainer>
void NBodyAssembler<GridType, VectorType>::
concatenateVectors(const VectorTypeContainer& parts, VectorType& whole)
{
    int totalSize = 0;
    for (size_t i=0; i<parts.size(); i++)
        totalSize += parts[i].size();

    whole.resize(totalSize);

    int idx = 0;
    for (size_t i=0; i<parts.size(); i++)
        for (size_t j=0; j<parts[i].size(); j++)
            whole[idx++] = parts[i][j];

}

// ////////////////////////////////////////////////////////////////////////////
//   Turn the initial solutions from the nodal basis to the transformed basis
// ////////////////////////////////////////////////////////////////////////////
template <class GridType, class VectorType>
template <class VectorTypeContainer>
void NBodyAssembler<GridType, VectorType>::
nodalToTransformed(const VectorTypeContainer& x,
                   VectorType& transformedX) const
{
    VectorType canonicalTotalX;

    concatenateVectors(x, canonicalTotalX);

    // Make small cg solver
    Dune::MatrixAdapter<MatrixType,VectorType,VectorType> op(getTransformationMatrix());

    // A preconditioner
    Dune::SeqILU<MatrixType,VectorType,VectorType> ilu0(getTransformationMatrix(),1.0);

    // A cg solver for nonsymmetric matrices
    Dune::BiCGSTABSolver<VectorType> bicgstab(op,ilu0,1E-4,100,0);

    // Object storing some statistics about the solving process
    Dune::InverseOperatorResult statistics;

    // Solve!
    transformedX = canonicalTotalX;  // seems to be a good initial value
    bicgstab.apply(transformedX, canonicalTotalX, statistics);
}

template <class GridType, class VectorType>
void NBodyAssembler<GridType, VectorType>::
computeTransformationMatrix()
{
    std::cout<<"Setup transformation matrix...";
    // compute offsets for the different grids
    std::vector<int> offsets(grids_.size());
    offsets[0] = 0;

    // P1 elements are hard-wired here
    size_t k;
    for (k=0; k<grids_.size()-1; k++)
        offsets[k+1] = offsets[k] + grids_[k]->size(dim);

    int nRows = offsets[k] + grids_[k]->size(dim);
    int nCols = nRows;

    // /////////////////////////////////////////////////////////////////
    //   First create the index structure
    // /////////////////////////////////////////////////////////////////

    Dune::MatrixIndexSet indicesBT(nRows, nCols);

    // BT_ is the identity plus some off-diagonal elements
    for (size_t i=0; i<indicesBT.rows(); i++)
        indicesBT.add(i,i);

    std::vector<std::vector<int> > nonmortarToGlobal(nCouplings());
    // Enter all the off-diagonal entries
    for (int i=0; i<nCouplings(); i++) {

        const auto& nonmortarBoundary = contactCoupling_[i]->nonmortarBoundary();
        // If the contact mapping could not be built at all then skip this coupling
        if (nonmortarBoundary.numVertices() == 0)
            continue;

        // The grids involved in this coupling
        int grid0Idx = coupling_[i].gridIdx_[0];
        int grid1Idx = coupling_[i].gridIdx_[1];

        // The mapping from nonmortar vertex indices to grid indices
        nonmortarToGlobal[i].resize(nonmortarBoundary.numVertices());
        int idx = 0;
        for (int j=0; j<grids_[grid0Idx]->size(dim); j++)
            if (nonmortarBoundary.containsVertex(j))
                nonmortarToGlobal[i][idx++] = j;

        // the off-diagonal part
        const MatrixType& M = contactCoupling_[i]->mortarLagrangeMatrix();
        for (size_t j=0; j<M.N(); j++) {

            const RowType& row = M[j];

            ConstColumnIterator cIt    = row.begin();
            ConstColumnIterator cEndIt = row.end();

            for (; cIt!=cEndIt; ++cIt)
                indicesBT.add(offsets[grid0Idx] + nonmortarToGlobal[i][j],
                              offsets[grid1Idx] + cIt.index());

        }

    }

    // ////////////////////////////////////////////////////////////
    //   Enter the values of the different couplings
    // ////////////////////////////////////////////////////////////

    indicesBT.exportIdx(BT_);
    BT_ = 0;

    // Enter identity part
    for (int i=0; i<nRows; i++)
        for (int j=0; j<dim; j++)
            BT_[i][i][j][j] = 1;

    for (int i=0; i<nCouplings(); i++) {

        const auto& nonmortarBoundary = contactCoupling_[i]->nonmortarBoundary();
        if (nonmortarBoundary.numVertices() == 0)
            continue;

        // The grids involved in this coupling
        int grid0Idx = coupling_[i].gridIdx_[0];
        int grid1Idx = coupling_[i].gridIdx_[1];

        // the off-diagonal part
        const MatrixType& M = contactCoupling_[i]->mortarLagrangeMatrix();
        for (size_t j=0; j<M.N(); j++) {

            const RowType& row = M[j];

            ConstColumnIterator cIt    = row.begin();
            ConstColumnIterator cEndIt = row.end();

            for (; cIt!=cEndIt; ++cIt)
                BT_[offsets[grid0Idx] + nonmortarToGlobal[i][j]][offsets[grid1Idx] +cIt.index()] = *cIt;

        }

        int offset = offsets[grid0Idx];

        // modify non-mortar dofs and rotate them in normal and tangential coordinates
        for (int k=0; k<grids_[grid0Idx]->size(dim); k++)
            if(nonmortarBoundary.containsVertex(k))
                BT_[offset + k][offset+k] =this->localCoordSystems_[offset + k];
    }
}

} /* namespace Contact */
} /* namespace Dune */
