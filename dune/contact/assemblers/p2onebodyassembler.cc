// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>

#include <dune/istl/bvector.hh>
#include <dune/istl/scaledidmatrix.hh>

#include <dune/fufem/prolongboundarypatch.hh>
#include <dune/fufem/referenceelementhelper.hh>
#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/functions/functionspacebases/pqknodalbasis.hh>

namespace Dune {
namespace Contact {

template <class GridType,class field_type>
void P2OneBodyAssembler<GridType,field_type>::computeLocalCoordinateSystems(const LeafBoundaryPatch& contactBoundary,
                                                                 std::vector<MatrixBlock>& coordSystems)
{

    // ///////////////////////////////////////////////////////////////
    //   Compute boundary normals at all dofs carrying obstacles
    // ///////////////////////////////////////////////////////////////


    using P2BasisType = DuneFunctionsBasis<typename Dune::Functions::PQkNodalBasis<typename GridType::LeafGridView,2> >;
    P2BasisType p2Basis(this->grid_->leafGridView());

    Dune::BlockVector<CoordinateType> vertexNormals(p2Basis.size());
    vertexNormals = 0;

    // Loop over all boundary segments
    for (const auto& it : contactBoundary) {


        const auto& inside = it.inside();
        const auto& localCoefficients = p2Basis.getLocalFiniteElement(inside).localCoefficients();

        assert(inside.type() == p2Basis.getLocalFiniteElement(inside).type());

        const auto& refElement = Dune::ReferenceElements<field_type, dim>::general(inside.type());

        const auto& geometryInInside = it.geometryInInside();

        // loop over all shape functions on the inside element
        for (size_t i=0; i<localCoefficients.size(); i++) {

            // //////////////////////////////////////////////////
            //   Test whether dof is on the boundary face
            // //////////////////////////////////////////////////

            int entity = localCoefficients.localKey(i).subEntity();
            unsigned int codim  = localCoefficients.localKey(i).codim();

            if(!ReferenceElementHelper<field_type, dim>::subEntityContainsSubEntity(inside.type(), it.indexInInside(), 1, entity,codim))
                continue;

            // Get the position of the dof on the current boundary segment
            const auto& pos = refElement.position(entity, codim);
            const auto posOnSegment = geometryInInside.local(pos);

            vertexNormals[p2Basis.index(inside,i)] += it.unitOuterNormal(posOnSegment);
        }

    }

    //   Normalize vertex normals
    for (size_t i=0; i<vertexNormals.size(); i++)
        if (vertexNormals[i].two_norm() > 0.5)
            vertexNormals[i] /= vertexNormals[i].two_norm();

    // /////////////////////////////////////////////////////////
    // Compute a full orthonormal coordinate system for each
    // boundary segment carrying an obstacle.  We do this by computing
    // the Householder reflection of the first canonical basis
    // vector into the surface normal.
    // /////////////////////////////////////////////////////////

    // Make first canonical basis vector
    CoordinateType e0(0);
    e0[0] = 1;

    coordSystems.resize(vertexNormals.size());

    for (unsigned int i=0; i<vertexNormals.size(); i++) {

        if (vertexNormals[i].two_norm() > 0.5) {
            // There is an obstacle at this vertex --> the coordinate system
            // will consist of the surface normal and tangential vectors
            this->householderReflection(e0, vertexNormals[i], coordSystems[i]);
        } else {
            // There is no obstacle.  The coordinate system will be the canonical one
            coordSystems[i]  = Dune::ScaledIdentityMatrix<field_type,dim>(1.0);
        }

    }

}

template <class GridType, class field_type>
void P2OneBodyAssembler<GridType,field_type>::assembleObstacles()
{

    BoundaryPatchProlongator<GridType>::prolong(*this->coarseObsPatch_,this->obsPatch_);

    // Compute local coordinate systems for the dofs with obstacles
    computeLocalCoordinateSystems(this->obsPatch_, this->localCoordSystems_);

    using P2BasisType = DuneFunctionsBasis<typename Dune::Functions::PQkNodalBasis<typename GridType::LeafGridView,2> >;
    P2BasisType p2Basis(this->grid_->leafGridView());

    this->obstacles_.resize(p2Basis.size());

    for (const auto& it : this->obsPatch_) {

        const auto& inside = it.inside();
        const auto& geometry = inside.geometry();

        Dune::GeometryType gt = inside.type();

        const auto& refElement = Dune::ReferenceElements<field_type, dim>::general(gt);

        const auto& localCoefficients = p2Basis.getLocalFiniteElement(inside).localCoefficients();

        for (size_t i=0; i<localCoefficients.size(); i++) {

            int entity = localCoefficients.localKey(i).subEntity();
            unsigned int codim  = localCoefficients.localKey(i).codim();

            if(!ReferenceElementHelper<field_type, dim>::subEntityContainsSubEntity(gt, it.indexInInside(), 1, entity,codim))
                continue;

            int dofIdx = p2Basis.index(inside, i);

            // world position of this degree of freedom
            const auto& pos = geometry.global(refElement.position(entity, codim));

            // the first row in the householder reflection is the search direction
            const auto& obsDir = this->localCoordSystems_[dofIdx][0];

            this->obstacles_[p2Basis.index(inside,i)].upper(0) = this->obstacleFunction_(pos,obsDir);

        }

    }
}

} /* namespace Contact */
} /* namespace Dune */
