#include <dune/common/bitsetvector.hh>
#include <dune/common/fvector.hh>
#include <dune/common/timer.hh>

namespace Dune {
namespace Contact {

template <int dim, class field_type>
bool DefectProblem<dim,field_type>::get_nlp_info(Ipopt::Index& n, Ipopt::Index& m, Ipopt::Index& nnz_jac_g,
        Ipopt::Index& nnz_h_lag, IndexStyleEnum& index_style)
{
    // The number of variables
    n = getNumVariables();

    //     m: number of equality constrains
    //     nnz_jac_g: number of nonzeros in the constraint jacobian;
    getEqualityConstraints(m, nnz_jac_g);

    // Number of nonzero entries in the Hessian matrix
    nnz_h_lag = getNumHessianNonZeros();

    // use the C style indexing (0-based)
    index_style = Ipopt::TNLP::C_STYLE;

    return true;
}

// Evaluate the objective function
template <int dim, class field_type>
bool DefectProblem<dim,field_type>::eval_f(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Number& obj_value)
{
    // Init return value
    obj_value = 0;

    ////////////////////////////////////
    // Compute 0.5 * x^T*A*x - b*x
    ////////////////////////////////////

    int blockOffset = 0;

    for (int block=0; block<numBlocks(); block++) {

        //    *f = 1/2 * (*x) * (*hessian_) * (*x);
        for (size_t rowIdx=0; rowIdx<(*hessians_)[block].N(); rowIdx++) {

            // We know that the matrices are block-diagonal
            const BlockType& cIt = (*hessians_)[block][rowIdx][rowIdx];

            // Multiply block
            for (int i=0; i<dim; i++)
                for (int j=0; j<dim; j++)
                    obj_value += 0.5 * x[blockOffset + rowIdx*dim+i] * x[blockOffset + rowIdx*dim+j] * cIt[i][j];

        }

        // -b(x)
        for (size_t i=0; i<rhs_->size(); i++)
            for (int j=0; j<dim; j++)
                obj_value -= x[blockOffset + i*dim+j] * (*rhs_)[block][i][j];

        blockOffset += (*hessians_)[block].N()*dim;

    }

    return true;

}


// Evaluate the objective function gradient
template <int dim, class field_type>
bool DefectProblem<dim,field_type>::eval_grad_f(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Number* grad_f)
{
    // Clear gradient vector
    for (int i=0; i<n; i++)
        grad_f[i] = 0;

    int blockOffset = 0;

    // \nabla J = A(x,.) - b(.)
    for (int block=0; block<numBlocks(); block++) {

        for (size_t i=0; i<(*hessians_)[block].N(); i++) {

            const BlockType& cIt = *(*hessians_)[block][i].begin();

            for (int j=0; j<dim; j++)
                for (int k=0; k<dim; k++)
                    grad_f[blockOffset + i*dim+j] += cIt[j][k] * x[blockOffset + i*dim+k];

        }

        // g = g - b
        for (size_t i=0; i<(*rhs_)[block].size(); i++)
            for (int j=0; j<dim; j++)
                grad_f[blockOffset + i*dim+j] -= (*rhs_)[block][i][j];

        blockOffset += (*hessians_)[block].N()*dim;

    }

    return true;
}


// Evaluate the constraints
template <int dim, class field_type>
bool DefectProblem<dim,field_type>::eval_g(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Index m, Ipopt::Number* g)
{
    typedef typename MatrixType::row_type RowType;

    // dofOffsets[i] tells you: where in the overall dof array do the
    // entries corresponding to the i'th grid begin?
    std::vector<int> dofOffsets(numBlocks()+1);
    dofOffsets[0] = 0;
    for (int i=0; i<numBlocks(); i++)
        dofOffsets[i+1] = dofOffsets[i] + (*x_)[i].size()*dim;

    // constraintOffsets[i] tells you: where in the overall constraints array do the
    // entries corresponding to the i-th coupling begin?
    std::vector<int> constraintOffsets(numCouplings()+1);
    constraintOffsets[0] = 0;
    for (int i=0; i<numCouplings(); i++)
        constraintOffsets[i+1] = constraintOffsets[i]
                + ((isRigidObstacle(i)) ? 0 : numObstacles(i)*dim);

    for (int i=0; i<numCouplings(); i++) {

        // Only two-body couplings create constraints
        if (isRigidObstacle(i))
            continue;

        int rowCounter = constraintOffsets[i];
        int nonmortarOffset = dofOffsets[couplings_[i][0]];
        int mortarOffset    = dofOffsets[couplings_[i][1]];

        for (size_t j=0; j<nonmortarLagrangeMatrix_[i]->N(); j++) {

            const RowType& nonmortarRow = (*nonmortarLagrangeMatrix_[i])[j];
            const RowType& mortarRow    = (*mortarLagrangeMatrix_[i])[j];

            // Skip empty rows, only nonempty rows contain constraints
            if (nonmortarRow.begin() == nonmortarRow.end()) {
                assert(mortarRow.begin() == mortarRow.end());
                continue;
            }

            for (int k=0; k<dim; k++)
                g[rowCounter+k] = 0;

            // nonmortar/lagrange part
            for (typename RowType::ConstIterator cIt = nonmortarRow.begin(); cIt!=nonmortarRow.end(); ++cIt) {

                for (int l=0; l<dim; l++)
                    for (int k=0; k<dim; k++)
                        if ( std::abs((*cIt)[l][k]) > jacobianCutoff_)
                            g[rowCounter+l] += (*cIt)[l][k] * x[nonmortarOffset + cIt.index()*dim+k];

            }

            // mortar/lagrange part
            for (typename RowType::ConstIterator cIt = mortarRow.begin(); cIt!=mortarRow.end(); ++cIt) {

                for (int l=0; l<dim; l++)
                    for (int k=0; k<dim; k++)
                        if ( std::abs((*cIt)[l][k]) > jacobianCutoff_)
                            g[rowCounter+l] -= (*cIt)[l][k] * x[mortarOffset + cIt.index()*dim+k];

            }

            // increment constraint counter
            rowCounter += dim;
        }

    }
    //     std::cout << "rhs:" << std::endl << (*this->rhs) << std::endl;

    //     for (int i=0; i<m; i++)
    //         printf("Constrained %d:  row %g\n", i, g[i]);
    //exit(0);
    return true;
}

// Evaluate gradient of the constraints
template <int dim, class field_type>
bool DefectProblem<dim,field_type>::eval_jac_g(Ipopt::Index n, const Ipopt::Number* x, bool new_x,
                                               Ipopt::Index m, Ipopt::Index nele_jac, Ipopt::Index* iRow, Ipopt::Index *jCol,
                                               Ipopt::Number* values)
{
    typedef typename MatrixType::row_type RowType;

    // dofOffsets[i] tells you: where in the overall dof array do the
    // entries corresponding to the i'th grid begin?
    std::vector<int> dofOffsets(numBlocks()+1);
    dofOffsets[0] = 0;
    for (int i=0; i<numBlocks(); i++)
        dofOffsets[i+1] = dofOffsets[i] + (*x_)[i].size()*dim;

    // constraintOffsets[i] tells you: where in the overall constraints array do the
    // entries corresponding to the i-th coupling begin?
    std::vector<int> constraintOffsets(numCouplings()+1);
    constraintOffsets[0] = 0;
    for (int i=0; i<numCouplings(); i++)
        constraintOffsets[i+1] = constraintOffsets[i]
                + ((isRigidObstacle(i)) ? 0 : numObstacles(i)*dim);

    int idx = 0;

    for (int i=0; i<numCouplings(); i++) {

        // Only two-body couplings lead to constraints
        if (isRigidObstacle(i))
            continue;

        int row = constraintOffsets[i];

        int nonmortarOffset = dofOffsets[couplings_[i][0]];
        int mortarOffset    = dofOffsets[couplings_[i][1]];

        if (values==NULL) {

            for (size_t j=0; j<nonmortarLagrangeMatrix_[i]->N(); j++) {

                const RowType& nonmortarRow = (*nonmortarLagrangeMatrix_[i])[j];
                const RowType& mortarRow    = (*mortarLagrangeMatrix_[i])[j];

                // Skip empty rows, only nonempty rows correspond to constraints
                if (nonmortarRow.begin() == nonmortarRow.end()) {
                    assert(mortarRow.begin() == mortarRow.end());
                    continue;
                }

                // nonmortar/lagrange part
                for (typename RowType::ConstIterator cIt = nonmortarRow.begin(); cIt!=nonmortarRow.end(); ++cIt) {

                    for (int l=0; l<dim; l++)
                        for (int k=0; k<dim; k++)
                            if ( std::abs((*cIt)[l][k]) > jacobianCutoff_) {

                                iRow[idx] = row + l;
                                jCol[idx] = nonmortarOffset + cIt.index()*dim+k;
                                idx++;

                            }

                }

                // mortar/lagrange part
                for (typename RowType::ConstIterator cIt = mortarRow.begin(); cIt!=mortarRow.end(); ++cIt) {

                    for (int l=0; l<dim; l++)
                        for (int k=0; k<dim; k++) {

                            if ( std::abs((*cIt)[l][k]) > jacobianCutoff_) {

                                iRow[idx] = row + l;
                                jCol[idx] = mortarOffset + cIt.index()*dim+k;
                                idx++;

                            }

                        }

                }

                row += dim;

            }

        } else {

            for (size_t j=0; j<nonmortarLagrangeMatrix_[i]->N(); j++) {

                const RowType& nonmortarRow = (*nonmortarLagrangeMatrix_[i])[j];
                const RowType& mortarRow    = (*mortarLagrangeMatrix_[i])[j];

                // Skip empty rows, only nonempty rows correspond to constraints
                if (nonmortarRow.begin() == nonmortarRow.end()) {
                    assert(mortarRow.begin() == mortarRow.end());
                    continue;
                }

                // nonmortar/lagrange part
                for (typename RowType::ConstIterator cIt = nonmortarRow.begin(); cIt!=nonmortarRow.end(); ++cIt) {

                    for (int l=0; l<dim; l++)
                        for (int k=0; k<dim; k++)
                            if ( std::abs((*cIt)[l][k]) > jacobianCutoff_)
                                values[idx++] = (*cIt)[l][k];

                }

                // mortar/lagrange part
                for (typename RowType::ConstIterator cIt = mortarRow.begin(); cIt!=mortarRow.end(); ++cIt) {

                    for (int l=0; l<dim; l++)
                        for (int k=0; k<dim; k++)
                            if ( std::abs((*cIt)[l][k]) > jacobianCutoff_)
                                values[idx++] = -(*cIt)[l][k];

                }

                row += dim;

            }

        }

    }

    //     std::cout << "The constraint jacobian structure:" << std::endl;
    //     if (values)
    //         for (int i=0; i<idx; i++)
    //             printf("value: %g\n", values[i]);
    //     else
    //         for (int i=0; i<idx; i++)
    //             printf("row: %d   col: %d\n", iRow[i], jCol[i]);
    return true;
}


//return the structure or values of the hessian
template <int dim, class field_type>
bool DefectProblem<dim, field_type>::eval_h(Ipopt::Index n, const Ipopt::Number* x, bool new_x,
                                            Ipopt::Number obj_factor, Ipopt::Index m, const Ipopt::Number* lambda,
                                            bool new_lambda, Ipopt::Index nele_hess, Ipopt::Index* iRow,
                                            Ipopt::Index* jCol, Ipopt::Number* values)
{
    int idx = 0;

    if (values == NULL) {

        // return the structure. This is a symmetric matrix, fill the lower left
        // triangle only.
        int blockOffset = 0;

        for (int block=0; block<numBlocks(); block++) {

            // Loop over all matrix rows
            for (size_t rowIdx=0; rowIdx<(*hessians_)[block].N(); rowIdx++) {

                // Current entry is on the diagonal
                // --> copy only its lower triangular half
                for (int i=0; i<dim; i++) {

                    for (int j=0; j<i+1; j++) {

                        iRow[idx] = blockOffset + rowIdx*dim + i;
                        jCol[idx] = blockOffset + rowIdx*dim + j;
                        idx++;

                    }

                }

            }

            blockOffset += (*hessians_)[block].N()*dim;

        }

        assert(idx == nele_hess);
        //         for (int i=0; i<nele_hess; i++)
        //             printf("%d %d \n", iRow[i], jCol[i]);

    } else {

        // return the values. This is a symmetric matrix, fill the lower left
        // triangle only

        for (int block=0; block<numBlocks(); block++) {

            // Loop over all matrix rows
            for (size_t rowIdx=0; rowIdx<(*hessians_)[block].N(); rowIdx++) {

                // We know that the matrices are block-diagonal
                const Dune::FieldMatrix<field_type,dim,dim>& cIt = *(*hessians_)[block][rowIdx].begin();

                // Current entry is on the diagonal
                // --> copy only its lower triangular half
                for (int i=0; i<dim; i++)
                    for (int j=0; j<i+1; j++)
                        values[idx++] = obj_factor * cIt[i][j];

            }

        }

        assert(idx == nele_hess);

        //         for (int i=0; i<nele_hess; i++)
        //             printf("%g\n", values[i]);

    }

    return true;
}

// Compute the number of nonzeros of the Hessian matrix
template <int dim, class field_type>
int DefectProblem<dim, field_type>::getNumHessianNonZeros() const {

    if (!hessians_)
        DUNE_THROW(SolverError, "No Hessian matrix has been supplied!");

    int numNonZeros = 0;
    // Each matrix contains only the diagonal entries
    for (size_t i=0; i<x_->size(); i++)
        numNonZeros += (*hessians_)[i].N();

    // IPOpt only expects the upper right triangular matrix
    return numNonZeros*(dim+1)*dim/2;
}

// Compute the total number of variables
template <int dim, class field_type>
int DefectProblem<dim, field_type>::getNumVariables() const {

    assert(x_->size() == rhs_->size());
    int numVariables = 0;
    for (size_t i=0; i<x_->size(); i++) {
        assert((*x_)[i].size() == (*rhs_)[i].size());
        numVariables += (*x_)[i].size();
    }

    // Boths functions and slack variables are vector-valued
    return numVariables*dim;
}

//   Return the number of nonzeros in the constraint Jacobian
template <int dim, class field_type>
void DefectProblem<dim, field_type>::getEqualityConstraints(int& constraints, int& numJacobianNonzeros)
{
    // Get number of equality constraints
    constraints = 0;
    for (int i=0; i<numCouplings(); i++)
        if (!isRigidObstacle(i))
            constraints += numObstacles(i) * dim;

    // ////////////////////////////////////////////////////////////////
    //   Return the number of nonzeros in the constraint Jacobian
    // ////////////////////////////////////////////////////////////////

    numJacobianNonzeros = 0;

    typedef typename MatrixType::row_type RowType;

    for (int i=0; i<numCouplings(); i++) {

        if (isRigidObstacle(i))
            continue;

        for (size_t j=0; j<nonmortarLagrangeMatrix_[i]->N(); j++) {

            const RowType& nonmortarRow = (*nonmortarLagrangeMatrix_[i])[j];
            const RowType& mortarRow    = (*mortarLagrangeMatrix_[i])[j];

            // dim for each 1 x dim entry in the nonmortar/lagrange matrix
            for (typename RowType::ConstIterator cIt = nonmortarRow.begin(); cIt!=nonmortarRow.end(); ++cIt)
                for (int k=0; k<dim; k++)
                    for (int l=0; l<dim; l++)
                        if ( std::abs((*cIt)[k][l]) > jacobianCutoff_ )
                            numJacobianNonzeros++;

            // dim for each 1 x dim entry in the mortar/lagrange matrix
            for (typename RowType::ConstIterator cIt = mortarRow.begin(); cIt!=mortarRow.end(); ++cIt)
                for (int k=0; k<dim; k++)
                    for (int l=0; l<dim; l++)
                        if ( std::abs((*cIt)[k][l]) > jacobianCutoff_ )
                            numJacobianNonzeros++;

        }

    }
}

template <int dim, class field_type>
bool DefectProblem<dim, field_type>::get_bounds_info(Ipopt::Index n, Ipopt::Number* x_l, Ipopt::Number* x_u,
                                                     Ipopt::Index m, Ipopt::Number* g_l, Ipopt::Number* g_u)
{
    // Be on the safe side: unset all bounds
    for (int i=0; i<n; i++) {
        x_l[i] = -std::numeric_limits<double>::max();
        x_u[i] =  std::numeric_limits<double>::max();
    }

    // /////////////////////////////////////////////////////////////
    //   Copy the obstacle values
    // /////////////////////////////////////////////////////////////

    // dofOffsets[i] tells you: where in the overall dof array do the
    // entries corresponding to the i'th grid begin?
    std::vector<int> dofOffsets(numBlocks()+1);
    dofOffsets[0] = 0;
    for (int i=0; i<numBlocks(); i++)
        dofOffsets[i+1] = dofOffsets[i] + (*x_)[i].size()*dim;

    // constraintOffsets[i] tells you: where in the overall constraints array do the
    // entries corresponding to the i-th coupling begin?
    std::vector<int> constraintOffsets(numCouplings()+1);
    constraintOffsets[0] = 0;
    for (int i=0; i<numCouplings(); i++)
        constraintOffsets[i+1] = constraintOffsets[i]
                + ((isRigidObstacle(i)) ? 0 : numObstacles(i)*dim);

    for (int i=0; i<numCouplings(); i++) {

        int idx = 0;

        for (size_t j=0; j<obstacles_[i].size(); j++) {

            if (isRigidObstacle(i)) {

                // //////////////////////////////////////////////////////////////
                //   A rigid obstacle: the constraint is on an actual variable
                // //////////////////////////////////////////////////////////////

                int gridIdx = couplings_[i][0];

                for (int k=0; k<dim; k++) {

                    x_l[dofOffsets[gridIdx] + j*dim + k ] = obstacles_[i][j].lower(k);
                    x_u[dofOffsets[gridIdx] + j*dim + k ] = obstacles_[i][j].upper(k);

                }

            } else {

                // //////////////////////////////////////////////////////////////
                //   A two-body obstacle: the constraint is on a slack variable
                // //////////////////////////////////////////////////////////////

                /** \bug This does not work when mixing rigid and two-body obstacles */
                if ((*nonmortarLagrangeMatrix_[i])[j].begin() != (*nonmortarLagrangeMatrix_[i])[j].end()) {

                    for (int k=0; k<dim; k++) {

                        g_l[constraintOffsets[i] + idx + k] = obstacles_[i][j].lower(k);
                        g_u[constraintOffsets[i] + idx + k] = obstacles_[i][j].upper(k);

                    }

                    idx += dim;

                }

            }

        }

    }

    // /////////////////////////////////////////////////////////////
    //   Bounds due to Dirichlet nodes
    // /////////////////////////////////////////////////////////////
    if (!dirichletNodes_)
        return true;

    int blockOffset = 0;
    for (int block=0; block<numBlocks(); block++) {

        for (size_t i=0; i<(*dirichletNodes_)[block].size(); i++)
            for (int j=0; j<dim; j++) {

                // zero Dirichlet (we solve a defect problem)
                if ((*dirichletNodes_)[block][i][j])
                    x_l[blockOffset + i*dim + j] = x_u[blockOffset + i*dim + j] = 0;

            }

        blockOffset += (*x_)[block].size()*dim;

    }

    return true;
}

template <int dim, class field_type>
bool DefectProblem<dim, field_type>::get_starting_point(Ipopt::Index n, bool init_x, Ipopt::Number* x,
                                                        bool init_z, Ipopt::Number* z_L, Ipopt::Number* z_U,
                                                        Ipopt::Index m, bool init_lambda, Ipopt::Number* lambda)
{
    // Here, we assume we only have starting values for x, if you code
    // your own NLP, you can provide starting values for the dual variables
    // if you wish
    assert(init_x == true);
    assert(init_z == false);
    assert(init_lambda == false);

    assert(n==getNumVariables());

    for (int i=0; i<n; i++)
        x[i] = 0;

    return true;
}

template <int dim, class field_type>
void DefectProblem<dim, field_type>::finalize_solution(Ipopt::SolverReturn status,
                                                       Ipopt::Index n, const Ipopt::Number* x, const Ipopt::Number* z_L, const Ipopt::Number* z_U,
                                                       Ipopt::Index m, const Ipopt::Number* g, const Ipopt::Number* lambda,
                                                       Ipopt::Number obj_value, const Ipopt::IpoptData* ip_data,
                                                       Ipopt::IpoptCalculatedQuantities* ip_cq)
{
    int blockOffset = 0;

    for (int block=0; block<numBlocks(); block++) {

        for (size_t i=0; i<(*x_)[block].size(); i++)
            for (int j=0; j<dim; j++)
                (*x_)[block][i][j] = x[blockOffset + i*dim+j];

        blockOffset += (*x_)[block].size()*dim;
    }

}

} /* namespace Contact */
} /* namespace Dune */
