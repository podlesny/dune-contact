#ifndef ZOU_KORNHUBER_BASIS_HH
#define ZOU_KORNHUBER_BASIS_HH

/**
   @file
   @brief

   @author Oliver Sander
 */

#include <dune/geometry/type.hh>

#include "zoukornhuberelement.hh"

#include <dune/fufem/functionspacebases/functionspacebasis.hh>

namespace Dune {
namespace Contact {

template <class GV, class RT=double>
class ZouKornhuberBasis
    : public FunctionSpaceBasis<GV, RT,
                                ZouKornhuberFiniteElement<typename GV::Grid::ctype, RT> >
{
    protected:
        typedef typename GV::Grid::template Codim<0>::Entity Element;
        typedef FunctionSpaceBasis<GV, RT, ZouKornhuberFiniteElement<typename GV::Grid::ctype, RT> > Base;

        static const int dim = GV::dimension;

    typedef Dune::MultipleCodimMultipleGeomTypeMapper<GV> ZouKornhuberMapper;

    public:
        typedef GV GridView;
        typedef RT ReturnType;
        typedef ZouKornhuberFiniteElement<typename GV::Grid::ctype, RT> LocalFiniteElement;
        typedef typename Base::LinearCombination LinearCombination;

        ZouKornhuberBasis(const GridView& gridview) :
            Base(gridview),
            mapper_(gridview,
                [](Dune::GeometryType gt, int localDim) {
                  assert(gt.isSimplex() && gt.dim() <= 2);
                  return gt.dim()==0 or gt.dim()==1 or ((int) gt.dim()==localDim);}),
            localFE_()
        {}

        size_t size() const
        {
            return mapper_.size();
        }

        const LocalFiniteElement& getLocalFiniteElement(const Element& e) const
        {
            return localFE_;
        }

    /** \brief Return global index of the i-th local shape function */
    int index(const Element& e, const int i) const
        {
            return mapper_.subIndex(e,
                               localFE_.localCoefficients().localKey(i).subEntity(),
                               localFE_.localCoefficients().localKey(i).codim());
        }


        bool isConstrained(const Element& e, const int i) const
        {
            return false;
        }

        bool isConstrained(int i) const
        {
            return false;
        }

    protected:
    const ZouKornhuberMapper mapper_;
    const LocalFiniteElement localFE_;
};

} /* namespace Contact */
} /* namespace Dune */

#endif

