#ifndef ZOU_KORNHUBER_SIGNORINI_ESTIMATOR_HH
#define ZOU_KORNHUBER_SIGNORINI_ESTIMATOR_HH

#include <dune/common/function.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/type.hh>

#include <dune/fufem/functiontools/boundarydofs.hh>
#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/estimators/refinementindicator.hh>
#include <dune/fufem/estimators/hierarchicestimatorbase.hh>

#include "zoukornhuberbasis.hh"

namespace Dune {
namespace Contact {

template <class GridType>
class ZouKornhuberSignoriniEstimator
{

    enum {dim = GridType::dimension};

    typedef double field_type;

    using FVector = Dune::FieldVector<field_type,dim>;
    using VectorType = Dune::BlockVector<FVector>;
    typedef Dune::FieldMatrix<field_type,dim,dim> BlockType;

    typedef BoundaryPatch<typename GridType::LeafGridView> LeafBoundaryPatch;

    typedef ZouKornhuberBasis<typename GridType::LeafGridView, field_type> BasisType;

    static void householderReflection(const Dune::FieldVector<field_type, dim>& v0,
                                      const Dune::FieldVector<field_type, dim>& v1,
                                      Dune::FieldMatrix<field_type, dim, dim>& mat)
    {
        Dune::FieldVector<field_type, dim> v = (v1 - v0);
        v *= 0.5;

        // The code from here to the end of the routine sets Orth to
        // the Householder reflexion matrix of v, that is
        // $Orth = I - 2 \frac{v v^T}{v^T v}$
        mat = 0;
        for (int i=0; i<dim; i++)
            mat[i][i] = 1;

        field_type norm = v.two_norm2();

        if(norm <= 1e-8)
            return;

        for(int i=0;i<dim;i++)
            for(int j=0;j<dim;j++)
                mat[i][j] -= 2.0 *v[i]*v[j] / norm;

    }

    static void transformMatrix(Dune::BCRSMatrix<Dune::FieldMatrix<field_type, dim, dim> >& mat,
                                std::vector<BlockType>& coordSystems)
    {
        typedef typename Dune::BCRSMatrix<Dune::FieldMatrix<field_type, dim, dim> >::row_type RowType;
        typedef typename RowType::Iterator ColumnIterator;

        for(size_t rowIdx=0; rowIdx<mat.N(); rowIdx++) {

            RowType& row = mat[rowIdx];

            ColumnIterator cIt    = row.begin();
            ColumnIterator cEndIt = row.end();

            for(; cIt!=cEndIt; ++cIt) {

                cIt->leftmultiply(coordSystems[rowIdx]);
                cIt->rightmultiply(coordSystems[cIt.index()]);

            }

        }

    }

    static void rotateVector(Dune::BlockVector<Dune::FieldVector<field_type,dim> >& x,
                             std::vector<BlockType>& coordSystems)
    {
        for (size_t i=0; i<x.size(); i++) {
            Dune::FieldVector<field_type, dim> tmp(0);
            coordSystems[i].umv(x[i], tmp);
            x[i] = tmp;
        }
    }

 public:

    ZouKornhuberSignoriniEstimator(GridType& grid,
                                   const LeafBoundaryPatch& obsPatch,
                                   const std::function<double(const FVector&, const FVector&)>& obstacleFunction)
        : grid_(grid), zouKornhuberBasis_(grid.leafGridView()),
          contactBoundary_(obsPatch),
          obstacleFunction_(obstacleFunction),
          hierarchicalIndicators_(grid)
        {

        }


    void estimate(const VectorType& solution,
                  const LeafBoundaryPatch* dirichletBoundary,
                  RefinementIndicator<GridType>& hierarchicError,
                  LocalOperatorAssembler<GridType,
                  typename ZouKornhuberBasis<typename GridType::LeafGridView>::LocalFiniteElement,
                  typename ZouKornhuberBasis<typename GridType::LeafGridView>::LocalFiniteElement,Dune::FieldMatrix<field_type,dim,dim> >* localStiffness);

    double getErrorSquared(int grid,
                           LocalOperatorAssembler<GridType,
                           typename ZouKornhuberBasis<typename GridType::LeafGridView>::LocalFiniteElement,
                           typename ZouKornhuberBasis<typename GridType::LeafGridView>::LocalFiniteElement,
                           Dune::FieldMatrix<field_type,dim,dim> >* localStiffness) const;

protected:

    /** \brief Compute local coordinate systems for the leaf boundary dofs
     *
     * \param hasObstacle Compute coordinate systems on all vertices which have
     their bit set in this bitfield
    */
    void computeLocalCoordinateSystems(std::vector<BlockType>& coordSystems) const;

    void setObstacles(std::vector<BoxConstraint<double,dim> >& obstacles,
                      const std::vector<BlockType>& coordSystems) const;

    // ///////////////////////////////////////
    //   Data members
    // ///////////////////////////////////////

    const GridType& grid_;

    BasisType zouKornhuberBasis_;

    const LeafBoundaryPatch& contactBoundary_;

    const std::function<field_type(const FVector&, const FVector&)>& obstacleFunction_;

    // Variable to store the various indicators
    RefinementIndicator<GridType> hierarchicalIndicators_;
};



template <class GridType>
void ZouKornhuberSignoriniEstimator<GridType>::estimate(const VectorType& solution,
                                                        const LeafBoundaryPatch* dirichletBoundary,
                                                        RefinementIndicator<GridType>& hierarchicError,
                                                        LocalOperatorAssembler<GridType,
                                                        typename ZouKornhuberBasis<typename GridType::LeafGridView>::LocalFiniteElement,
                                                        typename ZouKornhuberBasis<typename GridType::LeafGridView>::LocalFiniteElement,Dune::FieldMatrix<field_type,dim,dim> >* localStiffness)
{

    // express x in hierarchical basis
    VectorType xHierarchical;

    typedef P1NodalBasis<typename GridType::LeafGridView, field_type> P1NodalBasisType;
    P1NodalBasisType p1NodalBasis(grid_.leafGridView());
    BasisGridFunction<P1NodalBasisType,VectorType> solFunction(p1NodalBasis,solution);

    ::Functions::interpolate(zouKornhuberBasis_, xHierarchical, solFunction);

    // Local coordinate systems for the boundary vertices
    // The rows of the matrices are the normal and tangent vectors
    std::vector<BlockType> coordSystems;
    computeLocalCoordinateSystems(coordSystems);

    // /////////////////////////////////////////////////
    //   Assemble the preconditioned defect problem
    // /////////////////////////////////////////////////

    VectorType residual;

    Dune::BDMatrix<BlockType> matrix;

    HierarchicEstimatorBase<ZouKornhuberBasis<typename GridType::LeafGridView>,dim,field_type>::preconditionedDefectProblem(zouKornhuberBasis_,
                                                                                                                            xHierarchical,
                                                                                                                            NULL,      // volume term
                                                                                                                            NULL,      // Neumann term
                                                                                                                            residual, matrix, localStiffness);

    // transform matrix to local coordinates
    transformMatrix(matrix, coordSystems);

    // transform residual to local coordinates
    rotateVector(residual, coordSystems);

    // ///////////////////////////////////////////////////////////
    //   Construct the set of Dirichlet dofs for the extended problem
    // ///////////////////////////////////////////////////////////

    Dune::BitSetVector<1> dirichletDofs;

    constructBoundaryDofs(*dirichletBoundary, zouKornhuberBasis_, dirichletDofs);

    std::cout << "Dirichlet boundary consists of " << dirichletBoundary->numFaces() << " faces." << std::endl;
    std::cout << "Extended problem has " << dirichletDofs.count() << " Dirichlet dofs." << std::endl;

    // ////////////////////////////////////////////////////////////////////
    //   Compute the various indicators
    // ////////////////////////////////////////////////////////////////////

    // Clear all indicators
    hierarchicError.clear();

    // We now loop over all basis functions.  Since they cannot be accessed by index
    // we need to loop over all elements and local basis functions

    // We meet edge and vertex basis functions several times, but handle them only once
    Dune::BitSetVector<1> handled(zouKornhuberBasis_.size());
    handled.unsetAll();

    // compute obstacle in the enlarged space
    std::vector<BoxConstraint<double,dim> > obstacles;
    setObstacles(obstacles,coordSystems);

    // Variables to store the various indicators
    std::vector<double> normalEdgeIndicators(residual.size(),0);
    std::vector<double> tangentEdgeIndicators(residual.size(),0);

    for (const auto& e : elements(grid_.leafGridView())) {

        // Only triangle elements are currently implemented
        assert(e.type().isTriangle());

        ZouKornhuberFiniteElement<double,double> localFiniteElement;
        const ZouKornhuberLocalCoefficients& localCoefficients = localFiniteElement.localCoefficients();

        for (size_t i=0; i<localCoefficients.size(); i++) {

            // Global index
            unsigned int idx = zouKornhuberBasis_.index(e, i);

            if (handled[idx][0])
                continue;

            // Determine shape function type
            switch (localCoefficients.localKey(i).codim()) {
            case dim:
                // Vertex, do nothing
                break;
            case dim-1:
                // Edge indicators
                // First the normal edge-oriented indicator
                // \rho_{n,E} = \langle \sigma_S, \phi_E n \rangle \norm{\phi_E}^{-1}
                {

                    double totalEdgeIndicator = 0;
                    for (int j=0; j<dim; j++) {
                        double factor = residual[idx][j] / std::sqrt(matrix[idx][idx][j][j]);
                        totalEdgeIndicator += factor * factor;
                    }

                    totalEdgeIndicator = std::sqrt(totalEdgeIndicator);

                    // The normal edge indicator
                    double normalEdgeIndicator = residual[idx][0] / std::sqrt(matrix[idx][idx][0][0]);

                    // Then the tangential edge-oriented indicators
                    // \rho_{t,E} = \langle \sigma_S, \phi_E t \rangle \norm{\phi_E}^{-1}
                    // for the tangent vectors t
                    double tangentEdgeIndicator = 0;
                    for (int j=1; j<dim; j++) {
                        double factor = residual[idx][j] / std::sqrt(matrix[idx][idx][j][j]);
                        tangentEdgeIndicator += factor*factor;
                    }
                    tangentEdgeIndicator = std::sqrt(tangentEdgeIndicator);

                    assert(!std::isnan(tangentEdgeIndicator));
                    assert(!std::isnan(totalEdgeIndicator));

                    // Determine whether the current edge is active.  In this context, an edge
                    // is active if both its vertices are active.

                    const auto& refElement = Dune::ReferenceElements<double, dim>::general(e.type());

                    bool isActive = true;

                    for (int j=0; j<2; j++) {

                        // vertex number locally in this element
                        int v = refElement.subEntity(localCoefficients.localKey(i).subEntity(),
                                                     dim-1,
                                                     j, dim);  // vertex j of the current edge

                        // vertex number globally in the grid
                        int p1Index = p1NodalBasis.index(e,v);

                        // dof number in the ZouKornhuber basis (we only have the obstacles
                        // for this basis.  That is kind of stupid, we compute edge obstacles
                        // for nothing)
                        int zouKhIndex = -1;
                        for (size_t k=0; k<localCoefficients.size(); k++)
                            if (localCoefficients.localKey(k).codim() == dim
                                && ((int) localCoefficients.localKey(k).subEntity() == v)) {
                                zouKhIndex = zouKornhuberBasis_.index(e,k);
                                break;
                            }

                        assert(zouKhIndex >= 0);

//                         std::cout << "Normal: " << coordSystems[zouKhIndex][0] << std::endl;
//                         std::cout << "Normal displacement: " << solution[p1Index]*coordSystems[zouKhIndex][0]
//                                  << "   obstacle: " << obstacles[zouKhIndex].upper(0) << std::endl;

                        isActive &= (solution[p1Index]*coordSystems[zouKhIndex][0] >= obstacles[zouKhIndex].upper(0));

                    }

                    //                    std::cout << "active: " << isActive << std::endl;

                    // This is Qingsongs nomenclature: an edge is in Epsilon1_C if it is active
                    // and the normal edge indicator is nonnegative

                    bool edgeInEpsilon1_C = isActive && (normalEdgeIndicator >= 0);

                    if (!dirichletBoundary->contains(e, localCoefficients.localKey(i).subEntity())
                        && !edgeInEpsilon1_C)
                        hierarchicError.set(e, dim-1,  // codim
                                            localCoefficients.localKey(i).subEntity(),
                                            totalEdgeIndicator*totalEdgeIndicator);

                    if (edgeInEpsilon1_C)
                        hierarchicError.set(e, dim-1,  // codim
                                            localCoefficients.localKey(i).subEntity(),
                                            tangentEdgeIndicator*tangentEdgeIndicator);

                    break;
                }
            case 0:
                // Element indicators
                // These only contribute if the element borders the Dirichlet or the contact boundary
                assert(e.type().isTriangle());  //numfaces==3 is hardwired in the next line
                for (int j=0; j<3; j++) {

                    if (dirichletBoundary->contains(e,j) or contactBoundary_.contains(e,j)) {

                        // \rho_{E_i,\tau} = \langle \sigma_S, b_\tau E_i \rangle \norm{b_\tau E_i}^{-1}
                        Dune::FieldVector<double,dim> elementIndicator;
                        for (int k=0; k<dim; k++)
                            elementIndicator[k] = residual[idx][k] / std::sqrt(matrix[idx][idx][k][k]);

                        hierarchicError.set(e, 0,  // codim
                                            localCoefficients.localKey(i).subEntity(),
                                            elementIndicator.two_norm2());

                        // leave the loop: once is enough
                        break;

                    }

                }

                break;

            default:
                DUNE_THROW(Dune::NotImplemented, "Unsupported basis function type found!");

            }

            handled[idx] = true;

        }

    }

    // Store the indicators because getErrorSquared needs it
    hierarchicalIndicators_ = hierarchicError;

}

template <class GridType>
double ZouKornhuberSignoriniEstimator<GridType>::
getErrorSquared(int grid,
                LocalOperatorAssembler<GridType,
                typename ZouKornhuberBasis<typename GridType::LeafGridView>::LocalFiniteElement,
                typename ZouKornhuberBasis<typename GridType::LeafGridView>::LocalFiniteElement,
                Dune::FieldMatrix<field_type,dim,dim> >* localStiffness) const
{
    double errorSquared = 0;

    // We meet edge and vertex basis functions several times, but handle them only once
    Dune::BitSetVector<1> handled(zouKornhuberBasis_.size());
    handled.unsetAll();

    for (const auto& e : elements(grid_.leafGridView())) {

        // Only triangle elements are currently implemented
        assert(e.type().isTriangle());

        ZouKornhuberFiniteElement<double,double> localFiniteElement;
        const ZouKornhuberLocalCoefficients& localCoefficients = localFiniteElement.localCoefficients();

        for (size_t i=0; i<localCoefficients.size(); i++) {

            // Global index
            unsigned int idx = zouKornhuberBasis_.index(e, i);

            if (handled[idx][0])
                continue;

            double indicator = hierarchicalIndicators_.value(e,
                                                          localCoefficients.localKey(i).codim(),
                                                          localCoefficients.localKey(i).subEntity());

            errorSquared += indicator;// * indicator;

            handled[idx] = true;

        }

    }

    return errorSquared;
}



template <class GridType>
void ZouKornhuberSignoriniEstimator<GridType>::
computeLocalCoordinateSystems(std::vector<BlockType>& coordSystems) const
{

    // ///////////////////////////////////////////////////////////////
    //   Compute boundary normals at all dofs carrying obstacles
    // ///////////////////////////////////////////////////////////////

    VectorType normals(zouKornhuberBasis_.size());
    normals = 0;

    // Loop over all neighbors
    for (const auto& nIt : contactBoundary_) {

        const auto& inside = nIt.inside();

        const auto& refElement = Dune::ReferenceElements<field_type, dim>::general(inside.type());

        const auto& localCoefficients = zouKornhuberBasis_.getLocalFiniteElement(inside).localCoefficients();

        for (size_t i=0; i<localCoefficients.size(); i++) {

            // //////////////////////////////////////////////////
            //   Test whether dof is on the boundary face
            // //////////////////////////////////////////////////

            bool isOnFace = false;

            int entity = localCoefficients.localKey(i).subEntity();
            unsigned int codim  = localCoefficients.localKey(i).codim();

            if (codim!=0)  // disregard element dofs
                for (int j = 0; j<refElement.size(nIt.indexInInside(), 1, codim); j++)
                    if (refElement.subEntity(nIt.indexInInside(),1, j, codim) == entity) {
                        isOnFace = true;
                        break;
                    }

            if (isOnFace) {

                // Get the position of the dof on the current element
                const auto& pos = refElement.position(entity, codim);
                const auto& posOnSegment = nIt.geometryInInside().local(pos);

                size_t dofIdx = zouKornhuberBasis_.index(inside,i);

                // /////////////////////////////////////
                //   Compute surface normals
                // /////////////////////////////////////

                normals[dofIdx] += nIt.outerNormal(posOnSegment);

            }

        }
    }

    // //////////////////////////////////
    //   Normalize the normal vectors
    // //////////////////////////////////
    for (size_t i=0; i<normals.size(); i++)
        if (normals[i].two_norm() > 0.5)
            normals[i] /= normals[i].two_norm();


    // /////////////////////////////////////////////////////////
    // Compute a full orthonormal coordinate system for each
    // boundary segment carrying an obstacle.  We do this by computing
    // the Householder reflection of the first canonical basis
    // vector into the surface normal.
    // /////////////////////////////////////////////////////////

    // Make first canonical basis vector
    Dune::FieldVector<double, dim> e0(0);
    e0[0] = 1;

    coordSystems.resize(normals.size());

    for (unsigned int i=0; i<normals.size(); i++) {

        if (normals[i].two_norm() > 0.5) {
            // This is a boundary dof --> the coordinate system
            // will consist of the surface normal and tangential vectors
            householderReflection(e0, normals[i], coordSystems[i]);
        } else {
            // There is no obstacle.  The coordinate system will be the canonical one
            for (int j=0; j<dim; j++)
                for (int k=0; k<dim; k++)
                    coordSystems[i][j][k] = (j==k);

        }

    }

}

// Sets obstacles for the Zou-Kornhuber basis on the leaf level
template <class GridType>
void ZouKornhuberSignoriniEstimator<GridType>::
setObstacles(std::vector<BoxConstraint<double,dim> >& obstacles,
             const std::vector<BlockType>& coordSystems) const
{
    assert(coordSystems.size() == zouKornhuberBasis_.size());
    obstacles.resize(zouKornhuberBasis_.size());

    // //////////////////////////////////////////////////
    //   Loop over the contact boundary
    // //////////////////////////////////////////////////

    for (const auto& it : contactBoundary_) {

        const auto& inside = it.inside();
        const auto& refElement = Dune::ReferenceElements<double, dim>::general(inside.type());

        const auto& localCoefficients = zouKornhuberBasis_.getLocalFiniteElement(inside).localCoefficients();

        for (size_t i=0; i<localCoefficients.size(); i++) {

            // //////////////////////////////////////////////////
            //   Test whether dof is on the boundary face
            // //////////////////////////////////////////////////

            bool isOnFace = false;

            int entity = localCoefficients.localKey(i).subEntity();
            unsigned int codim  = localCoefficients.localKey(i).codim();

            if (codim!=0)  // disregard element dofs
                for (int j = 0; j<refElement.size(it.indexInInside(), 1, codim); j++)
                    if (refElement.subEntity(it.indexInInside(),1, j, codim) == entity) {
                        isOnFace = true;
                        break;
                    }

            if (isOnFace) {

                int dofIdx = zouKornhuberBasis_.index(inside, i);
                const auto& pos = inside.geometry().global(refElement.position(entity, codim));
                const auto& normal = coordSystems[dofIdx][0];

                obstacles[dofIdx].upper(0) = obstacleFunction_(pos,normal);
                if (obstacles[dofIdx].upper(0)<-1e-8)
                    obstacles[dofIdx].upper(0) = std::numeric_limits<field_type>::max();
            }

        }

    }
}

} /* namespace Contact */
} /* namespace Dune */

#endif
