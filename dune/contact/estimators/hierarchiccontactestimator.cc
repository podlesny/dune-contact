#include <dune/common/bitsetvector.hh>
#include <dune/common/timer.hh>

#include <dune/istl/bdmatrix.hh>

#include <dune/fufem/estimators/hierarchicestimatorbase.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>
#include <dune/fufem/functiontools/boundarydofs.hh>
#include <dune/fufem/functionspacebases/p2hierarchicalbasis.hh>
#include <dune/fufem/boundarypatchprolongator.hh>
#include <dune/fufem/estimators/refinementindicator.hh>
#include <dune/fufem/assemblers/functionalassembler.hh>
#include <dune/fufem/assemblers/boundaryfunctionalassembler.hh>
#include <dune/fufem/assemblers/localassemblers/neumannboundaryassembler.hh>

#include <dune/contact/assemblers/leafp2mortarcoupling.hh>
#include <dune/contact/assemblers/p2onebodyassembler.hh>
#include <dune/contact/estimators/defectproblem.hh>

#ifdef HAVE_IPOPT
#include "IpIpoptApplication.hpp"
#else
#error You need IPOpt to use the HierarchicContactEstimator
#endif

namespace Dune {
namespace Contact {

template <class BasisType, class EnlargedBasisType>
void HierarchicContactEstimator<BasisType, EnlargedBasisType>::estimate(const std::vector<std::shared_ptr<GridFunction> >& x,
                  const std::vector<std::shared_ptr<GridFunction> >& volumeTerm,
                  const std::vector<std::shared_ptr<GridFunction> >& neumannTerm,
                  const std::vector<std::shared_ptr<LeafBoundaryPatch> >& dirichletBoundary,
                  std::vector<std::shared_ptr<RefinementIndicator<GridType> > >& refinementIndicator,
                  const std::vector<std::shared_ptr<EnlargedBasisType> >& enlargedBasis,
                  std::vector<std::shared_ptr<EnlargedLocalAssembler> >& localStiffness)
{

    assert(x.size() == volumeTerm.size());
    assert(x.size() == neumannTerm.size());

    // ////////////////////////////////////////////////////////////////////
    //   embed x into an enlarged fe space
    // ////////////////////////////////////////////////////////////////////

    std::vector<VectorType> xEnlarged(numGrids());

    // interpolate solution to enlarged finite element space
    for (int i=0; i<numGrids(); i++)
        ::Functions::interpolate(*enlargedBasis[i], xEnlarged[i], *x[i]);

    // ////////////////////////////////////////////////////////////////////
    //   Assemble the separate preconditioned defect problems
    // ////////////////////////////////////////////////////////////////////

    std::vector<VectorType> residual(numGrids());
    std::vector<BDMatrix<BlockType> > matrix(numGrids());

    Timer watch;

    std::cout<< "warning: if the enlarged basis is not a proper direct sum of the original basis,\n"
              << "then a slight error is introduced by only subtracted the diagonal elements of the stiffness \n"
              << "times the old solution from the residual\n";
    for (int i=0; i<numGrids(); i++)
        HierarchicEstimatorBase<EnlargedBasisType, dim, field_type>::preconditionedDefectProblem(*enlargedBasis[i], xEnlarged[i],
                                                         volumeTerm[i].get(), neumannTerm[i].get(), residual[i], matrix[i], localStiffness[i].get());

    std::cout << watch.elapsed() << " to assemble" << std::endl;

    // ///////////////////////////////////////////////////////////
    //   Construct the set of Dirichlet dofs for the enlarged problems
    // ///////////////////////////////////////////////////////////

    std::vector<BitSetVector<dim> > enlargedDirichletNodes(numGrids());

    for (int i=0; i<numGrids(); i++)
        constructBoundaryDofs(*dirichletBoundary[i], *enlargedBasis[i], enlargedDirichletNodes[i]);

    // //////////////////////////////////////////////////////////////////////
    //   Set up solution vectors for the preconditioned defect problem
    // //////////////////////////////////////////////////////////////////////

    predXNodal_.resize(numGrids());
    for (int i=0; i<numGrids(); i++) {
        predXNodal_[i].resize(xEnlarged[i].size());
        predXNodal_[i] = 0;
    }

    // ///////////////////////////////////////////////////////
    //   Construct the enlarged mortar coupling matrices
    // ///////////////////////////////////////////////////////

    // WARNING: By now P2 is still hardwired here
    std::vector<LeafP2MortarCoupling<field_type,GridType> > mortarCoupling(numCouplings());

    for (int i=0; i<numCouplings(); i++) {

        if (couplings_[i].type_ != CouplingPairBase::RIGID) {

            int grid0Idx = couplings_[i].gridIdx_[0];
            int grid1Idx = couplings_[i].gridIdx_[1];

            // setup the higher order mortar coupling
            mortarCoupling[i].setGrids(*grids_[grid0Idx], *grids_[grid1Idx]);
            mortarCoupling[i].setupContactPatch(*couplings_[i].patch0(), *couplings_[i].patch1());

            auto enlargedFunction0 = ::Functions::makeFunction(*enlargedBasis[grid0Idx], xEnlarged[grid0Idx]);
            auto enlargedFunction1 = ::Functions::makeFunction(*enlargedBasis[grid1Idx], xEnlarged[grid1Idx]);

            mortarCoupling[i].setup(enlargedFunction0, enlargedFunction1);

        }

    }

    // ///////////////////////////////////////////////////////
    //   Set up and solve the defect obstacle problem
    // ///////////////////////////////////////////////////////

    DefectProblem<dim,field_type>* defectSolver = new DefectProblem<dim,field_type>(numGrids(), numCouplings());

    defectSolver->hessians_       = &matrix;
    defectSolver->x_              = &predXNodal_;
    defectSolver->rhs_            = &residual;
    defectSolver->dirichletNodes_ = &enlargedDirichletNodes;

    // Assemblers for possible rigid obstacles
    std::vector<P2OneBodyAssembler<GridType,field_type> > rigidObstacles(numCouplings());

    for (int i=0; i<numCouplings(); i++) {

        if (couplings_[i].type_ != CouplingPairBase::RIGID) {

            defectSolver->nonmortarLagrangeMatrix_[i] = &mortarCoupling[i].nonmortarLagrangeMatrix();
            defectSolver->mortarLagrangeMatrix_[i]    = &mortarCoupling[i].mortarLagrangeMatrix();
            defectSolver->couplings_[i][0]            = couplings_[i].gridIdx_[0];
            defectSolver->couplings_[i][1]            = couplings_[i].gridIdx_[1];

        } else {

            defectSolver->nonmortarLagrangeMatrix_[i] = NULL;
            defectSolver->mortarLagrangeMatrix_[i]    = NULL;
            defectSolver->couplings_[i][0]            = couplings_[i].gridIdx_[0];
            defectSolver->couplings_[i][1]            = couplings_[i].gridIdx_[0];  // Yes!  Twice the same grid!

        }

        // ///////////////////////////////////
        //   Set the obstacle values
        // ///////////////////////////////////

        defectSolver->obstacles_[i].resize(predXNodal_[couplings_[i].gridIdx_[0]].size());
        for (size_t j=0; j<defectSolver->obstacles_[i].size(); j++)
            defectSolver->obstacles_[i][j].clear();

        switch (couplings_[i].type_) {
            case CouplingPairBase::NONE:
                break;
            case CouplingPairBase::CONTACT:
                for (size_t j=0; j<defectSolver->obstacles_[i].size(); j++)
                    if (mortarCoupling[i].nonmortarLagrangeMatrix().getrowsize(j) > 0 )
                        defectSolver->obstacles_[i][j].upper(0) = mortarCoupling[i].weakObstacle_[j];
                break;
            case CouplingPairBase::GLUE:
                for (size_t j=0; j<defectSolver->obstacles_[i].size(); j++)
                    if (mortarCoupling[i].nonmortarLagrangeMatrix().getrowsize(j) > 0 )
                        for (int k=0; k<dim; k++)
                            defectSolver->obstacles_[i][j][k] = {{0, 0}};
                break;

            case CouplingPairBase::RIGID:

                rigidObstacles[i].setup(*grids_[couplings_[i].gridIdx_[0]],
                                        couplings_[i].obsFunction_,
                                        *couplings_[i].patch0());
                rigidObstacles[i].assembleObstacles();
                defectSolver->obstacles_[i] = rigidObstacles[i].obstacles_;

                break;

            default:
                DUNE_THROW(NotImplemented, "Coupling type not implemented!");
        }

        // /////////////////////////////////////////////////////////////////////////
        //   Add coordinate system changes for CONTACT and STICK_SLIP couplings
        // /////////////////////////////////////////////////////////////////////////

        if (couplings_[i].type_==CouplingPairBase::CONTACT
                || couplings_[i].type_ == CouplingPairBase::STICK_SLIP) {

            // Make first canonical basis vector
            FieldVector<field_type, dim> e0(0);
            e0[0] = 1;

            for (size_t j=0; j<defectSolver->obstacles_[i].size(); j++) {

                // Modify nonmortar - lagrange matrix
                for (auto& cIt : mortarCoupling[i].nonmortarLagrangeMatrix()[j]) {

                    // ///////////////////////////////////////////////
                    //   Householder reflection
                    // ///////////////////////////////////////////////

                    BlockType newCoordSystem;
                    householderReflection(e0, mortarCoupling[i].weakNormals()[j], newCoordSystem);

                    cIt.rightmultiply(newCoordSystem);

                }

                // Modify mortar - lagrange matrix
                for (auto& cIt : mortarCoupling[i].mortarLagrangeMatrix_[j]) {

                    // ///////////////////////////////////////////////
                    //   Householder reflection
                    // ///////////////////////////////////////////////

                    BlockType newCoordSystem;
                    householderReflection(e0, mortarCoupling[i].weakNormals()[j], newCoordSystem);

                    cIt.rightmultiply(newCoordSystem);

                }

            }

        } else if (couplings_[i].type_ == CouplingPairBase::RIGID) {
            rigidObstacles[i].transformMatrix(matrix[couplings_[i].gridIdx_[0]]);
        }

    }

    // Create a new instance of IpoptApplication
    Ipopt::SmartPtr<Ipopt::IpoptApplication> app = new Ipopt::IpoptApplication();

    // Change some options
    app->Options()->SetNumericValue("tol", 1e-8);
    app->Options()->SetNumericValue("acceptable_tol", 1e-8);
    app->Options()->SetIntegerValue("max_iter", 100);
    app->Options()->SetStringValue("mu_strategy", "adaptive");
    app->Options()->SetStringValue("output_file", "ipopt.out");
    app->Options()->SetIntegerValue("print_level", 0);

    // Specify solver
    //app->Options()->SetStringValue("linear_solver", "ma27");

    // Change memory settings
    //app->Options()->SetNumericValue("ma27_liw_init_factor", 5);
    //app->Options()->SetNumericValue("ma27_la_init_factor", 5);
    //app->Options()->SetNumericValue("ma27_meminc_factor", 10);

    // Intialize the IpoptApplication and process the options
    Ipopt::ApplicationReturnStatus status;
    status = app->Initialize();
    if (status != Ipopt::Solve_Succeeded)
        DUNE_THROW(SolverError, "Error during IPOpt initialization!");

    // Ask Ipopt to solve the problem
    Ipopt::SmartPtr<Ipopt::TNLP> defectSolverSmart = defectSolver;
    status = app->OptimizeTNLP(defectSolverSmart);

    if (status != Ipopt::Solve_Succeeded && status != Ipopt::Solved_To_Acceptable_Level)
        DUNE_THROW(SolverError, "Solving the defect problem failed!");

    // /////////////////////////////////////////////////////
    //   Postprocess the solution
    // /////////////////////////////////////////////////////

    //   Transform to canonical coordinates, if necessary
    for (int i=0; i<numCouplings(); i++)
        if (couplings_[i].type_ == CouplingPairBase::RIGID)
            rigidObstacles[i].transformVector(predXNodal_[couplings_[i].gridIdx_[0]]);

    // /////////////////////////////////////////////////
    //   Change from nodal to hierarchical basis
    // /////////////////////////////////////////////////

    std::vector<VectorType> hierarchicError(numGrids());

    for (int i=0; i<numGrids(); i++) {

        VectorType tmp;

        // TODO: This should be used as Enlarged Basis Type directly
        P2HierarchicalBasis<typename GridType::LeafGridView, field_type> p2HierarchicalBasis(grids_[i]->leafGridView());

        auto predXNodalFunction = ::Functions::makeFunction(*enlargedBasis[i], predXNodal_[i]);

        ::Functions::interpolate(p2HierarchicalBasis, tmp, predXNodalFunction);

        // /////////////////////////////////////////////////////////////////////
        //   Multiply defect solution in hierarchical basis by the diagonal
        //   matrix.  The edge bubbles can then be used as error indicators.
        // /////////////////////////////////////////////////////////////////////

        //hierarchicError[i].resize(predXNodal_[i].size());
        //hierarchicError[i] = 0;
        //matrix[i].umv(predXNodal_[i], hierarchicError[i]);

        hierarchicError[i].resize(tmp.size());
        hierarchicError[i] = 0;
        matrix[i].umv(tmp, hierarchicError[i]);

    }

    // ////////////////////////////////////////////////////////////////
    //   Set up refinement indicators.  Only the edge contributions
    //   of the hierarchical error are refinement indicators.
    // ////////////////////////////////////////////////////////////////

    for (int i=0; i<numGrids(); i++) {

        refinementIndicator[i]->clear();

        // Extract all contributions that correspond to edges
        for (const auto& e : elements(grids_[i]->leafGridView())) {

            const auto& localCoefficients = enlargedBasis[i]->getLocalFiniteElement(e).localCoefficients();

            for (size_t j=0; j<localCoefficients.size(); j++) {

                int codim     = localCoefficients.localKey(j).codim();
                int subEntity = localCoefficients.localKey(j).subEntity();

                if (codim == dim-1) {
                    unsigned int globalIdx = enlargedBasis[i]->index(e, j);
                    refinementIndicator[i]->set(e, dim-1, subEntity, hierarchicError[i][globalIdx].two_norm());
                }

            }

        }

    }
}

template <class BasisType, class EnlargedBasisType>
template <class ProblemType>
void HierarchicContactEstimator<BasisType, EnlargedBasisType>::estimate(const std::vector<std::shared_ptr<GridFunction> >& x,
                  const std::vector<std::shared_ptr<GridFunction> >& volumeTerm,
                  const std::vector<std::shared_ptr<GridFunction> >& neumannTerm,
                  const std::vector<std::shared_ptr<LeafBoundaryPatch> >& dirichletBoundary,
                  const std::vector<std::shared_ptr<LeafBoundaryPatch> >& neumannBoundary,
                  std::vector<std::shared_ptr<RefinementIndicator<GridType> > >& refinementIndicator,
                  const std::vector<std::shared_ptr<EnlargedBasisType> >& enlargedBasis,
                  std::shared_ptr<ProblemType> contactProblem)
{

    std::cout<<"Warning: This code is still experimental! \n";
    // ////////////////////////////////////////////////////////////////////
    //   embed x into a second order fe space with the nodal basis
    // ////////////////////////////////////////////////////////////////////

    std::vector<VectorType> xEnlarged(numGrids());

    // interpolate solution to enlarged finite element space
    for (int i=0; i<numGrids(); i++)
        ::Functions::interpolate(*enlargedBasis[i], xEnlarged[i], *x[i]);

    /////////////////////////////////////////////////////////////////////
    //    Assemble external forces
    ///////////////////////////////////////////////////////////////////

    std::vector<VectorType> residual(numGrids());
    std::vector<BDMatrix<BlockType> > matrix(numGrids());

    Timer watch;

    // assemble external forces
    std::vector<VectorType> externalForces(numGrids());
    for (int i=0; i<numGrids(); i++) {

        externalForces[i].resize(xEnlarged[i].size());
        externalForces[i] = 0;

        if (volumeTerm[i]) {

            // assemble volume forces
            L2FunctionalAssembler<GridType, EnlargedLfe, FieldVector<field_type,dim> > localL2Assembler(*volumeTerm[i]);

            FunctionalAssembler<EnlargedBasisType> funcAssembler(*enlargedBasis[i]);
            funcAssembler.assemble(localL2Assembler,externalForces[i]);
        }

        if (neumannTerm[i]) {

            // assemble Neumann forces
            BoundaryFunctionalAssembler<EnlargedBasisType> boundaryAssembler(*enlargedBasis[i],*neumannBoundary[i]);
            NeumannBoundaryAssembler<GridType, FieldVector<field_type,dim> > localNeumannAssembler(*neumannTerm[i]);
            boundaryAssembler.assemble(localNeumannAssembler, externalForces[i], false);
        }
    }

    contactProblem->setExternalForces(externalForces);
    contactProblem->assembleDefectQP(xEnlarged, residual, matrix);

    std::cout << watch.elapsed() << " to assemble the preconditioned defect problem." << std::endl;


    // ///////////////////////////////////////////////////////////
    //   Construct the set of Dirichlet dofs for the p2 problems
    // ///////////////////////////////////////////////////////////

    std::vector<BitSetVector<dim> > enlargedDirichletNodes(numGrids());

    for (int i=0; i<numGrids(); i++)
        constructBoundaryDofs(*dirichletBoundary[i], *enlargedBasis[i], enlargedDirichletNodes[i]);

    // //////////////////////////////////////////////////////////////////////
    //   Set up solution vectors for the preconditioned defect problem
    // //////////////////////////////////////////////////////////////////////

    predXNodal_.resize(numGrids());
    for (int i=0; i<numGrids(); i++) {
        predXNodal_[i].resize(xEnlarged[i].size());
        predXNodal_[i] = 0;
    }

    // ///////////////////////////////////////////////////////
    //   Construct the enlarged mortar coupling matrices
    // ///////////////////////////////////////////////////////

    // Warning: At this point the deformed grids should be used to compute the mortar projection
    std::vector<LeafP2MortarCoupling<field_type, GridType> > mortarCoupling(numCouplings());

    for (int i=0; i<numCouplings(); i++) {

        if (couplings_[i].type_ != CouplingPairBase::RIGID) {

            int grid0Idx = couplings_[i].gridIdx_[0];
            int grid1Idx = couplings_[i].gridIdx_[1];

            // setup the higher order mortar coupling
            mortarCoupling[i].setGrids(*grids_[grid0Idx], *grids_[grid1Idx]);
            mortarCoupling[i].setupContactPatch(*couplings_[i].patch0(), *couplings_[i].patch1());

            auto enlargedFunction0 = ::Functions::makeFunction(*enlargedBasis[grid0Idx], xEnlarged[grid0Idx]);
            auto enlargedFunction1 = ::Functions::makeFunction(*enlargedBasis[grid1Idx], xEnlarged[grid1Idx]);

            mortarCoupling[i].setup(enlargedFunction0, enlargedFunction1);
        }
    }

    // ///////////////////////////////////////////////////////
    //   Set up and solve the defect obstacle problem
    // ///////////////////////////////////////////////////////

    DefectProblem<dim,field_type>* defectSolver = new DefectProblem<dim,field_type>(numGrids(), numCouplings());

    defectSolver->hessians_       = &matrix;
    defectSolver->x_              = &predXNodal_;
    defectSolver->rhs_            = &residual;
    defectSolver->dirichletNodes_ = &enlargedDirichletNodes;

    // Assemblers for possible rigid obstacles
    std::vector<P2OneBodyAssembler<GridType,field_type> > rigidObstacles(numCouplings());


    for (int i=0; i<numCouplings(); i++) {

        if (couplings_[i].type_ != CouplingPairBase::RIGID) {

            defectSolver->nonmortarLagrangeMatrix_[i] = &mortarCoupling[i].nonmortarLagrangeMatrix();
            defectSolver->mortarLagrangeMatrix_[i]    = &mortarCoupling[i].mortarLagrangeMatrix();
            defectSolver->couplings_[i][0]            = couplings_[i].gridIdx_[0];
            defectSolver->couplings_[i][1]            = couplings_[i].gridIdx_[1];

        } else {

            defectSolver->nonmortarLagrangeMatrix_[i] = NULL;
            defectSolver->mortarLagrangeMatrix_[i]    = NULL;
            defectSolver->couplings_[i][0]            = couplings_[i].gridIdx_[0];
            defectSolver->couplings_[i][1]            = couplings_[i].gridIdx_[0];  // Yes!  Twice the same grid!

        }

        // ///////////////////////////////////
        //   Set the obstacle values
        // ///////////////////////////////////

        defectSolver->obstacles_[i].resize(predXNodal_[couplings_[i].gridIdx_[0]].size());
        for (size_t j=0; j<defectSolver->obstacles_[i].size(); j++)
            defectSolver->obstacles_[i][j].clear();

        switch (couplings_[i].type_) {
            case CouplingPairBase::NONE:
                break;
            case CouplingPairBase::CONTACT:
                for (size_t j=0; j<defectSolver->obstacles_[i].size(); j++)
                    if (mortarCoupling[i].nonmortarLagrangeMatrix().getrowsize(j) > 0 )
                        defectSolver->obstacles_[i][j].upper(0) = mortarCoupling[i].weakObstacle_[j];
                break;
            case CouplingPairBase::GLUE:
                for (size_t j=0; j<defectSolver->obstacles_[i].size(); j++)
                    if (mortarCoupling[i].nonmortarLagrangeMatrix().getrowsize(j) > 0 )
                        for (int k=0; k<dim; k++)
                            defectSolver->obstacles_[i][j][k] = {{0, 0}};
                break;

            case CouplingPairBase::RIGID:

                rigidObstacles[i].setup(*grids_[couplings_[i].gridIdx_[0]],
                                        couplings_[i].obsFunction_,
                                        *couplings_[i].patch0());
                rigidObstacles[i].assembleObstacles();
                defectSolver->obstacles_[i] = rigidObstacles[i].obstacles_;

                break;

            default:
                DUNE_THROW(NotImplemented, "Coupling type not implemented!");
        }

        // /////////////////////////////////////////////////////////////////////////
        //   Add coordinate system changes for CONTACT and STICK_SLIP couplings
        // /////////////////////////////////////////////////////////////////////////

        if (couplings_[i].type_==CouplingPairBase::CONTACT
                || couplings_[i].type_ == CouplingPairBase::STICK_SLIP) {

            // Make first canonical basis vector
            FieldVector<field_type, dim> e0(0);
            e0[0] = 1;

            for (size_t j=0; j<defectSolver->obstacles_[i].size(); j++) {

                // Modify nonmortar - lagrange matrix
                for (auto& cIt : mortarCoupling[i].nonmortarLagrangeMatrix()[j]) {

                    // ///////////////////////////////////////////////
                    //   Householder reflection
                    // ///////////////////////////////////////////////

                    BlockType newCoordSystem;
                    householderReflection(e0, mortarCoupling[i].weakNormals()[j], newCoordSystem);

                    cIt.rightmultiply(newCoordSystem);

                }

                // Modify mortar - lagrange matrix
                for (auto& cIt : mortarCoupling[i].mortarLagrangeMatrix_[j]) {

                    // ///////////////////////////////////////////////
                    //   Householder reflection
                    // ///////////////////////////////////////////////

                    BlockType newCoordSystem;
                    householderReflection(e0, mortarCoupling[i].weakNormals()[j], newCoordSystem);

                    cIt.rightmultiply(newCoordSystem);

                }

            }

        } else if (couplings_[i].type_ == CouplingPairBase::RIGID) {
            rigidObstacles[i].transformMatrix(matrix[couplings_[i].gridIdx_[0]]);
        }

    }

    // Create a new instance of IpoptApplication
    Ipopt::SmartPtr<Ipopt::IpoptApplication> app = new Ipopt::IpoptApplication();

    // Change some options
    app->Options()->SetNumericValue("tol", 1e-8);
    app->Options()->SetNumericValue("acceptable_tol", 1e-8);
    app->Options()->SetIntegerValue("max_iter", 100);
    app->Options()->SetStringValue("mu_strategy", "adaptive");
    app->Options()->SetStringValue("output_file", "ipopt.out");
    app->Options()->SetIntegerValue("print_level", 0);

    // Specify solver
    app->Options()->SetStringValue("linear_solver", "ma27");

    // Change memory settings
    app->Options()->SetNumericValue("ma27_liw_init_factor", 5);
    app->Options()->SetNumericValue("ma27_la_init_factor", 5);
    app->Options()->SetNumericValue("ma27_meminc_factor", 10);

    // Intialize the IpoptApplication and process the options
    Ipopt::ApplicationReturnStatus status;
    status = app->Initialize();
    if (status != Ipopt::Solve_Succeeded)
        DUNE_THROW(SolverError, "Error during IPOpt initialization!");

    // Ask Ipopt to solve the problem
    Ipopt::SmartPtr<Ipopt::TNLP> defectSolverSmart = defectSolver;
    status = app->OptimizeTNLP(defectSolverSmart);

    if (status != Ipopt::Solve_Succeeded && status != Ipopt::Solved_To_Acceptable_Level)
        DUNE_THROW(SolverError, "Solving the defect problem failed!");

    // /////////////////////////////////////////////////////
    //   Postprocess the solution
    // /////////////////////////////////////////////////////

    //   Transform to canonical coordinates, if necessary
    for (int i=0; i<numCouplings(); i++)
        if (couplings_[i].type_ == CouplingPairBase::RIGID)
            rigidObstacles[i].transformVector(predXNodal_[couplings_[i].gridIdx_[0]]);

    // /////////////////////////////////////////////////
    //   Change from nodal to hierarchical basis
    // /////////////////////////////////////////////////

    std::vector<VectorType> hierarchicError(numGrids());

    for (int i=0; i<numGrids(); i++) {

        VectorType tmp;

        P2HierarchicalBasis<typename GridType::LeafGridView, field_type> p2HierarchicalBasis(grids_[i]->leafGridView());

        auto predXNodalFunction = ::Functions::makeFunction(*enlargedBasis[i], predXNodal_[i]);

        ::Functions::interpolate(p2HierarchicalBasis, tmp, predXNodalFunction);

        // /////////////////////////////////////////////////////////////////////
        //   Multiply defect solution in hierarchical basis by the diagonal
        //   matrix.  The edge bubbles can then be used as error indicators.
        // /////////////////////////////////////////////////////////////////////

        hierarchicError[i].resize(tmp.size());
        hierarchicError[i] = 0;
        matrix[i].umv(tmp, hierarchicError[i]);

    }

    // ////////////////////////////////////////////////////////////////
    //   Set up refinement indicators.  Only the edge contributions
    //   of the hierarchical error are refinement indicators.
    // ////////////////////////////////////////////////////////////////

    for (int i=0; i<numGrids(); i++) {

        refinementIndicator[i]->clear();

        // Extract all contributions that correspond to edges
        for (const auto& e : elements(grids_[i]->leafGridView())) {

            const auto& localCoefficients = enlargedBasis[i]->getLocalFiniteElement(e).localCoefficients();
            int numLocalDofs = localCoefficients.size();

            for (int j=0; j<numLocalDofs; j++) {

                int codim     = localCoefficients.localKey(j).codim();
                int subEntity = localCoefficients.localKey(j).subEntity();

                if (codim == dim-1) {
                    unsigned int globalIdx = enlargedBasis[i]->index(e, j);
                    refinementIndicator[i]->set(e, dim-1, subEntity, hierarchicError[i][globalIdx].two_norm());
                }

            }

        }

    }
}

} /* namespace Contact */
} /* namespace Dune */
