#ifndef ZOU_KORNHUBER_FINITEELEMENT_HH
#define ZOU_KORNHUBER_FINITEELEMENT_HH

#include <dune/geometry/type.hh>

#include <dune/localfunctions/common/localfiniteelementtraits.hh>
#include <dune/localfunctions/common/localbasis.hh>
#include <dune/localfunctions/common/localkey.hh>
//#include <dune/localfunctions/common/localinterpolation.hh>

namespace Dune {
namespace Contact {

/**@ingroup LocalBasisImplementation
   \brief The local finite element needed for the Zou-Kornhuber estimator for Signorini problems

   This shape function set consists of three parts:
   - Linear shape functions associated to the element vertices
   - Piecewise linear edge bubbles
   - A cubic element bubble

   Currently this element exists only for triangles!

   The functions are associated to points by:

   f_0 ~ (0.0, 0.0)
   f_1 ~ (1.0, 0.0)
   f_2 ~ (0.0, 1.0)
   f_3 ~ (0.5, 0.0)
   f_4 ~ (0.0, 0.5)
   f_5 ~ (0.5, 0.5)
   f_6 ~ (1/3, 1/3)

   \tparam D Type to represent the field in the domain.
   \tparam R Type to represent the field in the range.

   \nosubgrouping
*/
template<class D, class R>
class ZouKornhuberLocalBasis
{
public:
    //! \brief export type traits for function signature
    using Traits = Dune::LocalBasisTraits<D,2,Dune::FieldVector<D,2>,R,1,Dune::FieldVector<R,1>,
                               Dune::FieldVector<Dune::FieldVector<R,2>,1> >;

    //! \brief number of shape functions
    unsigned int size () const
    {
        return 7;
    }

    //! \brief Evaluate all shape functions
    inline void evaluateFunction (const typename Traits::DomainType& in,
                                  std::vector<typename Traits::RangeType>& out) const
    {
        out.resize(7);

        int subElement;
        typename Traits::DomainType local;

        getSubElementCoordinates(in, subElement, local);

        // P1 functions
        out[0] = 1 - in[0] - in[1];
        out[1] = in[0];
        out[2] = in[1];

        // Piecewise linear edge bubbles
        switch (subElement) {
        case 0:

            out[3] = local[0];
            out[4] = local[1];
            out[5] = 0;
            break;

        case 1:

            out[3] = 1 - local[0] - local[1];
            out[4] = 0;
            out[5] = local[1];
            break;

        case 2:

            out[3] = 0;
            out[4] = 1 - local[0] - local[1];
            out[5] = local[0];
            break;
        case 3:

            out[3] = local[1];
            out[4] = local[0];
            out[5] = 1 - local[0] - local[1];
        }

        // Cubic element bubble
        out[6] = 27 * in[0] * in[1] * (1-in[0]-in[1]);

    }

    //! \brief Evaluate Jacobian of all shape functions
    inline void
    evaluateJacobian (const typename Traits::DomainType& in,         // position
                      std::vector<typename Traits::JacobianType>& out) const      // return value
    {
      out.resize(7);

      // P1 functions
      out[0][0][0] = -1;    out[0][0][1] = -1;
      out[1][0][0] =  1;    out[1][0][1] =  0;
      out[2][0][0] =  0;    out[2][0][1] =  1;

      // Piecewise linear edge bubbles
      int subElement = getSubElement(in);

      switch (subElement) {

      case 0:

          out[3][0][0] =  2;    out[3][0][1] =  0;
          out[4][0][0] =  0;    out[4][0][1] =  2;
          out[5][0][0] =  0;    out[5][0][1] =  0;
          break;

      case 1:

          out[3][0][0] = -2;    out[3][0][1] = -2;
          out[4][0][0] =  0;    out[4][0][1] =  0;
          out[5][0][0] =  0;    out[5][0][1] =  2;
          break;

      case 2:

          out[3][0][0] =  0;    out[3][0][1] =  0;
          out[4][0][0] = -2;    out[4][0][1] = -2;
          out[5][0][0] =  2;    out[5][0][1] =  0;
          break;
      case 3:

          out[3][0][0] =  0;    out[3][0][1] = -2;
          out[4][0][0] = -2;    out[4][0][1] =  0;
          out[5][0][0] =  2;    out[5][0][1] =  2;

      }

      // Cubic element bubble
      out[6][0][0] = 27 * in[1] * (1 - 2*in[0] - in[1]);
      out[6][0][1] = 27 * in[0] * (1 - 2*in[1] - in[0]);

    }

      /** \brief Polynomial order of the shape functions (in this case: 3)
          Doesn't really apply: the vertex shape functions are linear,
          the edge shape functions are piecewise linear, only the element bubble is cubic.
      */
    unsigned int order () const
    {
      return 3;
    }

private:
    /** \brief Get local coordinates in the subtriangle

    \param[in] global Coordinates in the reference triangle
    \param[out] subElement Which of the four subtriangles is global in?
    \param[out] local The local coordinates in the subtriangle
    */
    static void getSubElementCoordinates(const typename Traits::DomainType& global,
                                         int& subElement,
                                         typename Traits::DomainType& local)
    {
        if (global[0] + global[1] <= 0.5) {
            subElement = 0;
            local[0] = 2*global[0];
            local[1] = 2*global[1];
            return;
        } else if (global[0] >= 0.5) {
            subElement = 1;
            local[0] = 2*global[0]-1;
            local[1] = 2*global[1];
            return;
        } else if (global[1] >= 0.5) {
            subElement = 2;
            local[0] = 2*global[0];
            local[1] = 2*global[1]-1;
            return;
        }

        subElement = 3;
        local[0] = -2 * global[0] + 1;
        local[1] = -2 * global[1] + 1;

    }

    /** \brief Get the subtriangle

    \param[in] global Coordinates in the reference triangle
    \return Which of the four subtriangles is global in?
    */
    static int getSubElement(const typename Traits::DomainType& global)
    {
        if (global[0] + global[1] <= 0.5) {
            return 0;
        } else if (global[0] >= 0.5) {
            return 1;
        } else if (global[1] >= 0.5) {
            return 2;
        }

        return 3;
    }

};

/**@ingroup LocalLayoutImplementation
   \brief Layout map for the Zou-Kornhuber element

   \nosubgrouping
*/
class ZouKornhuberLocalCoefficients
{
public:
    //! \brief Standard constructor
    ZouKornhuberLocalCoefficients () : li(7)
    {
        li[0] = Dune::LocalKey(0,2,0);  // Vertex (0,0)
        li[1] = Dune::LocalKey(1,2,0);  // Vertex (1,0)
        li[2] = Dune::LocalKey(2,2,0);  // Vertex (0,1)
        li[3] = Dune::LocalKey(0,1,0);  // Edge (0.5, 0)
        li[4] = Dune::LocalKey(1,1,0);  // Edge (0, 0.5)
        li[5] = Dune::LocalKey(2,1,0);  // Edge (0.5, 0.5)
        li[6] = Dune::LocalKey(0,0,0);  // Element (1/3, 1/3)
    }

    //! number of coefficients
    size_t size () const
    {
        return 7;
    }

    //! get i'th index
    const Dune::LocalKey& localKey (size_t i) const
    {
        return li[i];
    }

private:
    std::vector<Dune::LocalKey> li;
};

template<class LB>
class ZouKornhuberLocalInterpolation
{
public:

    //! \brief Local interpolation of a function
    template<typename F, typename C>
    void interpolate (const F& f, std::vector<C>& out) const
    {
        typename LB::Traits::DomainType x;
        typename LB::Traits::RangeType y;

        out.resize(7);

        // vertices
        x[0] = 0.0; x[1] = 0.0; f.evaluate(x,y); out[0] = y;
        x[0] = 1.0; x[1] = 0.0; f.evaluate(x,y); out[1] = y;
        x[0] = 0.0; x[1] = 1.0; f.evaluate(x,y); out[2] = y;

        // edge bubbles
        x[0] = 0.5; x[1] = 0.0; f.evaluate(x,y);
        out[3] = y - out[0]*(1-x[0]) - out[1]*x[0];

        x[0] = 0.0; x[1] = 0.5; f.evaluate(x,y);
        out[4] = y - out[0]*(1-x[1]) - out[2]*x[1];

        x[0] = 0.5; x[1] = 0.5; f.evaluate(x,y);
        out[5] = y - out[1]*x[0] - out[2]*x[1];

        // element bubble
        x[0] = 1.0/3; x[1] = 1.0/3; f.evaluate(x,y);

        /** \todo Hack: extract the proper types */
        ZouKornhuberLocalBasis<double,double> shapeFunctions;
        std::vector<typename LB::Traits::RangeType> sfValues;
        shapeFunctions.evaluateFunction(x, sfValues);

        out[6] = y;
        for (int i=0; i<6; i++)
            out[6] -= out[i]*sfValues[i];

    }
};

/** \todo Please doc me !
 */
template<class D, class R>
class ZouKornhuberFiniteElement
{
public:
    /** \todo Please doc me !
     */
    typedef Dune::LocalFiniteElementTraits<ZouKornhuberLocalBasis<D,R>,
                                           ZouKornhuberLocalCoefficients,
                                           ZouKornhuberLocalInterpolation<ZouKornhuberLocalBasis<D,R> > > Traits;

/** \todo Please doc me !
 */
ZouKornhuberFiniteElement ()
{
    gt = Dune::GeometryTypes::triangle;
}

    /** \todo Please doc me !
     */
    const typename Traits::LocalBasisType& localBasis () const
    {
        return basis;
    }

    /** \todo Please doc me !
     */
    const typename Traits::LocalCoefficientsType& localCoefficients () const
    {
        return coefficients;
    }

    /** \todo Please doc me !
     */
    const typename Traits::LocalInterpolationType& localInterpolation () const
    {
        return interpolation;
    }

    /** \todo Please doc me !
     */
Dune::GeometryType type () const
    {
        return gt;
    }

private:
    ZouKornhuberLocalBasis<D,R> basis;
    ZouKornhuberLocalCoefficients coefficients;
    ZouKornhuberLocalInterpolation<ZouKornhuberLocalBasis<D,R> > interpolation;
    Dune::GeometryType gt;
};

} /* namespace Contact */
} /* namespace Dune */

#endif
