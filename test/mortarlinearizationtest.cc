// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#include <config.h>

#include <iostream>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/bitsetvector.hh>

#include <dune/istl/io.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>

#include <dune/grid/uggrid.hh>
#include <dune/common/parametertree.hh>

#include <dune/fufem/utilities/gridconstruction.hh>
#include <dune/fufem/utilities/boundarypatchfactory.hh>

#include <dune/contact/assemblers/largedeformationcoupling.hh>
#include <dune/contact/common/deformedcontinuacomplex.hh>

using namespace Dune;
using namespace Dune::Contact;

const int dim = 2;

using GridType = UGGrid<dim>;
using field_type = double;
using MatrixType = BCRSMatrix< Dune::FieldMatrix<field_type,dim,dim> >;
using JacobianType = BCRSMatrix< Dune::FieldMatrix<field_type,1,dim> >;
using VectorType =  BlockVector<FieldVector<field_type,dim> >;
using DeformedComplex =  DeformedContinuaComplex<GridType,VectorType>;
using DeformedGridType = typename DeformedComplex::DeformedGridType;
using ContactCoupling = LargeDeformationCoupling<field_type, DeformedGridType>;

template <class MatrixType>
double compare(const MatrixType& fd, const MatrixType& exact, const BitSetVector<dim>& notBuilt, double tol)
{
    auto diff = fd;
    diff -= exact;

    std::cout << "Testing " << fd.M()-notBuilt.count() << " entries!\n";
    double maxDiff = -1;

    for (size_t k=0; k < diff.N(); k++) {
        for (size_t j=0; j < diff.M(); j++) {
            if(!notBuilt[j].any() and diff[k][j].infinity_norm()>tol) {

                std::cout << "Component (" << k << "," << j << " is wrong: " << diff[k][j] << std::endl;
                std::cout << "Finite difference value is "<<fd[k][j]<<std::endl;
                if (exact.exists(k,j))
                  std::cout << "Exact value is "<<exact[k][j]<<std::endl;
                else
                  std::cout<<"Zero: 0 0 0\n";
            }

            if (!notBuilt[j].any())
                maxDiff = std::max(maxDiff,diff[k][j].infinity_norm());
        }
    }

    std::cout << "FD approximation could not be built for " << notBuilt.count() << " components\n ";
    std::cout << "Maximal error is " << maxDiff << std::endl;
    return maxDiff;
}

void remoteCornersFDapproximation(DeformedComplex& defGrids,
                           ContactCoupling& contactCoupling,
                           MatrixType& linRemCornersFDMat,
                           const field_type eps,
                           std::array<VectorType, 2>& displace,
                           BitSetVector<dim>& notBuilt)
{

    std::cout<< "Compute FD approximation of the linearised remote corners\n";
    notBuilt.resize(displace[0].size()+displace[1].size(),false);

    // dense matrix for the FD approximation
    int nNonM = contactCoupling.nonmortarBoundary().numVertices();
    int nM = contactCoupling.mortarBoundary().numVertices();

    int initialNMFaces = contactCoupling.nonmortarBoundary().numFaces();
    int initialMFaces = contactCoupling.mortarBoundary().numFaces();


    field_type averagedNMFaces(0), averagedMFaces(0);
    std::cout<<"Mortar coupling could be build for "<<nNonM<<" non-mortar and "<<nM<<" mortar vertices.\n";

    const auto& indexSet = contactCoupling.getGlue()->indexSet();
    MatrixIndexSet  mortarFDIndex(indexSet.size()*dim, displace[0].size()+displace[1].size());

    for (size_t i=0; i<indexSet.size()*dim;i++)
        for (size_t j=0; j<displace[0].size()+displace[1].size();j++)
            mortarFDIndex.add(i,j);

    mortarFDIndex.exportIdx(linRemCornersFDMat);
    linRemCornersFDMat = 0;

    for (size_t k=0; k<2; k++) {

    auto globalToLocal = (k==0) ? contactCoupling.unrednonmortarBoundary().makeGlobalToLocal()
                                : contactCoupling.unredmortarBoundary().makeGlobalToLocal();

    int offset = (k==0) ? 0 : displace[0].size();

    for (size_t i=0; i<displace[k].size(); i++)
        for (int j=0; j<dim; j++)  {

            if (globalToLocal[i] == -1)
                continue;

            displace[k][i][j]  += eps;
            defGrids.setDeformation(displace[k], k);
            contactCoupling.buildProjection();

            averagedNMFaces += contactCoupling.nonmortarBoundary().numFaces();
            averagedMFaces += contactCoupling.mortarBoundary().numFaces();

            const auto& glueSet = contactCoupling.getGlue()->indexSet();
            VectorType forNor(glueSet.size()*dim);
            forNor = 0;
            for (const auto& rIs : intersections(*contactCoupling.getGlue())) {

                if (!contactCoupling.nonmortarBoundary().contains(rIs.inside(),rIs.indexInInside()))
                    continue;

                int rIdx = glueSet.index(rIs);
                const auto& rGeom = rIs.geometry();
                for (int l=0; l<dim; l++)
                    forNor[rIdx*dim+l] = rGeom.corner(l);
            }

            if (contactCoupling.nonmortarBoundary().numFaces() != initialNMFaces
                     || contactCoupling.mortarBoundary().numFaces() != initialMFaces) {
                std::cout << "Column: (" << i << "," << j << ") could not be built.\n";
                notBuilt[offset + i][j] = true;
            }

            displace[k][i][j]  -= 2*eps;
            defGrids.setDeformation(displace[k], k);
            contactCoupling.buildProjection();

            const auto& glueSet2 = contactCoupling.getGlue()->indexSet();
            VectorType backNor(glueSet2.size()*dim);
            backNor = 0;
            for (const auto& rIs : intersections(*contactCoupling.getGlue())) {

                if (!contactCoupling.nonmortarBoundary().contains(rIs.inside(),rIs.indexInInside()))
                    continue;

                int rIdx = glueSet2.index(rIs);
                const auto& rGeom = rIs.geometry();
                for (int l=0; l<dim; l++)
                    backNor[rIdx*dim+l] = rGeom.corner(l);
            }

            if (contactCoupling.nonmortarBoundary().numFaces() != initialNMFaces
                     || contactCoupling.mortarBoundary().numFaces() != initialMFaces) {
                std::cout << "Column: (" << i << "," << j << ") could not be built.\n";
                notBuilt[offset + i][j] = true;
            }

            displace[k][i][j]  += eps;
            defGrids.setDeformation(displace[k], k);

            averagedNMFaces += contactCoupling.nonmortarBoundary().numFaces();
            averagedMFaces += contactCoupling.mortarBoundary().numFaces();

            for(size_t h=0; h<forNor.size(); h++)
                for(size_t l=0; l<dim; l++)
                    linRemCornersFDMat[h][offset + i][l][j] = (forNor[h][l]-backNor[h][l])/(2*eps);
        }
    }

    std::cout << "On the reference domain the contact mapping could be build for "
                        << initialNMFaces << " , " << initialMFaces <<" faces\n";

    int nUnredVerts = contactCoupling.unredmortarBoundary().numVertices()
                    + contactCoupling.unrednonmortarBoundary().numVertices();
    averagedNMFaces /= (nUnredVerts)*dim*2;
    averagedMFaces /= (nUnredVerts)*dim*2;

    std::cout << "During the FD approximation the contact mapping could be build for "
              << averagedNMFaces << " , " << averagedMFaces << " faces.\n";
}

void averageNormalFDapproximation(DeformedComplex& defGrids,
                           ContactCoupling& contactCoupling,
                           MatrixType& avNormFDMat,
                           const field_type eps,
                           VectorType& displace,
                           int gridIdx)
{

    std::cout<< "Compute FD approximation of the linearised normals\n";

    // check for which vertices the undeformed grid could built the mortar mapping
    int nVerts = (gridIdx==0) ? contactCoupling.nonmortarBoundary().numVertices()
                             : contactCoupling.mortarBoundary().numVertices();

    auto globalToLocal = (gridIdx==0) ? contactCoupling.nonmortarBoundary().makeGlobalToLocal()
                                      : contactCoupling.mortarBoundary().makeGlobalToLocal();
    auto globalToLocalBig = (gridIdx==0) ? contactCoupling.unrednonmortarBoundary().makeGlobalToLocal()
                                         : contactCoupling.unredmortarBoundary().makeGlobalToLocal();

    MatrixIndexSet  avNormalFDIndex(nVerts, displace.size());
    for (int i=0; i<nVerts; i++)
        for (size_t j=0; j<displace.size(); j++)
            avNormalFDIndex.add(i,j);

    // now the values
    avNormalFDIndex.exportIdx(avNormFDMat);
    avNormFDMat = 0;

    // derivatives w.r.t. nonmortar vertices
    for (size_t i=0; i<displace.size(); i++) {
        for (int j=0; j<dim; j++)  {

            if (globalToLocalBig[i] == -1)
                continue;

            displace[i][j]  += eps;
            defGrids.setDeformation(displace, gridIdx);

            BoundaryPatch<DeformedGridType::LeafGridView> b(contactCoupling.unrednonmortarBoundary());

            auto forNor  = b.getNormals();

            displace[i][j]  -= 2*eps;
            defGrids.setDeformation(displace, gridIdx);

            b = contactCoupling.unrednonmortarBoundary();

            auto backNor  = b.getNormals();

            displace[i][j]  += eps;
            defGrids.setDeformation(displace, gridIdx);

            for(size_t k=0; k<forNor.size(); k++)
                if (globalToLocal[k]>-1)
                    for (int l=0;l<dim;l++)
                        avNormFDMat[globalToLocal[k]][i][l][j] = (forNor[k][l]-backNor[k][l])/(2*eps);
       }
    }
    std::cout<<"Computed FD approximation of the linearised normals\n ";
}

// need the deformations
void mortarFDapproximation(DeformedComplex& defGrids,
                           ContactCoupling& contactCoupling,
                           std::array<JacobianType, 2>& mortarFDMat,
                           const field_type eps,
                           std::array<VectorType,2>&  disp,
                           std::array<Dune::BitSetVector<dim>, 2>& notBuilt)
{
    std::cout<< "Compute FD approximation of the mortar linearisation\n";

    // dense matrix for the FD approximation
    size_t nNonM = contactCoupling.nonmortarBoundary().numVertices();
    size_t nM = contactCoupling.mortarBoundary().numVertices();

    int initialNMFaces = contactCoupling.nonmortarBoundary().numFaces();
    int initialMFaces = contactCoupling.mortarBoundary().numFaces();

    field_type averagedNMFaces(0), averagedMFaces(0);

    std::cout<< "Mortar coupling could be build for " << nNonM << " non-mortar and " << nM << " mortar vertices.\n";
    for (size_t k=0; k<2; k++) {
        MatrixIndexSet  is(nNonM, disp[k].size());

        for (size_t i=0; i<nNonM;i++)
          for (size_t j=0; j<disp[k].size();j++)
              is.add(i,j);
        is.exportIdx(mortarFDMat[k]);
        mortarFDMat[k] = 0;

        auto globalToLocal = (k==0) ? contactCoupling.unrednonmortarBoundary().makeGlobalToLocal()
                                     : contactCoupling.unredmortarBoundary().makeGlobalToLocal();

        notBuilt[k].resize(disp[k].size(),false);

        for (size_t i=0; i<disp[k].size(); i++) {
            for (int j=0; j<dim; j++)  {

                if (globalToLocal[i] == -1)
                  continue;

                disp[k][i][j]  += eps;
                defGrids.setDeformation(disp[k], k);
                auto forNor  = contactCoupling.assembleObstacles(true);
                if (forNor.size() != nNonM ) {
                    std::cout<<"Didn't find exact number of faces "<<forNor.size()<<" should be "<<nNonM<<"\n";
                     std::cout << "Not build " << k << " Column: (" << i << "," << j << ") could not be built.\n";
                    notBuilt[k][i][j] = true;
                }

                averagedNMFaces += contactCoupling.nonmortarBoundary().numFaces();
                averagedMFaces += contactCoupling.mortarBoundary().numFaces();

                if (contactCoupling.nonmortarBoundary().numFaces() !=initialNMFaces
                    || contactCoupling.mortarBoundary().numFaces() !=initialMFaces) {
                  std::cout << "Grid " << k << " Column: (" << i << "," << j << ") could not be built.\n";
                  notBuilt[k][i][j] = true;
                }

                disp[k][i][j]  -= 2*eps;
                defGrids.setDeformation(disp[k], k);
                auto backNor = contactCoupling.assembleObstacles(true);

                if (backNor.size() != nNonM) {
                    std::cout<<"Didn't find exact number of faces "<<backNor.size()<<" should be "<< nNonM <<"\n";
                     std::cout << "Not build " << k << " Column: (" << i << "," << j << ") could not be built.\n";
                    notBuilt[k][i][j] = true;
                }
                disp[k][i][j]  += eps;

                averagedNMFaces += contactCoupling.nonmortarBoundary().numFaces();
                averagedMFaces += contactCoupling.mortarBoundary().numFaces();

                if (contactCoupling.nonmortarBoundary().numFaces() !=initialNMFaces
                    || contactCoupling.mortarBoundary().numFaces() !=initialMFaces) {
                  std::cout << "Grid " << k << " Column: (" << i << "," << j << ") could not be built.\n";
                  notBuilt[k][i][j] = true;
                }

                for(size_t l=0; l<forNor.size(); l++)
                  mortarFDMat[k][l][i][0][j] = (forNor[l]-backNor[l])/(2*eps);
            }
        }
        defGrids.setDeformation(disp[k], k);
    }

    std::cout << "On the reference domain the contact mapping could be build for "
              << initialNMFaces << " , " << initialMFaces << " faces\n";

    int nUnredVerts = contactCoupling.unredmortarBoundary().numVertices()
                    + contactCoupling.unrednonmortarBoundary().numVertices();
    averagedNMFaces /= (nUnredVerts)*dim*2;
    averagedMFaces /= (nUnredVerts)*dim*2;

    std::cout << "During the FD approximation the contact mapping could be build for "
              << averagedNMFaces << " , " << averagedMFaces << " faces.\n";
}

int main (int argc, char *argv[]) try
{

    Dune::MPIHelper::instance(argc, argv);

    double fdEps = 1e-6;
    double tol = 1e-6;

    if (argc == 2) {
      fdEps = std::atof(argv[1]);
    } else if (argc == 3) {
      fdEps = std::atof(argv[1]);
      tol = std::atof(argv[2]);
    }

    std::cout<<"eps "<<fdEps<<" and tol "<<tol<<std::endl;

    // Set the coupling
    std::string nonmortar = "die";
    std::string mortar = "box";
    int boxIdx = (nonmortar == "box") ? 0 : 1;
    int dieIdx = (nonmortar == "die") ? 0 : 1;

    std::vector<std::shared_ptr<GridType> > grids(2);


    // Dim == 2
    ParameterTree config;
    auto& rSub = config.sub("rect");
    rSub["lowerCorner"] = "-1.5 0";
    rSub["upperCorner"] = "1.4 1";
    rSub["elements"] = "2 1";
    rSub["tetrahedral"] = "1";
    grids[boxIdx] = GridConstruction<GridType,2>::createRectangle(rSub);

    auto& cSub = config.sub("circle");
    cSub["radius"] = "0.5";
    cSub["center"] = "0 1.6";
    grids[dieIdx] = GridConstruction<GridType,2>::createCircle(cSub);
    int minLevel= 2;
/*
    // dim == 3

    ParameterTree configBox;
    configBox["lowerCorner"] = "0 0 0";
    configBox["upperCorner"] = "4 9 3";
    configBox["elements"] = "4 8 2";
    configBox["tetrahedral"] = "1";
    grids[boxIdx] = GridConstruction<GridType,3>::createCuboid(configBox);

    ParameterTree configDie;
    configDie["center"] = "-0.7 4.5 6.2";
    configDie["thickness"] = "0.2";
    configDie["length"]= "5.2";
    configDie["innerRadius"]= "3";
    configDie["fromAngle"] = "3.1415";
    configDie["toAngle"] = "6.283";
    configDie["nElementRing"] = "25";
    configDie["nElementLength"] = "10";
    configDie["closeTube"] = "0";
    configDie["axis"] = "0";
    configDie["tetrahedra"] = "1";
    configDie["parameterizedBoundary"] = "0";
    grids[dieIdx] = GridConstruction<GridType,3>::createTubeSegment(configDie);
    int minLevel= 1;
*/
    for (int i=0;i<2;i++) {
        grids[i]->setRefinementType(GridType::COPY);

        for (int j=0; j<minLevel; j++)
            grids[i]->globalRefine(1);
    }

    DeformedComplex defGrids;

    defGrids.addGrid(*grids[0], nonmortar);
    defGrids.addGrid(*grids[1], mortar);

    // Set mortar couplings.  Currently not more than one for each grid
    using DefLevelBoundaryPatch =  BoundaryPatch<DeformedGridType::LevelGridView>;

    std::map<std::string,DefLevelBoundaryPatch> contactPatches;

    // read field describing the nonmortar boundary
    contactPatches["box"].setup(defGrids.grid("box").levelGridView(0));
    contactPatches["die"].setup(defGrids.grid("die").levelGridView(0));

    if (dim == 3) {

        // dim == 3
        ParameterTree config;
        config["nCriterions"] = "2";
        auto& c1 = config.sub("criterion0");
        c1["type"] = "LowerCentre";
        c1["component"] = "2";
        c1["bound"] = "4.5"; //5

        auto& c2 = config.sub("criterion1");
        c2["type"] = "LowerNormal";
        c2["component"] = "2";
        c2["bound"] = "-1e-1";
        BoundaryPatchFactory<DeformedGridType::LevelGridView>::setupBoundaryPatch(config,contactPatches["die"]);

        ParameterTree configBox;
        configBox["nCriterions"] = "1";
        auto& c3 = configBox.sub("criterion0");
        c3["type"] = "UpperCentre";
        c3["component"] = "2";
        c3["bound"] = "2.9999";

        BoundaryPatchFactory<DeformedGridType::LevelGridView>::setupBoundaryPatch(configBox,contactPatches["box"]);
    } else if (dim == 2) {
        // dim == 2
        ParameterTree config;
        config["nCriterions"] = "1";
        auto& c1 = config.sub("criterion0");
        c1["type"] = "LowerCentre";
        c1["component"] = "1";
        c1["bound"] = "1.5";

        BoundaryPatchFactory<DeformedGridType::LevelGridView>::setupBoundaryPatch(config,contactPatches["die"]);

        ParameterTree configBox;
        configBox["nCriterions"] = "1";
        auto& c2 = configBox.sub("criterion0");
        c2["type"] = "UpperCentre";
        c2["component"] = "1";
        c2["bound"] = "0.999";

        BoundaryPatchFactory<DeformedGridType::LevelGridView>::setupBoundaryPatch(configBox,contactPatches["box"]);
    }



    using ContactMerger = Dune::GridGlue::ContactMerge<dim, field_type>;
    using ProjectionType = ContactMerger::ProjectionType;
    ProjectionType type = ContactMerger::CLOSEST_POINT;
    auto merger = std::make_shared<ContactMerger>(0.1, type);

    LargeDeformationCoupling<field_type, DeformedGridType> contactCoupling(defGrids.grid(nonmortar),
                                                                           defGrids.grid(mortar),
                                                                           0.1,0.999, type);
    contactCoupling.setupContactPatch(contactPatches[nonmortar],contactPatches[mortar]);
    contactCoupling.gridGlueBackend_ = merger;

    std::array<VectorType, 2> displace;
    displace[0].resize(grids[0]->size(dim));
    displace[0] = 0;
    displace[1].resize(grids[1]->size(dim));
    displace[1] = 0;

    std::array<BitSetVector<dim>,2> notBuilt;
    field_type maxDiffNm(10),maxDiffM(10);

    {
        std::array<JacobianType,2> fdApprox;
        contactCoupling.setup(true);
        mortarFDapproximation(defGrids, contactCoupling, fdApprox, fdEps, displace, notBuilt);
        maxDiffNm = compare(fdApprox[0], contactCoupling.exactNonmortarMatrix(), notBuilt[0], tol);
        std::cout << "Check mortar part\n";
        maxDiffM = compare(fdApprox[1], contactCoupling.exactMortarMatrix(), notBuilt[1], tol);
    }

    auto maxDiff = std::fmax(maxDiffNm, maxDiffM);
    bool passed(maxDiff<1e-8);
    std::cout << "Checked full linearisation!\n";

    if (not passed) {
        std::cout << " FD approximation failed, checking smaller parts in more detail!\n";

        MatrixType fdApprox, nmExactApprox, mExactApprox;

        contactCoupling.buildProjection();
        std::cout << "Checking linearised averaged normals \n";
        LinearisedAveragedNormalsAssembler::assemble(contactCoupling.nonmortarBoundary(),
                                                     contactCoupling.unrednonmortarBoundary(),
                                                     nmExactApprox);

        LinearisedAveragedNormalsAssembler::assemble(contactCoupling.mortarBoundary(),
                                                     contactCoupling.unredmortarBoundary(),
                                                     mExactApprox);

        contactCoupling.buildProjection();
        averageNormalFDapproximation(defGrids, contactCoupling, fdApprox, fdEps, displace[0], 0);
        notBuilt[0].resize(displace[0].size());
        notBuilt[0].unsetAll();
        maxDiff = compare(fdApprox, nmExactApprox, notBuilt[0], tol);

        {
            std::cout << "Checking linearised remote vertices \n";

            MatrixType fdApproxRemote, exactApproxRemote;

            LinearisedIntersectionAssembler<ContactCoupling::Glue> remoteAssembler(contactCoupling.getGlue(),
                                                                                   (type == ContactMerger::OUTER_NORMAL));
            remoteAssembler.assemble(
                        contactCoupling.nonmortarBoundary(),
                        contactCoupling.unrednonmortarBoundary(),
                        contactCoupling.mortarBoundary(),
                        contactCoupling.unredmortarBoundary(),
                        merger->getOverlap(), merger->minNormalAngle(),
                        exactApproxRemote,(type == ContactMerger::OUTER_NORMAL) ? nmExactApprox : mExactApprox);
            std::cout << "Computed remote intersection linearisation! \n";

            remoteCornersFDapproximation(defGrids, contactCoupling, fdApproxRemote, fdEps, displace, notBuilt[0]);
            maxDiff = compare(fdApproxRemote, exactApproxRemote, notBuilt[0], tol);
            std::cout<<"Checked remote linearisation!\n";
        }
    }

    return passed ? 0 : 1;

} catch (Exception e) {

    std::cout << e << std::endl;

}
