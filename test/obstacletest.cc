#include "config.h"

#include <cmath>
#include <iostream>
#include <limits>
#include <memory>
#include <sstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/contact/assemblers/nbodyassembler.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>
#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/uggrid.hh>
#include <dune/grid-glue/merging/contactmerge.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/scaledidmatrix.hh>

const int dim = 3;
typedef Dune::UGGrid<dim> Grid;
typedef std::shared_ptr<Grid> GridPtr;
typedef Dune::GridFactory<Grid> GridFactory;
typedef BoundaryPatch<Grid::LeafGridView> LeafBoundaryPatch;
typedef BoundaryPatch<Grid::LevelGridView> LevelBoundaryPatch;

typedef Dune::FieldVector<double, dim> Vector;
typedef Dune::BlockVector<Vector> BigVector;
typedef Dune::FieldMatrix<double, dim, dim> Matrix;
typedef Dune::BCRSMatrix<Matrix> BigMatrix;

typedef Dune::Contact::NBodyAssembler<Grid, BigVector> Assembler;

void insertCube(GridFactory& factory, unsigned int& vertex, const Vector& offset)
{
  std::vector<unsigned int> vertices;

  for (unsigned i(0); i < 1<<dim; ++i) {
    Vector v(offset);
    for (unsigned j(0); j < dim; ++j) {
      if (i & (1 << j))
        v[j] += 1;
    }
    factory.insertVertex(v);
    vertices.push_back(vertex++);
  }

  factory.insertElement(Dune::GeometryTypes::cube(dim), vertices);
}

template<typename Projection, typename GridGlueBackend>
bool testTwoCubes(const std::string& name, std::shared_ptr<Projection> projection, std::shared_ptr<GridGlueBackend> gridGlueBackend, const Vector& offset, std::size_t expectedObstacles, unsigned level = 0)
{
  std::cout << "TEST: " << name << std::endl
            << "=============================================================" << std::endl;

  bool pass(true);

  std::array<GridPtr, 2> grids;
  for (unsigned i(0); i < grids.size(); ++i) {
    unsigned int vertex(0);
    GridFactory factory;
    Vector offset_i(offset); offset_i *= i;
    insertCube(factory, vertex, offset_i);
    grids[i] = GridPtr(factory.createGrid());
    grids[i]->globalRefine(level);
  }

  std::array< std::shared_ptr<LevelBoundaryPatch>, 2> patches;
  for (unsigned i(0); i < patches.size(); ++i) {
    patches[i] = std::make_shared<LevelBoundaryPatch>(grids[i]->levelGridView(0));
    auto selector = [](const Grid::LevelIntersection& intersection) -> bool {
      const auto inside = intersection.inside();
      const auto& ref = Dune::ReferenceElements<double, dim>::general(inside.type());
      const auto local = ref.template geometry<1>(intersection.indexInInside()).center();
      const auto global = inside.geometry().global(local);
      const auto epsilon = std::numeric_limits<double>::epsilon();
      return std::abs(global[dim-1] - 1.0) < epsilon;
    };
    patches[i]->insertFacesByProperty(selector);
    const auto nFaces = patches[i]->numFaces();
    const auto expectedFaces = 1u;
    if (nFaces != expectedFaces) {
      std::cerr << "ERROR: patches[" << i << "].numFaces = " << nFaces << " != expected (" << expectedFaces << ")" << std::endl;
      pass = false;
    }
  }

  Assembler assembler(grids.size(), 1);
  for (unsigned i(0); i < grids.size(); ++i) {
    assembler.grids_[i] = grids[i].get();
  }

  assembler.coupling_[0].set(0, 1, patches[0], patches[1], 1.0, Dune::Contact::CouplingPairBase::CONTACT, projection, gridGlueBackend);

  assembler.assembleTransferOperator();
  assembler.assembleObstacle();

  std::size_t nObstacles(0);
  for (const auto& o : assembler.totalObstacles_) {
    if (o.upper(0) != std::numeric_limits<double>::max())
      nObstacles++;
  }
  if (nObstacles != expectedObstacles) {
    std::cerr << "ERROR: number of obstacles = " << nObstacles << " != expected (" << expectedObstacles << ")" << std::endl;
    pass = false;
  }

  std::cout << std::endl << std::endl;

  return pass;
}

int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);

  bool pass(true);

  {
    auto projection = std::make_shared<Dune::Contact::NormalProjection<LeafBoundaryPatch> >();
    auto gridGlueBackend = std::make_shared<Dune::GridGlue::ContactMerge<dim, double> >(0);
    Vector offset(0); offset[dim-1] = 1;
    for (unsigned level = 0; level < 5; ++level) {
      std::size_t expectedObstacles = (1u << level) + 1u; expectedObstacles *= expectedObstacles;
      std::stringstream name;
      name << "NormalProjection + ContactMerge, level " << level;
      pass &= testTwoCubes(name.str(), projection, gridGlueBackend, offset, expectedObstacles, level);
    }
  }

  {
    auto projection = std::make_shared<Dune::Contact::NormalProjection<LeafBoundaryPatch> >();
    auto gridGlueBackend = std::make_shared<Dune::GridGlue::ContactMerge<dim, double> >(0);
    Vector offset(0); offset[0] = 0.75; offset[dim-1] = 1;
    {
      unsigned level = 2;
      std::size_t expectedObstacles = 10;
      pass &= testTwoCubes("NormalProjection + ContactMerge, shifted in x-direction", projection, gridGlueBackend, offset, expectedObstacles, level);
    }
  }

  {
    auto projection = std::make_shared<Dune::Contact::NormalProjection<LeafBoundaryPatch> >();
    auto gridGlueBackend = std::make_shared<Dune::GridGlue::ContactMerge<dim, double> >(0);
    Vector offset(0); offset[1] = 0.75; offset[dim-1] = 1;
    {
      unsigned level = 2;
      std::size_t expectedObstacles = 10;
      pass &= testTwoCubes("NormalProjection + ContactMerge, shifted in y-direction", projection, gridGlueBackend, offset, expectedObstacles, level);
    }
  }

  return pass ? 0 : 1;
}
