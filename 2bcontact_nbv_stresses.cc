#include <config.h>

#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include "dune/contact/contact2b.hh"

// from the 'preprocessing' module (dirtily hard-wired)
#include "../preprocessing/src/myamiramesh.hh"
#include "../preprocessing/src/affinetransform.hh"

// The data type used for the solution values
void setShiftParameter(Dune::ConfigParser &parameterSet, double shift) {
	std::stringstream shiftString;
	shiftString << shift;
	parameterSet["shiftScale"] = shiftString.str();
	return;
}

void assignShift(Dune::ConfigParser &parameterSet, double shift, double shiftRatio, const std::string &filenameSuffix) {
	// read some parameters
	const std::string path       = parameterSet.get<std::string>("path");
	const std::string resultPath = parameterSet.get("resultPath", "./");

	const std::string gridFile1        = parameterSet.sub("tibia").get<std::string>("gridFile");
	const std::string parFile1         = parameterSet.sub("tibia").get<std::string>("parFile");
	const std::string shiftValuesFile1 = parameterSet.sub("tibia").get<std::string>("shiftFile");

	std::string originalGridFile1, originalParFile1;

	if (!parameterSet.sub("tibia").hasKey("originalGridFile")) {
		originalGridFile1 = gridFile1;
		parameterSet.sub("tibia")["originalGridFile"] = gridFile1;
	}
	else
		originalGridFile1 = parameterSet.sub("tibia").get<std::string>("originalGridFile");
	if (!parameterSet.sub("tibia").hasKey("originalParFile")) {
		originalParFile1 = parFile1;
		parameterSet.sub("tibia")["originalParFile"] = originalParFile1;
	}
	else
		originalParFile1 = parameterSet.sub("tibia").get<std::string>("originalParFile");

	AmiraMeshFile *am_dirichlet = new AmiraMeshFile;
	am_dirichlet->loadValues(path + shiftValuesFile1, 3);
	Dune::FieldVector<double, 3> translation = am_dirichlet->getMaxNormEntry<3>();
	translation *= shiftRatio * shift;
	AffineTransform::translateGrid(path + originalGridFile1, resultPath + "shifted.grid" + filenameSuffix, translation);
	AffineTransform::translatePar(path + originalParFile1, resultPath + "shifted.par" + filenameSuffix, translation);
	setShiftParameter(parameterSet, (1 - shiftRatio) * shift);
}

int main (int argc, char *argv[]) try {

    // parse data file
    ParameterTree parameterSet;
    if (argc==2)
        ParameterTreeParser::readINITree(argv[1], parameterSet);
    else
        ParameterTreeParser::readINITree("parsets/2bcontact_nbv_stresses.parset", parameterSet);

	typedef Dune::UGGrid<3> GridType;

	Dune::ConfigParser parameterSet;
	if (argc==2)
		parameterSet.parseFile(argv[1]);
	else
		parameterSet.parseFile("parsets/2bcontact_nbv_stresses.parset");

	// Load settings
	const double shiftScale = parameterSet.get<double>("shiftScale");
	const double shiftRatio = parameterSet.get("shiftRatio", 0);

	// shift Grid and change dirichlet values if necessary
	assignShift(parameterSet, shiftScale, shiftRatio, "");

	KneeResultSet<GridType> resultSet;
	Contact2b<GridType>::contact(parameterSet, resultSet);

} catch (Dune::Exception e) {

    std::cout << e << std::endl;

}
