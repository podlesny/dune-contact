#include <config.h>

#include <algorithm>
#include <functional>

#include <dune/common/bitsetvector.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/uggrid.hh>
#ifdef HAVE_AMIRAMESH
#  include <dune/grid/io/file/amirameshwriter.hh>
#  include <dune/grid/io/file/amirameshreader.hh>
#else
#  include <dune/common/function.hh>
#  include <dune/fufem/functiontools/basisinterpolator.hh>
#  include <dune/grid/io/file/gmshreader.hh>
#  include <dune/grid/io/file/vtk/vtksequencewriter.hh>
#endif

#include <dune/istl/solvers.hh>
#include <dune/istl/io.hh>

#include <dune/fufem/functions/vtkbasisgridfunction.hh>
#include <dune/fufem/sampleonbitfield.hh>
#include <dune/fufem/prolongboundarypatch.hh>
#include <dune/fufem/readbitfield.hh>

#ifdef HAVE_IPOPT
#include <dune/solvers/solvers/quadraticipopt.hh>
#endif

#include <dune/solvers/iterationsteps/projectedblockgsstep.hh>
#include <dune/solvers/solvers/iterativesolver.hh>

#include <dune/solvers/norms/energynorm.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/improvegrid.hh>
#include <dune/fufem/differencenormsquared.hh>
#include <dune/fufem/estimators/refinementindicator.hh>
#include <dune/fufem/estimators/fractionalmarking.hh>
#include <dune/fufem/assemblers/localassemblers/stvenantkirchhoffassembler.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/fufem/functionspacebases/p2nodalbasis.hh>
#include <dune/fufem/functiontools/gridfunctionadaptor.hh>
#include <dune/fufem/functions/basisgridfunction.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include <dune/contact/estimators/hierarchiccontactestimator.hh>
#include <dune/contact/solvers/nsnewtonmgstep.hh>
#include <dune/contact/solvers/contacttransfer.hh>
#include <dune/contact/solvers/nsnewtoncontacttransfer.hh>
#include <dune/contact/assemblers/twobodyassembler.hh>

#include <dune/fufem/makehalfcircle.hh>
#include <dune/fufem/makesphere.hh>

using namespace Dune;
using namespace Dune::Contact;

// The grid dimension
#ifndef ADAPTMEASURE_DIM
#  define ADAPTMEASURE_DIM 3
#endif
const int dim = ADAPTMEASURE_DIM;

// Some types that I need
typedef FieldMatrix<double,dim,dim> BlockType;
typedef BCRSMatrix<FieldMatrix<double, dim, dim> > MatrixType;
typedef BlockVector<FieldVector<double, dim> >     VectorType;
typedef UGGrid<dim> GridType;
typedef BoundaryPatch<GridType::LevelGridView> LevelBoundary;
typedef BoundaryPatch<GridType::LeafGridView> LeafBoundary;
typedef DuneFunctionsBasis<Dune::Functions::LagrangeBasis<GridType::LeafGridView, 1> > P1Basis;
typedef P2NodalBasis<GridType::LeafGridView> P2Basis;

const double E  = 17e6;
const double nu = 0.3;

template <int numGrids>
void compute (const std::array<std::shared_ptr<GridType>, numGrids>& grid,
              std::array<VectorType,numGrids>& x,
              const std::array<VectorType,numGrids>& coarseDirichletValues,
              const std::array<BitSetVector<1>,numGrids>& coarseDirichletNodes,
              std::shared_ptr<const LevelBoundary> obsPatch,
              double obsDistance,
              double tolerance,
              int numIt)
{
    std::array<VectorType, numGrids> dirichletValues;
    std::array<BitSetVector<dim>, numGrids> dirichletNodes;
    std::array<LeafBoundary, numGrids> dirichletBoundary;

    std::array<VectorType, numGrids> rhs;

    std::size_t totalNumberOfVertices = 0;

    for (int i=0; i<numGrids; i++) {
        totalNumberOfVertices += grid[i]->size(dim);

        dirichletValues[i].resize(grid[i]->size(dim));


        LevelBoundary coarseDirichlet(grid[i]->levelGridView(0), coarseDirichletNodes[i]);

        // Initial solution
#warning Do not start from zero at every new refinement level!
        x[i].resize(grid[i]->size(dim));
        x[i] = 0;

        BoundaryPatchProlongator<GridType>::prolong(coarseDirichlet,dirichletBoundary[i]);


        dirichletNodes[i].resize(grid[i]->size(dim));
        for (int k=0; k<grid[i]->size(dim); k++)
            dirichletNodes[i][k] = dirichletBoundary[i].containsVertex(k);

        sampleOnBitField(*grid[i], coarseDirichletValues[i], dirichletValues[i], dirichletNodes[i]);

        //    Create rhs vectors

        rhs[i].resize(grid[i]->size(dim));
        rhs[i] = 0;

        // Copy Dirichlet information into the solution vector
        for (size_t j=0; j<rhs[i].size(); j++)
            for (int k=0; k<dim; k++) {
                if (dirichletNodes[i][j][k])
                    x[i][j][k] = dirichletValues[i][j][k];

            }

    }

    int toplevel = std::max(grid[0]->maxLevel(), grid[1]->maxLevel());


    // make dirichlet bitfields containing dirichlet information for both grids
    BitSetVector<dim> totalDirichletNodes(totalNumberOfVertices);
    {
        using std::copy;
        auto pos = totalDirichletNodes.begin();
        for (const auto& d : dirichletNodes)
            pos = std::copy(d.begin(), d.end(), pos);
    }

    // Assemble separate linear elasticity problems
    std::array<MatrixType, numGrids> submat;
    std::array<const MatrixType*, numGrids> submatptr;
    StVenantKirchhoffAssembler<GridType, typename P1Basis::LocalFiniteElement, typename P1Basis::LocalFiniteElement> localAssembler(E, nu);

    for (std::size_t i = 0; i < grid.size(); ++i) {
        P1Basis p1Basis(grid[i]->leafGridView());
        OperatorAssembler<P1Basis,P1Basis> globalAssembler(p1Basis, p1Basis);
        globalAssembler.assemble(localAssembler, submat[i]);
        submatptr[i] = &submat[i];
    }

    // mortar patch is the whole boundary
    auto mortarPatch = std::make_shared<LevelBoundary>(grid[1]->levelGridView(0),true);

    // Assemble contact problem
    TwoBodyAssembler<GridType, VectorType> contactAssembler;
    auto projection = std::make_shared< NormalProjection<LeafBoundary> >();

    contactAssembler.setup(*grid[0], *grid[1],
                           obsPatch, mortarPatch,
                           obsDistance,
                           CouplingPairBase::CONTACT, projection);

    MatrixType bilinearForm;
    contactAssembler.assembleJacobian(submatptr, bilinearForm);

    VectorType canonicalTotalX, totalX, totalRhs;

    // ////////////////////////////////////////////////////////////////////////////
    //   Turn the initial solutions from the nodal basis to the transformed basis
    // ////////////////////////////////////////////////////////////////////////////
    contactAssembler.nodalToTransformed(x, totalX);

    contactAssembler.concatenateVectors(rhs, totalRhs);

    // ////////////////////////////////////////////////
    //   Create a solver
    // ////////////////////////////////////////////////

    // First create a base solver
#ifdef HAVE_IPOPT
    QuadraticIPOptSolver<MatrixType,VectorType> baseSolver(1e-8, 100, NumProc::QUIET);
#endif

    // Make pre and postsmoothers
    ProjectedBlockGSStep<MatrixType, VectorType> presmoother;
    ProjectedBlockGSStep<MatrixType, VectorType> postsmoother;

    NonSmoothNewtonMGStep<MatrixType, VectorType> multigridStep(bilinearForm, totalX, totalRhs);

    multigridStep.setMGType(1, 3, 3);  // A V(3,3) cycle
    multigridStep.setIgnore(totalDirichletNodes);
    multigridStep.setBaseSolver(baseSolver);
    multigridStep.setSmoother(&presmoother,&postsmoother);
    multigridStep.setHasObstacle(contactAssembler.totalHasObstacle_);
    multigridStep.setObstacles(contactAssembler.totalObstacles_);
    multigridStep.setVerbosity(NumProc::QUIET);

    // //////////////////////////////////////////////
    //   Create the transfer operators
    // //////////////////////////////////////////////

    std::vector<NonSmoothNewtonContactTransfer<VectorType> > mgTransfers(toplevel);
    for (size_t i=0; i<mgTransfers.size(); i++) {
        mgTransfers[toplevel-i-1].setup(*grid[0], *grid[1], i,
                             contactAssembler.contactCoupling_[0]->mortarLagrangeMatrix(),
                             contactAssembler.localCoordSystems_,
                             *contactAssembler.contactCoupling_[0]->nonmortarBoundary().getVertices());
    }
    multigridStep.setTransferOperators(mgTransfers);

    EnergyNorm<MatrixType, VectorType> energyNorm(multigridStep);

    ::LoopSolver<VectorType> solver(multigridStep,
                                       // IPOpt doesn't like to be started at the solution, so make sure we really
                                       // call it only once when it is the only solver
                                       (toplevel==0) ? 1 : numIt,
                                       tolerance,
                                       energyNorm,
                                       Solver::FULL);

    // /////////////////////////////
    //    Solve !
    // /////////////////////////////
    solver.preprocess();

    // ///////////////////////////
    //   Compute solution
    // ///////////////////////////
    solver.solve();

    totalX = multigridStep.getSol();

    contactAssembler.postprocess(totalX, x);

}

#ifndef HAVE_AMIRAMESH

template<typename Domain, typename Range>
class FunctionAdapter
  : public Dune::VirtualFunction<Domain, Range>
{
public:
  FunctionAdapter(const std::function<Range(Domain)>& f)
    : m_f(f)
    { /* Nothing */ }
  void evaluate(const Domain& x, Range& y) const override
    { y = m_f(x); }
private:
  const std::function<Range(Domain)> m_f;
};

template<typename Domain, typename Range>
FunctionAdapter<Domain, Range>
toDuneFunction(const std::function<Range(Domain)>& f)
{
  return FunctionAdapter<Domain, Range>(f);
}

template<typename GridView, typename F>
BitSetVector<1>
evaluateAtNodes(const GridView& gv, const F& f)
{
    const auto& indexSet = gv.indexSet();
    BitSetVector<1> result(gv.size(GridView::dimension));
    for (const auto& v : vertices(gv)) {
        const auto x = v.geometry().center();
        result[indexSet.index(v)] = f(x);
    }
    return result;
}

template<typename Patch, typename F>
void
evaluateBoundaryPatch(Patch& patch, const F& f)
{
    const auto gv = patch.gridView();
    const auto vertices = evaluateAtNodes(gv, f);
    patch.setup(gv, vertices);
}

#endif

int main(int argc, char *argv[]) try
{
    Dune::MPIHelper::instance(argc, argv);

    // parse data file
    ParameterTree parameterSet;
    if (argc==2)
        ParameterTreeParser::readINITree(argv[1], parameterSet);
    else
        ParameterTreeParser::readINITree("adaptmeasure.parset", parameterSet);

    // read solver settings
    const int maxLevel         = parameterSet.get("maxLevel", int(0));
    const int numIt            = parameterSet.get("numIt", int(0));
    const double tolerance     = parameterSet.get("tolerance", double(0));
    const double refinementFraction = parameterSet.get("refinementFraction", double(1));
    const bool paramBoundaries = parameterSet.get("paramBoundaries", int(0));

    // read problem settings
    std::string path                 = parameterSet.get("path", "xyz");
    std::string object0Name          = parameterSet.get("gridFile0", "xyz");
    std::string object1Name          = parameterSet.get("gridFile1", "xyz");
    std::string parFile0             = parameterSet.get("parFile0", "xyz");
    std::string parFile1             = parameterSet.get("parFile1", "xyz");
    std::string dirichletFile0       = parameterSet.get("dirichletFile0", "xyz");
    std::string dirichletFile1       = parameterSet.get("dirichletFile1", "xyz");
    std::string dirichletValuesFile0 = parameterSet.get("dirichletValuesFile0", "xyz");
    std::string dirichletValuesFile1 = parameterSet.get("dirichletValuesFile1", "xyz");
    std::string obsFilename          = parameterSet.get("obsFilename", "xyz");
    double obsDistance               = parameterSet.get("obsDistance", double(0));
    double scaling                   = parameterSet.get("scaling", double(1));

    // ///////////////////////////////////////
    //    Create the two grids
    // ///////////////////////////////////////

    std::array<std::shared_ptr<GridType>, 2> uniformGrid;

#ifdef HAVE_AMIRAMESH
    if (paramBoundaries) {
        uniformGrid[0] = std::move(AmiraMeshReader<GridType>::read(path + object0Name, PSurfaceBoundary<dim-1>::read(path + parFile0)));
        uniformGrid[1] = std::move(AmiraMeshReader<GridType>::read(path + object1Name, PSurfaceBoundary<dim-1>::read(path + parFile1)));
    } else {
        uniformGrid[0] = std::move(AmiraMeshReader<GridType>::read(path + object0Name));
        uniformGrid[1] = std::move(AmiraMeshReader<GridType>::read(path + object1Name));
        //makeHalfCircleThreeTris(uniformGrid[0]);
        //makeSphere(uniformGrid[0], FieldVector<double,3>(0), 1);
    }
#else
    {
        using Reader = Dune::GmshReader<GridType>;
        uniformGrid[0] = Reader::read(path + object0Name);
        uniformGrid[1] = Reader::read(path + object1Name);
    }
#endif

    for (auto& grid : uniformGrid)
      grid->setRefinementType(GridType::COPY);

    // /////////////////////////////////////////////////////
    //   Read coarse grid Dirichlet information
    // /////////////////////////////////////////////////////
    std::array<VectorType, 2> dirichletValues;
    dirichletValues[0].resize(uniformGrid[0]->size(0, dim));
    dirichletValues[1].resize(uniformGrid[1]->size(0, dim));
#if HAVE_AMIRAMESH
    AmiraMeshReader<GridType>::readFunction(dirichletValues[0], path + dirichletValuesFile0);
    AmiraMeshReader<GridType>::readFunction(dirichletValues[1], path + dirichletValuesFile1);
#else
    {
        for (std::size_t i = 0; i < uniformGrid.size(); ++i) {
            using Vector = Dune::FieldVector<double, dim>;
            using DirichletFunction = std::function<Vector(Vector)>;

            // Dirichlet function
            DirichletFunction dirichletFunction = [&](Vector x) -> Vector {
                Vector y(0);
                y[dim-1] = -0.05 * x[dim-1];
                return y;
            };

            P1Basis p1Basis(uniformGrid[i]->leafGridView());
            ::Functions::interpolate(p1Basis, dirichletValues[i], toDuneFunction(dirichletFunction));
        }
    }
#endif

    // Scale Dirichlet values
    for (auto& v : dirichletValues)
      v *= scaling;

    std::array<BitSetVector<1>, 2> coarseDirichletNodes;
#if HAVE_AMIRAMESH
    readBitField(coarseDirichletNodes[0], uniformGrid[0]->size(0, dim), path + dirichletFile0);
    readBitField(coarseDirichletNodes[1], uniformGrid[1]->size(0, dim), path + dirichletFile1);
#else
    for (std::size_t i = 0; i < uniformGrid.size(); ++i) {
        using Vector = Dune::FieldVector<double, dim>;

        // Indicator function for Dirichlet boundaries
        auto dirichletBoundary = [&](Vector x) -> bool {
            using namespace std;
            return abs(x[dim-1]) > 0.99;
        };

        const auto gv = uniformGrid[i]->leafGridView();
        coarseDirichletNodes[i] = evaluateAtNodes(gv, dirichletBoundary);
    }
#endif

    // /////////////////////////////////////////////////////
    // read field describing the obstacles
    // /////////////////////////////////////////////////////

    auto obsPatch = std::make_shared<LevelBoundary>(uniformGrid[0]->levelGridView(0));
#ifdef HAVE_AMIRAMESH
    readBoundaryPatch<GridType>(*obsPatch, path + obsFilename);
#else
    auto obstacleBoundary = [](Dune::FieldVector<double, dim> x) -> bool {
        using std::abs;
        return abs(x[dim-1]) < 0.2;
    };
    evaluateBoundaryPatch(*obsPatch, obstacleBoundary);
#endif

    // /////////////////////////////////////////////////////
    // refine uniformly until minlevel
    // /////////////////////////////////////////////////////
    for (int i=0; i<maxLevel; i++) {
        for (size_t j=0; j < uniformGrid.size(); j++) {
            uniformGrid[j]->globalRefine(1);

            if (paramBoundaries)
                improveGrid(*uniformGrid[j], 10);
        }
    }

    // /////////////////////////////////////////////////////////
    //   Solver the problem on the uniformly refined grids
    // /////////////////////////////////////////////////////////
    std::array<VectorType, 2> uniformX;
    compute<2>(uniformGrid, uniformX, dirichletValues, coarseDirichletNodes,
            obsPatch, obsDistance, tolerance, numIt);
#ifndef HAVE_AMIRAMESH
    {
        using Writer = Dune::VTKWriter<GridType::LeafGridView>;
        using VTKDisplacement = VTKBasisGridFunction<P1Basis, VectorType>;

        P1Basis basis0(uniformGrid[0]->leafGridView());
        auto disp0 = std::make_shared<VTKDisplacement>(basis0, uniformX[0], "u");
        Writer writer0(uniformGrid[0]->leafGridView());
        writer0.addVertexData(disp0);
        writer0.write("adaptmeasure-0-uniform");

        P1Basis basis1(uniformGrid[1]->leafGridView());
        auto disp1 = std::make_shared<VTKDisplacement>(basis1, uniformX[1], "u");
        Writer writer1(uniformGrid[1]->leafGridView());
        writer1.addVertexData(disp1);
        writer1.write("adaptmeasure-1-uniform");
    }
#endif

    // /////////////////////////////////////////////////////////
    //   Load coarse grids again
    // /////////////////////////////////////////////////////////

    std::array<std::shared_ptr<GridType>, 2> adaptiveGrid;

#ifdef HAVE_AMIRAMESH
    if (paramBoundaries) {

        adaptiveGrid[0] = std::move(AmiraMeshReader<GridType>::read(path + object0Name, PSurfaceBoundary<dim-1>::read(path + parFile0)));
        adaptiveGrid[1] = std::move(AmiraMeshReader<GridType>::read(path + object1Name, PSurfaceBoundary<dim-1>::read(path + parFile1)));
    } else {
        adaptiveGrid[0] = AmiraMeshReader<GridType>::read(path + object0Name);
        adaptiveGrid[1] = AmiraMeshReader<GridType>::read(path + object1Name);
    }
#else
    {
        using Reader = Dune::GmshReader<GridType>;
        adaptiveGrid[0] = Reader::read(path + object0Name);
        adaptiveGrid[1] = Reader::read(path + object1Name);
    }
#endif

    for (auto& grid : adaptiveGrid)
      grid->setRefinementType(GridType::COPY);

    // /////////////////////////////////////////////////////
    // read field describing the obstacles again, so this time
    // it is attached to the adaptive grid.
    // /////////////////////////////////////////////////////

    obsPatch->setup(adaptiveGrid[0]->levelGridView(0));
#ifdef HAVE_AMIRAMESH
    readBoundaryPatch<GridType>(*obsPatch, path + obsFilename);
#else
    evaluateBoundaryPatch(*obsPatch, obstacleBoundary);
#endif

    // /////////////////////////////////////////////////////////
    //   Refinement loop
    // /////////////////////////////////////////////////////////

    std::ofstream logfile("adaptmeasure.results");

    std::array<VectorType,2> adaptiveX;

#ifndef HAVE_AMIRAMESH
    using VTKDisplacement = VTKBasisGridFunction<P1Basis, VectorType>;
    using SequenceWriter = Dune::VTKSequenceWriter<GridType::LeafGridView>;
    SequenceWriter sequenceWriter0(adaptiveGrid[0]->leafGridView(),
                                   "adaptmeasure-0", "", "");
    P1Basis basis0(adaptiveGrid[0]->leafGridView());
    auto disp0 = std::make_shared<VTKDisplacement>(basis0, adaptiveX[0], "u");
    sequenceWriter0.addVertexData(disp0);
    SequenceWriter sequenceWriter1(adaptiveGrid[1]->leafGridView(),
                                   "adaptmeasure-1", "", "");
    P1Basis basis1(adaptiveGrid[1]->leafGridView());
    auto disp1 = std::make_shared<VTKDisplacement>(basis1, adaptiveX[1], "u");
    sequenceWriter1.addVertexData(disp1);
#endif
    for (int i=0; i<maxLevel+1; i++) {

        // /////////////////////////////////////////////////////////////
        //   Solve problem on locally refined grid
        // /////////////////////////////////////////////////////////////
        compute<2>(adaptiveGrid, adaptiveX, dirichletValues, coarseDirichletNodes,
                obsPatch, obsDistance, tolerance, numIt);

        // /////////////////////////////////////////////////////////////
        //   Compute 'actual' error by comparing the the solution
        //   on the uniformly refined grid
        // /////////////////////////////////////////////////////////////
        StVenantKirchhoffAssembler<GridType, P1Basis::LocalFiniteElement,P1Basis::LocalFiniteElement> lstiff(E, nu);


        double actualError = 0;
        for (size_t j=0; j<uniformGrid.size(); j++)
            actualError += DifferenceNormSquared<GridType>::compute(*uniformGrid[j], uniformX[j],
                                                                    *adaptiveGrid[j], adaptiveX[j],
                                                                    &lstiff);

        actualError = std::sqrt(actualError);

        // /////////////////////////////////////////////////////////////
        //   Estimate the error with an a posteriori estimator
        // /////////////////////////////////////////////////////////////

        // ////////////////////////////////////////////////////
        //    Estimate error per element
        // ////////////////////////////////////////////////////
        HierarchicContactEstimator<P1Basis, P2Basis> estimator(*adaptiveGrid[0], *adaptiveGrid[1]);
        auto mortarPatch = std::make_shared<LevelBoundary>(adaptiveGrid[1]->levelGridView(0), true);

        estimator.setupContactCoupling(0, 0, 1, obsPatch, mortarPatch, obsDistance, CouplingPairBase::CONTACT);

        // make grid functions
        std::vector<std::shared_ptr<BasisGridFunction<P1Basis,VectorType> > >adaptiveXFunc(2);
        std::vector<std::shared_ptr<P1Basis> > p1Bases(2);
        for (size_t j=0; j < adaptiveGrid.size(); j++) {
            p1Bases[j] = std::make_shared<P1Basis>(adaptiveGrid[j]->leafGridView());
            adaptiveXFunc[j] = std::make_shared<BasisGridFunction<P1Basis,VectorType> >(*p1Bases[j],adaptiveX[j]);
        }

        std::vector<std::shared_ptr<LeafBoundary> > dirichletBoundary(2);
        for (size_t j=0; j < adaptiveGrid.size(); j++) {
            LevelBoundary coarse(adaptiveGrid[j]->levelGridView(0), coarseDirichletNodes[j]);
            dirichletBoundary[j] = std::make_shared<LeafBoundary>(adaptiveGrid[j]->leafGridView());
            BoundaryPatchProlongator<GridType>::prolong(coarse, *dirichletBoundary[j]);
        }

        std::vector<std::shared_ptr<RefinementIndicator<GridType> > > refinementIndicator(adaptiveGrid.size());
        std::transform(adaptiveGrid.begin(), adaptiveGrid.end(), refinementIndicator.begin(),
                       [](auto grid) { return std::make_shared<RefinementIndicator<GridType> >(*grid); });

        std::vector<std::shared_ptr<P2Basis> > p2Bases(2);
        std::transform(adaptiveGrid.begin(), adaptiveGrid.end(), p2Bases.begin(),
                       [](auto grid) { return std::make_shared<P2Basis>(grid->leafGridView()); });

        using P2Lfe = P2Basis::LocalFiniteElement;
        using BaseAssembler = LocalOperatorAssembler<GridType,P2Lfe, P2Lfe, BlockType>;
        using P2StVenantAssembler = StVenantKirchhoffAssembler<GridType, P2Lfe, P2Lfe>;
        std::vector<std::shared_ptr<BaseAssembler> > lstiffs(2);
        for (size_t i=0; i<lstiffs.size(); i++)
            lstiffs[i] = std::make_shared<P2StVenantAssembler>(E, nu);

        estimator.estimate(adaptiveXFunc,
                           dirichletBoundary,
                           refinementIndicator,
                           p2Bases, lstiffs);

       // ////////////////////////////////////////////////////////////
       //   Estimate estimator efficiency
       // ////////////////////////////////////////////////////////////

       double estimatedError = std::sqrt(estimator.getErrorSquared(0, lstiffs[0]) + estimator.getErrorSquared(1, lstiffs[1]));

       std::cout << "Actual error: " << actualError << "     "
                 << "estimated error: " << estimatedError << "     "
                 << "estimator efficiency: " << estimatedError/actualError << "   "
                 << "vertices: " << adaptiveGrid[0]->size(dim) + adaptiveGrid[1]->size(dim)
                 << std::endl;

       logfile << actualError << "     " << estimatedError
               << "     " << estimatedError/actualError
               << "     " << adaptiveGrid[0]->size(dim) + adaptiveGrid[1]->size(dim) << std::endl;

       if (i==maxLevel)
           break;

        // ////////////////////////////////////////////////////////
        //   Refine the grids and transfer the current solution
        // ////////////////////////////////////////////////////////

       std::vector<GridType*> adaptiveGridVector(2);
       adaptiveGridVector[0] = adaptiveGrid[0].get();
       adaptiveGridVector[1] = adaptiveGrid[1].get();
       FractionalMarkingStrategy<GridType>::mark(refinementIndicator, adaptiveGridVector, refinementFraction);

       for (size_t j=0; j < adaptiveGrid.size(); j++) {

            P1Basis p1Basis(adaptiveGrid[j]->leafGridView());
            GridFunctionAdaptor<P1Basis> adaptor(p1Basis,true,true);

            adaptiveGrid[j]->preAdapt();
            adaptiveGrid[j]->adapt();
            adaptiveGrid[j]->postAdapt();

            p1Basis.update();
            adaptor.adapt(adaptiveX[j]);

            if (paramBoundaries)
                improveGrid(*adaptiveGrid[j],10);

        }

        std::cout << "########################################################" << std::endl;
        std::cout << "  Grids refined" << std::endl;
        std::cout << "  Grid: 0   Level: " << adaptiveGrid[0]->maxLevel()
                  << "   vertices: " << adaptiveGrid[0]->size(adaptiveGrid[0]->maxLevel(), dim)
                  << "   elements: " << adaptiveGrid[0]->size(adaptiveGrid[0]->maxLevel(), 0) << std::endl;
        std::cout << "  Grid: 1   Level: " << adaptiveGrid[1]->maxLevel()
                  << "   vertices: " << adaptiveGrid[1]->size(adaptiveGrid[1]->maxLevel(), dim)
                  << "   elements: " << adaptiveGrid[1]->size(adaptiveGrid[1]->maxLevel(), 0) << std::endl;
        std::cout << "########################################################" << std::endl;

        // Output result
#ifdef HAVE_AMIRAMESH
        LeafAmiraMeshWriter<GridType> amiramesh0;
        amiramesh0.addLeafGrid(*adaptiveGrid[0],true);
        amiramesh0.write("0resultGrid");

        LeafAmiraMeshWriter<GridType> amiramesh1;
        amiramesh1.addLeafGrid(*adaptiveGrid[1],true);
        amiramesh1.write("1resultGrid");
#else
        basis0.update(adaptiveGrid[0]->leafGridView());
        sequenceWriter0.write(static_cast<double>(i));
        basis1.update(adaptiveGrid[1]->leafGridView());
        sequenceWriter1.write(static_cast<double>(i));
#endif
    }

    // Output result
    {
#ifdef HAVE_AMIRAMESH
        LeafAmiraMeshWriter<GridType> amiramesh0;
        amiramesh0.addLeafGrid(*adaptiveGrid[0],true);
        amiramesh0.addVertexData(adaptiveX[0], adaptiveGrid[0]->leafGridView());
        amiramesh0.write("0resultGrid");

        LeafAmiraMeshWriter<GridType> amiramesh1;
        amiramesh1.addLeafGrid(*adaptiveGrid[1],true);
        amiramesh1.addVertexData(adaptiveX[1], adaptiveGrid[1]->leafGridView());
        amiramesh1.write("1resultGrid");
#else
        using Writer = Dune::VTKWriter<GridType::LeafGridView>;
        using VTKDisplacement = VTKBasisGridFunction<P1Basis, VectorType>;

        P1Basis basis0(adaptiveGrid[0]->leafGridView());
        auto disp0 = std::make_shared<VTKDisplacement>(basis0, adaptiveX[0], "u");
        Writer writer0(adaptiveGrid[0]->leafGridView());
        writer0.addVertexData(disp0);
        writer0.write("adaptmeasure-0-adaptive-final");

        P1Basis basis1(adaptiveGrid[1]->leafGridView());
        auto disp1 = std::make_shared<VTKDisplacement>(basis1, adaptiveX[1], "u");
        Writer writer1(adaptiveGrid[1]->leafGridView());
        writer1.addVertexData(disp1);
        writer1.write("adaptmeasure-1-adaptive-final");
#endif
    }

    if (dim==2) {

        for (std::size_t i=0; i < adaptiveGrid.size(); i++) {

            const auto& leafView = adaptiveGrid[i]->leafGridView();
            const auto& leafIndexSet = adaptiveGrid[i]->leafIndexSet();

            for (const auto& v : vertices(leafView))
                adaptiveGrid[i]->setPosition(v, v.geometry().corner(0)
                                            + adaptiveX[i][leafIndexSet.index(v)]);
        }

#ifdef HAVE_AMIRAMESH
        LeafAmiraMeshWriter<GridType>::writeGrid(*adaptiveGrid[0], "0resultGridDef");
        LeafAmiraMeshWriter<GridType>::writeGrid(*adaptiveGrid[1], "1resultGridDef");
#endif

    }

} catch (Exception e) {

    std::cout << e << std::endl;

 }
