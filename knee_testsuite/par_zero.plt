set title 'Knee with parametrization, starting from zero'
set xlabel 'Iteration'
set ylabel 'Error'
set logscale y

plot 'par_mmg_cont_zero/statistics' using 1:3 with lines title "Contact problem"                    , \
     'par_mmg_lin_zero/statistics'  using 1:3 with lines title "Linear problem, transformed basis"  , \
     'par_mmg_can_zero/statistics'  using 1:3 with lines title "Linear problem, nodal basis"        , \
     'par_gmg_cont_zero/statistics' using 1:3 with lines title "Graeser multigrid"      

set output "par_zero.eps"
set terminal postscript eps color
replot

set output "par_zero.fig"
set terminal fig color
replot