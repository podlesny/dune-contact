#!/bin/bash

set -e

runComputation(){

RESULTPATH=`pwd`/$1/
PARAMETERFILE=${RESULTPATH}${1}".parset"
LOGFILE=${RESULTPATH}${1}".log"

#echo $RESULTPATH

# Set up directory where to store the results
if ! test -d "$RESULTPATH"; then
    mkdir $RESULTPATH
fi
#rm $RESULTPATH/*

cat > "${PARAMETERFILE}" <<EOF
# Top level upon entering the refinement loop
minLevel = 0

# Number of grid levels
maxLevel = 3

# Number of multigrid iterations
numIt = 1000

# Number of presmoothing steps
nu1 = 3

# Number of postsmoothing steps
nu2 = 3

# Number of coarse grid corrections
mu = 1

# Number of base solver iterations
baseIt = 100

# Tolerance of the solver
tolerance = 1e-4

# Tolerance of the solver
measureTolerance = 1e-12

# Tolerance of the base grid solver
baseTolerance = 1e-8

# Fraction of elements marked for refinement
refinementFraction = 0.3

# Parametrized grid boundaries?
paramBoundaries = $2

# Compute a linear transformed problem?
linearProblem = $3

# Compute a linear problem in the nodal basis?
linearCanonicalProblem = $4

# Use the nonsmooth Newton multigrid instead of the monotone multigrid?
nonsmoothNewtonMG = $5

# Start from zero on the last level and not from the solution
# on the previous level.
startFromZero = $6

# Where to write the results
resultPath = $RESULTPATH

####################################################
#   Problem Specifications
####################################################

path = /home/haile/sander/data/contact/vishum_short_stump_15mm/
gridFile0            = femur.grid
gridFile1            = tibia.grid
parFile0             = femur.par
parFile1             = tibia.par
dirichletFile0       = femur.dn
dirichletFile1       = tibia.dn
dirichletValuesFile0 = femur.dv
dirichletValuesFile1 = tibia.dv
obsFilename          = femur.obs
obsDistance          = 10
scaling              = 1.5

EOF

# run simulation
../2bcontact_instr ${PARAMETERFILE} | tee ${LOGFILE}
}

SUFFIX="_parallel"

# Parameters:
# 2: Parametrized grid boundaries?
# 3: Compute a linear transformed problem?
# 4: Compute a linear problem in the nodal basis?
# 5: Use the nonsmooth Newton multigrid instead of the monotone multigrid?
# 6: Start from zero on finest level

# ----------------------------------------------
# without parametrization, starting from zero
# ----------------------------------------------

# contact problem
runComputation "nopar_mmg_cont_zero"${SUFFIX} 0 0 0 0 1

# linear problem in transformed basis
runComputation "nopar_mmg_lin_zero"${SUFFIX}  0 1 0 0 1

# linear problem in nodal basis
runComputation "nopar_mmg_can_zero"${SUFFIX}  0 0 1 0 1

# Nonsmooth Newton multigrid
runComputation "nopar_gmg_cont_zero"${SUFFIX} 0 0 0 1 1



# ----------------------------------------------
# with parametrization, 
# ----------------------------------------------

# contact problem
runComputation "par_mmg_cont_zero"${SUFFIX} 1 0 0 0 1

# linear problem in transformed basis
runComputation "par_mmg_lin_zero"${SUFFIX}  1 1 0 0 1

# linear problem in nodal basis
runComputation "par_mmg_can_zero"${SUFFIX}  1 0 1 0 1

# Nonsmooth Newton multigrid
runComputation "par_gmg_cont_zero"${SUFFIX} 1 0 0 1 1



#####################################################
##  Now we do the whole thing again, but we do not
##  start from zero
#####################################################


# Parameters:
# 2: Parametrized grid boundaries?
# 3: Compute a linear transformed problem?
# 4: Compute a linear problem in the nodal basis?
# 5: Use the nonsmooth Newton multigrid instead of the monotone multigrid?
# 6: Start from zero on finest level

# ----------------------------------------------
# without parametrization, 
# ----------------------------------------------

# contact problem
runComputation "nopar_mmg_cont"${SUFFIX} 0 0 0 0 0

# linear problem in transformed basis
runComputation "nopar_mmg_lin"${SUFFIX}  0 1 0 0 0

# linear problem in nodal basis
runComputation "nopar_mmg_can"${SUFFIX}  0 0 1 0 0

# nonsmooth Newton multigrid
runComputation "nopar_gmg_cont"${SUFFIX} 0 0 0 1 0



# ----------------------------------------------
# with parametrization, 
# ----------------------------------------------

# contact problem
runComputation "par_mmg_cont"${SUFFIX} 1 0 0 0 0

# linear problem in transformed basis
runComputation "par_mmg_lin"${SUFFIX}  1 1 0 0 0

# linear problem in nodal basis
runComputation "par_mmg_can"${SUFFIX}  1 0 1 0 0

# nonsmooth Newton multigrid
runComputation "par_gmg_cont"${SUFFIX} 1 0 0 1 0
