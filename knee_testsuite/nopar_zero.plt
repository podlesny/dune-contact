set title 'Knee without parametrization, starting from zero'
set xlabel 'Iteration'
set ylabel 'Error'
set logscale y

plot 'nopar_mmg_cont_zero/statistics' using 1:3 with lines title "Contact problem"                    , \
     'nopar_mmg_lin_zero/statistics'  using 1:3 with lines title "Linear problem, transformed basis"  , \
     'nopar_mmg_can_zero/statistics'  using 1:3 with lines title "Linear problem, nodal basis"        , \
     'nopar_gmg_cont_zero/statistics' using 1:3 with lines title "Gr�ser multigrid"      

set output "nopar_zero.eps"
set terminal postscript eps color
replot

set output "nopar_zero.fig"
set terminal fig color
replot