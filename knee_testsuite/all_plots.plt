set xlabel 'iterations'
set ylabel 'error'
set logscale y

##############################################################
##  WITHOUT parametrization
##############################################################

##############################################################################################
#  No parametrization, normal obstacles, starting from the coarser level
##############################################################################################

set output "conv_nopar_normal.eps"
set terminal postscript eps color "Arial,20" linewidth 6

plot [0:160][1e-10:1e6] \
     'nopar_gmg_cont_normal/statistics' using 1:3 with lines lt 1 lc 0 title "Nonsmooth Newton multigrid", \
     'nopar_mmg_cont_normal/statistics' using 1:3 with lines lt 2 lc 1 title "Monotone multigrid",         \
     'nopar_mmg_can_normal/statistics'  using 1:3 with lines lt 7 lc 3 title "Linear problem"

#     'nopar_mmg_lin_normal/statistics'  using 1:3 with lines title "Linear problem, transformed basis"  , \


##############################################################################################
#  No parametrization, normal obstacles, starting from zero
##############################################################################################

set output "conv_nopar_zero_normal.eps"
set terminal postscript eps color "Arial,20" linewidth 6

plot [0:160][1e-10:1e6] \
     'nopar_gmg_cont_zero_normal/statistics' using 1:3 with lines lt 1 lc 0 title "Nonsmooth Newton multigrid", \
     'nopar_mmg_cont_zero_normal/statistics' using 1:3 with lines lt 2 lc 1 title "Monotone multigrid",         \
     'nopar_mmg_can_zero_normal/statistics'  using 1:3 with lines lt 7 lc 3 title "Linear problem"

#     'nopar_mmg_lin_zero_normal/statistics'  using 1:3 with lines title "Linear problem, transformed basis"  , \



##############################################################################################
#  No parametrization, parallel obstacles, starting from the coarser level
##############################################################################################

set output "conv_nopar_parallel.eps"
set terminal postscript eps color "Arial,20" linewidth 6

plot [0:160][1e-10:1e6] \
     'nopar_gmg_cont_parallel/statistics' using 1:3 with lines lt 1 lc 0 title "Nonsmooth Newton multigrid", \
     'nopar_mmg_cont_parallel/statistics' using 1:3 with lines lt 2 lc 1 title "Monotone multigrid",         \
     'nopar_mmg_can_parallel/statistics'  using 1:3 with lines lt 7 lc 3 title "Linear problem"

#     'nopar_mmg_lin_parallel/statistics'  using 1:3 with lines title "Linear problem, transformed basis"  , \



##############################################################################################
#  No parametrization, parallel obstacles, starting from zero
##############################################################################################

set output "conv_nopar_zero_parallel.eps"
set terminal postscript eps color "Arial,20" linewidth 6

plot [0:160][1e-10:1e6] \
     'nopar_gmg_cont_zero_parallel/statistics' using 1:3 with lines lt 1 lc 0 title "Nonsmooth Newton multigrid", \
     'nopar_mmg_cont_zero_parallel/statistics' using 1:3 with lines lt 2 lc 1 title "Monotone multigrid",         \
     'nopar_mmg_can_zero_parallel/statistics'  using 1:3 with lines lt 7 lc 3 title "Linear problem"

#     'nopar_mmg_lin_zero_parallel/statistics'  using 1:3 with lines title "Linear problem, transformed basis"  , \



##############################################################
##  WITH parametrization
##############################################################

##############################################################################################
#  Parametrization, normal obstacles, starting from the coarser level
##############################################################################################

set output "conv_par_normal.eps"
set terminal postscript eps color "Arial,20" linewidth 6

plot [0:250][1e-8:1e6] \
     'par_gmg_cont_normal/statistics' using 1:3 with lines lt 1 lc 0 title "Nonsmooth Newton multigrid", \
     'par_mmg_cont_normal/statistics' using 1:3 with lines lt 2 lc 1 title "Monotone multigrid",         \
     'par_mmg_can_normal/statistics'  using 1:3 with lines lt 7 lc 3 title "Linear problem"

#     'par_mmg_lin_normal/statistics'  using 1:3 with lines title "Linear problem, transformed basis"  , \



##############################################################################################
#  Parametrization, normal obstacles, starting from zero
##############################################################################################

set output "conv_par_zero_normal.eps"
set terminal postscript eps color "Arial,20" linewidth 6

plot [0:250][1e-8:1e6] \
     'par_gmg_cont_zero_normal/statistics' using 1:3 with lines lt 1 lc 0 title "Nonsmooth Newton multigrid", \
     'par_mmg_cont_zero_normal/statistics' using 1:3 with lines lt 2 lc 1 title "Monotone multigrid",         \
     'par_mmg_can_zero_normal/statistics'  using 1:3 with lines lt 7 lc 3 title "Linear problem"

#     'par_mmg_lin_zero_normal/statistics'  using 1:3 with lines title "Linear problem, transformed basis"  , \



##############################################################################################
#  Parametrization, parallel obstacles, starting from the coarser level
##############################################################################################

set output "conv_par_parallel.eps"
set terminal postscript eps color "Arial,20" linewidth 6

plot [0:250][1e-8:1e6] \
     'par_gmg_cont_parallel/statistics' using 1:3 with lines lt 1 lc 0 title "Nonsmooth Newton multigrid", \
     'par_mmg_cont_parallel/statistics' using 1:3 with lines lt 2 lc 1 title "Monotone multigrid",         \
     'par_mmg_can_parallel/statistics'  using 1:3 with lines lt 7 lc 3 title "Linear problem"

#     'par_mmg_lin_parallel/statistics'  using 1:3 with lines title "Linear problem, transformed basis"  , \



##############################################################################################
#  Parametrization, parallel obstacles, starting from zero
##############################################################################################

set output "conv_par_zero_parallel.eps"
set terminal postscript eps color "Arial,20" linewidth 6

plot [0:250][1e-8:1e6] \
     'par_gmg_cont_zero_parallel/statistics' using 1:3 with lines lt 1 lc 0 title "Nonsmooth Newton multigrid", \
     'par_mmg_cont_zero_parallel/statistics' using 1:3 with lines lt 2 lc 1 title "Monotone multigrid",         \
     'par_mmg_can_zero_parallel/statistics'  using 1:3 with lines lt 7 lc 3 title "Linear problem"

##     'par_mmg_lin_zero_parallel/statistics'  using 1:3 with lines title "Linear problem, transformed basis"  , \




