// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#include <config.h>

#ifndef HAVE_IPOPT
#error IPOpt is not optional for this file
#endif

#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/uggrid.hh>

#include <dune/grid/io/file/amirameshwriter.hh>
#include <dune/grid/io/file/amirameshreader.hh>
#include <dune/grid/io/file/vtk.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/io.hh>

#include <dune/fufem/readbitfield.hh>
#include <dune/fufem/improvegrid.hh>
#include <dune/fufem/utilities/dirichletbcassembler.hh>
#include <dune/fufem/makehalfcircle.hh>
#include <dune/fufem/estimators/refinementindicator.hh>
#include <dune/fufem/estimators/fractionalmarking.hh>
#include <dune/fufem/assemblers/localassemblers/stvenantkirchhoffassembler.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/fufem/functiontools/gridfunctionadaptor.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/functions/vtkbasisgridfunction.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/iterationsteps/mmgstep.hh>
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/solvers/quadraticipopt.hh>
#include <dune/solvers/iterationsteps/projectedblockgsstep.hh>

#include <dune/contact/assemblers/onebodyassembler.hh>
#include <dune/contact/solvers/contacttransfer.hh>
#include <dune/contact/solvers/contacttransferoperatorassembler.hh>
#include <dune/contact/estimators/hierarchiccontactestimator.hh>
#include <dune/contact/solvers/contactobsrestrict.hh>

// The grid dimension
const int dim = 2;

using namespace Dune;
using namespace Dune::Contact;

int main (int argc, char *argv[]) try
{
    // Some types that I need
    typedef double ctype;
    typedef FieldVector<ctype,dim> FVector;
    typedef BCRSMatrix<FieldMatrix<ctype, dim, dim> > MatrixType;
    typedef BlockVector<FieldVector<ctype,dim> >      VectorType;

    // parse data file
    ParameterTree parameterSet;
    if (argc==2)
        ParameterTreeParser::readINITree(argv[1], parameterSet);
    else
        ParameterTreeParser::readINITree("1bcontact.parset", parameterSet);

    // read solver settings
    const int minLevel         = parameterSet.get("minLevel", int(0));
    const int maxLevel         = parameterSet.get("maxLevel", int(0));
    const int numIt            = parameterSet.get("numIt", int(0));
    const int nu1              = parameterSet.get("nu1", int(0));
    const int nu2              = parameterSet.get("nu2", int(0));
    const int mu               = parameterSet.get("mu", int(0));
    const ctype tolerance     = parameterSet.get("tolerance", ctype(0));
    const ctype baseTolerance = parameterSet.get("baseTolerance", ctype(0));
    const ctype refinementFraction = parameterSet.get("refinementFraction", ctype(1));
    const bool paramBoundaries = parameterSet.get("paramBoundaries", int(0));

    // read problem settings
    std::string path                = parameterSet.get<std::string>("path");
    std::string resultPath          = parameterSet.get<std::string>("resultPath");
    std::string gridFile            = parameterSet.get("gridFile","xyz");
    std::string dirichletFile       = parameterSet.get<std::string>("dirichletFile");
    std::string dirichletValuesFile = parameterSet.get<std::string>("dirichletValuesFile");
    std::string obsFilename         = parameterSet.get<std::string>("obsFilename");
    ctype obsDistance               = parameterSet.get("obsDistance", ctype(0));
    const ctype E                   = parameterSet.get("E",ctype(17e6));
    const ctype nu                  = parameterSet.get("nu",ctype(0.3));

    // /////////////////////////////
    //   Generate the grid
    // /////////////////////////////

    using GridType = UGGrid<dim>;
    using DuneP1Basis = Dune::Functions::LagrangeBasis<typename GridType::LeafGridView, 1>;
    using P1Basis = DuneFunctionsBasis<DuneP1Basis>;
    using LevelBoundaryPatch = BoundaryPatch<GridType::LevelGridView>;
    using LeafBoundaryPatch = BoundaryPatch<GridType::LeafGridView>;
    std::unique_ptr<GridType> grid;

    if (paramBoundaries) {

#if HAVE_AMIRAMESH
        std::string parFile = parameterSet.get<std::string>("parFile");
        grid = AmiraMeshReader<GridType>::read(path + gridFile, PSurfaceBoundary<dim-1>::read(path + parFile));
#else
#error Parameterized boundaries only work with AmiraMesh
#endif
    } else {
        //AmiraMeshReader<GridType>::read(*grid, path + gridFile);
        FieldVector<ctype,2> center(0);
        center[1] = 15;

        grid = createCircle<GridType>(center,15);
    }

    grid->setRefinementType(GridType::COPY);

    // Read Dirichlet boundary values
    VectorType dirichletValues(grid->size(0, dim));
    AmiraMeshReader<GridType>::readFunction(dirichletValues, path + dirichletValuesFile);
    dirichletValues *= parameterSet.get("scaling", ctype(1));

    BitSetVector<dim> coarseDirichletNodes;
    readBitField(coarseDirichletNodes,grid->size(0,dim), path + dirichletFile);

    // refine uniformly until minlevel
    for (int i=0; i<minLevel; i++) {
        grid->globalRefine(1);

        if (paramBoundaries)
            improveGrid(*grid, 10);

    }

    // //////////////////////////////////////////////////
    //   Read field describing the obstacles
    // //////////////////////////////////////////////////

    auto obsPatch = std::make_shared<LevelBoundaryPatch>(grid->levelGridView(0));
    readBoundaryPatch<GridType>(*obsPatch, path + obsFilename);

    VectorType rhs(grid->size(dim));
    VectorType x(grid->size(dim));

    // Initial solution
    x = 0;

    // //////////////////////////////////////////////////
    //   The refinement loop
    // //////////////////////////////////////////////////

    while (true) {

        int toplevel = grid->maxLevel();

        // ////////////////////////////////////////////////////////////
        //   Prolong Dirichlet information to all levels
        // ////////////////////////////////////////////////////////////

        BitSetVector<dim> dirichletNodes(grid->size(dim));
        VectorType leafDirichletValues;

        DirichletBCAssembler<GridType>::assembleDirichletBC(*grid,coarseDirichletNodes,
                                                            dirichletValues,
                                                            dirichletNodes,leafDirichletValues);

        BitSetVector<1> leafNodes;
        DirichletBCAssembler<GridType>::inflateBitField(dirichletNodes,leafNodes);
        LeafBoundaryPatch leafDirichletBoundary(grid->leafGridView(),leafNodes);

        // ////////////////////////////////////////////////////////////
        //    Create rhs vector
        // ////////////////////////////////////////////////////////////

        rhs.resize(grid->size(dim));
        rhs = 0;

        for (size_t i=0; i<rhs.size(); i++)
            for (int j=0; j<dim; j++) {
                if (dirichletNodes[i][j])
                    x[i][j] = leafDirichletValues[i][j];
            }

        // /////////////////////////////////////////////////
        //      Assemble linear elasticity (bi-)linearforms
        // ///////////////////////////////////////////////////

        P1Basis p1Basis(grid->leafGridView());

        OperatorAssembler<P1Basis,P1Basis> globalAssembler(p1Basis,p1Basis);

	    StVenantKirchhoffAssembler<GridType, P1Basis::LocalFiniteElement, P1Basis::LocalFiniteElement> localStiffness(E, nu);

	    MatrixType stiffnessMatrix;
	    globalAssembler.assemble(localStiffness, stiffnessMatrix);

        // //////////////////////////////////////
        // Assemble contact problem
        // //////////////////////////////////////

        // Analytic obstacle given by the x-axis
        std::function<ctype(const FVector&, const FVector&)> obs = [] (const FVector& x, const FVector& dir)
        {
            return (std::fabs(dir[1])>1e-10) ? (-x[1]/dir[1]) : std::numeric_limits<ctype>::max();
        };

        OneBodyAssembler<GridType,ctype> contactAssembler(*grid, obs, *obsPatch);

        contactAssembler.assembleObstacles();
        contactAssembler.transformMatrix(stiffnessMatrix);

        // ////////////////////////////////////////////////////////////
        //   Compute bitvector of dofs with obstacles on the leaf grid
        // ////////////////////////////////////////////////////////////

        BitSetVector<dim> obsNodes(grid->size(dim));

        const auto& leafObsPatch = contactAssembler.obsPatch_;
        for (size_t i=0; i<obsNodes.size();i++)
            obsNodes[i][0]=leafObsPatch.containsVertex(i);


        // ////////////////////////////
        //   Create a solver
        // ////////////////////////////

        // First create a base solver
        QuadraticIPOptSolver<MatrixType,VectorType> baseSolver(baseTolerance, 100, NumProc::QUIET);

        // Make pre and postsmoothers
        ProjectedBlockGSStep<MatrixType, VectorType> presmoother, postsmoother;

        MonotoneMGStep<MatrixType, VectorType> multigridStep(stiffnessMatrix, x, rhs);

        multigridStep.setMGType(mu, nu1, nu2);
        multigridStep.setIgnore(dirichletNodes);
        multigridStep.setBaseSolver(baseSolver);
        multigridStep.setSmoother(presmoother, postsmoother);
        multigridStep.setHasObstacles(obsNodes);
        multigridStep.setObstacles(contactAssembler.getObstacles());
        multigridStep.setObstacleRestrictor(ContactObsRestriction<VectorType>());

        // Create the transfer operators
        std::vector<ContactMGTransfer<VectorType> > mgTransfers(toplevel);
        ContactTransferOperatorAssembler<VectorType,GridType>::assembleOperatorHierarchy(contactAssembler, mgTransfers);

        multigridStep.setTransferOperators(mgTransfers);

        EnergyNorm<MatrixType, BlockVector<FieldVector<ctype,dim> > > energyNorm(multigridStep);

        ::LoopSolver<VectorType> solver(multigridStep,
                                           // IPOpt doesn't like to be started at the solution, so make sure we really
                                           // call it only once when it is the only solver
                                           (toplevel==0) ? 1 : numIt,
                                           tolerance,
                                           energyNorm,
                                           Solver::FULL);

         // ///////////////////////////
        //   Compute solution
        // ///////////////////////////

        solver.preprocess();

        // Compute solution
        solver.solve();

        x = multigridStep.getSol();

        contactAssembler.transformVector(x);

        // ////////////////////////////////////////////////////////////////////////
        //    Leave adaptation loop if maximum number of levels has been reached
        // ////////////////////////////////////////////////////////////////////////

        if (grid->maxLevel() == maxLevel)
            break;

        // ////////////////////////////////////////////////////
        //    Estimate error per element
        // ////////////////////////////////////////////////////

        // DuneFunctionBasis<P2> in combination with the error estimator gives wrong results
        //using P2Basis = DuneFunctionsBasis<Dune::Functions::PQkNodalBasis<GridType::LeafGridView,2> >;
        using P2Basis = P2NodalBasis<GridType::LeafGridView>;
        auto p2Basis = std::make_shared<P2Basis>(grid->leafGridView());

        HierarchicContactEstimator<P1Basis, P2Basis> estimator(*grid);
        estimator.setupContactCoupling(obsPatch, obsDistance, CouplingPairBase::RIGID, obs);

        auto refinementIndicator = std::make_shared<RefinementIndicator<GridType> >(*grid);

        using P2FiniteElement = P2Basis::LocalFiniteElement;
        auto estimatorLocalStiffness = std::make_shared<StVenantKirchhoffAssembler<GridType,P2FiniteElement,P2FiniteElement> >(E,nu);

        // make grid function of the linear solution
        auto xFunction = ::Functions::makeFunction(p1Basis,x);

        estimator.estimate(stackobject_to_shared_ptr(xFunction),
                           stackobject_to_shared_ptr(leafDirichletBoundary),
                           refinementIndicator,
                           p2Basis,
                           estimatorLocalStiffness);
        // ////////////////////////////////////////////////////////
        //   Refine the grids and transfer the current solution
        // ////////////////////////////////////////////////////////

        FractionalMarkingStrategy<GridType>::mark(*refinementIndicator, *grid, refinementFraction);


        GridFunctionAdaptor<P1Basis> adaptor(p1Basis,true,true);

        grid->preAdapt();
        grid->adapt();
        grid->postAdapt();
        p1Basis.update();
        adaptor.adapt(x);

        if (paramBoundaries)
            improveGrid(*grid,10);


        std::cout << "########################################################" << std::endl;
        std::cout << "  Grid refined,  Level: " << grid->maxLevel()
                  << "   vertices: " << grid->size(grid->maxLevel(), dim)
                  << "   elements: " << grid->size(grid->maxLevel(), 0) << std::endl;
        std::cout << "########################################################" << std::endl;

    }

    // ///////////////////////////
    // Output result
    // ///////////////////////////

    std::string writer = parameterSet.get("writer","Vtk");
    if (writer=="Amira") {
#if HAVE_AMIRAMESH
        LeafAmiraMeshWriter<GridType> amiramesh;
        amiramesh.addLeafGrid(*grid,true);
        amiramesh.addVertexData(x, grid->leafGridView());
        amiramesh.write(resultPath+"1bcontact.results",1);
#else
#error You can only write in AmiraMesh format if the library is installed!
#endif
    } else {

        P1Basis p1Basis(grid->leafGridView());
        VTKWriter<GridType::LeafGridView> vtkWriter0(grid->leafGridView());
        std::shared_ptr<VTKBasisGridFunction<P1Basis,VectorType> > displacement0
            = std::make_shared<VTKBasisGridFunction<P1Basis,VectorType> >(p1Basis, x, "Displacement");
        vtkWriter0.addVertexData(displacement0);
        vtkWriter0.write(resultPath + "1bcontact.results");

    }

 } catch (Exception e) {

    std::cout << e << std::endl;

 }



