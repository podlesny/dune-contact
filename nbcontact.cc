// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#include <config.h>

#include <dune/common/bitsetvector.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/amirameshwriter.hh>
#include <dune/grid/io/file/amirameshreader.hh>

#include <dune/istl/io.hh>
#include <dune/istl/solvers.hh>

#include <dune/fufem/sampleonbitfield.hh>
#include <dune/fufem/readbitfield.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/fufem/assemblers/localassemblers/stvenantkirchhoffassembler.hh>
#ifdef HAVE_IPOPT
#include <dune/solvers/solvers/quadraticipopt.hh>
#endif
#include <dune/solvers/iterationsteps/projectedblockgsstep.hh>
#include <dune/solvers/solvers/iterativesolver.hh>
#include <dune/solvers/norms/energynorm.hh>
#include <dune/fufem/estimators/geometricmarking.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/solvers/iterationsteps/mmgstep.hh>

#include "dune/contact/assemblers/nbodyassembler.hh"
#include "dune/contact/solvers/contacttransfer.hh"
#include "dune/contact/solvers/contactobsrestrict.hh"


// Choose a solver
//#define IPOPT
//#define GAUSS_SEIDEL
#define MULTIGRID

#define IPOPT_BASE

// The grid dimension
const int dim = 3;

using namespace Dune;
using namespace Dune::Contact;
using std::string;

bool refineCondition(const FieldVector<double,dim>& pos) {
    return 0.8 - fabs(pos[2]-0.2) > 0;
}

// double refineCondition(const FieldVector<double,dim>& pos) {
//     return 30 - fabs(pos[2]);
// }

int main (int argc, char *argv[]) try
{
    // Some types that I need
    typedef BCRSMatrix<FieldMatrix<double, dim, dim> > MatrixType;
    typedef BlockVector<FieldVector<double,dim> >      VectorType;

    // parse data file
    ParameterTree parameterSet;
    if (argc==2)
        ParameterTreeParser::readINITree(argv[1], parameterSet);
    else
        ParameterTreeParser::readINITree("nbcontact.parset", parameterSet);

    // read solver settings
    const int minLevel         = parameterSet.get("minLevel", int(0));
    const int maxLevel         = parameterSet.get("maxLevel", int(0));
    const int numIt            = parameterSet.get("numIt", int(0));
    const int nu1              = parameterSet.get("nu1", int(0));
    const int nu2              = parameterSet.get("nu2", int(0));
    const int mu               = parameterSet.get("mu", int(0));
    const double tolerance     = parameterSet.get("tolerance", double(0));

    // read problem settings
    string path = parameterSet.get("path", "xyz");
    string resultPath = parameterSet.get("resultPath", "xyz");
    int nGrids = parameterSet.get("nGrids", int(-1));
    int nCouplings = parameterSet.get("nCouplings", int(-1));

    string gridFile[3];
    gridFile[0] = parameterSet.get("gridFile0", "xyz");
    gridFile[1] = parameterSet.get("gridFile1", "xyz");
    gridFile[2] = parameterSet.get("gridFile2", "xyz");

    string dnFile[3];
    dnFile[0]       = parameterSet.get("dnFile0", "xyz");
    dnFile[1]       = parameterSet.get("dnFile1", "xyz");
    dnFile[2]       = parameterSet.get("dnFile2", "xyz");

    string dvFile[3];
    dvFile[0] = parameterSet.get("dvFile0", "xyz");
    dvFile[1] = parameterSet.get("dvFile1", "xyz");
    dvFile[2] = parameterSet.get("dvFile2", "xyz");

    string obsFile[3];
    obsFile[0] = parameterSet.get("obsFile0", "xyz");
    obsFile[1] = parameterSet.get("obsFile1", "xyz");
    obsFile[2] = parameterSet.get("obsFile2", "xyz");

    double obsDistance[3];
    obsDistance[0] = parameterSet.get("obsDistance0", double(0));
    obsDistance[1] = parameterSet.get("obsDistance1", double(0));
    obsDistance[2] = parameterSet.get("obsDistance2", double(0));

    int gridIdx0[3] = {0,1,2};
    int gridIdx1[3] = {1,2,0};



    // ///////////////////////////////////////
    //    Create the two grids
    // ///////////////////////////////////////

    typedef UGGrid<dim> GridType;
    typedef BoundaryPatch<GridType::LevelGridView> LevelBoundaryPatch;
    typedef BoundaryPatch<GridType::LeafGridView> LeafBoundaryPatch;

    std::vector<GridType*> grids(nGrids);

    for (int i=0; i<nGrids; i++) {

        grids[i] = new GridType;

#ifndef HAVE_PARAMETRIZATION_LIBRARY
        AmiraMeshReader<GridType>::read(*grids[i], path + gridFile[i]);
#else
        AmiraMeshReader<GridType>::read(*grids[i], path + gridFile[0], path + parFile[0]);
#endif

    }

    for (int i=0; i<nGrids; i++)
        grids[i]->globalRefine(minLevel);

    // Set up hierarchy of bitfields containing the Dirichlet values
    std::vector<std::vector<VectorType > > dirichletValues(nGrids);

    for (int i=0; i<nGrids; i++) {
        dirichletValues[i].resize(maxLevel+1);
        dirichletValues[i][0].resize(grids[i]->size(0, dim));
        dirichletValues[i][0] = 0;
        AmiraMeshReader<GridType>::readFunction(dirichletValues[i][0], path + dvFile[i]);
    }

    // Set up hierarchy of bitfields containing the Dirichlet flags
    std::vector<std::vector<LevelBoundaryPatch> > dirichletBoundary(nGrids);

    for (int i=0; i<nGrids; i++) {
        dirichletBoundary[i].resize(maxLevel+1);
        dirichletBoundary[i][0].setup(grids[i]->levelGridView(0));
        //dirichletNodes[i][0].resize(grids[i]->size(0,dim)*dim, false);
        readBoundaryPatch<GridType>(dirichletBoundary[i][0], path + dnFile[i]);
        //dirichletBoundary[i][0].setup(*grids[i], 0, dirichletNodes[i][0]);
    }


    // read field describing the obstacles
    std::vector<LevelBoundaryPatch> obsPatch(nGrids);
    for (int i=0; i<nGrids; i++) {
        obsPatch[i].setup(grids[i]->levelGridView(0));
        readBoundaryPatch<GridType>(obsPatch[i], path + obsFile[i]);
    }

    std::vector<VectorType > rhs(nGrids);
    std::vector<VectorType > x(nGrids);

    while (true) {

        int toplevel = grids[0]->maxLevel();

        std::vector<std::vector<BitSetVector<dim> > > dirichletNodes(nGrids);
        for (int i=0; i<nGrids; i++) {

            // Extend the dirichlet information to all grid levels
            BoundaryPatchProlongator<GridType>::prolong(dirichletBoundary[i]);
            dirichletNodes[i].resize(toplevel+1);

            for (int j=0; j<=toplevel; j++) {

                int fSSize = grids[i]->size(j,dim);
                dirichletNodes[i][j].resize(fSSize);
                for (int k=0; k<fSSize; k++)
                    for (int l=0; l<dim; l++)
                        dirichletNodes[i][j][k][l] = dirichletBoundary[i][j].containsVertex(k);

            }

            sampleOnBitField(*grids[i], dirichletValues[i], dirichletNodes[i]);

        }



        // ////////////////////////////////////////////////////////////
        //    Create solution and rhs vectors
        // ////////////////////////////////////////////////////////////

        for (int i=0; i<nGrids; i++) {

            rhs[i].resize(grids[i]->size(toplevel, dim));
            x[i].resize(grids[i]->size(toplevel, dim));

            // Right hand side
            rhs[i] = 0;

            // Initial solution
            x[i] = 0;

            for (size_t j=0; j<x[i].size(); j++)
                for (int k=0; k<dim; k++) {
                    if (dirichletNodes[i][toplevel][j][k])
                        x[i][j][k] = dirichletValues[i][toplevel][j][k];

                }
        }

        // make dirichlet bitfields containing dirichlet information for all grids
        std::vector<BitSetVector<dim> > totalDirichletNodes(toplevel+1);

        for (int i=0; i<=toplevel; i++) {

            int totalSize = 0;
            for (int j=0; j<nGrids; j++)
                totalSize += grids[j]->size(i,dim);

            totalDirichletNodes[i].resize(totalSize);

            int idx=0;
            for (int j=0; j<nGrids; j++)
                for (size_t k=0; k<dirichletNodes[j][i].size(); k++)
                	for (int l=0; l<dim; l++)
                		totalDirichletNodes[i][idx++][l] = dirichletNodes[j][i][k][l];
        }

        // /////////////////////////////////////////////////////
        //   Assemble the separate linear elasticity problems
        // /////////////////////////////////////////////////////
        typedef P1NodalBasis<GridType::LeafGridView,double> P1Basis;

		std::vector<MatrixType>  subMatrices(grids.size());
		std::vector<const MatrixType*> subMat(grids.size());
		std::vector<OperatorAssembler<P1Basis,P1Basis>* > A(grids.size());

		for (size_t i=0; i<grids.size(); i++) {

            P1Basis p1Basis(grids[i]->leafGridView());
            OperatorAssembler<P1Basis,P1Basis> globalAssembler(p1Basis,p1Basis);

            double E  = 2.5e5;
            double nu = 0.3;

            StVenantKirchhoffAssembler<GridType, P1Basis::LocalFiniteElement, P1Basis::LocalFiniteElement> localAssembler(E, nu);
            globalAssembler.assemble(localAssembler, subMatrices[i]);
            subMat[i] = &subMatrices[i];
        }


        // ///////////////////////////////////
        //   Assemble full contact problem
        // ///////////////////////////////////
        NBodyAssembler<GridType, VectorType > contactAssembler(nGrids, nCouplings);

        for (int i=0; i<nGrids; i++)
            contactAssembler.grids_[i] = grids[i];

        for (int i=0; i<nCouplings; i++) {

            contactAssembler.coupling_[i].set(gridIdx0[i],
                                              gridIdx1[i],
                                              &obsPatch[i],
                                              obsDistance[i],
                                              CouplingPairBase::CONTACT);

        }

        contactAssembler.assembleObstacle();
        contactAssembler.assembleTransferOperator();

        MatrixType bilinearForm;
        contactAssembler.assembleJacobian(subMat, bilinearForm);

        VectorType totalRhs;
        contactAssembler.assembleRightHandSide(rhs, totalRhs);

        // Set initial solution
        VectorType totalX(totalRhs.size());
        totalX = 0;

        int idx = 0;
        for (int j=0; j<nGrids; j++)
            for (size_t k=0; k<x[j].size(); k++)
                totalX[idx++] = x[j][k];

        // Create a solver
#if defined IPOPT

        typedef LinearIPOptSolver<VectorType > SolverType;

        SolverType solver(*bilinearForm, totalX, totalRhs);
        solver.dirichletNodes_ = &totalDirichletNodes[toplevel];
        solver.hasObstacle_    = &contactAssembler.totalHasObstacle_[toplevel];
        solver.obstacles_      = &contactAssembler.totalObstacles_[toplevel];

#elif defined GAUSS_SEIDEL

        typedef ProjectedBlockGSStep<MatrixType, VectorType > SmootherType;
        SmootherType projectedBlockGSStep(*bilinearForm, totalX, totalRhs);
        projectedBlockGSStep.dirichletNodes_ = &totalDirichletNodes[toplevel];
        projectedBlockGSStep.hasObstacle_    = &contactAssembler.totalHasObstacle_[toplevel];
        projectedBlockGSStep.obstacles_      = &contactAssembler.totalObstacles_[toplevel];

        IterativeSolver<MatrixType, VectorType > solver;
        EnergyNorm<MatrixType, VectorType > energyNorm(projectedBlockGSStep);

        solver.iterationStep = &projectedBlockGSStep;
        solver.numIt = numIt;
        solver.verbosity_ = Solver::FULL;
        solver.errorNorm_ = &energyNorm;
        solver.tolerance_ = tolerance;

#elif defined MULTIGRID


        // First create a base solver
#ifdef IPOPT_BASE

#ifdef HAVE_IPOPT
        QuadraticIPOptSolver<MatrixType,VectorType> baseSolver;
#endif
        baseSolver.verbosity_ = Solver::QUIET;

#else // Gauss-Seidel is the base solver

        ProjectedBlockGSStep<MatrixType, VectorType > baseSolverStep;
        EnergyNorm<MatrixType, VectorType > baseEnergyNorm(baseSolverStep);

        IterativeSolver<MatrixType, VectorType > baseSolver;
        baseSolver.iterationStep = &baseSolverStep;
        baseSolver.numIt = parameterSet.get("baseIt", int(0));
        baseSolver.verbosity_ = Solver::QUIET;
        baseSolver.errorNorm_ = &baseEnergyNorm;
        baseSolver.tolerance_ = parameterSet.get("baseTolerance", double(0));
#endif

        // Make pre and postsmoothers
        ProjectedBlockGSStep<MatrixType, VectorType > presmoother;
        ProjectedBlockGSStep<MatrixType, VectorType > postsmoother;


        MonotoneMGStep<MatrixType, VectorType> multigridStep(bilinearForm, totalX, totalRhs);

        multigridStep.setMGType(mu, nu1, nu2);
        multigridStep.ignoreNodes_       = &totalDirichletNodes[toplevel];
        multigridStep.basesolver_        = &baseSolver;
        multigridStep.setSmoother(&presmoother, &postsmoother);
        multigridStep.hasObstacle_       = &contactAssembler.totalHasObstacle_;
        multigridStep.obstacles_         = &contactAssembler.totalObstacles_;
        multigridStep.obstacleRestrictor_ = new ContactObsRestriction<VectorType>;

        // /////////////////////////////////////////////
        //   Create the transfer operators
        // /////////////////////////////////////////////
        std::vector<ContactMGTransfer<VectorType>* > mgTransfers(toplevel);
        for (size_t i=0; i<mgTransfers.size(); i++) {

            std::vector<const BitSetVector<1>*> coarseHasObstacle(nCouplings);
            std::vector<const BitSetVector<1>*> fineHasObstacle(nCouplings);

            std::vector<const MatrixType*> mortarTransfer(nCouplings);
            std::vector<array<int,2> > gridIdx(nCouplings);

            for (int j=0; j<nCouplings; j++) {

                coarseHasObstacle[j]  = contactAssembler.nonmortarBoundary_[j][i].getVertices();
                fineHasObstacle[j]    = contactAssembler.nonmortarBoundary_[j][i+1].getVertices();

                mortarTransfer[j] = &contactAssembler.contactCoupling_[j].mortarLagrangeMatrix(i);
                gridIdx[j]        = contactAssembler.coupling_[j].gridIdx_;

            }

            mgTransfers[i] = new ContactMGTransfer<VectorType>;
            mgTransfers[i]->setup(grids, i, i+1,
                                 contactAssembler.localCoordSystems_[i],
                                 contactAssembler.localCoordSystems_[i+1],
                                 coarseHasObstacle, fineHasObstacle,
                                 mortarTransfer,
                                 gridIdx);
        }
        multigridStep.setTransferOperators(mgTransfers);

        EnergyNorm<MatrixType, VectorType > energyNorm(multigridStep);

        ::LoopSolver<VectorType> solver(&multigridStep,
                                        numIt,
                                        tolerance,
                                        &energyNorm,
                                        Solver::FULL);

#else
#warning You have to specify a solver!
#endif

        // /////////////////////////////
        //    Solve !
        // /////////////////////////////
        solver.preprocess();

        // Compute solution
        solver.solve();

#ifdef MULTIGRID
        totalX = multigridStep.getSol();
#endif

        contactAssembler.postprocess(totalX, x);

        // ////////////////////////////////////////////////////////////////////////
        //    Leave adaptation loop if maximum number of levels has been reached
        // ////////////////////////////////////////////////////////////////////////
        //std::cout << "totalX:\n" << totalX << std::endl;

        if (grids[0]->maxLevel() == maxLevel)
            break;

        // ////////////////////////////////////////////////////
        //    Estimate a posteriori errors per element
        // ////////////////////////////////////////////////////

        GeometricEstimator<GridType> estimator;
        for (size_t i=0; i<grids.size(); i++)
            estimator.estimate(*grids[i], refineCondition);

        // ////////////////////////////////////////////////////
        //   Refine grids
        // ////////////////////////////////////////////////////
        for (size_t i=0; i<grids.size(); i++)
            grids[i]->adapt();

        std::cout << "########################################################" << std::endl;
        std::cout << "  Grids refined" << std::endl;
        for (size_t i=0; i<grids.size(); i++)
            std::cout << "  Grid: " << i << "   Level: " << grids[i]->maxLevel()
                      << "   vertices: " << grids[i]->size(grids[i]->maxLevel(), dim)
                      << "   elements: " << grids[i]->size(grids[i]->maxLevel(), 0) << std::endl;
        std::cout << "########################################################" << std::endl;

        for (size_t i=0; i<mgTransfers.size(); i++)
            delete(mgTransfers[i]);
    }


//     for (int i=0; i<nGrids; i++)
//         std::cout << "x[" << i <<"]:\n" << x[i] << std::endl;

    // ///////////////////////////////
    //   Output result
    // ///////////////////////////////
    for (int i=0; i<nGrids; i++) {

        LeafAmiraMeshWriter<GridType>::writeGrid(*grids[i], resultPath + gridFile[i] + "._grid");
        LeafAmiraMeshWriter<GridType>::writeBlockVector(*grids[i], x[i], resultPath + gridFile[i] + ".disp");

    }

 } catch (Exception e) {

    std::cout << e << std::endl;

 }
